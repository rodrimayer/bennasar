SELECT d.id_mercaderia, m.nombre as nombre, tc.tipo_cantidad, (select case 
 when (sum(d.cantidad)-floor(sum(d.cantidad))) = 0.5 and (tc.tipo_cantidad = 'Cajon' or tc.tipo_cantidad = 'Bolsa') THEN (
CONCAT(cast(floor(sum((d.cantidad))) as char),  ' ½' )) else sum(d.cantidad)
 end) as Cantidad, DATE_FORMAT(p.fecha_pedido, '%d-%m-%Y') as Fecha, b.zona, m.orden as orden, d.id_tipocant as tipocant, b.id_boleta
FROM detalle AS d
JOIN boleta AS b ON d.id_boleta = b.id_boleta
JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia
JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant
JOIN pedido as p on b.id_pedido = p.id_pedido
WHERE b.id_pedido = $P{id_pedido} and b.zona = $P{zona} and (tc.tipo_cantidad = 'Cajon' or tc.tipo_cantidad = 'Ristra' or tc.tipo_cantidad = 'Bandeja' or tc.tipo_cantidad = 'Bolsa' or (tc.tipo_cantidad = 'Unidad' and (m.id_mercaderia <> 'NC' and m.id_mercaderia <> 'NT' and m.id_mercaderia <> 'NO' and m.id_mercaderia <> 'LI')))
group by m.id_mercaderia, tc.tipo_cantidad
UNION(
    SELECT d.id_mercaderia, m.nombre, tc.tipo_cantidad, (select case 
     when (d.cantidad-floor(d.cantidad)) = 0.5 and (tc.tipo_cantidad = 'Cajon' or tc.tipo_cantidad = 'Bolsa') THEN (
    CONCAT(cast(floor(d.cantidad) as char),  ' ½' )) else d.cantidad
     end) as Cantidad, DATE_FORMAT(p.fecha_pedido, '%d-%m-%Y') as Fecha, b.zona, m.orden as orden, d.id_tipocant as tipocant, b.id_boleta
    FROM detalle AS d
    JOIN boleta AS b ON d.id_boleta = b.id_boleta
    JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia
    JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant
    JOIN pedido as p on b.id_pedido = p.id_pedido
    WHERE b.id_pedido = $P{id_pedido} and b.zona = $P{zona} and (tc.tipo_cantidad = 'Kg' or (tc.tipo_cantidad = 'Unidad' and (m.id_mercaderia = 'NC' or m.id_mercaderia = 'NT' or m.id_mercaderia = 'NO' or m.id_mercaderia = 'LI')))
)
order by orden asc, nombre asc, tipocant asc


SELECT d.id_mercaderia, m.nombre as nombre, tc.tipo_cantidad, (select case 
 when (sum(d.cantidad)-floor(sum(d.cantidad))) = 0.5 and (tc.tipo_cantidad = 'Cajon' or tc.tipo_cantidad = 'Bolsa') THEN (
CONCAT(cast(floor(sum((d.cantidad))) as char),  ' ½' )) else sum(d.cantidad)
 end) as Cantidad, DATE_FORMAT(p.fecha_pedido, '%d-%m-%Y') as Fecha, b.zona, m.orden as orden, d.id_tipocant as tipocant, b.id_boleta
FROM detalle AS d
JOIN boleta AS b ON d.id_boleta = b.id_boleta
JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia
JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant
JOIN pedido as p on b.id_pedido = p.id_pedido
WHERE b.id_pedido = 180 and b.zona = 1 and tc.tipo_cantidad = "Unidad" and m.sumaUnidad = 1
group by m.id_mercaderia, tc.tipo_cantidad
UNION(
    SELECT d.id_mercaderia, m.nombre, tc.tipo_cantidad, (select case 
     when (d.cantidad-floor(d.cantidad)) = 0.5 and (tc.tipo_cantidad = 'Cajon' or tc.tipo_cantidad = 'Bolsa') THEN (
    CONCAT(cast(floor(d.cantidad) as char),  ' ½' )) else d.cantidad
     end) as Cantidad, DATE_FORMAT(p.fecha_pedido, '%d-%m-%Y') as Fecha, b.zona, m.orden as orden, d.id_tipocant as tipocant, b.id_boleta
    FROM detalle AS d
    JOIN boleta AS b ON d.id_boleta = b.id_boleta
    JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia
    JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant
    JOIN pedido as p on b.id_pedido = p.id_pedido
    WHERE b.id_pedido = 180 and b.zona = 1 and tc.tipo_cantidad = "Unidad" and m.sumaUnidad = 0 )
order by orden asc, nombre asc, tipocant asc



SELECT d.id_mercaderia, m.nombre as nombre, tc.tipo_cantidad, (select case 
 when (sum(d.cantidad)-floor(sum(d.cantidad))) = 0.5 and (tc.tipo_cantidad = 'Cajon' or tc.tipo_cantidad = 'Bolsa') THEN (
CONCAT(cast(floor(sum((d.cantidad))) as char),  ' ½' )) else sum(d.cantidad)
 end) as Cantidad, DATE_FORMAT(p.fecha_pedido, '%d-%m-%Y') as Fecha, b.zona, m.orden as orden, d.id_tipocant as tipocant, b.id_boleta
FROM detalle AS d
JOIN boleta AS b ON d.id_boleta = b.id_boleta
JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia
JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant
JOIN pedido as p on b.id_pedido = p.id_pedido
WHERE b.id_pedido = $P{id_pedido} and b.zona = $P{zona} and tc.tipo_cantidad = "Unidad" and m.sumaUnidad = 1
group by m.id_mercaderia, tc.tipo_cantidad
UNION(
    SELECT d.id_mercaderia, m.nombre, tc.tipo_cantidad, (select case 
     when (d.cantidad-floor(d.cantidad)) = 0.5 and (tc.tipo_cantidad = 'Cajon' or tc.tipo_cantidad = 'Bolsa') THEN (
    CONCAT(cast(floor(d.cantidad) as char),  ' ½' )) else d.cantidad
     end) as Cantidad, DATE_FORMAT(p.fecha_pedido, '%d-%m-%Y') as Fecha, b.zona, m.orden as orden, d.id_tipocant as tipocant, b.id_boleta
    FROM detalle AS d
    JOIN boleta AS b ON d.id_boleta = b.id_boleta
    JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia
    JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant
    JOIN pedido as p on b.id_pedido = p.id_pedido
    WHERE b.id_pedido = $P{id_pedido} and b.zona = $P{zona} and tc.tipo_cantidad = "Unidad" and m.sumaUnidad = 0 )
UNION(
    SELECT d.id_mercaderia, m.nombre as nombre, tc.tipo_cantidad, (select case 
    when (sum(d.cantidad)-floor(sum(d.cantidad))) = 0.5 and (tc.tipo_cantidad = 'Cajon' or tc.tipo_cantidad = 'Bolsa') THEN (
    CONCAT(cast(floor(sum((d.cantidad))) as char),  ' ½' )) else sum(d.cantidad)
     end) as Cantidad, DATE_FORMAT(p.fecha_pedido, '%d-%m-%Y') as Fecha, b.zona, m.orden as orden, d.id_tipocant as tipocant, b.id_boleta
    FROM detalle AS d
    JOIN boleta AS b ON d.id_boleta = b.id_boleta
    JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia
    JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant
    JOIN pedido as p on b.id_pedido = p.id_pedido
    WHERE b.id_pedido = $P{id_pedido} and b.zona = $P{zona} and tc.tipo_cantidad = "Kg" and m.sumaKg = 1
    group by m.id_mercaderia, tc.tipo_cantidad )
UNION(
    SELECT d.id_mercaderia, m.nombre, tc.tipo_cantidad, (select case 
     when (d.cantidad-floor(d.cantidad)) = 0.5 and (tc.tipo_cantidad = 'Cajon' or tc.tipo_cantidad = 'Bolsa') THEN (
    CONCAT(cast(floor(d.cantidad) as char),  ' ½' )) else d.cantidad
     end) as Cantidad, DATE_FORMAT(p.fecha_pedido, '%d-%m-%Y') as Fecha, b.zona, m.orden as orden, d.id_tipocant as tipocant, b.id_boleta
    FROM detalle AS d
    JOIN boleta AS b ON d.id_boleta = b.id_boleta
    JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia
    JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant
    JOIN pedido as p on b.id_pedido = p.id_pedido
    WHERE b.id_pedido = $P{id_pedido} and b.zona = $P{zona} and tc.tipo_cantidad = "Kg" and m.sumaKg = 0 )
UNION(
    SELECT d.id_mercaderia, m.nombre as nombre, tc.tipo_cantidad, (select case 
     when (sum(d.cantidad)-floor(sum(d.cantidad))) = 0.5 and (tc.tipo_cantidad = 'Cajon' or tc.tipo_cantidad = 'Bolsa') THEN (
    CONCAT(cast(floor(sum((d.cantidad))) as char),  ' ½' )) else sum(d.cantidad)
     end) as Cantidad, DATE_FORMAT(p.fecha_pedido, '%d-%m-%Y') as Fecha, b.zona, m.orden as orden, d.id_tipocant as tipocant, b.id_boleta
    FROM detalle AS d
    JOIN boleta AS b ON d.id_boleta = b.id_boleta
    JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia
    JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant
    JOIN pedido as p on b.id_pedido = p.id_pedido
    WHERE b.id_pedido = $P{id_pedido} and b.zona = $P{zona} and (tc.tipo_cantidad = "Cajon" or tc.tipo_cantidad = "Bolsa" or tc.tipo_cantidad = "Bandeja" or tc.tipo_cantidad = "Ristra")
    group by m.id_mercaderia, tc.tipo_cantidad )
order by orden asc, nombre asc, tipocant asc