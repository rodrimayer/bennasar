/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author rmmayer
 */
public class Licencia {
    
    private String id_licencia;
    private int activo;
    private String version_instalada;

    public String getId_licencia() {
        return id_licencia;
    }

    public void setId_licencia(String id_licencia) {
        this.id_licencia = id_licencia;
    }

    public int getActivo() {
        return activo;
    }

    public void setActivo(int activo) {
        this.activo = activo;
    }

    public String getVersion_instalada() {
        return version_instalada;
    }

    public void setVersion_instalada(String version_instalada) {
        this.version_instalada = version_instalada;
    }
    
    
    
}
