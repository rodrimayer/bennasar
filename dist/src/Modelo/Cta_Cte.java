/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author rmmayer
 */
public class Cta_Cte {
    
    private int id_factCtaCte;
    private Cliente cliente;
    private String fecha;
    private float monto;

    public int getId_factCtaCte() {
        return id_factCtaCte;
    }

    public void setId_factCtaCte(int id_factCtaCte) {
        this.id_factCtaCte = id_factCtaCte;
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public float getMonto() {
        return monto;
    }

    public void setMonto(float monto) {
        this.monto = monto;
    }
    
    
    
}
