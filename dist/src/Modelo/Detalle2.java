/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Administrador
 */
public class Detalle2 {
    
    private int id_detalle;
    private int id_boleta;
    private String id_mercaderia;
    private float precio;
    private float cantCajon;
    private float cantBolsa;
    private float cantKg;
    private float cantUnidad;
    private float cantBandeja;
    private float cantRistra;

    public float getCantBandeja() {
        return cantBandeja;
    }

    public void setCantBandeja(float cantBandeja) {
        this.cantBandeja = cantBandeja;
    }

    public float getCantBolsa() {
        return cantBolsa;
    }

    public void setCantBolsa(float cantBolsa) {
        this.cantBolsa = cantBolsa;
    }

    public float getCantCajon() {
        return cantCajon;
    }

    public void setCantCajon(float cantCajon) {
        this.cantCajon = cantCajon;
    }

    public float getCantKg() {
        return cantKg;
    }

    public void setCantKg(float cantKg) {
        this.cantKg = cantKg;
    }

    public float getCantUnidad() {
        return cantUnidad;
    }

    public void setCantUnidad(float cantUnidad) {
        this.cantUnidad = cantUnidad;
    }

    public int getId_boleta() {
        return id_boleta;
    }

    public void setId_boleta(int id_boleta) {
        this.id_boleta = id_boleta;
    }

    public int getId_detalle() {
        return id_detalle;
    }

    public void setId_detalle(int id_detalle) {
        this.id_detalle = id_detalle;
    }

    public String getId_mercaderia() {
        return id_mercaderia;
    }

    public void setId_mercaderia(String id_mercaderia) {
        this.id_mercaderia = id_mercaderia;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getCantRistra() {
        return cantRistra;
    }

    public void setCantRistra(float cantRistra) {
        this.cantRistra = cantRistra;
    }
    
    
    
    
    
    
}
