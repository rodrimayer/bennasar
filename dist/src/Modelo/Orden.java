/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Administrador
 */
public class Orden {
    
    private String id_orden;
    private int id_tipo;
    private String antes;
    private String despues;

    public String getAntes() {
        return antes;
    }

    public void setAntes(String antes) {
        this.antes = antes;
    }

    public String getDespues() {
        return despues;
    }

    public void setDespues(String despues) {
        this.despues = despues;
    }

    public String getId_mercaderia() {
        return id_orden;
    }

    public void setId_mercaderia(String id_mercaderia) {
        this.id_orden = id_mercaderia;
    }

    public int getId_tipo() {
        return id_tipo;
    }

    public void setId_tipo(int id_tipo) {
        this.id_tipo = id_tipo;
    }
    
    
}
