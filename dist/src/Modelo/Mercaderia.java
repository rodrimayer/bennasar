/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Administrador
 */
public class Mercaderia {

    private String nombre;
    private String id_mercaderia;
    private int id_tipo;
    private float precio;
    private int pesoLogistica;
    private int id_tipoCant;
    private int inactivo;
    private int sumaUnidad;
    private int sumaKg;

    public int getId_tipoCant() {
        return id_tipoCant;
    }

    public void setId_tipoCant(int id_tipoCant) {
        this.id_tipoCant = id_tipoCant;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getPesoLogistica() {
        return pesoLogistica;
    }

    public void setPesoLogistica(int pesoLogistica) {
        this.pesoLogistica = pesoLogistica;
    }

    public String getId_mercaderia() {
        return id_mercaderia;
    }

    public void setId_mercaderia(String id_mercaderia) {
        this.id_mercaderia = id_mercaderia;
    }

    public int getId_tipo() {
        return id_tipo;
    }

    public void setId_tipo(int id_tipo) {
        this.id_tipo = id_tipo;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public int getInactivo() {
        return inactivo;
    }

    public void setInactivo(int inactivo) {
        this.inactivo = inactivo;
    }

    public int getSumaUnidad() {
        return sumaUnidad;
    }

    public void setSumaUnidad(int sumaUnidad) {
        this.sumaUnidad = sumaUnidad;
    }

    public int getSumaKg() {
        return sumaKg;
    }

    public void setSumaKg(int sumaKg) {
        this.sumaKg = sumaKg;
    }

    @Override
    public String toString() {
        return this.nombre;
    }
    
    
    
    
}
