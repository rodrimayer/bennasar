/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Administrador
 */
public class Contacto {
    
    private int id_contacto;
    private int id_cliente;
    private String dueño;
    private String encargado1;
    private String encargado2;
    private String encargado3;
    private String encargado4;
    private String telefono;
    private String celular;
    private String mail;

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getDueño() {
        return dueño;
    }

    public void setDueño(String dueño) {
        this.dueño = dueño;
    }

    public String getEncargado1() {
        return encargado1;
    }

    public void setEncargado1(String encargado1) {
        this.encargado1 = encargado1;
    }

    public String getEncargado2() {
        return encargado2;
    }

    public void setEncargado2(String encargado2) {
        this.encargado2 = encargado2;
    }

    public String getEncargado3() {
        return encargado3;
    }

    public void setEncargado3(String encargado3) {
        this.encargado3 = encargado3;
    }

    public String getEncargado4() {
        return encargado4;
    }

    public void setEncargado4(String encargado4) {
        this.encargado4 = encargado4;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public int getId_contacto() {
        return id_contacto;
    }

    public void setId_contacto(int id_contacto) {
        this.id_contacto = id_contacto;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }
    
    
    
}
