/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Administrador
 */
public class Tipo_Cantidad {
    
    private int id_tipocant;
    private String tipo_cantidad;

    public int getId_tipocant() {
        return id_tipocant;
    }

    public void setId_tipocant(int id_tipocant) {
        this.id_tipocant = id_tipocant;
    }

    public String getTipo_cantidad() {
        return tipo_cantidad;
    }

    public void setTipo_cantidad(String tipo_cantidad) {
        this.tipo_cantidad = tipo_cantidad;
    }
    
    
    
}
