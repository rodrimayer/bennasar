/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author rmmayer
 */
public class Condicion_Vta {
    
    private int id_condVta;
    private String desc_condVta;

    public int getId_condVta() {
        return id_condVta;
    }

    public void setId_condVta(int id_condVta) {
        this.id_condVta = id_condVta;
    }

    public String getDesc_condVta() {
        return desc_condVta;
    }

    public void setDesc_condVta(String desc_condVta) {
        this.desc_condVta = desc_condVta;
    }
    
    
    
}
