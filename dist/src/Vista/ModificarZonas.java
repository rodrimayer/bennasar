/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Controlador.manejadorBoleta;
import Controlador.manejadorCliente;
import Controlador.manejadorTablas;
import Modelo.Boleta;
import Modelo.Cliente;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author rmmayer
 */
public class ModificarZonas extends javax.swing.JFrame {

    public class MiModelo extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    MiModelo modeloZona1 = new MiModelo();
    MiModelo modeloZona2 = new MiModelo();
    MiModelo modeloZona3 = new MiModelo();
    MiModelo modeloZona4 = new MiModelo();
    MiModelo modeloZona5 = new MiModelo();
    MiModelo modeloZona6 = new MiModelo();
    
    Connection con;

    public ModificarZonas(Connection con) {
        initComponents();
        this.con = con;
        manejadorTablas mt = new manejadorTablas(con);
        mt.modeloTablaBoletasZonas(tablaZona1, modeloZona1, 1, Login.user.getId_usuario());
        mt.modeloTablaBoletasZonas(tablaZona2, modeloZona2, 2, Login.user.getId_usuario());
        mt.modeloTablaBoletasZonas(tablaZona3, modeloZona3, 3, Login.user.getId_usuario());
        mt.modeloTablaBoletasZonas(tablaZona4, modeloZona4, 4, Login.user.getId_usuario());
        mt.modeloTablaBoletasZonas(tablaZona5, modeloZona5, 5, Login.user.getId_usuario());
        mt.modeloTablaBoletasZonas(tablaZona6, modeloZona6, 6, Login.user.getId_usuario());

        class PopUpZona1 extends JPopupMenu {

            manejadorBoleta mb = new manejadorBoleta(con);
            Object[] o = new Object[3];

            public PopUpZona1() {
                //JMenuItem moverZona1 = new JMenuItem("Mover a Zona 1");
                JMenuItem moverZona2 = new JMenuItem("Mover a Zona 2");
                JMenuItem moverZona3 = new JMenuItem("Mover a Zona 3");
                JMenuItem moverZona4 = new JMenuItem("Mover a Zona 4");
                JMenuItem moverZona5 = new JMenuItem("Mover a Zona 5");
                JMenuItem moverZona6 = new JMenuItem("Mover a Zona 6");

                moverZona2.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona1.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona1.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 2);

                        o[0] = id_boleta;
                        o[1] = tablaZona1.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona1.getValueAt(filaS, 2).toString());

                        modeloZona1.removeRow(filaS);
                        modeloZona2.addRow(o);
                    }

                });

                moverZona3.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona1.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona1.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 3);

                        o[0] = id_boleta;
                        o[1] = tablaZona1.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona1.getValueAt(filaS, 2).toString());

                        modeloZona1.removeRow(filaS);
                        modeloZona3.addRow(o);
                    }

                });

                moverZona4.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona1.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona1.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 4);

                        o[0] = id_boleta;
                        o[1] = tablaZona1.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona1.getValueAt(filaS, 2).toString());

                        modeloZona1.removeRow(filaS);
                        modeloZona4.addRow(o);
                    }

                });

                moverZona5.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona1.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona1.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 5);

                        o[0] = id_boleta;
                        o[1] = tablaZona1.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona1.getValueAt(filaS, 2).toString());

                        modeloZona1.removeRow(filaS);
                        modeloZona5.addRow(o);

                    }

                });

                moverZona6.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona1.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona1.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 6);

                        o[0] = id_boleta;
                        o[1] = tablaZona1.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona1.getValueAt(filaS, 2).toString());

                        modeloZona1.removeRow(filaS);
                        modeloZona6.addRow(o);
                    }

                });

                //add(moverZona1);
                add(moverZona2);
                add(moverZona3);
                add(moverZona4);
                add(moverZona5);
                add(moverZona6);

            }

        }

        class PopUpZona2 extends JPopupMenu {

            manejadorBoleta mb = new manejadorBoleta(con);
            Object[] o = new Object[3];

            public PopUpZona2() {
                JMenuItem moverZona1 = new JMenuItem("Mover a Zona 1");
                //JMenuItem moverZona2 = new JMenuItem("Mover a Zona 2");
                JMenuItem moverZona3 = new JMenuItem("Mover a Zona 3");
                JMenuItem moverZona4 = new JMenuItem("Mover a Zona 4");
                JMenuItem moverZona5 = new JMenuItem("Mover a Zona 5");
                JMenuItem moverZona6 = new JMenuItem("Mover a Zona 6");

                moverZona1.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona2.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona2.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 1);

                        o[0] = id_boleta;
                        o[1] = tablaZona2.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona2.getValueAt(filaS, 2).toString());

                        modeloZona2.removeRow(filaS);
                        modeloZona1.addRow(o);
                    }

                });

                moverZona3.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona2.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona2.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 3);

                        o[0] = id_boleta;
                        o[1] = tablaZona2.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona2.getValueAt(filaS, 2).toString());

                        modeloZona2.removeRow(filaS);
                        modeloZona3.addRow(o);
                    }

                });

                moverZona4.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona2.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona2.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 4);

                        o[0] = id_boleta;
                        o[1] = tablaZona2.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona2.getValueAt(filaS, 2).toString());

                        modeloZona2.removeRow(filaS);
                        modeloZona4.addRow(o);
                    }

                });

                moverZona5.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona2.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona2.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 5);

                        o[0] = id_boleta;
                        o[1] = tablaZona2.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona2.getValueAt(filaS, 2).toString());

                        modeloZona2.removeRow(filaS);
                        modeloZona5.addRow(o);
                    }

                });

                moverZona6.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona2.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona2.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 6);

                        o[0] = id_boleta;
                        o[1] = tablaZona2.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona2.getValueAt(filaS, 2).toString());

                        modeloZona2.removeRow(filaS);
                        modeloZona6.addRow(o);

                    }

                });

                add(moverZona1);
                //add(moverZona2);
                add(moverZona3);
                add(moverZona4);
                add(moverZona5);
                add(moverZona6);

            }

        }

        class PopUpZona3 extends JPopupMenu {

            public PopUpZona3() {
                manejadorBoleta mb = new manejadorBoleta(con);
                Object[] o = new Object[3];

                JMenuItem moverZona1 = new JMenuItem("Mover a Zona 1");
                JMenuItem moverZona2 = new JMenuItem("Mover a Zona 2");
                //JMenuItem moverZona3 = new JMenuItem("Mover a Zona 3");
                JMenuItem moverZona4 = new JMenuItem("Mover a Zona 4");
                JMenuItem moverZona5 = new JMenuItem("Mover a Zona 5");
                JMenuItem moverZona6 = new JMenuItem("Mover a Zona 6");

                moverZona1.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona3.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona3.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 1);

                        o[0] = id_boleta;
                        o[1] = tablaZona3.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona3.getValueAt(filaS, 2).toString());

                        modeloZona3.removeRow(filaS);
                        modeloZona1.addRow(o);
                    }

                });

                moverZona2.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona3.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona3.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 2);

                        o[0] = id_boleta;
                        o[1] = tablaZona3.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona3.getValueAt(filaS, 2).toString());

                        modeloZona3.removeRow(filaS);
                        modeloZona2.addRow(o);
                    }

                });

                moverZona4.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona3.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona3.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 4);

                        o[0] = id_boleta;
                        o[1] = tablaZona3.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona3.getValueAt(filaS, 2).toString());

                        modeloZona3.removeRow(filaS);
                        modeloZona4.addRow(o);
                    }

                });

                moverZona5.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona3.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona3.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 5);

                        o[0] = id_boleta;
                        o[1] = tablaZona3.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona3.getValueAt(filaS, 2).toString());

                        modeloZona3.removeRow(filaS);
                        modeloZona5.addRow(o);
                    }

                });

                moverZona6.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona3.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona3.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 6);

                        o[0] = id_boleta;
                        o[1] = tablaZona3.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona3.getValueAt(filaS, 2).toString());

                        modeloZona3.removeRow(filaS);
                        modeloZona6.addRow(o);
                    }

                });

                add(moverZona1);
                add(moverZona2);
                //add(moverZona3);
                add(moverZona4);
                add(moverZona5);
                add(moverZona6);

            }

        }

        class PopUpZona4 extends JPopupMenu {

            public PopUpZona4() {
                manejadorBoleta mb = new manejadorBoleta(con);
                Object[] o = new Object[3];

                JMenuItem moverZona1 = new JMenuItem("Mover a Zona 1");
                JMenuItem moverZona2 = new JMenuItem("Mover a Zona 2");
                JMenuItem moverZona3 = new JMenuItem("Mover a Zona 3");
                //JMenuItem moverZona4 = new JMenuItem("Mover a Zona 4");
                JMenuItem moverZona5 = new JMenuItem("Mover a Zona 5");
                JMenuItem moverZona6 = new JMenuItem("Mover a Zona 6");

                moverZona1.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona4.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona4.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 1);

                        o[0] = id_boleta;
                        o[1] = tablaZona4.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona4.getValueAt(filaS, 2).toString());

                        modeloZona4.removeRow(filaS);
                        modeloZona1.addRow(o);
                    }

                });

                moverZona2.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona4.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona4.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 2);

                        o[0] = id_boleta;
                        o[1] = tablaZona4.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona4.getValueAt(filaS, 2).toString());

                        modeloZona4.removeRow(filaS);
                        modeloZona2.addRow(o);
                    }

                });

                moverZona3.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona4.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona4.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 3);

                        o[0] = id_boleta;
                        o[1] = tablaZona4.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona4.getValueAt(filaS, 2).toString());

                        modeloZona4.removeRow(filaS);
                        modeloZona3.addRow(o);
                    }

                });

                moverZona5.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona4.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona4.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 5);

                        o[0] = id_boleta;
                        o[1] = tablaZona4.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona4.getValueAt(filaS, 2).toString());

                        modeloZona4.removeRow(filaS);
                        modeloZona5.addRow(o);
                    }

                });

                moverZona6.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona4.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona4.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 6);

                        o[0] = id_boleta;
                        o[1] = tablaZona4.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona4.getValueAt(filaS, 2).toString());

                        modeloZona4.removeRow(filaS);
                        modeloZona6.addRow(o);
                    }

                });

                add(moverZona1);
                add(moverZona2);
                add(moverZona3);
                //add(moverZona4);
                add(moverZona5);
                add(moverZona6);

            }

        }

        class PopUpZona5 extends JPopupMenu {

            public PopUpZona5() {
                manejadorBoleta mb = new manejadorBoleta(con);
                Object[] o = new Object[3];

                JMenuItem moverZona1 = new JMenuItem("Mover a Zona 1");
                JMenuItem moverZona2 = new JMenuItem("Mover a Zona 2");
                JMenuItem moverZona3 = new JMenuItem("Mover a Zona 3");
                JMenuItem moverZona4 = new JMenuItem("Mover a Zona 4");
                //JMenuItem moverZona5 = new JMenuItem("Mover a Zona 5");
                JMenuItem moverZona6 = new JMenuItem("Mover a Zona 6");

                moverZona1.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona5.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona5.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 1);

                        o[0] = id_boleta;
                        o[1] = tablaZona5.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona5.getValueAt(filaS, 2).toString());

                        modeloZona5.removeRow(filaS);
                        modeloZona1.addRow(o);
                    }

                });

                moverZona2.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona5.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona5.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 2);

                        o[0] = id_boleta;
                        o[1] = tablaZona5.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona5.getValueAt(filaS, 2).toString());

                        modeloZona5.removeRow(filaS);
                        modeloZona2.addRow(o);
                    }

                });

                moverZona3.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona5.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona5.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 3);

                        o[0] = id_boleta;
                        o[1] = tablaZona5.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona5.getValueAt(filaS, 2).toString());

                        modeloZona5.removeRow(filaS);
                        modeloZona3.addRow(o);
                    }

                });

                moverZona4.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona5.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona5.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 4);

                        o[0] = id_boleta;
                        o[1] = tablaZona5.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona5.getValueAt(filaS, 2).toString());

                        modeloZona5.removeRow(filaS);
                        modeloZona4.addRow(o);
                    }

                });

                moverZona6.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona5.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona5.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 6);

                        o[0] = id_boleta;
                        o[1] = tablaZona5.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona5.getValueAt(filaS, 2).toString());

                        modeloZona5.removeRow(filaS);
                        modeloZona6.addRow(o);
                    }

                });

                add(moverZona1);
                add(moverZona2);
                add(moverZona3);
                add(moverZona4);
                //add(moverZona5);
                add(moverZona6);

            }

        }

        class PopUpZona6 extends JPopupMenu {

            public PopUpZona6() {
                manejadorBoleta mb = new manejadorBoleta(con);
                Object[] o = new Object[3];

                JMenuItem moverZona1 = new JMenuItem("Mover a Zona 1");
                JMenuItem moverZona2 = new JMenuItem("Mover a Zona 2");
                JMenuItem moverZona3 = new JMenuItem("Mover a Zona 3");
                JMenuItem moverZona4 = new JMenuItem("Mover a Zona 4");
                JMenuItem moverZona5 = new JMenuItem("Mover a Zona 5");
                //JMenuItem moverZona6 = new JMenuItem("Mover a Zona 6");

                moverZona1.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona6.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona6.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 1);

                        o[0] = id_boleta;
                        o[1] = tablaZona6.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona6.getValueAt(filaS, 2).toString());

                        modeloZona6.removeRow(filaS);
                        modeloZona1.addRow(o);

                    }

                });

                moverZona2.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona6.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona6.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 2);

                        o[0] = id_boleta;
                        o[1] = tablaZona6.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona6.getValueAt(filaS, 2).toString());

                        modeloZona6.removeRow(filaS);
                        modeloZona2.addRow(o);
                    }

                });

                moverZona3.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona6.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona6.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 3);

                        o[0] = id_boleta;
                        o[1] = tablaZona6.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona6.getValueAt(filaS, 2).toString());

                        modeloZona6.removeRow(filaS);
                        modeloZona3.addRow(o);
                    }

                });

                moverZona4.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona6.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona6.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 4);

                        o[0] = id_boleta;
                        o[1] = tablaZona6.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona6.getValueAt(filaS, 2).toString());

                        modeloZona6.removeRow(filaS);
                        modeloZona4.addRow(o);
                    }

                });

                moverZona5.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        int filaS = tablaZona6.getSelectedRow();
                        int id_boleta = Integer.parseInt(tablaZona6.getValueAt(filaS, 0).toString());

                        mb.actualizarZonaBoleta(id_boleta, 5);

                        o[0] = id_boleta;
                        o[1] = tablaZona6.getValueAt(filaS, 1).toString();
                        o[2] = Integer.parseInt(tablaZona6.getValueAt(filaS, 2).toString());

                        modeloZona6.removeRow(filaS);
                        modeloZona5.addRow(o);
                    }

                });

                add(moverZona1);
                add(moverZona2);
                add(moverZona3);
                add(moverZona4);
                add(moverZona5);
                //add(moverZona6);                

            }

        }

        final PopUpZona1 pop1 = new PopUpZona1();
        final PopUpZona2 pop2 = new PopUpZona2();
        final PopUpZona3 pop3 = new PopUpZona3();
        final PopUpZona4 pop4 = new PopUpZona4();
        final PopUpZona5 pop5 = new PopUpZona5();
        final PopUpZona6 pop6 = new PopUpZona6();

        tablaZona1.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (SwingUtilities.isRightMouseButton(me)) {
                    int r = tablaZona1.rowAtPoint(me.getPoint());
                    if (r >= 0 && r < tablaZona1.getRowCount()) {
                        tablaZona1.setRowSelectionInterval(r, r);
                    } else {
                        tablaZona1.clearSelection();
                    }
                    //pop.remove(0);
                    pop1.show(me.getComponent(), me.getX(), me.getY());
                }
            }
        });
        tablaZona2.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (SwingUtilities.isRightMouseButton(me)) {
                    int r = tablaZona2.rowAtPoint(me.getPoint());
                    if (r >= 0 && r < tablaZona2.getRowCount()) {
                        tablaZona2.setRowSelectionInterval(r, r);
                    } else {
                        tablaZona2.clearSelection();
                    }
                    //pop.remove(1);
                    pop2.show(me.getComponent(), me.getX(), me.getY());
                }
            }
        });
        tablaZona3.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (SwingUtilities.isRightMouseButton(me)) {
                    int r = tablaZona3.rowAtPoint(me.getPoint());
                    if (r >= 0 && r < tablaZona3.getRowCount()) {
                        tablaZona3.setRowSelectionInterval(r, r);
                    } else {
                        tablaZona3.clearSelection();
                    }
                    //pop.remove(2);
                    pop3.show(me.getComponent(), me.getX(), me.getY());
                }
            }
        });
        tablaZona4.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (SwingUtilities.isRightMouseButton(me)) {
                    int r = tablaZona4.rowAtPoint(me.getPoint());
                    if (r >= 0 && r < tablaZona4.getRowCount()) {
                        tablaZona4.setRowSelectionInterval(r, r);
                    } else {
                        tablaZona4.clearSelection();
                    }
                    //pop.remove(3);
                    pop4.show(me.getComponent(), me.getX(), me.getY());
                }
            }
        });
        tablaZona5.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (SwingUtilities.isRightMouseButton(me)) {
                    int r = tablaZona5.rowAtPoint(me.getPoint());
                    if (r >= 0 && r < tablaZona5.getRowCount()) {
                        tablaZona5.setRowSelectionInterval(r, r);
                    } else {
                        tablaZona5.clearSelection();
                    }
                    //pop.remove(4);
                    pop5.show(me.getComponent(), me.getX(), me.getY());
                }
            }
        });
        tablaZona6.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (SwingUtilities.isRightMouseButton(me)) {
                    int r = tablaZona6.rowAtPoint(me.getPoint());
                    if (r >= 0 && r < tablaZona6.getRowCount()) {
                        tablaZona6.setRowSelectionInterval(r, r);
                    } else {
                        tablaZona6.clearSelection();
                    }
                    //pop.remove(5);
                    pop6.show(me.getComponent(), me.getX(), me.getY());
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        tablaZona2 = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablaZona1 = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaZona3 = new javax.swing.JTable();
        jScrollPane4 = new javax.swing.JScrollPane();
        tablaZona4 = new javax.swing.JTable();
        jScrollPane5 = new javax.swing.JScrollPane();
        tablaZona5 = new javax.swing.JTable();
        jScrollPane6 = new javax.swing.JScrollPane();
        tablaZona6 = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("REVISAR ZONAS");

        tablaZona2.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        tablaZona2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tablaZona2);

        tablaZona1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.TOP));
        tablaZona1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaZona1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaZona1MouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tablaZona1);

        tablaZona3.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        tablaZona3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane3.setViewportView(tablaZona3);

        tablaZona4.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        tablaZona4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane4.setViewportView(tablaZona4);

        tablaZona5.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        tablaZona5.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane5.setViewportView(tablaZona5);

        tablaZona6.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        tablaZona6.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane6.setViewportView(tablaZona6);

        jLabel1.setText("ZONA 1:");

        jLabel2.setText("ZONA 2:");

        jLabel3.setText("ZONA 3:");

        jLabel4.setText("ZONA 4:");

        jLabel5.setText("ZONA 5:");

        jLabel6.setText("ZONA 6:");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(20, 20, 20)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel1)
                    .addComponent(jLabel3)
                    .addComponent(jLabel5))
                .addGap(40, 40, 40)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel4)
                    .addComponent(jLabel2)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(20, 20, 20))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel1)
                        .addGap(10, 10, 10)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2)
                        .addGap(10, 10, 10)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addGap(10, 10, 10))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(10, 10, 10)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel5)
                        .addGap(10, 10, 10)
                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel6)
                        .addGap(10, 10, 10)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 245, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablaZona1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaZona1MouseClicked
        // TODO add your handling code here:
        if (evt.getClickCount() == 2 && !evt.isConsumed()) {
            evt.consume();
            manejadorBoleta mb = new manejadorBoleta(con);
            int id_boleta = Integer.parseInt(tablaZona1.getValueAt(tablaZona1.getSelectedRow(), 0).toString());
            Boleta boleta = null;
            mb.traerBoleta(id_boleta, boleta);

            //mb.traerBoleta(id_boleta, boleta);

            CargarPedidoDidactico cp = new CargarPedidoDidactico(null, 0, boleta, 2, null, null, con, Login.user);
            //PreciosBoletas cp = new PreciosBoletas(tablaBoletasSinCerrar, 0, boleta, 2, modelo, null);
            cp.setLocationRelativeTo(null);
            cp.setVisible(true);


            //int filaS = tablaBoletasSinCerrar.getSelectedRow();
            //this.tablaBoletasSinCerrar.clearSelection();
            //this.tablaBoletasSinCerrar.setRowSelectionInterval(filaS+1, filaS+1);
            this.requestFocus();
        }
    }//GEN-LAST:event_tablaZona1MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(ModificarZonas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(ModificarZonas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(ModificarZonas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ModificarZonas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ModificarZonas(null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTable tablaZona1;
    private javax.swing.JTable tablaZona2;
    private javax.swing.JTable tablaZona3;
    private javax.swing.JTable tablaZona4;
    private javax.swing.JTable tablaZona5;
    private javax.swing.JTable tablaZona6;
    // End of variables declaration//GEN-END:variables
}
