/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Conexion.ConexionDBPool;
import Conexion.conexionDB;
import Controlador.manejadorBoleta;
import Controlador.manejadorCliente;
import Controlador.manejadorCosto;
import Controlador.manejadorFechas;
import Controlador.manejadorPedido;
import Controlador.manejadorTablas;
import Modelo.Boleta;
import Modelo.Cliente;
import Modelo.Usuario;
import Vista.*;
//import static com.sun.xml.internal.fastinfoset.alphabet.BuiltInRestrictedAlphabets.table;
import java.awt.Desktop;
import java.awt.Point;
import java.awt.Window;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;

/**
 *
 * @author Rodri Mayer
 */
public class InicioNuevo extends javax.swing.JFrame {

    /**
     * Creates new form InicioNuevo
     */
    public class MiModelo extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }
    MiModelo modelo = new MiModelo();

    //manejadorBoleta mb = new manejadorBoleta(con);
    //manejadorCliente mcli = new manejadorCliente(con);
    Connection con;
    Usuario user = new Usuario();
    boolean cierre;
    Boleta boleta = new Boleta();
    Thread hilo;
    Runnable runnable;
    
    ArrayList<Boleta> arrayBoletas;

    public InicioNuevo(Connection con, Usuario user, boolean cierre) {
        initComponents();
        manejadorBoleta mb = new manejadorBoleta(con);
        manejadorTablas mt = new manejadorTablas(con);
        manejadorFechas mf = new manejadorFechas(con);
        this.con = con;
        this.user = (Usuario) user;
        this.cierre = cierre;
        String version = "20211214";

        this.setTitle("Bennasar Delivery Version " + version + " | Usuario Conectado: " + user.getNombre_usuario() + " " + user.getApellido_usuario());

        this.tablaBoletasSinCerrar.setModel(modelo);

        this.setResizable(false);
        this.txtBusquedaCliente.requestFocus();

        botonImprimir.setEnabled(false);
        botonEditarBoleta.setEnabled(false);
        botonEliminarBoleta.setEnabled(false);
        this.botonCargarPrecios.setEnabled(false);

        mt.modeloTablaBoletasPedido(tablaBoletasSinCerrar, modelo);
        mb.actualizarBoletas(tablaBoletasSinCerrar, 0, modelo, user.getId_usuario());

        runnable = new Runnable() {
            @Override
            public void run() {
                // Esto se ejecuta en segundo plano una única vez
                while (true) {
                    // Pero usamos un truco y hacemos un ciclo infinito
                    try {
                        // En él, hacemos que el hilo duerma
                        Thread.sleep(15000);
                        // Y después realizamos las operaciones
                        //System.out.println("Me imprimo cada segundo");
                        mb.actualizarBoletas(tablaBoletasSinCerrar, 0, modelo, user.getId_usuario());
                        String busqueda = txtBusquedaCliente.getText();
                        int index = comboBoxZona.getSelectedIndex();
                        int zona = 0;

                        if (index != -1) {
                            zona = comboBoxZona.getSelectedIndex();
                        }

                        int totalFilas = tablaBoletasSinCerrar.getRowCount();
                        //System.out.println("cantidad de filas: " + totalFilas);

                        for (int i = 0; totalFilas > i; i++) {
                            //System.out.println("cantidad de filas: " + modelo.getRowCount());
                            modelo.removeRow(0);
                        }
                        //manejadorBoleta mb = new manejadorBoleta(con);
                        mb.filtroBoletaUltPedido(modelo, busqueda, zona, user.getId_usuario());
                        //con.notify();
                        // Así, se da la impresión de que se ejecuta cada cierto tiempo
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
        // Creamos un hilo y le pasamos el runnable
        hilo = new Thread(runnable);
        hilo.start();

        this.menuInicio.removeAll();

        JMenu menu = new JMenu("Menu");

        menuInicio.add(menu);
        JMenuItem clientes = new JMenuItem("ABM Clientes");
        JMenuItem mercaderias = new JMenuItem("ABM Mercaderias");
        JMenuItem cuentacorriente = new JMenuItem("Cuenta Corriente");
        JMenuItem configuraciones = new JMenuItem("Configuraciones");

        menu.add(clientes);

        menu.add(mercaderias);

        menu.add(cuentacorriente);

        menu.add(configuraciones);

        JMenu ventas = new JMenu("Ventas");

        menuInicio.add(ventas);
        JMenuItem nuevoPedido = new JMenuItem("Nuevo Pedido");
        JMenuItem gestionPreciosUltPedido = new JMenuItem("Gestionar Precios Ult Pedido");
        JMenuItem gestionPreciosOtrosPedidos = new JMenuItem("Gestionar Precios Otros Pedidos");
        JMenuItem gestionCostos = new JMenuItem("Gestionar Costos");

        ventas.add(nuevoPedido);

        ventas.add(gestionPreciosUltPedido);
        //ventas.add(gestionPreciosOtrosPedidos);

        ventas.add(gestionCostos);

        JMenu reportes = new JMenu("Reportes");

        menuInicio.add(reportes);
        JMenuItem ultimoPedido = new JMenuItem("Ultimo Pedido");
        JMenuItem imprimirDocumentos = new JMenuItem("Imprimir Documentos");

        reportes.add(ultimoPedido);

        reportes.add(imprimirDocumentos);

        JMenu acercaDe = new JMenu("(?)");

        menuInicio.add(acercaDe);
        JMenuItem info = new JMenuItem("Acerca De");

        acercaDe.add(info);

        clientes.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent ev) {
                Clientes cm = new Clientes(tablaBoletasSinCerrar, modelo, con, user);
                cm.setLocationRelativeTo(null);
                cm.setVisible(true);
            }
        }
        );

        mercaderias.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                Mercaderias am = new Mercaderias(con);
                am.setLocationRelativeTo(null);
                am.setVisible(true);
            }
        });

        cuentacorriente.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                CuentaCorrienteReporte ccr = new CuentaCorrienteReporte(con);
                ccr.setLocationRelativeTo(null);
                ccr.setVisible(true);
            }
        });

        nuevoPedido.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                CargarPedidoDidactico cp = new CargarPedidoDidactico(tablaBoletasSinCerrar, 0, null, 1, modelo, null, con, user);
                cp.setLocationRelativeTo(null);
                cp.setVisible(true);
            }
        });

        gestionPreciosUltPedido.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                /*GestionPrecios gp = new GestionPrecios();
                gp.setLocationRelativeTo(null);
                gp.setVisible(true);*/
                tablaBoletasSinCerrar.setRowSelectionInterval(0, 0);
                PreciosBoletas cp = new PreciosBoletas(tablaBoletasSinCerrar, null, 1, con);
                cp.setLocationRelativeTo(null);
                cp.setVisible(true);
            }
        });

        gestionPreciosOtrosPedidos.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {

            }
        });

        gestionCostos.addActionListener(new ActionListener() {
            manejadorCosto mc = new manejadorCosto(con);

            public void actionPerformed(ActionEvent ev) {
                if (mc.cantPedidosSinCosto() == 0) {
                    JOptionPane.showMessageDialog(null, "TODAS LAS GESTIONES DE COSTOS SE ENCUENTRAN CARGADAS");

                } else {
                    PedidosSinGestionDeCostos ps;
                    ps = new PedidosSinGestionDeCostos(con, InicioNuevo.this, user);
                    ps.setLocationRelativeTo(null);
                    ps.setVisible(true);

                }
            }
        });

        ultimoPedido.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                manejadorBoleta mb = new manejadorBoleta(con);
                //conexionDB c = new conexionDB();
                //Connection con = c.conectar();
                UltimosReportes rep = new UltimosReportes(mb.obtenerUltPedido(con), con, user);
                rep.setLocationRelativeTo(null);
                rep.setVisible(true);
            }
        });

        imprimirDocumentos.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                BusquedaReportes br = new BusquedaReportes(con, user);
                br.setLocationRelativeTo(null);
                br.setVisible(true);
            }
        });

        info.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                JOptionPane.showMessageDialog(null, new JLabel("<html>BENNASAR DELIVERY SOFT<br/>"
                        + "RELEASE " + version + "<br/>"
                        + "<br/>"
                        + "BISS IT ®<br/>"
                        + "Ing. Rodrigo Mayer<br/>"
                        + "rmmayer@bissit.tech<br/>"
                        + "cel: 3886525523</html>", JLabel.LEFT));
            }
        });

        this.setFocusable(true);
        this.requestFocus();

        this.botonAgregarCliente.setToolTipText("VER CLIENTES");
        this.botonNuevaBoleta.setToolTipText("NUEVO BOLETA");
        this.botonEditarBoleta.setToolTipText("EDITAR BOLETA SELECCIONADO");
        this.botonEliminarBoleta.setToolTipText("ELIMINAR BOLETA SELECCIONADO");
        this.botonImprimir.setToolTipText("IMPRIMIR BOLETA SELECCIONADO");
        this.botonCargarPrecios.setToolTipText("CARGAR PRECIOS AL PEDIDO SELECCIONADO");

        class PopUpDemo extends JPopupMenu {

            public PopUpDemo() {
                JMenuItem editarCliente = new JMenuItem("Editar Cliente");
                JMenuItem verCC = new JMenuItem("Ver Cta Cte");

                editarCliente.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        //Clientes cm = new Clientes(tablaBoletasSinCerrar, InicioNuevo.this, con);
                        //cm.setLocationRelativeTo(null);
                        //cm.setVisible(true);
                        //int id_cliente = tablaBoletasSinCerrar.getC
                        int id_cliente = Integer.parseInt(tablaBoletasSinCerrar.getValueAt(tablaBoletasSinCerrar.getSelectedRow(), 4).toString());
                        System.out.println("ID CLIENTE : " + id_cliente);
                        AgregarCliente ac = new AgregarCliente(null, null, tablaBoletasSinCerrar, 3, id_cliente, modelo, con, user);
                        ac.setLocationRelativeTo(null);
                        ac.setVisible(true);
                    }

                });

                verCC.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent ae) {
                        manejadorCliente mc = new manejadorCliente(con);
                        Cliente cli = mc.obtenerCliente(Integer.parseInt(tablaBoletasSinCerrar.getValueAt(tablaBoletasSinCerrar.getSelectedRow(), 4).toString()));
                        CuentaCorriente cc = new CuentaCorriente(cli, con);
                        cc.setLocationRelativeTo(null);
                        cc.setVisible(true);
                    }

                });

                add(editarCliente);
                add(verCC);

            }

        }

        final PopUpDemo pop = new PopUpDemo();
        tablaBoletasSinCerrar.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent me) {
                if (SwingUtilities.isRightMouseButton(me)) {
                    int r = tablaBoletasSinCerrar.rowAtPoint(me.getPoint());
                    if (r >= 0 && r < tablaBoletasSinCerrar.getRowCount()) {
                        tablaBoletasSinCerrar.setRowSelectionInterval(r, r);
                    } else {
                        tablaBoletasSinCerrar.clearSelection();
                    }
                    pop.show(me.getComponent(), me.getX(), me.getY());
                }
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaBoletasSinCerrar = new javax.swing.JTable();
        botonActualizar = new javax.swing.JButton();
        botonCerrarPedido = new javax.swing.JButton();
        botonNuevaBoleta = new javax.swing.JButton();
        botonCargarPrecios = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtBusquedaCliente = new javax.swing.JTextField();
        botonEditarBoleta = new javax.swing.JButton();
        botonEliminarBoleta = new javax.swing.JButton();
        botonImprimir = new javax.swing.JButton();
        botonAgregarCliente = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        comboBoxZona = new javax.swing.JComboBox<>();
        menuInicio = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jMenu2 = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                formKeyReleased(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("BOLETAS SIN CERRAR");
        jLabel1.setToolTipText("");

        tablaBoletasSinCerrar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaBoletasSinCerrar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaBoletasSinCerrarMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaBoletasSinCerrar);

        botonActualizar.setText("ACTUALIZAR BOLETAS");
        botonActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonActualizarActionPerformed(evt);
            }
        });

        botonCerrarPedido.setText("CERRAR PEDIDO");
        botonCerrarPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCerrarPedidoActionPerformed(evt);
            }
        });

        botonNuevaBoleta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/add.png"))); // NOI18N
        botonNuevaBoleta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonNuevaBoletaActionPerformed(evt);
            }
        });

        botonCargarPrecios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/price.png"))); // NOI18N
        botonCargarPrecios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonCargarPreciosActionPerformed(evt);
            }
        });

        jLabel2.setText("Cliente: ");

        txtBusquedaCliente.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtBusquedaClienteKeyReleased(evt);
            }
        });

        botonEditarBoleta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/edit (1).png"))); // NOI18N
        botonEditarBoleta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEditarBoletaActionPerformed(evt);
            }
        });

        botonEliminarBoleta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/erase.png"))); // NOI18N
        botonEliminarBoleta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarBoletaActionPerformed(evt);
            }
        });

        botonImprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/print.png"))); // NOI18N
        botonImprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonImprimirActionPerformed(evt);
            }
        });

        botonAgregarCliente.setText("( + )");
        botonAgregarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarClienteActionPerformed(evt);
            }
        });

        jLabel3.setText("Zona:");

        comboBoxZona.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "TODAS", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        comboBoxZona.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxZonaActionPerformed(evt);
            }
        });

        jMenu1.setText("File");
        menuInicio.add(jMenu1);

        jMenu2.setText("Edit");
        menuInicio.add(jMenu2);

        setJMenuBar(menuInicio);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(botonActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 336, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(39, 39, 39)
                        .addComponent(botonCerrarPedido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtBusquedaCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 323, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(botonAgregarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(comboBoxZona, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addComponent(jScrollPane1)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(botonNuevaBoleta, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addComponent(botonEditarBoleta, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addComponent(botonEliminarBoleta, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addComponent(botonCargarPrecios, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(80, 80, 80)
                        .addComponent(botonImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 74, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(21, 21, 21))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {botonCargarPrecios, botonEditarBoleta, botonEliminarBoleta, botonImprimir, botonNuevaBoleta});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txtBusquedaCliente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonAgregarCliente)
                    .addComponent(jLabel3)
                    .addComponent(comboBoxZona, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(botonNuevaBoleta, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(botonEditarBoleta, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(botonEliminarBoleta, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(botonCargarPrecios, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(botonImprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonCerrarPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {botonCargarPrecios, botonEditarBoleta, botonEliminarBoleta, botonImprimir, botonNuevaBoleta});

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablaBoletasSinCerrarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaBoletasSinCerrarMouseClicked
        // TODO add your handling code here:

        if (tablaBoletasSinCerrar.getSelectedRowCount() == 1) {
            this.botonImprimir.setEnabled(true);
            botonEditarBoleta.setEnabled(true);
            botonEliminarBoleta.setEnabled(true);
            this.botonCargarPrecios.setEnabled(true);
        } else {
            this.botonImprimir.setEnabled(false);
            botonEditarBoleta.setEnabled(false);
            botonEliminarBoleta.setEnabled(false);
            this.botonCargarPrecios.setEnabled(false);
        }

        if (evt.getClickCount() == 2 && !evt.isConsumed()) {
            evt.consume();
            manejadorBoleta mb = new manejadorBoleta(con);
            int id_boleta = Integer.parseInt(tablaBoletasSinCerrar.getValueAt(tablaBoletasSinCerrar.getSelectedRow(), 0).toString());

            mb.traerBoleta(id_boleta, boleta);

            CargarPedidoDidactico cp = new CargarPedidoDidactico(tablaBoletasSinCerrar, 0, boleta, 2, modelo, null, con, user);
            //PreciosBoletas cp = new PreciosBoletas(tablaBoletasSinCerrar, 0, boleta, 2, modelo, null);
            cp.setLocationRelativeTo(null);
            cp.setVisible(true);

            this.botonEditarBoleta.setEnabled(false);
            this.botonImprimir.setEnabled(false);
            this.botonEliminarBoleta.setEnabled(false);
            this.botonCargarPrecios.setEnabled(false);

            //int filaS = tablaBoletasSinCerrar.getSelectedRow();
            //this.tablaBoletasSinCerrar.clearSelection();
            //this.tablaBoletasSinCerrar.setRowSelectionInterval(filaS+1, filaS+1);
            this.requestFocus();
        }


    }//GEN-LAST:event_tablaBoletasSinCerrarMouseClicked

    private void botonActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonActualizarActionPerformed
        // TODO add your handling code here:

        botonImprimir.setEnabled(false);
        botonEditarBoleta.setEnabled(false);
        botonEliminarBoleta.setEnabled(false);
        this.botonCargarPrecios.setEnabled(false);

        manejadorBoleta mb = new manejadorBoleta(con);
        //mb.actualizarBoletas(tablaBoletasSinCerrar, 0, modelo, user.getId_usuario());
        String busqueda = txtBusquedaCliente.getText();
        int index = comboBoxZona.getSelectedIndex();
        int zona = 0;

        if (index != -1) {
            zona = this.comboBoxZona.getSelectedIndex();
        }

        int totalFilas = tablaBoletasSinCerrar.getRowCount();
        //System.out.println("cantidad de filas: " + totalFilas);

        for (int i = 0; totalFilas > i; i++) {
            //System.out.println("cantidad de filas: " + modelo.getRowCount());
            modelo.removeRow(0);
        }
        //manejadorBoleta mb = new manejadorBoleta(con);
        mb.filtroBoletaUltPedido(modelo, busqueda, zona, user.getId_usuario());
    }//GEN-LAST:event_botonActualizarActionPerformed

    private void botonCerrarPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCerrarPedidoActionPerformed
        // TODO add your handling code here:

        /*
        if (this.tablaBoletasSinCerrar.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "NO HAY BOLETAS DISPONIBLES PARA CERRAR EL PEDIDO");
        } else {
            if (mc.cantPedidosSinCosto() != 0) {
                int k = JOptionPane.showConfirmDialog(null, "EXISTEN PEDIDOS ANTERIORES SIN CARGA DE COSTOS, DESEA CERRAR DE TODAS MANERAS EL PEDIDO?\nPosteriormente podrá cargar las gestiones de costo de los pedidos sin cerrar", "Confirmar accion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (k == 0) {
                    int n = JOptionPane.showConfirmDialog(null, "REALMENTE DESEA CERRAR EL PEDIDO?\nUna vez cerrado no se podran editar las boletas", "Confirmar accion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (n == 0) {
                        manejadorPedido mp = new manejadorPedido();
                        mp.guardarPedido(tablaBoletasSinCerrar);
                        mp.guardarReportes(mp.obtenerUltimoPedido().getId_pedido());
                    }
                }
            } else {
                int n = JOptionPane.showConfirmDialog(null, "REALMENTE DESEA CERRAR EL PEDIDO? Una vez cerrado no se podran editar las boletas", "Confirmar accion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (n == 0) {
                    manejadorPedido mp = new manejadorPedido();
                    mp.guardarPedido(tablaBoletasSinCerrar);
                    mp.guardarReportes(mp.obtenerUltimoPedido().getId_pedido());
                }
            }
        }*/
        this.tablaBoletasSinCerrar.setRowSelectionInterval(0, 0);
        CerrarPedido cp = new CerrarPedido(tablaBoletasSinCerrar, con, this, user);
        cp.setLocationRelativeTo(null);
        cp.setVisible(true);


    }//GEN-LAST:event_botonCerrarPedidoActionPerformed

    private void botonNuevaBoletaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonNuevaBoletaActionPerformed
        // TODO add your handling code here:
        CargarPedidoDidactico cp = new CargarPedidoDidactico(tablaBoletasSinCerrar, 0, null, 1, modelo, null, con, user);
        cp.setLocationRelativeTo(null);
        cp.setVisible(true);
    }//GEN-LAST:event_botonNuevaBoletaActionPerformed

    private void formKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_formKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyCode() == KeyEvent.VK_1) {
            CargarPedidoDidactico cp = new CargarPedidoDidactico(tablaBoletasSinCerrar, 0, null, 1, modelo, null, con, user);
            cp.setLocationRelativeTo(null);
            cp.setVisible(true);
        }
    }//GEN-LAST:event_formKeyReleased

    private void botonCargarPreciosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonCargarPreciosActionPerformed
        // TODO add your handling code here:
        PreciosBoletas cp = new PreciosBoletas(tablaBoletasSinCerrar, null, 1, con);
        cp.setLocationRelativeTo(null);
        cp.setVisible(true);
    }//GEN-LAST:event_botonCargarPreciosActionPerformed

    private void botonEditarBoletaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEditarBoletaActionPerformed
        // TODO add your handling code here:
        manejadorBoleta mb = new manejadorBoleta(con);
        int id_boleta = Integer.parseInt(tablaBoletasSinCerrar.getValueAt(tablaBoletasSinCerrar.getSelectedRow(), 0).toString());

        mb.traerBoleta(id_boleta, boleta);

        CargarPedidoDidactico cp = new CargarPedidoDidactico(tablaBoletasSinCerrar, 0, boleta, 2, modelo, null, con, user);
        cp.setLocationRelativeTo(null);
        cp.setVisible(true);
    }//GEN-LAST:event_botonEditarBoletaActionPerformed

    private void botonEliminarBoletaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarBoletaActionPerformed
        // TODO add your handling code here:
        int filaS = this.tablaBoletasSinCerrar.getSelectedRow();
        String fechaAño = tablaBoletasSinCerrar.getValueAt(filaS, 3).toString().substring(6, 10);
        String fechaMes = tablaBoletasSinCerrar.getValueAt(filaS, 3).toString().substring(3, 5);
        String fechaDia = tablaBoletasSinCerrar.getValueAt(filaS, 3).toString().substring(0, 2);
        String fechaBoleta = fechaAño + fechaMes + fechaDia;

        //System.out.println("AÑO: "+fechaAño);
        //System.out.println("MES: "+fechaMes);
        //System.out.println("DIA: "+fechaDia);
        int boleta = Integer.parseInt(tablaBoletasSinCerrar.getValueAt(filaS, 0).toString());
        String cliente = tablaBoletasSinCerrar.getValueAt(filaS, 1).toString();

        System.out.println("BOLETA A ELIMINAR ID: " + boleta);
        System.out.println("FECHA BOLETA A ELIMINAR: " + fechaBoleta);

        int n = JOptionPane.showConfirmDialog(null, "Realmente desea ELIMINAR la boleta de " + cliente + "?", "Confirmar accion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

        if (n == 0) {
            JOptionPane.showMessageDialog(null, "BOLETA ELIMINADA CON EXITO");
            manejadorBoleta mb = new manejadorBoleta(con);
            manejadorFechas mf = new manejadorFechas(con);
            mb.eliminarBoleta(boleta, fechaBoleta, cliente);
            mb.actualizarBoletas(tablaBoletasSinCerrar, 0, modelo, user.getId_usuario());
        } else {
            JOptionPane.showMessageDialog(null, "NO SE ELIMINO LA BOLETA");
        }
    }//GEN-LAST:event_botonEliminarBoletaActionPerformed

    private void botonImprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonImprimirActionPerformed
        // TODO add your handling code here:
        manejadorBoleta mb = new manejadorBoleta(con);
        Boleta b = new Boleta();
        int filaS = tablaBoletasSinCerrar.getSelectedRow();
        String cliente = tablaBoletasSinCerrar.getValueAt(filaS, 1).toString();
        int id_boleta = Integer.parseInt(tablaBoletasSinCerrar.getValueAt(filaS, 0).toString());
        float total;

        mb.traerBoleta(id_boleta, b);

        mb.imprimirBoleta(b);
    }//GEN-LAST:event_botonImprimirActionPerformed

    private void botonAgregarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarClienteActionPerformed
        // TODO add your handling code here:
        /*CargarPedidoDidactico cp = new CargarPedidoDidactico(tablaBoletasSinCerrar, 0, null, 1, modelo, null);
        cp.setLocationRelativeTo(null);
        cp.setVisible(true);*/

        Clientes cm = new Clientes(tablaBoletasSinCerrar, modelo, con, user);
        cm.setLocationRelativeTo(null);
        cm.setVisible(true);
    }//GEN-LAST:event_botonAgregarClienteActionPerformed

    private void txtBusquedaClienteKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBusquedaClienteKeyReleased
        // TODO add your handling code here:

        /*String busqueda = txtBusquedaCliente.getText();
        int totalFilas = tablaBoletasSinCerrar.getRowCount();
        //System.out.println("cantidad de filas: " + totalFilas);

        for (int i = 0; totalFilas > i; i++) {
            System.out.println("cantidad de filas: "+totalFilas);
            modelo.removeRow(0);
        }
        
        mb.filtroBoletaUltPedido(modelo, busqueda);*/
        String busqueda = txtBusquedaCliente.getText();
        int index = comboBoxZona.getSelectedIndex();
        int zona = 0;

        if (index != -1) {
            zona = this.comboBoxZona.getSelectedIndex();
        }

        int totalFilas = tablaBoletasSinCerrar.getRowCount();
        //System.out.println("cantidad de filas: " + totalFilas);

        for (int i = 0; totalFilas > i; i++) {
            //System.out.println("cantidad de filas: " + modelo.getRowCount());
            modelo.removeRow(0);
        }
        manejadorBoleta mb = new manejadorBoleta(con);
        mb.filtroBoletaUltPedido(modelo, busqueda, zona, user.getId_usuario());
    }//GEN-LAST:event_txtBusquedaClienteKeyReleased

    private void comboBoxZonaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxZonaActionPerformed
        // TODO add your handling code here:
        String busqueda = txtBusquedaCliente.getText();
        int index = comboBoxZona.getSelectedIndex();
        int zona = 0;

        if (index != -1) {
            zona = this.comboBoxZona.getSelectedIndex();
        }

        int totalFilas = tablaBoletasSinCerrar.getRowCount();
        //System.out.println("cantidad de filas: " + totalFilas);

        for (int i = 0; totalFilas > i; i++) {
            //System.out.println("cantidad de filas: " + modelo.getRowCount());
            modelo.removeRow(0);
        }
        manejadorBoleta mb = new manejadorBoleta(con);
        mb.filtroBoletaUltPedido(modelo, busqueda, zona, user.getId_usuario());
        //mb.filtroBoletaZona(modelo, zona);
    }//GEN-LAST:event_comboBoxZonaActionPerformed

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
        if (cierre) {
            int o = JOptionPane.showConfirmDialog(null, "Está seguro que desea cerrar la sesion?");

            if (o == 0) {
                Window[] windows = Window.getWindows();

                for (Window window : windows) {
                    window.dispose();
                }

                hilo.stop();

                Login l = new Login(con);
                l.setVisible(true);
                l.setLocationRelativeTo(null);

            } else {
                InicioNuevo i = new InicioNuevo(con, user, true);
                i.setVisible(true);
                i.setLocationRelativeTo(null);
            }
        }


    }//GEN-LAST:event_formWindowClosed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        // TODO add your handling code here:

    }//GEN-LAST:event_formWindowClosing

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InicioNuevo.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InicioNuevo.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InicioNuevo.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InicioNuevo.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InicioNuevo(null, null, true).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonActualizar;
    private javax.swing.JButton botonAgregarCliente;
    private javax.swing.JButton botonCargarPrecios;
    private javax.swing.JButton botonCerrarPedido;
    private javax.swing.JButton botonEditarBoleta;
    private javax.swing.JButton botonEliminarBoleta;
    private javax.swing.JButton botonImprimir;
    private javax.swing.JButton botonNuevaBoleta;
    private javax.swing.JComboBox<String> comboBoxZona;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JMenuBar menuInicio;
    private javax.swing.JTable tablaBoletasSinCerrar;
    private javax.swing.JTextField txtBusquedaCliente;
    // End of variables declaration//GEN-END:variables
}
