/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Conexion.conexionDB;
import Controlador.manejadorCosto;
import Controlador.manejadorFechas;
import Controlador.manejadorPedido;
import Modelo.Pedido;
import Modelo.Usuario;
import java.io.File;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author rmmayer
 */
public class CerrarPedido extends javax.swing.JFrame {

    /**
     * Creates new form CerrarPedido
     */
    Connection con;
    Usuario user;
    JTable tablaBoletasSinCerrar;
    JFrame inicio;

    public CerrarPedido(JTable tablaBoletasSinCerrar, Connection con, JFrame inicio, Usuario user) {
        initComponents();
        this.con = con;
        this.user = user;
        this.tablaBoletasSinCerrar = tablaBoletasSinCerrar;
        this.inicio = inicio;

        /*
        if(mc.verificarGestionCosto()){
            this.btnPreciosBoletas.setEnabled(true);
        }else{
            this.btnPreciosBoletas.setEnabled(false);
        }*/
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jButton1 = new javax.swing.JButton();
        btnPreciosCostos = new javax.swing.JButton();
        btnFinalizarPedido = new javax.swing.JButton();
        btnPreciosBoletas = new javax.swing.JButton();
        btnListaDeCompras = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("REVISAR PEDIDO");

        jButton1.setText("REVISAR ZONAS");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        btnPreciosCostos.setText("CARGAR PRECIOS DE COSTO");
        btnPreciosCostos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreciosCostosActionPerformed(evt);
            }
        });

        btnFinalizarPedido.setText("FINALIZAR");
        btnFinalizarPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnFinalizarPedidoActionPerformed(evt);
            }
        });

        btnPreciosBoletas.setText("CARGAR PRECIOS BOLETAS");
        btnPreciosBoletas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnPreciosBoletasActionPerformed(evt);
            }
        });

        btnListaDeCompras.setText("IMPRIMIR DOCUMENTOS CONTROL");
        btnListaDeCompras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnListaDeComprasActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(23, 23, 23)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(btnListaDeCompras, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPreciosBoletas, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnFinalizarPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnPreciosCostos, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 334, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(23, 23, 23))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnListaDeCompras, btnPreciosBoletas, btnPreciosCostos, jButton1});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnPreciosCostos, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnPreciosBoletas, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnListaDeCompras, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(btnFinalizarPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 107, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21))
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnListaDeCompras, btnPreciosBoletas, btnPreciosCostos, jButton1});

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnPreciosCostosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreciosCostosActionPerformed
        // TODO add your handling code here:
        Pedido p = new Pedido();
        manejadorPedido mp = new manejadorPedido(con);

        //int filaS = tablaPedidosSinCosto.getSelectedRow();
        p = mp.obtenerPedidoSinCargar();

        //GestionDeCosto gc = new GestionDeCosto(p, this);
        GestionDeCostoNueva gc = new GestionDeCostoNueva(p, tablaBoletasSinCerrar, con, inicio,user);
        gc.setLocationRelativeTo(null);
        gc.setVisible(true);

        this.dispose();
    }//GEN-LAST:event_btnPreciosCostosActionPerformed

    private void btnPreciosBoletasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnPreciosBoletasActionPerformed
        // TODO add your handling code here:
        //PreciosBoletas cp = new PreciosBoletas(tablaBoletasSinCerrar, 0, boleta, 2, modelo, null);
        PreciosBoletas cp = new PreciosBoletas(tablaBoletasSinCerrar, null, 1, con);
        cp.setLocationRelativeTo(null);
        cp.setVisible(true);

    }//GEN-LAST:event_btnPreciosBoletasActionPerformed

    private void btnListaDeComprasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnListaDeComprasActionPerformed
        // TODO add your handling code here:
        /*String ruta;
        manejadorPedido mp = new manejadorPedido(con);
        ruta = "src\\Reportes\\reporteListaDeComprasPreview.jrxml";
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        //String rutaPDF = mf.obtenerDireccionCarpetasPedidos(mf.obtenerFechaPedido(0));
        String rutaPDF = new File(".").getAbsolutePath() + "\\Google Drive\\PedidosPreView\\";
        File dir = new File(rutaPDF);
        System.out.println(dir.getAbsolutePath());
        //File reporte = new File(".");
        dir.mkdirs();

        float bultosSinHuevo = mp.obtenerBultosSinHuevos(0);
        float bultosHuevos = mp.obtenerBultosHuevos(0);

        float bultos = bultosSinHuevo + bultosHuevos;

        try {
            Map parametro = new HashMap();
            parametro.put("id_pedido", 0);
            parametro.put("bultos", bultos);
            //parametro.put("id_pedidoFruta", mb.obtenerUltPedido(con3));
            //parametro.put("id_pedidoVerdura", mb.obtenerUltPedido(con3));
            //parametro.put("id_pedido2", mb.obtenerUltPedido(con3));

            JasperReport reporte = JasperCompileManager.compileReport(ruta);
            JasperPrint mostrarReporte = JasperFillManager.fillReport(reporte, parametro, con);
            JasperExportManager.exportReportToPdfFile(mostrarReporte, rutaPDF + "\\listaDeCompras_pedidoNuevo.pdf");
            JasperViewer.viewReport(mostrarReporte, false);

        } catch (JRException ex) {
            Logger.getLogger(Clientes.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        
        manejadorPedido mp = new manejadorPedido(con);
        mp.guardarReportesNuevo(0, user);
    }//GEN-LAST:event_btnListaDeComprasActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        ModificarZonas mz = new ModificarZonas(con);
        mz.setLocationRelativeTo(null);
        mz.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnFinalizarPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnFinalizarPedidoActionPerformed
        // TODO add your handling code here:
        manejadorCosto mc = new manejadorCosto(con);
        if (this.tablaBoletasSinCerrar.getRowCount() == 0) {
            JOptionPane.showMessageDialog(null, "NO HAY BOLETAS DISPONIBLES PARA CERRAR EL PEDIDO");
        } else {
            if (mc.cantPedidosSinCosto() != 0) {
                int k = JOptionPane.showConfirmDialog(null, "EXISTEN PEDIDOS ANTERIORES SIN CARGA DE COSTOS, DESEA CERRAR DE TODAS MANERAS EL PEDIDO?\nPosteriormente podrá cargar las gestiones de costo de los pedidos sin cerrar", "Confirmar accion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (k == 0) {
                    int n = JOptionPane.showConfirmDialog(null, "REALMENTE DESEA CERRAR EL PEDIDO?\nUna vez cerrado no se podran editar las boletas", "Confirmar accion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                    if (n == 0) {
                        manejadorPedido mp = new manejadorPedido(con);
                        mp.guardarPedido(tablaBoletasSinCerrar);
                        mp.guardarReportes(mp.obtenerUltimoPedido().getId_pedido(), user);
                        JOptionPane.showMessageDialog(null, "PEDIDO CERRADO CON EXITO");
                        inicio.dispose();
                        this.dispose();
                        InicioNuevo in = new InicioNuevo(con,user, false);
                        in.setLocationRelativeTo(null);
                        in.setVisible(true);
                    }
                }
            } else {
                int n = JOptionPane.showConfirmDialog(null, "REALMENTE DESEA CERRAR EL PEDIDO? EL PEDIDO SE CERRARÁ PARA TODOS LOS USUARIOS. Una vez cerrado no se podran editar las boletas", "Confirmar accion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (n == 0) {
                    manejadorPedido mp = new manejadorPedido(con);
                    mp.guardarPedido(tablaBoletasSinCerrar);
                    mp.guardarReportes(mp.obtenerUltimoPedido().getId_pedido(), user);
                    JOptionPane.showMessageDialog(null, "PEDIDO CERRADO CON EXITO");
                    inicio.dispose();
                    this.dispose();
                    InicioNuevo in = new InicioNuevo(con,user, false);
                    in.setLocationRelativeTo(null);
                    in.setVisible(true);
                }
            }
        }
    }//GEN-LAST:event_btnFinalizarPedidoActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CerrarPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CerrarPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CerrarPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CerrarPedido.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new CerrarPedido(null, null,null,null).setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnFinalizarPedido;
    private javax.swing.JButton btnListaDeCompras;
    private javax.swing.JButton btnPreciosBoletas;
    private javax.swing.JButton btnPreciosCostos;
    private javax.swing.JButton jButton1;
    // End of variables declaration//GEN-END:variables
}
