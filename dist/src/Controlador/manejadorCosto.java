/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.conexionDB;
import Modelo.Pedido;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;

/**
 *
 * @author Administrador
 */
public class manejadorCosto {

    Connection con;

    public manejadorCosto(Connection con) {
        this.con = con;
    }

    public void cargarCostos(JComboBox gastos) {

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        Object[] gasto = new Object[2];

        String sqlGastos = "SELECT id_gastos, concepto from concepto_gastos where concepto <> 'Mercaderia'";
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sqlGastos);

            while (rs1.next()) {
                //gasto[0] = rs1.getInt(1);
                //gasto[1] = rs1.getString(2);
                gastos.addItem(rs1.getString(2));
            }
            gastos.setSelectedIndex(-1);
            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCosto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int obtenerGastoID(String concepto) {
        int id = 0;

        String sql = "SELECT id_gastos from concepto_gastos where concepto = '" + concepto + "'";

        //conexionDB c = new conexionDB();
        // Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                id = rs1.getInt(1);
            }
            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCosto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return id;

    }

    public void guardarCostoMercaderias(JTable costoMercaderias, int fila, int id_pedido) {
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "INSERT into gestion_costo "
                + "(id_pedido, id_gastos, detalle, cantidad, costo_unitario, costo_total) "
                + "VALUES(?,?,?,?,?,?)";
        try {
            PreparedStatement ps1 = con.prepareStatement(sql);
            ps1.setInt(1, id_pedido);
            ps1.setInt(2, 1);
            ps1.setString(3, costoMercaderias.getValueAt(fila, 0).toString());
            ps1.setString(4, costoMercaderias.getValueAt(fila, 3).toString());
            ps1.setFloat(5, Float.parseFloat(costoMercaderias.getValueAt(fila, 4).toString()));
            ps1.setFloat(6, Float.parseFloat(costoMercaderias.getValueAt(fila, 5).toString()));

            ps1.executeUpdate();

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCosto.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean verificarGestionCosto() {
        Pedido p = new Pedido();
        boolean b = false;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sql = "SELECT * FROM gestion_costo2 "
                + "WHERE id_pedido = 0";

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                b = true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCosto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return b;
    }

    public void guardarCostoMercaderias2(JTable costoMercaderias, int fila, int id_pedido) {
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "INSERT into gestion_costo2 "
                + "(id_pedido, id_gastos, detalle, cantidad, id_tipocant, costo_unitario, costo_total) "
                + "VALUES(?,?,?,?,?,?,?)";
        try {
            PreparedStatement ps1 = con.prepareStatement(sql);
            ps1.setInt(1, id_pedido);
            ps1.setInt(2, 1);
            ps1.setString(3, costoMercaderias.getValueAt(fila, 0).toString());
            ps1.setFloat(4, Float.parseFloat(costoMercaderias.getValueAt(fila, 3).toString()));
            ps1.setInt(5, Integer.parseInt(costoMercaderias.getValueAt(fila, 2).toString()));
            ps1.setFloat(6, Float.parseFloat(costoMercaderias.getValueAt(fila, 5).toString()));
            ps1.setFloat(7, Float.parseFloat(costoMercaderias.getValueAt(fila, 6).toString()));

            ps1.executeUpdate();

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCosto.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void actualizarCostosMercaderias(JTable tabla, float costo_unitario, int id_detalle) {

        String sql = "UPDATE detalle "
                + "SET costo_unitario = " + costo_unitario + " "
                + "WHERE id_detalle = " + id_detalle;
        
        System.out.println(sql);

        try {
            Statement st = con.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCosto.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void guardarOtrosGastos(JTable otrosGastos, int fila, int id_pedido) {
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "INSERT into gestion_costo2 "
                + "(id_pedido, id_gastos, detalle, cantidad, costo_total) "
                + "VALUES(?,?,?,?,?)";

        //System.out.println(sql);
        try {
            PreparedStatement ps1 = con.prepareStatement(sql);
            ps1.setInt(1, id_pedido);
            ps1.setInt(2, Integer.parseInt(otrosGastos.getValueAt(fila, 0).toString()));
            ps1.setInt(4, 1);
            ps1.setString(3, otrosGastos.getValueAt(fila, 1).toString());
            ps1.setFloat(5, Float.parseFloat(otrosGastos.getValueAt(fila, 2).toString()));

            ps1.executeUpdate();

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCosto.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public float obtenerCostoTotal(int id_pedido) {
        float total = 0;
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "SELECT sum(costo_total) from gestion_costo2 "
                + "WHERE id_pedido = " + id_pedido;
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                total = rs1.getFloat(1);
            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCosto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return total;
    }

    public int cantPedidosSinCosto() {
        int res = 0;
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sql = "select count(id_pedido) from pedido where gestion_costo = 0 ";
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                res = rs1.getInt(1);
            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCosto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return res;
    }

    public boolean isPedidoConGestionDeCosto(int id_pedido) {
        boolean b = true;
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sql = "select gestion_costo from pedido where id_pedido = " + id_pedido;
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {

                if (rs1.getInt(1) == 1) {
                    b = true;
                } else {
                    b = false;
                }

            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCosto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return b;
    }

    public float actualizarTotales(JTable tablaCostosMercaderia, JTable tablaOtrosCostos) {
        float totalC = 0;
        for (int i = 0; i < tablaCostosMercaderia.getRowCount(); i++) {
            int filaS = i;
            if (!tablaCostosMercaderia.getValueAt(i, 6).toString().equals("")) {
                float cantidad = Float.parseFloat(tablaCostosMercaderia.getValueAt(filaS, 5).toString());
                float precioU = Float.parseFloat(tablaCostosMercaderia.getValueAt(filaS, 6).toString());
                float total = cantidad * precioU;
                totalC = totalC + total;
                tablaCostosMercaderia.setValueAt(total, filaS, 7);
            } else {
                tablaCostosMercaderia.setValueAt("", filaS, 6);
            }
        }

        for (int i = 0; i < tablaOtrosCostos.getRowCount(); i++) {
            int filaS = i;
            if (!tablaOtrosCostos.getValueAt(i, 2).toString().equals("")) {
                //float cantidad = Float.parseFloat(tablaOtrosCostos.getValueAt(filaS, 3).toString());
                float precioU = Float.parseFloat(tablaOtrosCostos.getValueAt(filaS, 2).toString());
                totalC = totalC + precioU;
                //tablaOtrosCostos.setValueAt(total, filaS, 6);
            } else {
                tablaOtrosCostos.setValueAt("", filaS, 6);
            }
        }

        return totalC;
    }

    public void eliminarCostos(int id_pedido) {
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "DELETE FROM gestion_costo2 "
                + "WHERE id_pedido = 0";

        //System.out.println(sql);
        Statement st1;
        try {
            st1 = con.createStatement();
            st1.executeUpdate(sql);

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCosto.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
