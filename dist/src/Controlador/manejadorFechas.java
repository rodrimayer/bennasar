/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.conexionDB;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrador
 */
public class manejadorFechas {

    Connection con;

    public manejadorFechas(Connection con) {
        this.con = con;
    }

    public String obtenerFechaActual() {

        Calendar fecha = Calendar.getInstance();
        String dia, mes, annio;
        String fechaSql = null;

        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        //System.out.println("Hora: " + hourFormat.format(date));
        //System.out.println("HORA: " + Integer.parseInt(hourFormat.format(date).toString().substring(0, 2)));

        boolean mayor19;

        if (Integer.parseInt(hourFormat.format(date).toString().substring(0, 2)) >= 0 && Integer.parseInt(hourFormat.format(date).toString().substring(0, 2)) < 19) {
            mayor19 = false;
        } else {
            mayor19 = true;
        }

        if (mayor19) {
            //System.out.println("MAYOR A 19");
            fecha.add(Calendar.DAY_OF_YEAR, 1);

        } else {
            //System.out.println("MENOR A 19");            
        }

        if ((fecha.get(Calendar.DATE)) < 10) {
            dia = "0" + Integer.toString(fecha.get(Calendar.DATE));
        } else {
            dia = Integer.toString(fecha.get(Calendar.DATE));

        }

        if (fecha.get(Calendar.MONTH) + 1 < 10) {
            mes = "0" + Integer.toString(fecha.get(Calendar.MONTH) + 1);

        } else {
            mes = Integer.toString(fecha.get(Calendar.MONTH) + 1);

        }

        annio = Integer.toString(fecha.get(Calendar.YEAR));

        fechaSql = annio + mes + dia;
        //System.out.println("FECHA SQL: " + fechaSql);

        return fechaSql;

    }

    public String obtenerDireccionCarpetasBoletas(String fecha) {

        String año = fecha.substring(0, 4);
        String mes = fecha.substring(4, 6);
        String dia = fecha.substring(6, 8);

        //System.out.println("fecha: " + fecha);
        //System.out.println("año: " + fecha.substring(0, 4));
        //System.out.println("mes: " + fecha.substring(4, 6));
        //System.out.println("dia: " + fecha.substring(6, 8));
        //String año = fecha.substring(beginIndex, endIndex)
        String rutaPDF = new File(".").getAbsolutePath() + "\\Google Drive\\Boletas\\" + año + "\\" + mes + "_" + mesTexto(fecha) + "\\" + dia + "\\";

        //System.out.println("ruta: " + rutaPDF);
        return rutaPDF;
    }

    public String obtenerDireccionCarpetasPedidos(String fecha) {

        String año = fecha.substring(0, 4);
        String mes = fecha.substring(4, 6);
        String dia = fecha.substring(6, 8);

        //System.out.println("fecha: " + fecha);
        //System.out.println("año: " + fecha.substring(0, 4));
        //System.out.println("mes: " + fecha.substring(4, 6));
        //System.out.println("dia: " + fecha.substring(6, 8));
        //String año = fecha.substring(beginIndex, endIndex)
        String rutaPDF = new File(".").getAbsolutePath() + "\\Google Drive\\Pedidos\\" + año + "\\" + mes + "_" + mesTexto(fecha) + "\\" + dia + "\\";

        //System.out.println("ruta: " + rutaPDF);
        return rutaPDF;
    }

    public String mesTexto(String fecha) {

        String mes = "";

        switch (fecha.substring(4, 6)) {
            case "01":
                mes = "ENERO";
                break;
            case "02":
                mes = "FEBRERO";
                break;
            case "03":
                mes = "MARZO";
                break;
            case "04":
                mes = "ABRIL";
                break;
            case "05":
                mes = "MAYO";
                break;
            case "06":
                mes = "JUNIO";
                break;
            case "07":
                mes = "JULIO";
                break;
            case "08":
                mes = "AGOSTO";
                break;
            case "09":
                mes = "SEPTIEMBRE";
                break;
            case "10":
                mes = "OCTUBRE";
                break;
            case "11":
                mes = "NOVIEMBRE";
                break;
            case "12":
                mes = "DICIEMBRE";
                break;
        }

        return mes;

    }

    public String obtenerFechaPedido(int id_pedido) {
        String fecha = "";
        String dia = null, mes = null, annio = null;

        String sql = "select year(fecha_pedido), month(fecha_pedido), day(fecha_pedido) "
                + "from pedido "
                + "where id_pedido = " + id_pedido;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                if (rs1.getInt(3) < 10) {
                    dia = "0" + Integer.toString(rs1.getInt(3));
                } else {
                    dia = Integer.toString(rs1.getInt(3));

                }

                if (rs1.getInt(2) < 10) {
                    mes = "0" + Integer.toString(rs1.getInt(2));

                } else {
                    mes = Integer.toString(rs1.getInt(2));

                }

                annio = Integer.toString(rs1.getInt(1));

            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorFechas.class.getName()).log(Level.SEVERE, null, ex);
        }

        fecha = annio + mes + dia;
        //System.out.println("FECHA SQL: " + fecha);

        return fecha;
    }

    public String obtenerFechaBoleta(int id_boleta) {
        String fecha = "";
        String dia = null, mes = null, annio = null;

        String sql = "select year(fecha), month(fecha), day(fecha) "
                + "from boleta "
                + "where id_boleta = " + id_boleta;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                if (rs1.getInt(3) < 10) {
                    dia = "0" + Integer.toString(rs1.getInt(3));
                } else {
                    dia = Integer.toString(rs1.getInt(3));

                }

                if (rs1.getInt(2) < 10) {
                    mes = "0" + Integer.toString(rs1.getInt(2));

                } else {
                    mes = Integer.toString(rs1.getInt(2));

                }

                annio = Integer.toString(rs1.getInt(1));

            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorFechas.class.getName()).log(Level.SEVERE, null, ex);
        }

        fecha = annio + mes + dia;
        //System.out.println("FECHA SQL: " + fecha);

        return fecha;
    }

    public String convertirFecha2(String fecha) {
        String fechaConv = fecha.substring(8, 10) + "/" + fecha.substring(5, 7) + "/" + fecha.substring(0, 4);
        //System.out.println("fecha convertida: " + fechaConv);
        return fechaConv;
    }

    public void obtenerFechaHoy() {

        Calendar fecha = Calendar.getInstance();
        String dia, mes, annio;
        String fechaSql = null;

        Date date = new Date();
        //Caso 1: obtener la hora y salida por pantalla con formato:
        DateFormat hourFormat = new SimpleDateFormat("HH:mm:ss");
        //System.out.println("Hora: " + hourFormat.format(date));
        //System.out.println("HORA: " + Integer.parseInt(hourFormat.format(date).toString().substring(0, 2)));

        fechaSql = hourFormat.format(date);
        System.out.println(fechaSql);

    }

}
