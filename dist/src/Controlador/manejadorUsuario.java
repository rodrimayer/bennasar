/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Usuario;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rmmayer
 */
public class manejadorUsuario {
    
    Connection con;
    
    public manejadorUsuario(Connection con){
        this.con = con;
    }
    
    public boolean verificarUsuario(String usuario, String pass, Usuario user){
        boolean b = false;
        
        String sql = "SELECT id_usuario, pass_usuario, nombre_usuario, apellido_usuario, id_privilegio "
                + "FROM usuario "
                + "WHERE id_usuario = '"+usuario+"' and pass_usuario = '"+pass+"'";
        
        //System.out.println(sql);
        
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            while(rs.next()){
                //System.out.println("entro aqui");
                //System.out.println("usuario: "+rs.getString(1));
                b = true;
                user.setId_usuario(rs.getString(1));
                user.setPass_usuario(rs.getString(2));
                user.setNombre_usuario(rs.getString(3));
                user.setApellido_usuario(rs.getString(4));
                user.setId_privilegio(rs.getInt(5));
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(manejadorUsuario.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return b;
        
    }
    
    
}
