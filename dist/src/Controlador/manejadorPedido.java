/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.conexionDB;
import Modelo.Mercaderia;
import Modelo.Pedido;
import Modelo.Tipo_Cantidad;
import Modelo.Usuario;
import Vista.Clientes;
import Vista.InicioNuevo;
import Vista.UltimosReportes;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Administrador
 */
public class manejadorPedido {

    Connection con;

    public manejadorPedido(Connection con) {
        this.con = con;
    }

    public class MiModelo extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            if (column == 5 || column == 6) { //columnIndex: the column you want to make it editable
                return true;
            } else {
                return false;
            }
        }
    }
    MiModelo modeloMercaderia = new MiModelo();
    MiModelo modeloReporte = new MiModelo();

    manejadorCosto mc = new manejadorCosto(con);

    public void guardarPedido(JTable boletasSinCerrar) {

        int cant_boletas = boletasSinCerrar.getRowCount();
        //System.out.println("cantidad de boletas a cerrar: " + cant_boletas);

        manejadorFechas mf = new manejadorFechas(con);
        String fechaActual = mf.obtenerFechaActual();

        insertarPedido(cant_boletas, fechaActual, boletasSinCerrar);

        String sqlActualizarBoleta = "";

    }

    public void insertarPedido(int cant_boletas, String fechaActual, JTable boletasSinCerrar) {

        int id_pedido = 0;

        String sqlGuardarPedido = "INSERT INTO pedido (fecha_pedido, cant_boletas, gestion_costo) VALUES('" + fechaActual + "'," + cant_boletas + ", 0)";
        //System.out.println(sqlGuardarPedido);

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            int n = st1.executeUpdate(sqlGuardarPedido);

            if (n > 0) {
                //JOptionPane.showMessageDialog(null, "REGISTRO GUARDADO CON EXITO");
            }

        } catch (Exception e) {
            System.out.println("ERROR AL CARGAR PEDIDO: " + e);
        }

        String sqlId = "SELECT id_pedido from pedido order by fecha_pedido desc, id_pedido desc limit 1";

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sqlId);

            while (rs1.next()) {
                id_pedido = rs1.getInt(1);
                //System.out.println("id pedido: " + id_pedido);
            }

            for (int i = 0; i < cant_boletas; i++) {
                int id_boleta = Integer.parseInt(boletasSinCerrar.getValueAt(i, 0).toString());
                insertarBoletaPedido(id_pedido, id_boleta);
            }

            JOptionPane.showMessageDialog(null, "BOLETAS CARGADAS CON EXITO EN EL PEDIDO");

            manejadorBoleta mb = new manejadorBoleta(con);

            //mb.actualizarBoletas(boletasSinCerrar, 0);
            //con.close();
        } catch (Exception e) {
            System.out.println("ERROR AL ENCONTRAR ID_PEDIDO: " + e);
        }

    }

    public void insertarBoletaPedido(int id_pedido, int id_boleta) {

        //String sqlGuardarBoletaPedido = "INSERT INTO boleta_pedido (id_boleta, id_pedido) VALUES ("+id_boleta+","+id_pedido+")";
        String sqlGuardarBoletaPedido = "UPDATE boleta "
                + "SET id_pedido = " + id_pedido + " "
                + "WHERE id_boleta = " + id_boleta;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            int n = st1.executeUpdate(sqlGuardarBoletaPedido);

        } catch (Exception e) {
            System.out.println("ERROR AL CARGAR BOLETA_PEDIDO: " + e);
        }

    }

    public Pedido obtenerUltimoPedido() {
        Pedido pe = new Pedido();
        manejadorFechas mf = new manejadorFechas(con);
        String sqlId = "SELECT id_pedido, DATE_FORMAT(fecha_pedido, '%d-%m-%Y'), cant_boletas from pedido order by fecha_pedido desc, id_pedido desc limit 1";
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sqlId);

            while (rs1.next()) {
                pe.setId_pedido(rs1.getInt(1));
                pe.setFecha_pedido(rs1.getString(2));
                pe.setCant_boletas(rs1.getInt(3));
                //System.out.println("id pedido: "+id_pedido);
            }
        } catch (Exception e) {
            System.out.println("Error al traer ultimo pedido: " + e);
        }

        return pe;
    }

    public Pedido obtenerPedido(int id_pedido) {
        Pedido pe = new Pedido();
        manejadorFechas mf = new manejadorFechas(con);
        String sqlId = "SELECT id_pedido, DATE_FORMAT(fecha_pedido, '%d-%m-%Y'), cant_boletas from pedido where id_pedido = " + id_pedido;
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sqlId);

            while (rs1.next()) {
                pe.setId_pedido(rs1.getInt(1));
                pe.setFecha_pedido(rs1.getString(2));
                pe.setCant_boletas(rs1.getInt(3));
                //System.out.println("id pedido: "+id_pedido);
            }
        } catch (Exception e) {
            System.out.println("Error al traer ultimo pedido: " + e);
        }

        return pe;
    }

    public void costoMercaderia(JTable tablaCostoMercaderia, int id_pedido) {
        tablaCostoMercaderia.setModel(modeloMercaderia);
        modeloMercaderia.addColumn("ID_MERCADERIA");
        modeloMercaderia.addColumn("MERCADERIA");
        modeloMercaderia.addColumn("ID_TIPO");
        modeloMercaderia.addColumn("CANTIDAD");
        modeloMercaderia.addColumn("TIPO_CANTIDAD");
        modeloMercaderia.addColumn("PRECIO UNITARIO");
        modeloMercaderia.addColumn("TOTAL");

        Object[] costo = new Object[7];

        //CODIGO PARA OCULTAR LA COLUMNA ID_MERCADERIA
        tablaCostoMercaderia.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaCostoMercaderia.getColumnModel().getColumn(0).setMinWidth(0);
        tablaCostoMercaderia.getColumnModel().getColumn(0).setPreferredWidth(0);

        //CODIGO PARA OCULTAR LA COLUMNA ID_TIPO
        tablaCostoMercaderia.getColumnModel().getColumn(2).setMaxWidth(0);
        tablaCostoMercaderia.getColumnModel().getColumn(2).setMinWidth(0);
        tablaCostoMercaderia.getColumnModel().getColumn(2).setPreferredWidth(0);

        String sql = "SELECT d.id_mercaderia, m.nombre, tc.tipo_cantidad, SUM( d.cantidad ) as Cantidad, DATE_FORMAT(p.fecha_pedido, '%d-%m-%Y') as Fecha, tc.id_tipocant "
                + "FROM detalle AS d "
                + "JOIN boleta as b ON d.id_boleta = b.id_boleta "
                + "JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia "
                + "JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant "
                + "JOIN pedido as p on b.id_pedido = p.id_pedido "
                + "WHERE b.id_pedido = " + id_pedido + " and d.id_tipocant <> 3 and d.id_tipocant <> 4 "
                + " GROUP BY d.id_mercaderia, d.id_tipocant "
                + "order by m.orden asc, m.nombre asc, d.id_tipocant asc";

        //System.out.println(sql);
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                costo[0] = rs1.getString(1);
                costo[1] = rs1.getString(2);
                costo[2] = rs1.getInt(6);
                costo[3] = Float.toString(rs1.getFloat(4));
                costo[4] = rs1.getString(3);
                costo[5] = "";
                costo[6] = "";

                modeloMercaderia.addRow(costo);
            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void pedidoPrecios(DefaultTableModel modelo, int id_pedido) {

        Object[] costo = new Object[4];

        String sql = "SELECT d.id_mercaderia, m.nombre, tc.tipo_cantidad, SUM( d.cantidad ) as Cantidad, DATE_FORMAT(p.fecha_pedido, '%d-%m-%Y') as Fecha, tc.id_tipocant "
                + "FROM detalle AS d "
                + "JOIN boleta as b ON d.id_boleta = b.id_boleta "
                + "JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia "
                + "JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant "
                + "JOIN pedido as p on b.id_pedido = p.id_pedido "
                + "WHERE b.id_pedido = " + id_pedido
                + " GROUP BY d.id_mercaderia, d.id_tipocant "
                + "order by m.id_tipo asc, m.orden asc, m.nombre asc, d.id_tipocant asc";

        //System.out.println(sql);
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                costo[0] = rs1.getString(1);
                costo[1] = rs1.getString(2);
                costo[2] = rs1.getString(3);
                costo[3] = "";

                modelo.addRow(costo);
            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void busquedaPedidos(JTable pedidos, String fechaDesde, String fechaHasta) {
        modeloReporte.addColumn("PEDIDO NRO");
        modeloReporte.addColumn("CANT BOLETAS");
        modeloReporte.addColumn("FECHA");
        pedidos.setModel(modeloReporte);

        Object[] reporte = new Object[3];
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "SELECT id_pedido, DATE_FORMAT(fecha_pedido, '%d-%m-%Y') as fecha, cant_boletas from pedido "
                + "where fecha_pedido > '" + fechaDesde + "' and fecha_pedido < '" + fechaHasta + "' "
                + "order by id_pedido asc";

        //System.out.println("SQl: " + sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                reporte[0] = rs1.getInt(1);
                reporte[1] = rs1.getInt(3);
                reporte[2] = rs1.getString(2);

                modeloReporte.addRow(reporte);
            }

            //con.close();
        } catch (Exception e) {
            System.err.println("ERROR AL TRAER BOLETAS: " + e);
        }
    }

    public void busquedaPedidosCostos(JTable pedidos, String fechaDesde, String fechaHasta) {
        modeloReporte.addColumn("PEDIDO NRO");
        modeloReporte.addColumn("CANT BOLETAS");
        modeloReporte.addColumn("FECHA");
        pedidos.setModel(modeloReporte);

        Object[] reporte = new Object[3];
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "SELECT id_pedido, DATE_FORMAT(fecha_pedido, '%d-%m-%Y') as fecha, cant_boletas from pedido "
                + "where fecha_pedido > '" + fechaDesde + "' and fecha_pedido < '" + fechaHasta + "' and gestion_costo = 1 "
                + "order by fecha asc";

        //System.out.println("SQl: " + sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                reporte[0] = rs1.getInt(1);
                reporte[1] = rs1.getInt(3);
                reporte[2] = rs1.getString(2);

                modeloReporte.addRow(reporte);
            }

            //con.close();
        } catch (Exception e) {
            System.err.println("ERROR AL TRAER BOLETAS: " + e);
        }
    }

    public void traerPedidosSinGestionDeCosto(JTable pedidosSinCosto) {
        modeloReporte.addColumn("PEDIDO NRO");
        modeloReporte.addColumn("CANT BOLETAS");
        modeloReporte.addColumn("FECHA");
        pedidosSinCosto.setModel(modeloReporte);

        Object[] reporte = new Object[3];
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "SELECT id_pedido, DATE_FORMAT(fecha_pedido, '%d-%m-%Y') as fecha, cant_boletas from pedido "
                + "where gestion_costo = 0 "
                + "order by id_pedido asc";

        //System.out.println("SQl: " + sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                reporte[0] = rs1.getInt(1);
                reporte[1] = rs1.getInt(3);
                reporte[2] = rs1.getString(2);

                modeloReporte.addRow(reporte);
            }

            //con.close();
        } catch (Exception e) {
            System.err.println("ERROR AL TRAER BOLETAS: " + e);
        }
    }

    public void actualizarPedidoCosto(int id_pedido) {
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sqlUpdate = "UPDATE pedido "
                + "SET gestion_costo = 1 "
                + "WHERE id_pedido = " + id_pedido;
        try {
            Statement st1 = con.createStatement();
            st1.executeUpdate(sqlUpdate);
            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void guardarReportes(int id_pedido, Usuario user) {
        String usuario = user.getId_usuario();
        manejadorFechas mf = new manejadorFechas(con);
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        manejadorBoleta mb = new manejadorBoleta(con);
        String rutaPDF = mf.obtenerDireccionCarpetasPedidos(mf.obtenerFechaPedido(id_pedido));
        File dir = new File(rutaPDF);
        //System.out.println(dir.getAbsolutePath());
        //File reporte = new File(".");
        dir.mkdirs();
        JasperReport jr = null;

        //REPORTE LISTA A PREPARAR
        String ruta = "src\\Reportes\\reportePreparar2.jasper";

        try {
            Map parametro = new HashMap();
            parametro.put("id_pedido", mb.obtenerUltPedido(con));
            jr = (JasperReport) JRLoader.loadObjectFromFile(ruta);
            JasperPrint jp = JasperFillManager.fillReport(jr, parametro, con);
            //JasperViewer jv = new JasperViewer(jp, false);
            //jv.setVisible(true);
            //jv.setTitle("LISTA A PREPARAR");

            JasperExportManager.exportReportToPdfFile(jp, rutaPDF + "\\listaPreparar_pedidoNro_" + id_pedido + ".pdf");
            //jv.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        } catch (JRException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }

        // REPORTE POR ZONAS
        ruta = "src\\Reportes\\reporteZona.jasper";

        String sqlZonas = "select distinct zona "
                + "from boleta "
                + "where id_pedido = " + mb.obtenerUltPedido(con);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sqlZonas);

            while (rs1.next()) {
                try {
                    Map parametro = new HashMap();
                    parametro.put("id_pedido", mb.obtenerUltPedido(con));
                    parametro.put("zona", rs1.getInt(1));
                    jr = (JasperReport) JRLoader.loadObjectFromFile(ruta);
                    JasperPrint jp = JasperFillManager.fillReport(jr, parametro, con);
                    //JasperViewer jv = new JasperViewer(jp, false);
                    //jv.setVisible(true);
                    //jv.setTitle("REPORTE ZONA");
                    JasperExportManager.exportReportToPdfFile(jp, rutaPDF + "\\listaZona" + rs1.getInt(1) + "_pedidoNro_" + id_pedido + ".pdf");

                } catch (JRException ex) {
                    Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }

        //LISTA DE COMPRAS
        ruta = "src\\Reportes\\reporteListaDeCompras.jrxml";

        float bultosSinHuevo = obtenerBultosSinHuevos(id_pedido, usuario);
        float bultosHuevos = obtenerBultosHuevos(id_pedido, usuario);

        float bultos = bultosSinHuevo + bultosHuevos;

        try {
            Map parametro = new HashMap();
            parametro.put("id_pedido", mb.obtenerUltPedido(con));
            parametro.put("bultos", bultos);
            //parametro.put("id_pedidoFruta", mb.obtenerUltPedido(con3));
            //parametro.put("id_pedidoVerdura", mb.obtenerUltPedido(con3));
            //parametro.put("id_pedido2", mb.obtenerUltPedido(con3));

            JasperReport reporte = JasperCompileManager.compileReport(ruta);
            JasperPrint mostrarReporte = JasperFillManager.fillReport(reporte, parametro, con);
            JasperExportManager.exportReportToPdfFile(mostrarReporte, rutaPDF + "\\listaDeCompras_pedidoNro_" + id_pedido + ".pdf");

        } catch (JRException ex) {
            Logger.getLogger(Clientes.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void guardarReportesNuevo(int id_pedido, Usuario user) {
        String usuario = user.getId_usuario();
        manejadorFechas mf = new manejadorFechas(con);
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        manejadorBoleta mb = new manejadorBoleta(con);
        //String rutaPDF = mf.obtenerDireccionCarpetasPedidos(mf.obtenerFechaPedido(id_pedido));
        String rutaPDF = new File(".").getAbsolutePath() + "\\Google Drive\\Pedidos\\Nuevo";
        File dir = new File(rutaPDF);
        //System.out.println(dir.getAbsolutePath());
        //File reporte = new File(".");
        dir.mkdirs();
        JasperReport jr = null;

        //REPORTE LISTA A PREPARAR
        String ruta = "src\\Reportes\\reportePreparar_Nuevo.jasper";

        try {
            System.out.println("Usuario que cargo el pedido: "+user.getId_usuario());
            //Map<Integer, String> parametro = new HashMap<Integer, String>();            
            Map parametro = new HashMap<Integer, String>();            
            parametro.put("id_pedido", 0);
            parametro.put("id_user", user.getId_usuario());
            jr = (JasperReport) JRLoader.loadObjectFromFile(ruta);
            JasperPrint jp = JasperFillManager.fillReport(jr, parametro, con);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);
            jv.setTitle("LISTA A PREPARAR");

            JasperExportManager.exportReportToPdfFile(jp, rutaPDF + "\\listaPreparar_Nuevo.pdf");
            //jv.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        } catch (JRException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }

        // REPORTE POR ZONAS
        ruta = "src\\Reportes\\reporteZona_Nuevo.jasper";

        String sqlZonas = "select distinct zona "
                + "from boleta "
                + "where id_pedido = 0 and id_usuario = '"+user.getId_usuario()+"'";

        System.out.println(sqlZonas);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sqlZonas);

            while (rs1.next()) {
                try {
                    Map parametro = new HashMap();
                    parametro.put("id_pedido", 0);
                    parametro.put("zona", rs1.getInt(1));
                    parametro.put("id_user", user.getId_usuario());
                    jr = (JasperReport) JRLoader.loadObjectFromFile(ruta);
                    JasperPrint jp = JasperFillManager.fillReport(jr, parametro, con);
                    JasperViewer jv = new JasperViewer(jp, false);
                    jv.setVisible(true);
                    jv.setTitle("REPORTE ZONA");
                    JasperExportManager.exportReportToPdfFile(jp, rutaPDF + "\\listaZona" + rs1.getInt(1) + "_Nuevo.pdf");

                } catch (JRException ex) {
                    Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }

        //LISTA DE COMPRAS
        ruta = "src\\Reportes\\reporteListaDeCompras_Nuevo.jrxml";

        float bultosSinHuevo = obtenerBultosSinHuevos(id_pedido, usuario);
        float bultosHuevos = obtenerBultosHuevos(id_pedido, usuario);

        float bultos = bultosSinHuevo + bultosHuevos;

        try {
            Map parametro = new HashMap<Integer, String>();
            //Map parametro = new HashMap();
            parametro.put("id_pedido", 0);
            parametro.put("bultos", bultos);
            parametro.put("id_user", user.getId_usuario());
            //parametro.put("id_pedidoFruta", mb.obtenerUltPedido(con3));
            //parametro.put("id_pedidoVerdura", mb.obtenerUltPedido(con3));
            //parametro.put("id_pedido2", mb.obtenerUltPedido(con3));

            JasperReport reporte = JasperCompileManager.compileReport(ruta);
            JasperPrint mostrarReporte = JasperFillManager.fillReport(reporte, parametro, con);
            JasperViewer jv = new JasperViewer(mostrarReporte, false);
            jv.setVisible(true);
            jv.setTitle("LISTA DE COMPRAS");
            JasperExportManager.exportReportToPdfFile(mostrarReporte, rutaPDF + "\\listaDeCompras_pedidoNro_" + id_pedido + ".pdf");
            
            

        } catch (JRException ex) {
            Logger.getLogger(Clientes.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void actualizarCantBoletasPedido(int id_pedido, int cantidad) {
        String sql = "UPDATE pedido "
                + "SET cant_boletas = " + cantidad + " "
                + "WHERE id_pedido = " + id_pedido;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            st1.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cargarNuevoCosto(Mercaderia m, Tipo_Cantidad tc, float cantidad) {
        Object[] costo = new Object[7];

        costo[0] = m.getId_mercaderia();
        costo[1] = m.getNombre();
        costo[2] = tc.getId_tipocant();
        costo[3] = cantidad;
        costo[4] = tc.getTipo_cantidad();
        costo[5] = "";
        costo[6] = "";

        modeloMercaderia.addRow(costo);
    }

    public float obtenerBultosSinHuevos(int id_pedido, String id_user) {
        float bultos = 0;

        //conexionDB con = new conexionDB();
        //Connection c = con.conectar();

        String sql = "select sum(d.cantidad) as bultos "
                + "from detalle as d "
                + "join boleta as b "
                + "on b.id_boleta = d.id_boleta "
                + "where b.id_pedido = " + id_pedido + " and (d.id_tipocant = 1 or d.id_tipocant = 2) and d.id_mercaderia <> 'HC' and d.id_mercaderia <> 'HB' and id_usuario = '"+id_user+"'"
                + "group by b.id_pedido";

        //System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                bultos = rs1.getFloat(1);
                //System.out.println("bultos sin huevo: " + bultos);
            }

            //con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }

        return bultos;
    }

    public float obtenerBultosHuevos(int id_pedido, String id_user) {
        float cajones = 0;
        int cajonesBandejas = 0;

        //conexionDB con = new conexionDB();
        //Connection c = con.conectar();

        String sql = "select sum(d.cantidad) as bultos "
                + "from detalle as d "
                + "join boleta as b "
                + "on b.id_boleta = d.id_boleta "
                + "where b.id_pedido = " + id_pedido + " and (d.id_tipocant = 1 or d.id_tipocant = 2) and (d.id_mercaderia = 'HC' or d.id_mercaderia = 'HB') and id_usuario = '"+id_user+"'"
                + "group by b.id_pedido";

        //System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                cajones = rs1.getFloat(1);
                //System.out.println("cajones sin equivalencia: " + cajones);
            }

            //c.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }

        String sql2 = "select (sum(d.cantidad)/12) as bultos "
                + "from detalle as d "
                + "join boleta as b "
                + "on b.id_boleta = d.id_boleta "
                + "where b.id_pedido = " + id_pedido + " and (d.id_tipocant = 5) and (d.id_mercaderia = 'HC' or d.id_mercaderia = 'HB') "
                + "group by b.id_pedido";

        //System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql2);

            while (rs1.next()) {
                cajonesBandejas = rs1.getInt(1);
                //System.out.println("cantidad equivalente de cajones: " + cajonesBandejas);
            }

            //con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }

        float bultos = cajones + cajonesBandejas;

        return bultos;
    }

    public boolean existePedido(int id_pedido) {
        boolean b = false;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "select id_pedido from pedido where id_pedido = " + id_pedido;
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            if (rs1.next()) {
                b = true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }

        return b;
    }

    public float totalPedido(int id_pedido) {
        float total = 0;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "select sum(d.precio) "
                + "from detalle as d "
                + "join boleta as b "
                + "on b.id_boleta = d.id_boleta "
                + "join pedido as p "
                + "on p.id_pedido = b.id_pedido "
                + "where p.id_pedido = " + id_pedido;

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            if (rs1.next()) {
                total = rs1.getFloat(1);
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }

        return total;
    }

    public Pedido obtenerPedidoSinCargar() {
        Pedido pe = new Pedido();
        manejadorFechas mf = new manejadorFechas(con);
        String sqlId = "SELECT DATE_FORMAT(now(), '%d-%m-%Y'), count(id_boleta) from boleta where id_pedido = 0";
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sqlId);

            while (rs1.next()) {
                pe.setId_pedido(0);
                pe.setFecha_pedido(rs1.getString(1));
                pe.setCant_boletas(rs1.getInt(2));
                pe.setGestion_costo(0);
                //System.out.println("id pedido: "+id_pedido);

                //con.close();
            }
        } catch (Exception e) {
            System.out.println("Error al traer ultimo pedido: " + e);
        }

        return pe;
    }

    public void costoMercaderiaUltPedidoSinCostos(JTable tablaCostoMercaderia, int id_pedido) {
        tablaCostoMercaderia.setModel(modeloMercaderia);
        modeloMercaderia.addColumn("ID_MERCADERIA");
        modeloMercaderia.addColumn("MERCADERIA");
        modeloMercaderia.addColumn("ID_TIPO");
        modeloMercaderia.addColumn("CANTIDAD");
        modeloMercaderia.addColumn("TIPO_CANTIDAD");
        modeloMercaderia.addColumn("PRECIO UNITARIO");
        modeloMercaderia.addColumn("TOTAL");

        String sql = "";

        /*if (mc.verificarGestionCosto()) {
            System.out.println("TIENE COSTOS CARGADOS");
        } else {
            System.out.println("NO TIENE COSTOS CARGADOS");
        }*/
        Object[] costo = new Object[7];

        //CODIGO PARA OCULTAR LA COLUMNA ID_MERCADERIA
        tablaCostoMercaderia.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaCostoMercaderia.getColumnModel().getColumn(0).setMinWidth(0);
        tablaCostoMercaderia.getColumnModel().getColumn(0).setPreferredWidth(0);

        //CODIGO PARA OCULTAR LA COLUMNA ID_TIPO
        tablaCostoMercaderia.getColumnModel().getColumn(2).setMaxWidth(0);
        tablaCostoMercaderia.getColumnModel().getColumn(2).setMinWidth(0);
        tablaCostoMercaderia.getColumnModel().getColumn(2).setPreferredWidth(0);

        /*
        sql = "SELECT d.id_mercaderia, m.nombre, tc.tipo_cantidad, SUM( d.cantidad ) as Cantidad, DATE_FORMAT(now(), '%d-%m-%Y') as Fecha, tc.id_tipocant, gc.costo_unitario "
                + "FROM detalle AS d "
                + "JOIN boleta as b ON d.id_boleta = b.id_boleta "
                + "JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia "
                + "JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant "
                + "JOIN gestion_costo2 as gc ON m.id_mercaderia = gc.detalle "
                + "WHERE b.id_pedido = 0 "
                + " GROUP BY d.id_mercaderia, d.id_tipocant "
                + "order by m.orden asc, m.nombre asc, d.id_tipocant asc";*/
        sql = "SELECT d.id_mercaderia, m.nombre, tc.tipo_cantidad, SUM( d.cantidad ) as Cantidad, DATE_FORMAT(now(), '%d-%m-%Y') as Fecha, tc.id_tipocant "
                + "FROM detalle AS d "
                + "JOIN boleta as b ON d.id_boleta = b.id_boleta "
                + "JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia "
                + "JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant "
                + "WHERE b.id_pedido = 0 "
                + " GROUP BY d.id_mercaderia, d.id_tipocant "
                + "order by m.orden asc, m.nombre asc, d.id_tipocant asc";

        //System.out.println(sql);
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                costo[0] = rs1.getString(1);
                costo[1] = rs1.getString(2);
                costo[2] = rs1.getInt(6);
                costo[3] = Float.toString(rs1.getFloat(4));
                costo[4] = rs1.getString(3);
                if (id_pedido != 1) {
                    costo[5] = "";
                } else {
                    costo[5] = rs1.getFloat(7);
                }

                costo[6] = "";

                modeloMercaderia.addRow(costo);
            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void modeloCostoMercaderiaUltPedido(JTable tablaCostoMercaderia, int id_pedido) {
        tablaCostoMercaderia.setModel(modeloMercaderia);
        modeloMercaderia.addColumn("ID_DETALLE");
        modeloMercaderia.addColumn("ID_MERCADERIA");
        modeloMercaderia.addColumn("MERCADERIA");
        modeloMercaderia.addColumn("ID_TIPOCANT");
        modeloMercaderia.addColumn("UNIDAD");
        modeloMercaderia.addColumn("CANTIDAD");
        modeloMercaderia.addColumn("COSTO UNITARIO");
        modeloMercaderia.addColumn("TOTAL");


        /*if (mc.verificarGestionCosto()) {
            System.out.println("TIENE COSTOS CARGADOS");
        } else {
            System.out.println("NO TIENE COSTOS CARGADOS");
        }*/
        Object[] costo = new Object[8];

        //CODIGO PARA OCULTAR LA COLUMNA ID_MERCADERIA
        tablaCostoMercaderia.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaCostoMercaderia.getColumnModel().getColumn(0).setMinWidth(0);
        tablaCostoMercaderia.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablaCostoMercaderia.getColumnModel().getColumn(1).setMaxWidth(0);
        tablaCostoMercaderia.getColumnModel().getColumn(1).setMinWidth(0);
        tablaCostoMercaderia.getColumnModel().getColumn(1).setPreferredWidth(0);

        //CODIGO PARA OCULTAR LA COLUMNA ID_TIPO
        tablaCostoMercaderia.getColumnModel().getColumn(3).setMaxWidth(0);
        tablaCostoMercaderia.getColumnModel().getColumn(3).setMinWidth(0);
        tablaCostoMercaderia.getColumnModel().getColumn(3).setPreferredWidth(0);

        /*String sql = "SELECT d.id_mercaderia, m.nombre, tc.tipo_cantidad, SUM( d.cantidad ) as Cantidad, DATE_FORMAT(now(), '%d-%m-%Y') as Fecha, tc.id_tipocant, gc.costo_unitario "
                + "FROM detalle AS d "
                + "JOIN boleta as b ON d.id_boleta = b.id_boleta "
                + "JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia "
                + "JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant "
                + "JOIN gestion_costo2 as gc ON m.id_mercaderia = gc.detalle "
                + "WHERE b.id_pedido = 0 "
                + " GROUP BY d.id_mercaderia, d.id_tipocant "
                + "order by m.orden asc, m.nombre asc, d.id_tipocant asc";*/
 /*String sql = "SELECT d.id_mercaderia, m.nombre, tc.tipo_cantidad, SUM( d.cantidad ) as Cantidad, DATE_FORMAT(now(), '%d-%m-%Y') as Fecha, tc.id_tipocant, gc.costo_unitario "
                + "FROM detalle AS d "
                + "JOIN boleta as b ON d.id_boleta = b.id_boleta "
                + "JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia "
                + "JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant "
                + "JOIN gestion_costo2 as gc ON m.id_mercaderia = gc.detalle "
                + "WHERE b.id_pedido = 0 GROUP BY d.id_mercaderia, d.id_tipocant "
                + "UNION "
                + "(SELECT d.id_mercaderia, m.nombre, tc.tipo_cantidad, SUM( d.cantidad ) as Cantidad, DATE_FORMAT(now(), '%d-%m-%Y') as Fecha, tc.id_tipocant, 0 as costo_unitario "
                + "FROM detalle AS d "
                + "JOIN boleta as b ON d.id_boleta = b.id_boleta "
                + "JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia "
                + "JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant "
                + "WHERE b.id_pedido = 0 and m.id_mercaderia "
                + "not in "
                + "(SELECT d.id_mercaderia "
                + "FROM detalle AS d "
                + "JOIN boleta as b ON d.id_boleta = b.id_boleta "
                + "JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia "
                + "JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant "
                + "JOIN gestion_costo2 as gc ON m.id_mercaderia = gc.detalle "
                + "WHERE b.id_pedido = 0 GROUP BY d.id_mercaderia, d.id_tipocant) order by m.orden asc, m.nombre asc, d.id_tipocant asc)";*/
        String sql = "SELECT d.id_detalle, d.id_mercaderia, m.nombre, tc.id_tipocant, tc.tipo_cantidad, SUM( d.cantidad ) as Cantidad, d.costo_unitario "
                + "FROM detalle AS d "
                + "JOIN boleta as b ON d.id_boleta = b.id_boleta "
                + "JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia "
                + "JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant "
                + "WHERE b.id_pedido = " + id_pedido + " "
                + " GROUP BY d.id_mercaderia, d.id_tipocant "
                + "order by m.orden asc, m.nombre asc, d.id_tipocant asc";

        System.out.println(sql);

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {

                if (rs1.getString(1) != null) {
                    //System.out.println("es nulo");
                    costo[0] = rs1.getInt(1);
                    costo[1] = rs1.getString(2);
                    costo[2] = rs1.getString(3);
                    costo[3] = rs1.getInt(4);
                    costo[4] = rs1.getString(5);
                    costo[5] = rs1.getFloat(6);
                    costo[6] = rs1.getFloat(7);
                    costo[7] = rs1.getFloat(6) * rs1.getFloat(7);

                    modeloMercaderia.addRow(costo);
                }

            }

            //con.close();            
        } catch (SQLException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void cargarCostosPedido(JTable tablaCostos, int id_pedido) {
        String sql = "SELECT d.id_mercaderia, m.nombre, tc.tipo_cantidad, SUM( d.cantidad ) as Cantidad, DATE_FORMAT(now(), '%d-%m-%Y') as Fecha, tc.id_tipocant, gc.costo_unitario "
                + "FROM detalle AS d "
                + "JOIN boleta as b ON d.id_boleta = b.id_boleta "
                + "JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia "
                + "JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant "
                + "JOIN gestion_costo2 as gc ON m.id_mercaderia = gc.detalle "
                + "WHERE b.id_pedido = " + id_pedido + " "
                + "GROUP BY d.id_mercaderia, d.id_tipocant";

        System.out.println(sql);

        Statement st;
        try {
            st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            System.out.println("cantidad de filas de costos :" + tablaCostos.getRowCount());

            while (rs.next()) {
                for (int i = 0; i < tablaCostos.getRowCount(); i++) {
                    //System.out.println("EL ARTICULO : " + rs.getString(1) + " EN CANTIDAD: " + rs.getInt(6) + " POSEE COSTOS CARGADOS");
                    if ((rs.getString(1).equals(tablaCostos.getValueAt(i, 0).toString())) && (rs.getInt(6) == Integer.parseInt(tablaCostos.getValueAt(i, 2).toString()))) {
                        //System.out.println("EL ARTICULO : " + rs.getString(2) + " EN CANTIDAD: " + rs.getString(3) + " POSEE COSTOS CARGADOS");
                        float costoUni = rs.getFloat(7);
                        float costoTot = costoUni * rs.getFloat(4);

                        tablaCostos.setValueAt(costoUni, i, 5);
                        tablaCostos.setValueAt(costoTot, i, 6);
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorPedido.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
