/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.conexionDB;
import Modelo.Mercaderia;
import Vista.DetallesCliente;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Administrador
 */
public class manejadorMercaderia {
    
    Connection con;
    
    public manejadorMercaderia (Connection con){
        this.con = con;
    }

    public class MiModelo extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }
    MiModelo modeloMercaderia = new MiModelo();

    public Mercaderia buscarMercaderia(String codigo) {

        Mercaderia m = new Mercaderia();
        m.setId_mercaderia(codigo);
        String sql;
        //conexionDB c = new conexionDB();
        //Connection ct = c.conectar();

        sql = "SELECT nombre from mercaderia where id_mercaderia = '" + codigo + "'";

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                m.setNombre(rs1.getString(1));
            }

        } catch (Exception e) {
            System.out.println("error: " + e);
        }

        return m;
    }

    public int obtenerOrden(String nombre) {
        int id = 0;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sqlId = "SELECT orden "
                + "from mercaderia "
                + "where nombre = '" + nombre + "'";

        //System.out.println(sqlId);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sqlId);

            while (rs1.next()) {
                id = rs1.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        return id;
    }

    public int insertarMercaderia(Mercaderia m) {
        int n = 0;
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sql = "INSERT INTO mercaderia (id_mercaderia, nombre, id_tipo, orden, inactivo, id_tipoCant) VALUES (?,?,?,?,?,?)";

        String sqlOrden = "UPDATE mercaderia "
                + "SET orden = orden + 1 "
                + "WHERE id_tipo = " + m.getId_tipo();

        //System.out.println("SQL INSERTAR: "+sqlOrden);
        //System.out.println("INSERT INTO mercaderia (id_mercaderia, nombre, id_tipo, orden, inactivo) "
        //      + "VALUES ('" + m.getId_mercaderia() + "','" + m.getNombre() + "'," + m.getId_tipo() + "," + m.getPesoLogistica() + ", 0, " + m.getId_tipoCant() + ")");
        try {

            Statement st1 = con.createStatement();
            st1.executeUpdate(sqlOrden);

            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, m.getId_mercaderia());
            ps.setString(2, m.getNombre());
            ps.setInt(3, m.getId_tipo());
            ps.setInt(4, 0);
            ps.setInt(5, 0);
            ps.setInt(6, m.getId_tipoCant());

            //System.out.println(sql);
            n = ps.executeUpdate();
            //con.close();

        } catch (SQLException ex) {
            Logger.getLogger(DetallesCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;
    }

    public void actualizarOrden(int ordenInicio, int tipo) {

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sqlUpdateOrden = "UPDATE mercaderia "
                + "set orden = orden+1 "
                + "where orden >= " + ordenInicio + " and id_tipo = " + tipo;

        //System.out.println(sqlUpdateOrden);
        try {
            Statement st1 = con.createStatement();
            st1.executeUpdate(sqlUpdateOrden);
            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void actualizarOrdenInhabilitado(int ordenInicio, int tipo) {

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sqlUpdateOrden = "UPDATE mercaderia "
                + "set orden = orden-1 "
                + "where orden > " + ordenInicio + " and orden < 900 and id_tipo = " + tipo;

        //System.out.println(sqlUpdateOrden);
        try {
            Statement st1 = con.createStatement();
            st1.executeUpdate(sqlUpdateOrden);
            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void actualizarOrdenHabilitado(int ordenInicio, int tipo, String id_mercaderia) {

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sqlUpdateMercaderiaHabilitada = "UPDATE mercaderia "
                + "SET orden = " + ordenInicio + " "
                + "WHERE id_mercaderia = '" + id_mercaderia + "'";

        //System.out.println(sqlUpdateMercaderiaHabilitada);
        try {
            Statement st1 = con.createStatement();
            st1.executeUpdate(sqlUpdateMercaderiaHabilitada);
        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        String sqlUpdateOrden = "UPDATE mercaderia "
                + "set orden = orden+1 "
                + "where orden >= " + ordenInicio + " and id_tipo = " + tipo + " and id_mercaderia <> '" + id_mercaderia + "'";

        //System.out.println(sqlUpdateOrden);
        try {
            Statement st1 = con.createStatement();
            st1.executeUpdate(sqlUpdateOrden);
            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Mercaderia obtenerMercaderia(String id_mercaderia) {
        Mercaderia m = new Mercaderia();
        String sql = "SELECT id_mercaderia, nombre, id_tipo, orden, precio, id_tipoCant, sumaKg, sumaUnidad "
                + "from mercaderia "
                + "where id_mercaderia = '" + id_mercaderia + "'";

        //System.out.println(sql);
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                m.setId_mercaderia(id_mercaderia);
                m.setNombre(rs1.getString(2));
                m.setPesoLogistica(rs1.getInt(4));
                m.setId_tipo(rs1.getInt(3));
                m.setId_tipoCant(rs1.getInt(6));
                m.setSumaUnidad(rs1.getInt(8));
                m.setSumaKg(rs1.getInt(7));
            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        return m;
    }

    public void actualizarOrdenModificado(int ordenNuevo, int ordenAnterior, int id_tipo, int actualizacion) {
        String sql = "";

        if (actualizacion == 1) {
            sql = "UPDATE mercaderia "
                    + "SET orden = orden+1 "
                    + "WHERE orden >= " + ordenNuevo + " and orden < " + ordenAnterior + " and id_tipo = " + id_tipo;
        } else {
            sql = "UPDATE mercaderia "
                    + "SET orden = orden-1 "
                    + "WHERE orden > " + ordenAnterior + " and orden <= " + ordenNuevo + " and id_tipo = " + id_tipo;
        }

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        //System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            st1.executeUpdate(sql);
            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public int actualizarMercaderia(Mercaderia m, String id) {
        int n = 0;
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        /*

        String sql = "UPDATE mercaderia "
                + "SET id_mercaderia = ?, nombre = ?, id_tipoCant = ? "
                + "WHERE id_mercaderia = '" + id + "'";*/
        String sql = "UPDATE mercaderia "
                + "SET id_mercaderia = ?, nombre = ?, id_tipoCant = ?, sumaKg = ?, sumaUnidad = ? "
                + "WHERE id_mercaderia = '" + id + "'";

        //System.out.println("UPDATE mercaderia "
        //       + "SET id_mercaderia = '" + m.getId_mercaderia() + "', nombre = '" + m.getNombre() + "', id_tipoCant = " + m.getId_tipoCant()
        //     + " WHERE id_mercaderia = '" + id + "'");
        try {
            PreparedStatement st1 = con.prepareStatement(sql);
            st1.setString(1, m.getId_mercaderia());
            st1.setString(2, m.getNombre());
            st1.setInt(3, m.getId_tipoCant());
            st1.setInt(4, m.getSumaKg());
            st1.setInt(5, m.getSumaUnidad());
            n = st1.executeUpdate();

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;
    }

    public int actualizarMercaderiaHabilitada(Mercaderia m, String id) {
        int n = 0;
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "UPDATE mercaderia "
                + "SET id_mercaderia = ?, nombre = ?, id_tipoCant = ? "
                + "WHERE id_mercaderia = '" + id + "'";

        try {
            PreparedStatement st1 = con.prepareStatement(sql);
            st1.setString(1, m.getId_mercaderia());
            st1.setString(2, m.getNombre());
            st1.setInt(3, m.getId_tipoCant());
            n = st1.executeUpdate();

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        return n;
    }

    public int moverMercaderia(Mercaderia m, int tipoAnterior) {
        int ordenAnterior = obtenerOrden(m.getNombre());
        int n = 0;
        String sqlDelete = "DELETE FROM mercaderia "
                + "where id_mercaderia = '" + m.getId_mercaderia() + "'";

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        //System.out.println(sqlDelete);
        try {
            Statement st1 = con.createStatement();
            st1.executeUpdate(sqlDelete);

        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        String sqlUpdate = "UPDATE mercaderia "
                + "set orden = orden - 1 "
                + "where orden > " + ordenAnterior + " and id_tipo = " + tipoAnterior;

        //System.out.println(sqlUpdate);
        try {
            Statement st2 = con.createStatement();
            st2.executeUpdate(sqlUpdate);

          //  con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        //System.out.println("nuevo orden: " + m.getPesoLogistica() + " nuevo tipo: " + m.getId_tipo());
        actualizarOrden(m.getPesoLogistica(), m.getId_tipo());
        n = insertarMercaderia(m);

        return n;
    }

    public boolean existeID(String id) {
        boolean b = false;
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sql = "select * from mercaderia where id_mercaderia = '" + id + "'";
        //System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                b = true;
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }
        //System.out.println("BANDERA: " + b);
        return b;
    }

    public void traerTodaMercaderia(JComboBox comboMercaderia) {
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "SELECT nombre from mercaderia";
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                comboMercaderia.addItem(rs1.getString(1));
            }
            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        comboMercaderia.setSelectedIndex(-1);
    }

    public void traerTipoCantidad(JComboBox comboTipos) {
        comboTipos.removeAllItems();
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "SELECT tipo_cantidad from tipocantidad";
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                comboTipos.addItem(rs1.getString(1));
            }
            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        comboTipos.setSelectedIndex(-1);
    }

    public void seleccionarVerdurasEspeciales(String comparar, JComboBox tipo) {

        tipo.setSelectedIndex(getIdTipoCant(comparar) - 1);

    }

    public int getIdTipoCant(String nombre) {
        int id = 0;
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sql = "SELECT tc.id_tipocant "
                + "FROM tipocantidad as tc "
                + "JOIN mercaderia as m "
                + "ON m.id_tipocant = tc.id_tipocant "
                + "WHERE m.nombre = '" + nombre + "'";

        //System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                id = rs1.getInt(1);
            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        return id;
    }

    public void getMercaderia(String busqueda, JTable mercaderia) {
        String sql = "SELECT id_mercaderia, nombre, id_tipo from mercaderia where inactivo = 0 and nombre like '%" + busqueda + "%'";

        DefaultTableModel modelo = (DefaultTableModel) mercaderia.getModel();

        //System.out.println(sql);
        Object[] mercaderias = new Object[3];
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                //System.out.println("RESULTADOS: "+rs1.getString(1));
                mercaderias[0] = rs1.getString(1);
                mercaderias[1] = rs1.getString(2);
                mercaderias[2] = rs1.getInt(3);

                modelo.addRow(mercaderias);
            }

            //con.close();

        } catch (Exception e) {
            System.err.println("Error busqueda: " + e);
        }

    }

    public void traerMercaderiasInhabilitadas(JTable mercaderiasInhabilitadas) {
        //DefaultTableModel modeloMercaderias = (DefaultTableModel) mercaderiasInhabilitadas.getModel();
        mercaderiasInhabilitadas.setModel(modeloMercaderia);
        modeloMercaderia.addColumn("ID_MERCADERIA");
        modeloMercaderia.addColumn("ID_TIPO");
        modeloMercaderia.addColumn("MERCADERIA");

        //CODIGO PARA OCULTAR LA COLUMNA ID MERCADERIA
        mercaderiasInhabilitadas.getColumnModel().getColumn(0).setMaxWidth(0);
        mercaderiasInhabilitadas.getColumnModel().getColumn(0).setMinWidth(0);
        mercaderiasInhabilitadas.getColumnModel().getColumn(0).setPreferredWidth(0);

        //CODIGO PARA OCULTAR LA COLUMNA ID TIPO
        mercaderiasInhabilitadas.getColumnModel().getColumn(1).setMaxWidth(0);
        mercaderiasInhabilitadas.getColumnModel().getColumn(1).setMinWidth(0);
        mercaderiasInhabilitadas.getColumnModel().getColumn(1).setPreferredWidth(0);

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        Object[] mercaderia = new Object[3];

        String sql = "SELECT id_mercaderia, id_tipo, nombre "
                + "FROM mercaderia "
                + "WHERE inactivo = 1 "
                + "ORDER BY nombre asc";
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                mercaderia[0] = rs1.getString(1);
                mercaderia[1] = rs1.getInt(2);
                mercaderia[2] = rs1.getString(3);

                modeloMercaderia.addRow(mercaderia);
            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void inhabilitarMercaderia(String id_mercaderia) {
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "UPDATE mercaderia "
                + "SET inactivo = 1, orden = 1000 "
                + "WHERE id_mercaderia = '" + id_mercaderia + "'";

        //System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            st1.executeUpdate(sql);
            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void actualizarMercaderiasHabilitadas(JTable mercaderias) {
        DefaultTableModel modelo = (DefaultTableModel) mercaderias.getModel();

        int totalFilas = mercaderias.getRowCount();
        //System.out.println("cantidad de filas: " + totalFilas);

        for (int i = 0; totalFilas > i; i++) {
            //System.out.println("cantidad de filas: "+modelo.getRowCount());
            modelo.removeRow(0);
        }

        Object[] mercaderia = new Object[3];

        String sql = "SELECT id_mercaderia, nombre, id_tipo from mercaderia where inactivo = 0";
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                mercaderia[0] = rs1.getString(1);
                mercaderia[1] = rs1.getString(2);
                mercaderia[2] = rs1.getInt(3);

                modelo.addRow(mercaderia);
            }

            //con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO MERCADERIAS: " + e);
        }
    }

    public int cantidadMercaderiaEnPedido(int id_pedido, String id_mercaderia) {
        int cantidad = 0;
        id_pedido = 0;

        //conexionDB con = new conexionDB();
        //Connection c = con.conectar();

        /*
        String sql = "select count(d.id_mercaderia) "
                + "from detalle as d "
                + "join boleta as b "
                + "on b.id_boleta = d.id_boleta "
                + "join pedido as p "
                + "on p.id_pedido = b.id_pedido "
                + "where p.id_pedido = " + id_pedido + " and d.id_mercaderia = '" + id_mercaderia + "'";*/
        String sql = "select count(d.id_mercaderia) "
                + "from detalle as d "
                + "join boleta as b "
                + "on b.id_boleta = d.id_boleta "
                + "where b.id_pedido = " + id_pedido + " and d.id_mercaderia = '" + id_mercaderia + "'";

        //System.out.println("SQL INHABILITAR: " + sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                cantidad = rs1.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        return cantidad;
    }

    public void habilitarMercaderia(String id_mercaderia) {
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "UPDATE mercaderia "
                + "SET inactivo = 0 "
                + "WHERE id_mercaderia = '" + id_mercaderia + "'";

        //System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            st1.executeUpdate(sql);
            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public boolean esUltimo(Mercaderia m) {
        boolean b = false;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sql = "select count(id_mercaderia) "
                + "from mercaderia "
                + "where id_tipo = " + m.getId_tipo() + " and inactivo = 0";

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                if (rs1.getInt(1) == m.getPesoLogistica()) {
                    b = true;
                }
                //System.out.println("LA CANTIDAD TOTAL DE ESE TIPO ES DE: " + rs1.getInt(1));
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        return b;
    }

    public boolean inhabilitado(String mercaderia) {
        boolean b = true;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String in = "SELECT inactivo from mercaderia "
                + "where nombre = '" + mercaderia + "'";

        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(in);

            while (rs.next()) {
                if (rs.getInt(1) == 0) {
                    b = false;
                } else {
                    b = true;
                }
            }

            //con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        return b;

    }

    public Mercaderia getMercaderia(String id_mercaderia) {

        Mercaderia m = new Mercaderia();

        String sql = "SELECT id_mercaderia, nombre, id_tipo, id_tipocant, orden, inactivo, sumaKg, sumaUnidad "
                + "FROM mercaderia "
                + "where id_mercaderia = '" + id_mercaderia + "'";

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                m.setId_mercaderia(rs.getString(1));
                m.setNombre(rs.getString(2));
                m.setId_tipo(rs.getInt(3));
                m.setId_tipoCant(rs.getInt(4));
                m.setPesoLogistica(rs.getInt(5));
                m.setInactivo(rs.getInt(6));
                m.setSumaKg(rs.getInt(7));
                m.setSumaUnidad(rs.getInt(8));
            }

           // con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        return m;
    }

    public Mercaderia getMercaderiaPorNombre(String nombre) {

        Mercaderia m = new Mercaderia();

        String sql = "SELECT id_mercaderia, nombre, id_tipo, id_tipocant, orden, inactivo, sumaKg, sumaUnidad "
                + "FROM mercaderia "
                + "where nombre = '" + nombre + "'";

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                m.setId_mercaderia(rs.getString(1));
                m.setNombre(rs.getString(2));
                m.setId_tipo(rs.getInt(3));
                m.setId_tipoCant(rs.getInt(4));
                m.setPesoLogistica(rs.getInt(5));
                m.setInactivo(rs.getInt(6));
                m.setSumaKg(rs.getInt(7));
                m.setSumaUnidad(rs.getInt(8));
            }

            //con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        return m;
    }

    public int totalMercaderiaHabilitada(int id_tipo) {
        int total = 0;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "SELECT COUNT(*) "
                + "FROM mercaderia "
                + "WHERE id_tipo = " + id_tipo + " "
                + "AND inactivo = 0";

        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                total = rs.getInt(1);
            }

            //con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        return total;
    }

    public void actualizarOrdenesDefinitivo(int tipoActualizacion, Mercaderia mOrden, Mercaderia m) {
        int nuevoOrden = 0;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        switch (tipoActualizacion) {
            case 1:
                String sqlCase1 = "UPDATE mercaderia "
                        + "SET orden = orden + 1 "
                        + "WHERE orden >= " + mOrden.getPesoLogistica() + " AND id_tipo = " + mOrden.getId_tipo() + " AND inactivo <> 1";

                String sqlOrden = "UPDATE mercaderia "
                        + "SET orden = " + mOrden.getPesoLogistica() + ", inactivo = 0 "
                        + "WHERE id_mercaderia = '" + m.getId_mercaderia() + "'";

                //System.out.println(sqlCase1);
                //System.out.println(sqlOrden);
                try {
                    Statement st = con.createStatement();
                    st.executeUpdate(sqlCase1);
                    st.executeUpdate(sqlOrden);

                    //con.close();

                } catch (SQLException ex) {
                    Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;

            case 2:
                int totalMercaderia = this.totalMercaderiaHabilitada(m.getId_tipo());

                String sqlCase2 = "UPDATE mercaderia "
                        + "SET orden = " + totalMercaderia + ", inactivo = 0 "
                        + "WHERE id_mercaderia = '" + m.getId_mercaderia() + "'";

                //System.out.println(sqlCase2);
                try {
                    Statement st = con.createStatement();
                    st.executeUpdate(sqlCase2);

                    //con.close();

                } catch (SQLException ex) {
                    Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;

            case 3:
                int totalMercaderiaCase3 = this.totalMercaderiaHabilitada(mOrden.getId_tipo());

                String sqlOrdenCase3 = "UPDATE mercaderia "
                        + "SET orden = orden - 1 "
                        + "WHERE orden > " + m.getPesoLogistica() + " and id_tipo = " + m.getId_tipo() + " AND inactivo <> 1";

                String sqlCase3 = "UPDATE mercaderia "
                        + "SET orden = " + totalMercaderiaCase3 + " "
                        + "WHERE id_mercaderia = '" + m.getId_mercaderia() + "'";

                //System.out.println(sqlOrdenCase3);
                //System.out.println(sqlCase3);
                try {
                    Statement st = con.createStatement();
                    st.executeUpdate(sqlCase3);
                    st.executeUpdate(sqlOrdenCase3);

                    //con.close();

                } catch (SQLException ex) {
                    Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;

            case 4:
                nuevoOrden = mOrden.getPesoLogistica() - 1;

                String sqlOrdenCase4 = "UPDATE mercaderia "
                        + "SET orden = orden - 1 "
                        + "WHERE orden > " + m.getPesoLogistica() + " and orden < " + mOrden.getPesoLogistica() + " and id_tipo = " + m.getId_tipo() + " AND inactivo <> 1";

                String sqlCase4 = "UPDATE mercaderia "
                        + "SET orden = " + nuevoOrden + " "
                        + "WHERE id_mercaderia = '" + m.getId_mercaderia() + "'";

                //System.out.println(sqlOrdenCase4);
                //System.out.println(sqlCase4);
                try {
                    Statement st = con.createStatement();
                    st.executeUpdate(sqlOrdenCase4);
                    st.executeUpdate(sqlCase4);

                    //con.close();

                } catch (SQLException ex) {
                    Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;

            case 5:
                nuevoOrden = mOrden.getPesoLogistica();

                String sqlOrdenCase5 = "UPDATE mercaderia "
                        + "SET orden = orden + 1 "
                        + "WHERE orden < " + m.getPesoLogistica() + " and orden >= " + mOrden.getPesoLogistica() + " and id_tipo = " + mOrden.getId_tipo() + " AND inactivo <> 1";

                String sqlCase5 = "UPDATE mercaderia "
                        + "SET orden = " + nuevoOrden + " "
                        + "WHERE id_mercaderia = '" + m.getId_mercaderia() + "'";

                //System.out.println(sqlOrdenCase5);
                //System.out.println(sqlCase5);
                try {
                    Statement st = con.createStatement();
                    st.executeUpdate(sqlOrdenCase5);
                    st.executeUpdate(sqlCase5);

                    //con.close();

                } catch (SQLException ex) {
                    Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;

            case 6:
                nuevoOrden = mOrden.getPesoLogistica();

                String sqlOrdenTipoAntCase6 = "UPDATE mercaderia "
                        + "SET orden = orden - 1 "
                        + "WHERE orden > " + m.getPesoLogistica() + " and id_tipo = " + m.getId_tipo() + " AND inactivo <> 1";

                String sqlOrdenTipoActCase6 = "UPDATE mercaderia "
                        + "SET orden = orden + 1 "
                        + "WHERE orden >= " + mOrden.getPesoLogistica() + " and id_tipo = " + mOrden.getId_tipo() + " AND inactivo <> 1";

                String sqlCase6 = "UPDATE mercaderia "
                        + "SET orden = " + nuevoOrden
                        + " WHERE id_mercaderia = '" + m.getId_mercaderia() + "'";

                //System.out.println(sqlOrdenTipoAntCase6);
                //System.out.println(sqlOrdenTipoActCase6);
                //System.out.println(sqlCase6);
                try {
                    Statement st = con.createStatement();
                    st.executeUpdate(sqlOrdenTipoAntCase6);
                    st.executeUpdate(sqlOrdenTipoActCase6);
                    st.executeUpdate(sqlCase6);

                    //con.close();

                } catch (SQLException ex) {
                    Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;

            case 7:
                nuevoOrden = mOrden.getPesoLogistica();

                String sqlOrdenCase7 = "UPDATE mercaderia "
                        + "SET orden = orden - 1 "
                        + "WHERE orden > " + m.getPesoLogistica() + " and id_tipo = " + m.getId_tipo() + " AND inactivo <> 1";

                String sqlCase7 = "UPDATE mercaderia "
                        + "SET orden = " + nuevoOrden
                        + " WHERE id_mercaderia = '" + m.getId_mercaderia() + "'";

                //System.out.println(sqlOrdenCase7);
                //System.out.println(sqlCase7);
                try {
                    Statement st = con.createStatement();
                    st.executeUpdate(sqlOrdenCase7);
                    st.executeUpdate(sqlCase7);

                    //con.close();

                } catch (SQLException ex) {
                    Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
                }

                break;

            case 8:
                nuevoOrden = mOrden.getPesoLogistica();

                System.out.println("va a ir al ultimo del tipo contrario y su orden es: " + nuevoOrden);

                String sqlCase8 = "UPDATE mercaderia "
                        + "SET orden = " + nuevoOrden + ", inactivo = 0"
                        + " WHERE id_mercaderia = '" + m.getId_mercaderia() + "'";

                //System.out.println(sqlCase8);
                try {
                    Statement st = con.createStatement();
                    st.executeUpdate(sqlCase8);

                    //con.close();

                } catch (SQLException ex) {
                    Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
                }
                break;

        }
    }

    public void actualizarMercaderiaDefinitiva(Mercaderia m) {

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "UPDATE mercaderia "
                + "SET nombre = '" + m.getNombre() + "', id_tipo = " + m.getId_tipo() + ", id_tipocant = " + m.getId_tipoCant() + ", sumaKg = " + m.getSumaKg() + ", sumaUnidad = " + m.getSumaUnidad()
                + " WHERE id_mercaderia = '" + m.getId_mercaderia() + "'";

        try {
            Statement st = con.createStatement();
            st.executeUpdate(sql);

            //con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }

        //System.out.println(sql);
    }

    public void actualizarOrden(JTable tablaVerduras, JTable tablaFrutas) {

        for (int i = 0; i < tablaVerduras.getRowCount(); i++) {
            int orden = Integer.parseInt(tablaVerduras.getValueAt(i, 0).toString());
            String id = tablaVerduras.getValueAt(i, 1).toString();

            String sql = "UPDATE mercaderia "
                    + "SET orden = " + orden + ", id_tipo = 1 "
                    + "WHERE id_mercaderia = '" + id + "' ";

            //System.out.println(sql);
            try {
                //conexionDB c = new conexionDB();
                //Connection con = c.conectar();
                Statement st = con.createStatement();
                st.executeUpdate(sql);

                //con.close();

            } catch (SQLException ex) {
                Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

        for (int i = 0; i < tablaFrutas.getRowCount(); i++) {
            int orden = Integer.parseInt(tablaFrutas.getValueAt(i, 0).toString());
            String id = tablaFrutas.getValueAt(i, 1).toString();

            String sql = "UPDATE mercaderia "
                    + "SET orden = " + orden + ", id_tipo = 2 "
                    + "WHERE id_mercaderia = '" + id + "' ";

            //System.out.println(sql);
            try {
                //conexionDB c = new conexionDB();
                //Connection con = c.conectar();
                Statement st = con.createStatement();
                st.executeUpdate(sql);

                //con.close();

            } catch (SQLException ex) {
                Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
            }

        }

    }

    public void habilitarMercaderia(Mercaderia m) {
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        
        String sqlOrden = "UPDATE mercaderia "
                + "SET orden = orden + 1 "
                + "WHERE id_tipo = " + m.getId_tipo();
        
        String sql = "UPDATE mercaderia "
                + "SET nombre = '" + m.getNombre() + "', orden = 0, inactivo = 0, id_tipo = " + m.getId_tipo() + ", id_tipocant = " + m.getId_tipoCant() + ", sumaKg = " + m.getSumaKg() + ", sumaUnidad = " + m.getSumaUnidad()
                + " WHERE id_mercaderia = '" + m.getId_mercaderia() + "'";

        try {
            Statement st = con.createStatement();
            st.executeUpdate(sqlOrden);
            st.executeUpdate(sql);

            //con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorMercaderia.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void traerMercaderiasArray(ArrayList<Mercaderia> arrayMercaderia){
        //String sql = "SELECT nombre from cliente where inactivo = 0";
        
        
        String sql = "SELECT id_mercaderia, nombre, id_tipocant from mercaderia ";
        
        Statement st;
        try {
            st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            while(rs.next()){
                Mercaderia mer = new Mercaderia();
                mer.setId_mercaderia(rs.getString(1));
                mer.setNombre(rs.getString(2));
                mer.setId_tipoCant(rs.getInt(3));
                
                arrayMercaderia.add(mer);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

}
