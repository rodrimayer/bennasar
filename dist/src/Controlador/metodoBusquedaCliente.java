/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.conexionDB;
import Modelo.Cliente;
import Modelo.Mercaderia;
//import Vista.CargarPedido;
import Vista.CargarPedidoDidactico;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.SwingConstants;
import Vista.CuentaCorriente;
import java.util.ArrayList;
import javax.swing.JComboBox;

/**
 *
 * @author rodrimayer
 */
public class metodoBusquedaCliente {

    public CargarPedidoDidactico c;
    public CuentaCorriente cc;
    int tipo;
    Connection con;
    //public JFrame c;

    public metodoBusquedaCliente(JFrame c, int tipo, Connection con) {
        this.tipo = tipo;
        this.con = con;
        if (tipo == 1) {
            this.c = (CargarPedidoDidactico) c;
        } else {
            this.cc = (CuentaCorriente) c;
        }

    }

    public boolean comparar(String cadena, int tipo) {
        Object[] lista = this.c.getcomboBoxNombreCliente().getSelectedObjects();//       traer todos los items mejor.
        boolean encontrado = false;

        if (tipo == 1) {
            int xx = this.c.getcomboBoxNombreCliente().getItemCount();

            for (int i = 0; i < xx; i++) {
                //  System.out.println("elemento seleccionado - "+lista[i]);
                if (cadena.equalsIgnoreCase(this.c.getcomboBoxNombreCliente().getItemAt(i).toString())) {
                    //  nn=(String)boxNombre.getItemAt(i).toString(); 
                    encontrado = true;
                    break;
                }

            }
        }

        return encontrado;

    }

    public boolean compararArray(String cadena, int tipo, ArrayList arrayClientes) {
        Object[] lista = this.c.getcomboBoxNombreCliente().getSelectedObjects();//       traer todos los items mejor.
        boolean encontrado = false;

        if (tipo == 1) {
            int xx = this.c.getcomboBoxNombreCliente().getItemCount();

            for (int i = 0; i < xx; i++) {
                //  System.out.println("elemento seleccionado - "+lista[i]);
                if (cadena.equals(this.c.getcomboBoxNombreCliente().getItemAt(i))) {
                    //  nn=(String)boxNombre.getItemAt(i).toString(); 
                    encontrado = true;
                    break;
                }

            }
        }

        return encontrado;
    }

    public DefaultComboBoxModel getLista(String cadenaEscrita, String campo) {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        try {
            String sql = "";
            //conexionDB con = new conexionDB();
            //Connection conex = con.conectar();

            sql = "select nombre from cliente WHERE " + campo + " LIKE '" + cadenaEscrita + "%' and inactivo = 0";
            Statement st1 = con.createStatement();
            ResultSet res = st1.executeQuery(sql);

            while (res.next()) {
                modelo.addElement(res.getString("nombre"));
            }
            res.close();
            //conex.close();
        } catch (SQLException ex) {

        }
        return modelo;
    }

    public DefaultComboBoxModel getListaClientesArray(String cadenaEscrita, ArrayList<Cliente> arrayClientes) {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        int tam = cadenaEscrita.length();

        for (int i = 0; i < arrayClientes.size(); i++) {
            //System.out.println("CADENA ESCRITA: "+cadenaEscrita.substring(0,tam)+" COMPARA CON: "+arrayClientes.get(i).getNombre().toString().substring(0,tam));
            if (cadenaEscrita.substring(0, tam).equalsIgnoreCase(arrayClientes.get(i).getNombre().substring(0, tam))) {
                //System.out.println("entro aqui");
                modelo.addElement(arrayClientes.get(i).toString());
            }

        }

        return modelo;
    }

    public void filtrarClientes(JComboBox comboClientes, String cadenaEscrita, ArrayList<Cliente> arrayClientes) {
        int tam = cadenaEscrita.length();
        comboClientes.removeAllItems();

        for (int i = 0; i < arrayClientes.size(); i++) {
            //System.out.println("CADENA ESCRITA: "+cadenaEscrita.substring(0,tam)+" COMPARA CON: "+arrayClientes.get(i).getNombre().toString().substring(0,tam));

            if (tam <= arrayClientes.get(i).getNombre().length()) {
                if (cadenaEscrita.substring(0, tam).equalsIgnoreCase(arrayClientes.get(i).getNombre().substring(0, tam))) {
                    //System.out.println("entro aqui");
                    //comboClientes.addItem(arrayClientes.get(i).toString());
                    comboClientes.addItem(arrayClientes.get(i));
                }
            }

        }

    }

    public void filtrarMercaderias(JComboBox comboMercaderias, String cadenaEscrita, ArrayList<Mercaderia> arrayMercaderias) {
        int tam = cadenaEscrita.length();
        //System.out.println("tamaño: "+tam);
        comboMercaderias.removeAllItems();

        for (int i = 0; i < arrayMercaderias.size(); i++) {
            if (tam <= arrayMercaderias.get(i).getNombre().length()) {
                if (cadenaEscrita.substring(0, tam).equalsIgnoreCase(arrayMercaderias.get(i).getNombre().substring(0, tam))) {
                    //System.out.println("entro aqui");
                    //comboClientes.addItem(arrayClientes.get(i).toString());
                    System.out.println("CADENA ESCRITA: "+cadenaEscrita.substring(0,tam)+" COMPARA CON: "+arrayMercaderias.get(i).getNombre().toString().substring(0,tam));
                    comboMercaderias.addItem(arrayMercaderias.get(i));
                }
            }
            //System.out.println("CADENA ESCRITA: "+cadenaEscrita.substring(0,tam)+" COMPARA CON: "+arrayClientes.get(i).getNombre().toString().substring(0,tam));

        }

    }

}
