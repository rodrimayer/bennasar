/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import com.mysql.jdbc.log.Log;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.SocketException;
import org.apache.commons.logging.LogFactory;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.commons.net.ftp.FTPReply;

/**
 *
 * @author rmmayer
 */
public class manejadorFTP {

//private final static Log logger = LogFactory.getLog(FtpUtil.class);  
    public static FTPClient getFTPClient(String ftpHost, String ftpUserName,
            String ftpPassword, int ftpPort) {
        FTPClient ftpClient = new FTPClient();
        try {
            ftpClient = new FTPClient();
            ftpClient.connect(ftpHost, ftpPort); // Conéctese al servidor FTP  
            ftpClient.login(ftpUserName, ftpPassword); // Inicie sesión en el servidor FTP  
            if (!FTPReply.isPositiveCompletion(ftpClient.getReplyCode())) {
                System.out.println("No conectado a FTP, nombre de usuario o contraseña incorrectos");
                ftpClient.disconnect();
            } else {
                System.out.println("Conexión FTP exitosa");
            }
        } catch (SocketException e) {
            e.printStackTrace();
            System.out.println("La dirección IP de FTP puede ser incorrecta, configúrela correctamente");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error de puerto FTP, configúrelo correctamente");
        }
        return ftpClient;
    }

    public static void downloadFtpFile(FTPClient ftpClient, String ftpPath, String localPath,
            String fileName) throws IOException {

        //FTPClient ftpClient = null;
        try {
            //ftpClient = getFTPClient(ftpHost, ftpUserName, ftpPassword, ftpPort);
            ftpClient.setControlEncoding("UTF-8"); // Soporte chino  
            ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
            ftpClient.enterLocalPassiveMode();
            ftpClient.changeWorkingDirectory(ftpPath);

            File localFile = new File(localPath + File.separatorChar + fileName);
            //File localFile = new File(localPath);
            OutputStream os = new FileOutputStream(localFile);
            ftpClient.retrieveFile(fileName, os);
            os.close();
            ftpClient.logout();

        } catch (FileNotFoundException e) {
            System.out.println("No encontrado" + ftpPath + "Archivo");
            e.printStackTrace();
        } catch (SocketException e) {
            System.out.println("Error al conectarse a FTP");
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error de lectura de archivo");
            e.printStackTrace();
        }

    }

    public static boolean downloadFile(FTPClient ftpClient, String remoteFileName, String localDires,
            String remoteDownLoadPath) {
        String strFilePath = localDires + remoteFileName;
        BufferedOutputStream outStream = null;
        boolean success = false;
        try {
            ftpClient.changeWorkingDirectory(remoteDownLoadPath);
            outStream = new BufferedOutputStream(new FileOutputStream(
                    strFilePath));
            System.out.println(remoteFileName + "Iniciar descarga ...");
            success = ftpClient.retrieveFile(remoteFileName, outStream);
            if (success == true) {
                System.out.println(remoteFileName + "Se descargó correctamente a" + strFilePath);
                return success;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(remoteFileName + "Error de descarga");
        } finally {
            if (null != outStream) {
                try {
                    outStream.flush();
                    outStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        if (success == false) {
            System.out.println(remoteFileName + "Falló la descarga !!!");
        }
        return success;
    }

    public static boolean downLoadDirectory(FTPClient ftpClient, String ftpPath, String localPath) throws IOException {
        //FTPClient ftpClient = null;
        ftpClient.setControlEncoding("UTF-8"); // Soporte chino  
        ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);
        ftpClient.enterLocalPassiveMode();
        ftpClient.changeWorkingDirectory(ftpPath);
        //FTPFile[] allFile = ftpClient.listFiles(ftpPath);

        try {
            String fileName;
            System.out.println("URL FTP: " + ftpPath);
            fileName = new File(ftpPath).getName();
            System.out.println("FILE NAME: " + fileName);
            localPath = localPath + "//"+fileName + "//";
            System.out.println("FILE PATH: " + localPath);
            new File(localPath).mkdirs();
            FTPFile[] allFile = ftpClient.listFiles(ftpPath);
            System.out.println("cantidad de archivos: " + allFile.length);
            for (int currentFile = 0;currentFile < allFile.length;currentFile++) {
                if (!allFile[currentFile].isDirectory()) {
                    downloadFile(ftpClient, allFile[currentFile].getName(),localPath, ftpPath);
                }
            }
            for (int currentFile = 0;currentFile < allFile.length;currentFile++) {
                if (allFile[currentFile].isDirectory()) {
                    String strremoteDirectoryPath = ftpPath + "/"+ allFile[currentFile].getName();
                    downLoadDirectory(ftpClient, localPath,strremoteDirectoryPath);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("Error al descargar la carpeta");
            return false;
        }
        return true;
    }

    public static boolean retrieveFile(FTPClient clienteFTP, String pathRemoto, String archivoLocal)
            throws IOException {
        //DBDataBackup info = new DBDataBackup();
        System.err.println("RetrieveFile");
        System.err.println("Path remoto:" + pathRemoto + "Path guardado: " + archivoLocal);
        //info.consoleArea.append("RetrieveFile\n");
        //info.consoleArea.append("Path remoto:" + pathRemoto + "Path guardado: " + archivoLocal + "\n");

        File archivoDescarga = new File(archivoLocal);
        File directorio = archivoDescarga.getParentFile();

        if (!directorio.exists()) {
            //info.consoleArea.append("Archivo: " + directorio.getName() + "\n");
            directorio.mkdir();
        }
        try (OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(archivoDescarga))) {
            clienteFTP.setFileType(FTP.BINARY_FILE_TYPE);
            return clienteFTP.retrieveFile(pathRemoto, outputStream);
        } catch (IOException ex) {
            throw ex;
        }
    }

    /**
     * Descarga un directorio de un FTP
     *
     * @param clienteFtp El cliente FTP que se va a usar
     * @param directorioRemoto El directorio que se va a descargar
     *
     *
     * @param directorioLocal Path donde se guardara la copia del directorio
     * @throws IOException Excepción de cualquier error de archivo
     */
    public static void retrieveDir(FTPClient clienteFtp, String directorioRemoto, String directorioLocal) throws IOException {
        clienteFtp.setControlEncoding("UTF-8"); // Soporte chino  
        clienteFtp.setFileType(FTPClient.BINARY_FILE_TYPE);
        clienteFtp.enterLocalPassiveMode();
        clienteFtp.changeWorkingDirectory(directorioRemoto);
        //directorioLocal += "//dist";
        //DBDataBackup info = new DBDataBackup();
        System.err.println("RetrieveDir");
        //info.consoleArea.append("RetrieveDir\n");
        System.err.println("Path remoto:" + directorioRemoto + "Path guardado: " + directorioLocal);
        //info.consoleArea.append("Path remoto:" + directorioRemoto + " Path guardado: " + directorioLocal + "\n");
        FTPFile[] ftpFiles = clienteFtp.listFiles(directorioRemoto);
        System.out.println("cant archivos: " + ftpFiles.length);
        if (ftpFiles == null || ftpFiles.length == 0) {
            return;
        }
        for (FTPFile ftpFile : ftpFiles) {
            //info.consoleArea.append("Directorio: " + ftpFile.getName() + "\n");
            String ftpFileNombre = ftpFile.getName();
            if (ftpFileNombre.equals(".") || ftpFileNombre.equals("..")) {

                continue;
            }
            String archivoLocal = directorioLocal + "/" + ftpFileNombre;
            String archivoRemoto = directorioRemoto + "/" + ftpFileNombre;
            if (ftpFile.isDirectory()) {

                File nuevoDirectorio = new File(archivoLocal);
                nuevoDirectorio.mkdirs();

                retrieveDir(clienteFtp, archivoRemoto, archivoLocal);
            } else {

                retrieveFile(clienteFtp, archivoRemoto, archivoLocal);
            }
        }
    }

}
