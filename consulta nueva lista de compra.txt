SELECT d.id_mercaderia as ID_VERDURA, m.nombre as VERDURA, 
CONCAT(IF( sum(d.cantCajon)>0 and FLOOR( SUM(d.cantBandeja) / 12 ) >0,
	CONCAT ((select case 
 when (SUM(d.cantCajon)-floor(SUM(d.cantCajon))) = 0.5 THEN (
CONCAT( cast((floor(SUM(d.cantCajon))+floor(SUM(d.cantBandeja)/12)) as char(20)) ,  ' ½ ' )
)
 else sum(d.cantCajon)+ floor(SUM(d.cantBandeja)/12)
 end), ' Cajon, '),
	IF( SUM(d.cantCajon) > 0,
		CONCAT( (select case 
 when (SUM(d.cantCajon)-floor(SUM(d.cantCajon))) = 0.5 THEN (
CONCAT( cast(floor(SUM(d.cantCajon)) as char(20)) ,  ' ½ ' )
)
 else sum(d.cantCajon)
 end),' Cajon, '),
		IF ( (FLOOR( SUM(d.cantBandeja) / 12 ) >0) and (d.id_mercaderia = 'HC' or d.id_mercaderia = 'HB'),
		CONCAT( FLOOR( SUM(d.cantBandeja) / 12 ), ' Cajon, '),
		''
		)
	)
), if(sum(d.cantBolsa)=0,'',concat((select case 
 when (SUM(d.cantBolsa)-floor(SUM(d.cantBolsa))) = 0.5 THEN (
CONCAT( cast(floor(SUM(d.cantBolsa)) as char(20)) ,  ' ½ ' )
)
 else sum(d.cantBolsa)
 end),' Bolsa, ')),if(sum(d.cantKg)=0,'',concat(sum(d.cantKg),' Kg, ')),if(sum(d.cantUnidad)=0,'',concat(sum(d.cantUnidad),' Unidad, ')),
if(sum(d.cantRistra)=0,'',concat(sum(d.cantRistra),' Ristra, ')), 

       IF ( (FLOOR( SUM(d.cantBandeja)/12 ) > 0) and (d.id_mercaderia = 'HC' or d.id_mercaderia = 'HB'),
	
           IF ( (MOD ( SUM( d.cantBandeja), 12 ) = 0) and (d.id_mercaderia = 'HC' or d.id_mercaderia = 'HB'),
		
               '',
		
               CONCAT ( MOD ( SUM(d.cantBandeja) , 12 ), ' Bandeja' )
		),
		IF( SUM( d.cantBandeja ) = 0,
	'',
	CONCAT ( SUM( d.cantBandeja ), ' Bandeja' )
	)
	
)) as Cantidad, m.orden as ordenverdura 
FROM detalle2 AS d
JOIN boleta AS b ON d.id_boleta = b.id_boleta
JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia
WHERE b.id_pedido = $P{id_pedidoVerdura} and d.id_mercaderia <> "ZPC" 
AND m.id_tipo =1
GROUP BY d.id_mercaderia
UNION(
SELECT d.id_mercaderia as ID_VERDURA, m.nombre as VERDURA,
CONCAT(if(sum(d.cantCajon) = 0, '', concat(sum(d.cantCajon), ' Cajon, ')), if(sum(d.cantBolsa) = 0, '', concat(sum(d.cantBolsa), ' Bolsa, ')), if(sum(d.cantUnidad) = 0, '', concat(sum(d.cantUnidad), ' Unidad, ')), REPLACE(GROUP_CONCAT(d.cantKg, ' Kg'),',0 Kg', '')), m.orden
FROM detalle2 AS d
JOIN boleta AS b ON d.id_boleta = b.id_boleta
JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia
WHERE b.id_pedido = $P{id_pedidoVerdura} and d.id_mercaderia = "ZPC" 
group by d.id_mercaderia)
UNION (

SELECT id_mercaderia, nombre, '', orden
FROM mercaderia AS m
WHERE id_tipo =1
AND m.inactivo = 0
AND m.id_mercaderia NOT 
IN (

SELECT d.id_mercaderia
FROM detalle2 AS d
JOIN boleta AS b ON d.id_boleta = b.id_boleta
JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia
WHERE b.id_pedido = $P{id_pedidoVerdura}
AND m.id_tipo =1
)
)
ORDER BY ordenverdura