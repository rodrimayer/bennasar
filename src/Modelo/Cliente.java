/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Administrador
 */
public class Cliente {

    private int id_cliente;
    private String nombre;
    private String direccion;
    private int id_localidad;
    private int inactivo;
    private int zona_def;
    private Contacto contacto;
    private int cta_cte_inactiva;

    public int getInactivo() {
        return inactivo;
    }

    public void setInactivo(int inactivo) {
        this.inactivo = inactivo;
    }    
    
    public Contacto getContacto() {
        return contacto;
    }

    public void setContacto(Contacto contacto) {
        this.contacto = contacto;
    }    
    

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public int getIdCliente() {
        return id_cliente;
    }

    public void setIdCliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public int getIdLocalidad() {
        return id_localidad;
    }

    public void setLocalidad(int id_localidad) {
        this.id_localidad = id_localidad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getZona_def() {
        return zona_def;
    }

    public void setZona_def(int zona_def) {
        this.zona_def = zona_def;
    }

    public int getCta_cte_inactiva() {
        return cta_cte_inactiva;
    }

    public void setCta_cte_inactiva(int cta_cte_inactiva) {
        this.cta_cte_inactiva = cta_cte_inactiva;
    }    

    @Override
    public String toString() {
        return this.nombre;
    }
    
    
    
}
