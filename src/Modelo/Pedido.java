/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

/**
 *
 * @author Administrador
 */
public class Pedido {
    
    private int id_pedido;
    private String fecha_pedido;
    private int cant_boletas;
    private int gestion_costo;

    public int getCant_boletas() {
        return cant_boletas;
    }

    public void setCant_boletas(int cant_boletas) {
        this.cant_boletas = cant_boletas;
    }

    public String getFecha_pedido() {
        return fecha_pedido;
    }

    public void setFecha_pedido(String fecha_pedido) {
        this.fecha_pedido = fecha_pedido;
    }

    public int getId_pedido() {
        return id_pedido;
    }

    public void setId_pedido(int id_pedido) {
        this.id_pedido = id_pedido;
    }

    public int getGestion_costo() {
        return gestion_costo;
    }

    public void setGestion_costo(int gestion_costo) {
        this.gestion_costo = gestion_costo;
    }
    
    
    
    
    
}
