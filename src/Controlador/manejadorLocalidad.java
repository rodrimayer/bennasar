/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.conexionDB;
import Modelo.Localidad;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;

/**
 *
 * @author Administrador
 */
public class manejadorLocalidad {
    
    Connection con;
    
    public manejadorLocalidad(Connection con){
        this.con = con;
    }
    
    public Localidad obtenerLocalidad(int codigo){
        
        Localidad l = new Localidad();
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        
        String sql = "SELECT nombre from localidad where id_localidad = '"+codigo+"'";
        
        //System.out.println("SQL PARA OBTENER LOCALIDAD: "+sql);
        try{
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);
            
            while(rs1.next()){
                l.setId_localidad(codigo);
                l.setLocalidad(rs1.getString(1));
            }
            
        }catch(Exception e){
            System.out.println("Error de sql localidad: "+e);
        }
        
        
        return l;
        
        
    }
    
    public void cargarLocalidades(JComboBox localidades){
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        
        String sql= "SELECT nombre from localidad order by id_localidad asc";
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);
            
            while(rs1.next()){
                localidades.addItem(rs1.getString(1));
            }
            
            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorLocalidad.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public void agregarLocalidad(String nombre){
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        
        String sql= "INSERT INTO localidad (nombre) values ('"+nombre+"')";
        int n;
        
        try {
            Statement st1 = con.createStatement();
            n = st1.executeUpdate(sql);  
            
            if(n>0){
                JOptionPane.showMessageDialog(null, "LOCALIDAD AGREGADA CON EXITO");
            }else{
                JOptionPane.showMessageDialog(null, "FALLO AL CARGAR EL REGISTRO");
            }
            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorLocalidad.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public boolean existeLocalidad(String nombre){
        boolean b = false;
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        
        String sql= "SELECT * FROM localidad where nombre = '"+nombre+"'";
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);
            
            while(rs1.next()){
                b = true;
            }
            //con.close();
            
        } catch (SQLException ex) {
            Logger.getLogger(manejadorLocalidad.class.getName()).log(Level.SEVERE, null, ex);
        }        
        
        return b;
    }
    
    
}
