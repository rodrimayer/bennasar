/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Licencia;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author rmmayer
 */
public class manejadorLicencia {

    Connection con;

    public manejadorLicencia(Connection con) {
        this.con = con;
    }
    
    public Licencia traerLicencia(String id_maquina){
        Licencia l = new Licencia();
        
        String sql = "SELECT activo "
                + "FROM licencia "
                + "WHERE id_licencia = '" + id_maquina + "' and activo = 1";
        
        return l;
    }

    public boolean licenciaHabilitada(String id_maquina, Licencia l) {
        boolean b = false;
        String sql = "SELECT id_licencia, activo, version_instalada "
                + "FROM licencia "
                + "WHERE id_licencia = '" + id_maquina + "' and activo = 1";

        Statement st;
        try {
            st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                b = true;
                l.setId_licencia(id_maquina);
                l.setActivo(rs.getInt(2));
                l.setVersion_instalada(rs.getString(3));
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorLicencia.class.getName()).log(Level.SEVERE, null, ex);
        }

        return b;

    }

    public boolean necesitaActualizar(String version) {
        boolean b = false;

        String sql = "SELECT id_version_actual "
                + "from version_actual "
                + "where produccion = 1";

        Statement st;
        try {
            st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                System.out.println("HAY UNA VERSION EN PRODUCCION");
                if (!rs.getString(1).equals(version)) {
                    System.out.println("ingreso aqui");
                    b = true;
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorLicencia.class.getName()).log(Level.SEVERE, null, ex);
        }

        return b;

    }
}
