/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.conexionDB;
import Modelo.Cliente;
import Modelo.Contacto;
import Vista.Clientes;
import java.awt.Color;
//import Vista.CargarPedido;
import java.sql.*;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

/**
 *
 * @author Administrador
 */
public class manejadorCliente {
    Connection con;
    
   public manejadorCliente(Connection con){
       this.con = con;
   }

    public class MiModelo extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }
    MiModelo modelo = new MiModelo();
    MiModelo modeloClientes = new MiModelo();
    MiModelo modeloBoletas = new MiModelo();
    

    public boolean guardarCliente(Cliente c, Contacto ct, Connection con) {

        boolean correcto = true;
        int id_cliente = 0;
        //conexionDB con = new conexionDB();
        //Connection reg = con.conectar();

        String sql = "INSERT INTO cliente (nombre, direccion, id_localidad, inactivo, zona_def) VALUES (?,?,?,?,?)";

        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setString(1, c.getNombre());
            ps.setString(2, c.getDireccion());
            ps.setInt(3, c.getIdLocalidad());
            ps.setInt(4,0);
            ps.setInt(5,c.getZona_def());
            //ps.setString(4, c.getContacto());
            //ps.setString(5, c.getTelefono());
            //ps.setString(6, c.getCelular());
            //ps.setString(7, c.getMail());

            //System.out.println(sql);
            int n = ps.executeUpdate();

            /*
             * if (n > 0) { JOptionPane.showMessageDialog(null, "Registro
             * guardado con exito"); reg.close();                
            }
             */

        } catch (SQLException ex) {
            correcto = false;
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

        String sqlIdCliente = "SELECT id_cliente from cliente where nombre = '" + c.getNombre() + "'";

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sqlIdCliente);

            while (rs1.next()) {
                id_cliente = rs1.getInt(1);
            }

            //con.close();

        } catch (SQLException ex) {

            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

        agregarContacto(ct, id_cliente);

        return correcto;

    }

    public void agregarContacto(Contacto ct, int id_cliente) {

        String sqlAgregarContacto = "INSERT INTO contacto (id_cliente, dueño, encargado1, encargado2, encargado3, encargado4, telefono, celular, mail) VALUES (?,?,?,?,?,?,?,?,?)";
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        
        try {
            PreparedStatement ps1 = con.prepareStatement(sqlAgregarContacto);
            ps1.setInt(1, id_cliente);
            ps1.setString(2, ct.getDueño());
            ps1.setString(3, ct.getEncargado1());
            ps1.setString(4, ct.getEncargado2());
            ps1.setString(5, ct.getEncargado3());
            ps1.setString(6, ct.getEncargado4());
            ps1.setString(7, ct.getTelefono());
            ps1.setString(8, ct.getCelular());
            ps1.setString(9, ct.getMail());
            int n = ps1.executeUpdate();

            if (n > 0) {
                JOptionPane.showMessageDialog(null, "Registro guardado con exito");
                //con.close();
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public Cliente obtenerCliente(String bus) {

        String sql = "SELECT c.id_cliente, c.nombre, c.direccion, l.nombre, l.id_cliente, c.cta_cte_inactiva "
                + "from cliente as c "
                + "join localidad as l "
                + "on c.id_localidad = l.id_localidad "
                + "where nombre = '" + bus + "'";

        String codigo = "";
        Cliente cli = new Cliente();

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                //cli.setIdCliente(rs1.getInt(1));
                cli.setNombre(rs1.getString(2));
                cli.setDireccion(rs1.getString(3));
                cli.setLocalidad(rs1.getInt(5));
                cli.setCta_cte_inactiva(rs1.getInt(6));
            }

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

        return cli;
    }

    public Cliente obtenerCliente(int id_cliente) {

        String sql = "SELECT c.nombre, c.direccion, l.id_localidad, l.nombre, ct.dueño, ct.telefono, ct.celular, "
                + "ct.mail, ct.encargado1, ct.encargado2, ct.encargado3, ct.encargado4, c.zona_def, c.cta_cte_inactiva "
                + "from cliente as c "
                + "join localidad as l "
                + "on c.id_localidad = l.id_localidad "
                + "join contacto as ct "
                + "on ct.id_cliente = c.id_cliente "
                + "where c.id_cliente = " + id_cliente;

        Cliente cli = new Cliente();
        Contacto ct = new Contacto();

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                cli.setIdCliente(id_cliente);
                cli.setNombre(rs1.getString(1));
                cli.setDireccion(rs1.getString(2));
                cli.setLocalidad(rs1.getInt(3));                

                ct.setDueño(rs1.getString(5));
                ct.setTelefono(rs1.getString(6));
                ct.setCelular(rs1.getString(7));
                ct.setMail(rs1.getString(8));
                ct.setEncargado1(rs1.getString(9));
                ct.setEncargado2(rs1.getString(10));
                ct.setEncargado3(rs1.getString(11));
                ct.setEncargado4(rs1.getString(12));

                cli.setZona_def(rs1.getInt(13));
                cli.setCta_cte_inactiva(rs1.getInt(14));
                cli.setContacto(ct);
            }

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

        return cli;
    }

    public void actualizarCliente(Cliente cli, int id_cliente) {

        String updateCliente = "UPDATE cliente "
                + "SET nombre = ?, direccion = ?, id_localidad = ?, zona_def = ? "
                + "WHERE id_cliente = ?";

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            PreparedStatement st1 = con.prepareStatement(updateCliente);
            st1.setString(1, cli.getNombre());
            st1.setString(2, cli.getDireccion());
            st1.setInt(3, cli.getIdLocalidad());
            st1.setInt(4, cli.getZona_def());
            st1.setInt(5, id_cliente);


            st1.executeUpdate();
            //mostrarCliente(cli);
            actualizarContacto(cli.getContacto(), id_cliente);

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    public void actualizarContacto(Contacto ct, int id_cliente) {
        mostrarContacto(ct);
        String updateCliente = "UPDATE contacto "
                + "SET dueño = ?, telefono = ?, celular = ?, mail = ?, encargado1 = ?, "
                + "encargado2 = ?, encargado3 = ?, encargado4 = ? "
                + "WHERE id_cliente = ?";
        
        System.out.println("UPDATE contacto set dueño = "+ct.getDueño()+", telefono = "+ct.getTelefono()+", celular = "+ct.getCelular()+", mail = "+ct.getMail()+", encargado1 = "+ct.getEncargado1()+" where id_cliente = "+ct.getId_cliente());

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            PreparedStatement st1 = con.prepareStatement(updateCliente);
            st1.setString(1, ct.getDueño());
            st1.setString(2, ct.getTelefono());
            st1.setString(3, ct.getCelular());
            st1.setString(4, ct.getMail());
            st1.setString(5, ct.getEncargado1());
            st1.setString(6, ct.getEncargado2());
            st1.setString(7, ct.getEncargado3());
            st1.setString(8, ct.getEncargado4());
            st1.setInt(9, id_cliente);


            st1.executeUpdate();


        } catch (SQLException ex) {
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }


    }

    public void actualizarTablaClientes(JTable clientes) {
        clientes.setModel(modelo);
        modelo.addColumn("Id_Cliente");
        modelo.addColumn("Clientes");

        //CODIGO PARA OCULTAR LA COLUMNA ID CLIENTE
        clientes.getColumnModel().getColumn(0).setMaxWidth(0);
        clientes.getColumnModel().getColumn(0).setMinWidth(0);
        clientes.getColumnModel().getColumn(0).setPreferredWidth(0);


        int totalFilas = clientes.getRowCount();
        //System.out.println("cantidad de filas: "+totalFilas);

        for (int i = 0; totalFilas > i; i++) {
            //System.out.println("cantidad de filas: "+modelo.getRowCount());
            modelo.removeRow(0);
        }

        Object[] cliente = new Object[2];

        String sql = "SELECT id_cliente, nombre from cliente";
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                cliente[0] = rs1.getInt(1);
                cliente[1] = rs1.getString(2);

                modelo.addRow(cliente);
            }

            //con.close();

        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO CLIENTES: " + e);
        }

    }

    public void inhabilitarCliente(int id_cliente) {
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "UPDATE cliente "
                + "SET inactivo = 1 "
                + "WHERE id_cliente = " + id_cliente;

        //System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            st1.executeUpdate(sql);
            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void traerClientes(JTable tablaClientes, int tipo, Clientes.MiModelo modeloClientes) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        
        tablaClientes.setModel(modeloClientes);

        if (tipo == 1) {
            modeloClientes.addColumn("ID_CLIENTE");
            modeloClientes.addColumn("CLIENTE");
        }

        int totalFilas = tablaClientes.getRowCount();
        //System.out.println("cantidad de filas: " + totalFilas);

        for (int i = 0; totalFilas > i; i++) {
            //System.out.println("cantidad de filas: "+modelo.getRowCount());
            modeloClientes.removeRow(0);
        }

        //CODIGO PARA OCULTAR LA COLUMNA ID CLIENTE
        tablaClientes.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaClientes.getColumnModel().getColumn(0).setMinWidth(0);
        tablaClientes.getColumnModel().getColumn(0).setPreferredWidth(0);
        
        tablaClientes.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        Object[] cliente = new Object[2];

        String sql = "SELECT id_cliente, nombre "
                + "FROM cliente "
                + "WHERE inactivo = 0 "
                + "ORDER BY nombre asc";
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                cliente[0] = rs1.getInt(1);
                cliente[1] = rs1.getString(2);

                modeloClientes.addRow(cliente);
            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void busquedaClientesTabla(Clientes.MiModelo modelo, String bus) {

        //int totalFilas = tablaClientes.getRowCount();
        //System.out.println("cantidad de filas: " + totalFilas);

        /*for (int i = 0; totalFilas > i; i++) {
            //System.out.println("cantidad de filas: "+modelo.getRowCount());
            modeloClientes.removeRow(0);
        }*/
        
        //DefaultTableModel modelo = (DefaultTableModel) tablaClientes.getModel();

        String busqueda = bus;

        String sql = "SELECT id_cliente, nombre from cliente where nombre like '%" + busqueda + "%' and inactivo = 0";

        //System.out.println(sql);

        Object[] cliente = new Object[2];
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                //System.out.println("RESULTADOS: " + rs1.getString(1));
                cliente[0] = rs1.getInt(1);
                cliente[1] = rs1.getString(2);

                modelo.addRow(cliente);
            }

            //con.close();

        } catch (Exception e) {
            System.err.println("Error busqueda: " + e);
        }
    }
    
    public void busquedaClientesBoleta(JTable tablaBoletas, String bus) {
        
        int totalFilas = tablaBoletas.getRowCount();
        //System.out.println("cantidad de filas: " + totalFilas);

        for (int i = 0; totalFilas > i; i++) {
            //System.out.println("cantidad de filas: "+modelo.getRowCount());
            modeloBoletas.removeRow(0);
        }

        String busqueda = bus;

        String sql = "SELECT id_cliente, nombre from cliente where nombre like '%" + busqueda + "%' and inactivo = 0";

        //System.out.println(sql);

        Object[] cliente = new Object[2];
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                //System.out.println("RESULTADOS: " + rs1.getString(1));
                cliente[0] = rs1.getInt(1);
                cliente[1] = rs1.getString(2);

                modeloClientes.addRow(cliente);
            }

            //con.close();

        } catch (Exception e) {
            System.err.println("Error busqueda: " + e);
        }
    }

    public void traerClientesInhabilitados(JTable clientesInhabilitados) {
        clientesInhabilitados.setModel(modeloClientes);
        modeloClientes.addColumn("ID_CLIENTE");
        modeloClientes.addColumn("CLIENTE");

        //CODIGO PARA OCULTAR LA COLUMNA ID CLIENTE
        clientesInhabilitados.getColumnModel().getColumn(0).setMaxWidth(0);
        clientesInhabilitados.getColumnModel().getColumn(0).setMinWidth(0);
        clientesInhabilitados.getColumnModel().getColumn(0).setPreferredWidth(0);

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        Object[] cliente = new Object[2];

        String sql = "SELECT id_cliente, nombre "
                + "FROM cliente "
                + "WHERE inactivo = 1 "
                + "ORDER BY nombre asc";
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                cliente[0] = rs1.getInt(1);
                cliente[1] = rs1.getString(2);

                modeloClientes.addRow(cliente);
            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void habilitarCliente(int id_cliente) {
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "UPDATE cliente "
                + "SET inactivo = 0 "
                + "WHERE id_cliente = " + id_cliente;
        try {
            Statement st1 = con.createStatement();
            st1.executeUpdate(sql);
            //con.close();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public void mostrarCliente(Cliente c){
        
        System.out.println("ID: "+c.getIdCliente());
        System.out.println("NOMBRE: "+c.getNombre());
        System.out.println("DIRECCION: "+c.getDireccion());
        System.out.println("DUEÑO: "+c.getContacto().getDueño());
        System.out.println("ENCARGADO 1: "+c.getContacto().getEncargado1());
        System.out.println("ENCARGADO 2: "+c.getContacto().getEncargado2());
        System.out.println("ENCARGADO 3: "+c.getContacto().getEncargado3());
        System.out.println("ENCARGADO 4: "+c.getContacto().getEncargado4());
        System.out.println("MAIL: "+c.getContacto().getMail());
        System.out.println("TELEFONO: "+c.getContacto().getTelefono());
        System.out.println("CELULAR: "+c.getContacto().getCelular());
        
    }
    
    public void mostrarContacto(Contacto c){
        
        System.out.println("ID: "+c.getId_cliente());
        System.out.println("DUEÑO: "+c.getDueño());
        System.out.println("ENCARGADO 1: "+c.getEncargado1());
        System.out.println("ENCARGADO 2: "+c.getEncargado2());
        System.out.println("ENCARGADO 3: "+c.getEncargado3());
        System.out.println("ENCARGADO 4: "+c.getEncargado4());
        System.out.println("MAIL: "+c.getMail());
        System.out.println("TELEFONO: "+c.getTelefono());
        System.out.println("CELULAR: "+c.getCelular());
        
    }
    
    public void traerClientesArray(ArrayList<Cliente> arrayClientes){
        //String sql = "SELECT nombre from cliente where inactivo = 0";
        
        
        String sql = "SELECT c.id_cliente, c.nombre, c.direccion, c.zona_def, l.nombre, l.id_localidad, con.dueño, con.encargado1, con.encargado2, con.encargado3, con.encargado4, con.telefono, con.celular, con.mail "
                + "from cliente as c "
                + "join localidad as l "
                + "on c.id_localidad = l.id_localidad "
                + "join contacto as con "
                + "on c.id_cliente = con.id_cliente";
        
        Statement st;
        try {
            st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            
            while(rs.next()){
                Cliente cli = new Cliente();
                Contacto cont = new Contacto();
                cli.setIdCliente(rs.getInt(1));
                cli.setNombre(rs.getString(2));
                cli.setDireccion(rs.getString(3));
                cli.setZona_def(rs.getInt(4));
                cli.setLocalidad(rs.getInt(6));
                cont.setDueño(rs.getString(7));
                cont.setEncargado1(rs.getString(8));
                cont.setEncargado2(rs.getString(9));
                cont.setEncargado3(rs.getString(10));
                cont.setEncargado4(rs.getString(11));
                cont.setTelefono(rs.getString(12));
                cont.setCelular(rs.getString(13));
                cont.setMail(rs.getString(14));
                cli.setContacto(cont);
                
                arrayClientes.add(cli);
            }
            
        } catch (SQLException ex) {
            Logger.getLogger(manejadorCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    public Cliente obtenerClienteArray(String bus, ArrayList<Cliente> arrayClientes) {
        Cliente cli = new Cliente();
        manejadorLocalidad ml = new manejadorLocalidad(con);
        manejadorCtaCte mcc = new manejadorCtaCte(con);

        for (int i = 0; i < arrayClientes.size(); i++){
            System.out.println("bus: "+bus);
            if(bus.equalsIgnoreCase(arrayClientes.get(i).getNombre())){
                cli = arrayClientes.get(i);
            }
        }
         

        return cli;
    }

}
