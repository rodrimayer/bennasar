/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Modelo.Condicion_Vta;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTable;

/**
 *
 * @author rmmayer
 */
public class manejadorFacturacion {

    Connection con;

    public manejadorFacturacion(Connection con) {
        this.con = con;
    }

    public void cargarCondVta(JComboBox condiciones) {

        try {
            String sql = "SELECT id_condVta, desc_condVta from condicion_vta";

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            Condicion_Vta cv = new Condicion_Vta();

            while (rs.next()) {
                cv.setId_condVta(rs.getInt(1));
                cv.setDesc_condVta(rs.getString(2));

                condiciones.addItem(cv.getDesc_condVta());

            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorFacturacion.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public Condicion_Vta traerCondVta(String desc) {
        Condicion_Vta cv = new Condicion_Vta();

        try {
            String sql = "SELECT id_condVta, desc_condVta from condicion_vta where desc_condVta = '" + desc + "'";
            System.out.println(sql);

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                cv.setId_condVta(rs.getInt(1));
                cv.setDesc_condVta(rs.getString(2));
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorFacturacion.class.getName()).log(Level.SEVERE, null, ex);
        }

        return cv;
    }

    public float pagoParcialCtaCte(float monto, int id) {
        //float aux = monto;
        float upd = 0;
        String sql = "SELECT saldo from deuda_cta_cte where id_deudaCtaCte = " + id;
        System.out.println(sql);

        try {

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                if (monto < rs.getFloat(1)) {
                    upd = rs.getFloat(1) - monto;
                    monto = 0;
                } else {
                    monto = monto - rs.getFloat(1);
                    upd = 0;
                }

            }

            sql = "UPDATE deuda_cta_cte set saldo = " + upd + " where id_deudaCtaCte = " + id;

            st.execute(sql);

            //insertarFactCtaCte(id_cliente, id_condVta, monto);
            System.out.println(sql);

            //st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(manejadorFacturacion.class.getName()).log(Level.SEVERE, null, ex);
        }

        return monto;

    }

    public float pagoBoletaParcialCtaCte(float monto, int id_boleta) {
        //float aux = monto;
        float upd = 0;
        String sql = "SELECT saldo from boleta where id_boleta = " + id_boleta;
        System.out.println(sql);

        try {

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                System.out.println("MONTO PARA PAGAR: " + monto);
                System.out.println("SALDO DE LA BOLETA: " + rs.getFloat(1));
                if (monto < rs.getFloat(1)) {
                    upd = rs.getFloat(1) - monto;
                    monto = 0;
                } else {
                    monto = monto - rs.getFloat(1);
                    upd = 0;
                }

            }

            sql = "UPDATE boleta set saldo = " + upd + " where id_boleta = " + id_boleta;

            st.execute(sql);

            //insertarFactCtaCte(id_cliente, id_condVta, monto);
            System.out.println(sql);

            //st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(manejadorFacturacion.class.getName()).log(Level.SEVERE, null, ex);
        }

        return monto;

    }

    public void pagoBoletaTotalCtaCte(int id_boleta) {
        //float aux = monto;
        float upd = 0;
        String sql = "UPDATE boleta set saldo = 0 where id_boleta = " + id_boleta;
        System.out.println(sql);

        try {

            Statement st = con.createStatement();

            st.execute(sql);

            //System.out.println(sql);

        } catch (SQLException ex) {
            Logger.getLogger(manejadorFacturacion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void insertarFactCtaCte(int id_cliente, int id_condVta, float monto) {
        Calendar calendario = GregorianCalendar.getInstance();
        Date fecha = calendario.getTime();
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("yyyy/MM/dd HH:mm");

        System.out.println(formatoDeFecha.format(fecha));

        String sql = "INSERT INTO facturacion_ctacte (id_cliente, fecha_factCtaCte, monto, id_condVta) VALUES (?,?,?,?)";
        System.out.println(sql);

        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, id_cliente);
            ps.setString(2, formatoDeFecha.format(fecha));
            ps.setFloat(3, monto);
            ps.setInt(4, id_condVta);

            ps.execute();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorFacturacion.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
