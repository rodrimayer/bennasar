/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.conexionDB;
import Modelo.Boleta;
import Modelo.Cliente;
import Modelo.Detalle;
import Modelo.Detalle2;
import Vista.Clientes;
import Vista.InicioNuevo;
import Vista.Login;
import com.lowagie.text.Document;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.*;
import java.awt.Desktop;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Administrador
 */
public class manejadorBoleta {

    public class MiModelo extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    Connection con;

    public manejadorBoleta(Connection con) {
        this.con = con;
    }

    MiModelo modeloBoletasSinCerrar = new MiModelo();
    MiModelo modeloResumenPedido = new MiModelo();
    MiModelo modeloBoleta = new MiModelo();
    MiModelo modeloReporte = new MiModelo();
    //MiModelo modeloBoletasUltPedido = new MiModelo();
    JTable tablaPedido;
    JTable detalles;

    public void guardarBoleta(Boleta b, JTable tablaPedido, String cliente, String id_usuario) {

        //Boleta b = new Boleta();
        this.tablaPedido = tablaPedido;
        Detalle d = new Detalle();
        Detalle2 d2 = new Detalle2();
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        //Date date2 = new java.sql.Date();

        System.out.println(b.getId_cliente() + " " + b.getFecha() + " " + b.getIdPedido());

        String sql;
        sql = "INSERT INTO boleta (id_cliente, zona, id_pedido, cantidad_items, fecha, id_usuario) VALUES (?,?,?,?,'" + b.getFecha() + "',?)";

        try {
            PreparedStatement ps = con.prepareStatement(sql);
            ps.setInt(1, b.getId_cliente());
            ps.setInt(2, b.getZona());
            ps.setInt(3, b.getIdPedido());
            ps.setInt(4, b.getCantidad_items());
            ps.setString(5, id_usuario);
            //ps.setDate(2, "12052016");            

            int n = ps.executeUpdate();

            String sqlId = "SELECT id_boleta from boleta where id_cliente = '" + b.getId_cliente() + "' order by id_boleta desc limit 1";

            try {
                Statement st1 = con.createStatement();
                ResultSet rs1 = st1.executeQuery(sqlId);

                while (rs1.next()) {
                    //System.out.println("boleta recien guardada id: " + rs1.getInt(1));
                    b.setId_boleta(rs1.getInt(1));
                }

            } catch (Exception e) {
                System.err.println("Error lectura de id_boleta: " + e);
            }

            int contador = 0;

            for (int i = 0; i < b.getCantidad_items(); i++) {
                String tipo_cantidad = "Cajon";

                if (!(tablaPedido.getValueAt(i, 2) == null) || tablaPedido.getValueAt(i, 2).toString() != "" || !tablaPedido.getValueAt(i, 2).toString().isEmpty()) {

                    /////CASO DETALLE 1
                    //System.out.println("Id boleta: " + b.getId_boleta());
                    d.setId_boleta(b.getId_boleta());
                    //System.out.println("Id mercaderia: " + tablaPedido.getValueAt(i, 0).toString());
                    d.setId_mercaderia(tablaPedido.getValueAt(i, 0).toString());
                    //System.out.println("Cantidad: " + tablaPedido.getValueAt(i, 1).toString());
                    d.setCantidad(Float.parseFloat(tablaPedido.getValueAt(i, 2).toString()));
                    if (!(tablaPedido.getValueAt(i, 3) == null)) {
                        d.setId_tipo(obtenerIdTipo(tablaPedido.getValueAt(i, 3).toString()));
                        //System.out.println("TIPO CANTIDAD:" + tablaPedido.getValueAt(i, 3).toString());
                        //System.out.println("Id tipo cantidad: " + d.getId_tipo());
                    } else {
                        d.setId_tipo(obtenerIdTipo(tipo_cantidad));
                        //System.out.println("TIPO CANTIDAD:" + tipo_cantidad);
                        //System.out.println("Id tipo cantidad: " + d.getId_tipo());
                    }

                    guardarDetalle(d);

                    d2.setId_boleta(b.getId_boleta());
                    d2.setId_mercaderia(tablaPedido.getValueAt(i, 0).toString());
                    //int cantidad = Integer.parseInt(tablaPedido.getValueAt(i, 1).toString());
                    float cantidad = Float.parseFloat(tablaPedido.getValueAt(i, 2).toString());

                    if ((tablaPedido.getValueAt(i, 3) == null)) {
                        d2.setCantCajon(cantidad);
                        d2.setCantBolsa(0);
                        d2.setCantKg(0);
                        d2.setCantUnidad(0);
                        d2.setCantBandeja(0);
                        d2.setCantRistra(0);
                    } else {
                        switch (obtenerIdTipo(tablaPedido.getValueAt(i, 3).toString())) {
                            case 1:
                                d2.setCantCajon(cantidad);
                                d2.setCantBolsa(0);
                                d2.setCantKg(0);
                                d2.setCantUnidad(0);
                                d2.setCantBandeja(0);
                                d2.setCantRistra(0);
                                break;
                            case 2:
                                d2.setCantCajon(0);
                                d2.setCantBolsa(cantidad);
                                d2.setCantKg(0);
                                d2.setCantUnidad(0);
                                d2.setCantBandeja(0);
                                d2.setCantRistra(0);
                                break;
                            case 3:
                                d2.setCantCajon(0);
                                d2.setCantBolsa(0);
                                d2.setCantKg(cantidad);
                                d2.setCantUnidad(0);
                                d2.setCantBandeja(0);
                                d2.setCantRistra(0);
                                break;
                            case 4:
                                d2.setCantCajon(0);
                                d2.setCantBolsa(0);
                                d2.setCantKg(0);
                                d2.setCantUnidad(cantidad);
                                d2.setCantBandeja(0);
                                d2.setCantRistra(0);
                                break;
                            case 5:
                                d2.setCantCajon(0);
                                d2.setCantBolsa(0);
                                d2.setCantKg(0);
                                d2.setCantUnidad(0);
                                d2.setCantBandeja(cantidad);
                                d2.setCantRistra(0);
                                break;
                            case 6:
                                d2.setCantCajon(0);
                                d2.setCantBolsa(0);
                                d2.setCantKg(0);
                                d2.setCantUnidad(0);
                                d2.setCantBandeja(0);
                                d2.setCantRistra(cantidad);
                                break;

                        }
                    }

                    guardarDetalle2(d2);

                }

            }

            if (n > 0) {

                JOptionPane.showMessageDialog(null, "Registro guardado con exito");

                //this.imprimirBoleta(b);
            }

            //con.close();
        } catch (Exception e) {
            System.out.println("ERROR al cargar detalle: " + e);
        }

    }

    public int contadorItems(JTable tabla) {

        int contador = 0;

        for (int i = 0; i < tabla.getRowCount(); i++) {

            if (tabla.getValueAt(i, 2).toString() != "" || !tabla.getValueAt(i, 2).toString().isEmpty()) {
                //System.out.println(tabla.getValueAt(i, 0).toString()+"     VALOR:"+tabla.getValueAt(i, 2).toString());

                contador++;
            }

        }

        return contador;

    }

    public void traerBoleta(JTable tablaResumenPedido, int id_boleta) {

        tablaResumenPedido.setModel(modeloResumenPedido);
        //modeloResumenPedido.addColumn("ID DETALLE");
        modeloResumenPedido.addColumn("ID");
        modeloResumenPedido.addColumn("MERCADERIA");
        modeloResumenPedido.addColumn("CANTIDAD");
        modeloResumenPedido.addColumn("TIPO CANTIDAD");

        String sqlCantidad = "SELECT cantCajon, cantBolsa, cantKg, cantUnidad, cantBandeja "
                + "from detalle 2 "
                + "where id_boleta = " + id_boleta;

        String sqlDetalle = "SELECT d.id_detalle, d.id_mercaderia, m.nombre, d.cantCajon, d.cantBolsa, d.cantKg, d.cantUnidad, d.cantBandeja "
                + "FROM detalle2 as d "
                + "JOIN mercaderia as m on d.id_mercaderia = m.id_mercaderia "
                + "WHERE id_boleta = " + id_boleta;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        float cantCajon, cantBolsa, cantKg, cantUnidad, cantBandeja;
        String tipo;
        float cantidad;
        Object[] detalle = new Object[4];

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sqlDetalle);

            while (rs1.next()) {
                //detalle[0] = rs1.getInt(1);
                detalle[0] = rs1.getString(2);
                detalle[1] = rs1.getString(3);
                if (rs1.getFloat(4) != 0) {
                    tipo = "Cajon";
                    cantidad = rs1.getFloat(4);
                } else if (rs1.getFloat(5) != 0) {
                    tipo = "Bolsa";
                    cantidad = rs1.getFloat(5);
                } else if (rs1.getFloat(6) != 0) {
                    tipo = "Kg";
                    cantidad = rs1.getFloat(6);
                } else if (rs1.getFloat(7) != 0) {
                    tipo = "Unidad";
                    cantidad = rs1.getFloat(7);
                } else {
                    tipo = "Bandeja";
                    cantidad = rs1.getFloat(8);
                }
                detalle[2] = cantidad;
                detalle[3] = tipo;

                modeloResumenPedido.addRow(detalle);
            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorBoleta.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void traerBoleta(int id_boleta, Boleta b) {

        String sql = "SELECT id_boleta, id_cliente, DATE_FORMAT(fecha, '%d-%m-%Y'), zona, id_pedido, cantidad_items "
                + "FROM boleta "
                + "WHERE id_boleta = " + id_boleta;

        /*String sql = "SELECT id_boleta, id_cliente, fecha, zona, id_pedido, cantidad_items "
                + "FROM boleta "
                + "WHERE id_boleta = " + id_boleta;*/
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                b.setId_boleta(id_boleta);
                b.setId_cliente(rs1.getInt(2));
                b.setFecha(rs1.getString(3));
                b.setZona(rs1.getInt(4));
                b.setIdPedido(rs1.getInt(5));
                b.setCantidad_items(rs1.getInt(6));
            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorBoleta.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public float traerTotalBoleta(int id_boleta) {
        float total = 0;

        String sql = "SELECT SUM(precio*cantidad) "
                + "FROM detalle "
                + "WHERE id_boleta = " + id_boleta;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        System.out.println("SQL TRAER TOTAL: " + sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                total = rs1.getFloat(1);
            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorBoleta.class.getName()).log(Level.SEVERE, null, ex);
        }

        return total;
    }

    public void guardarDetalle(Detalle d) {

        String sqlInsert = "INSERT INTO detalle (id_boleta, id_mercaderia, cantidad, id_tipocant) VALUES (?,?,?,?)";
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            PreparedStatement ps = con.prepareStatement(sqlInsert);
            ps.setInt(1, d.getId_boleta());
            ps.setString(2, d.getId_mercaderia());
            ps.setFloat(3, d.getCantidad());
            ps.setInt(4, d.getId_tipo());

            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorBoleta.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void guardarDetalle2(Detalle2 d2) {
        String sqlInsert = "INSERT INTO detalle2 (id_boleta, id_mercaderia, cantCajon, cantBolsa, cantKg, cantUnidad, cantBandeja, cantRistra) VALUES (?,?,?,?,?,?,?,?)";
        System.out.println("ENTRO A CARGAR DETALLES");
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            PreparedStatement ps = con.prepareStatement(sqlInsert);
            ps.setInt(1, d2.getId_boleta());
            ps.setString(2, d2.getId_mercaderia());
            ps.setFloat(3, d2.getCantCajon());
            ps.setFloat(4, d2.getCantBolsa());
            ps.setFloat(5, d2.getCantKg());
            ps.setFloat(6, d2.getCantUnidad());
            ps.setFloat(7, d2.getCantBandeja());
            ps.setFloat(8, d2.getCantRistra());

            ps.executeUpdate();

        } catch (SQLException ex) {
            Logger.getLogger(manejadorBoleta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public int obtenerIdTipo(String tipo_cantidad) {
        int id = 0;
        String sql = "select id_tipocant from tipocantidad where tipo_cantidad = '" + tipo_cantidad + "'";
        System.out.println(sql);
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                id = rs1.getInt(1);
            }

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

        return id;
    }

    public void actualizarBoletas(JTable tablaBoletasSinCerrar, int id_pedido, InicioNuevo.MiModelo modelo, String id_usuario) {
        System.out.println("cantidad de filas: " + tablaBoletasSinCerrar.getRowCount());
        int totalFilas = tablaBoletasSinCerrar.getRowCount();
        //System.out.println("cantidad de filas: "+totalFilas);

        for (int i = 0; totalFilas > i; i++) {
            //System.out.println("cantidad de filas: "+modelo.getRowCount());
            modelo.removeRow(0);
        }

        Object[] boleta = new Object[5];
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "SELECT b.id_boleta, c.nombre, b.cantidad_items, DATE_FORMAT(b.fecha, '%d-%m-%Y'), b.id_cliente "
                + "from boleta as b "
                + "join cliente as c "
                + "on b.id_cliente = c.id_cliente "
                + "where b.id_pedido = " + id_pedido + " and b.id_usuario = '" + id_usuario + "' "
                + "order by fecha asc";

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                boleta[0] = rs1.getInt(1);
                boleta[1] = rs1.getString(2);
                boleta[2] = rs1.getInt(3);
                boleta[3] = rs1.getString(4);
                boleta[4] = rs1.getInt(5);

                modelo.addRow(boleta);
            }

            //con.close();
        } catch (Exception e) {
            System.err.println("ERROR AL TRAER BOLETAS: " + e);
        }

    }

    public void boletasPedido(JTable tablaUltimasBoletas, int id_pedido) {
        tablaUltimasBoletas.setModel(modeloBoletasSinCerrar);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        modeloBoletasSinCerrar.addColumn("Nro");
        modeloBoletasSinCerrar.addColumn("Cliente");
        modeloBoletasSinCerrar.addColumn("Cant Items");
        modeloBoletasSinCerrar.addColumn("Fecha");
        modeloBoletasSinCerrar.addColumn("Id_cliente");

        //CODIGO PARA OCULTAR LA COLUMNA DE ID CLIENTE
        tablaUltimasBoletas.getColumnModel().getColumn(4).setMaxWidth(0);
        tablaUltimasBoletas.getColumnModel().getColumn(4).setMinWidth(0);
        tablaUltimasBoletas.getColumnModel().getColumn(4).setPreferredWidth(0);

        tablaUltimasBoletas.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaUltimasBoletas.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaUltimasBoletas.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaUltimasBoletas.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);

        int totalFilas = tablaUltimasBoletas.getRowCount();
        //System.out.println("cantidad de filas: "+totalFilas);

        for (int i = 0; totalFilas > i; i++) {
            //System.out.println("cantidad de filas: "+modelo.getRowCount());
            modeloBoletasSinCerrar.removeRow(0);
        }
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        int ultPedido = obtenerUltPedido(con);

        Object[] boleta = new Object[5];

        String sql = "SELECT b.id_boleta, c.nombre, b.cantidad_items, DATE_FORMAT(b.fecha, '%d-%m-%Y'), b.id_cliente "
                + "from boleta as b "
                + "join cliente as c "
                + "on b.id_cliente = c.id_cliente "
                + "where b.id_pedido = " + id_pedido
                + " order by b.id_boleta desc";

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                boleta[0] = rs1.getInt(1);
                boleta[1] = rs1.getString(2);
                boleta[2] = rs1.getInt(3);
                boleta[3] = rs1.getString(4);
                boleta[4] = rs1.getInt(5);

                modeloBoletasSinCerrar.addRow(boleta);
            }

            //con.close();
        } catch (Exception e) {
            System.err.println("ERROR AL TRAER BOLETAS: " + e);
        }
    }

    public int obtenerUltPedido(Connection con) {

        String sqlUltPedido = "SELECT id_pedido from pedido order by id_pedido desc limit 1";
        int ultPedido = 0;

        try {

            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sqlUltPedido);

            while (rs1.next()) {
                ultPedido = rs1.getInt(1);
                //System.out.println("ULTIMO PEDIDO: " + ultPedido);
            }

            //con.close();
        } catch (Exception e) {
            System.err.println("ERROR AL BUSCAR ULTIMO PEDIDO: " + e);
        }

        return ultPedido;
    }

    public void guardarCambios(JTable detalles, int id_boleta, int cont, int zona, String fecha, String cliente) {

        String sqlActualizarBoleta = "UPDATE boleta set cantidad_items = " + cont + ", zona = " + zona + " where id_boleta = " + id_boleta;

        String sqlDelete = "DELETE FROM detalle2 where id_boleta = " + id_boleta;
        String sqlDelete2 = "DELETE FROM detalle where id_boleta = " + id_boleta;

        Detalle2 d2 = new Detalle2();

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st0 = con.createStatement();
            st0.executeUpdate(sqlActualizarBoleta);

        } catch (SQLException ex) {
            Logger.getLogger(manejadorBoleta.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            Statement st = con.createStatement();
            st.executeUpdate(sqlDelete);

        } catch (SQLException ex) {
            Logger.getLogger(manejadorBoleta.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {
            Statement st = con.createStatement();
            st.executeUpdate(sqlDelete2);

        } catch (SQLException ex) {
            Logger.getLogger(manejadorBoleta.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.detalles = detalles;
        Detalle d = new Detalle();

        for (int i = 0; i < detalles.getRowCount(); i++) {

            String tipo_cantidad = "Cajon";
            //System.out.println("Id boleta: " + id_boleta);
            d.setId_boleta(id_boleta);
            //System.out.println("Id mercaderia: " + detalles.getValueAt(i, 0).toString());
            d.setId_mercaderia(detalles.getValueAt(i, 0).toString());
            //System.out.println("Cantidad: " + detalles.getValueAt(i, 1).toString());
            d.setCantidad(Float.parseFloat(detalles.getValueAt(i, 2).toString()));
            if (!(detalles.getValueAt(i, 3) == null)) {
                d.setId_tipo(obtenerIdTipo(detalles.getValueAt(i, 3).toString()));
                //System.out.println("TIPO CANTIDAD:" + detalles.getValueAt(i, 3).toString());
                //System.out.println("Id tipo cantidad: " + d.getId_tipo());
            } else {
                d.setId_tipo(obtenerIdTipo(tipo_cantidad));
                //System.out.println("TIPO CANTIDAD:" + tipo_cantidad);
                //System.out.println("Id tipo cantidad: " + d.getId_tipo());
            }

            guardarDetalle(d);

            String sqlInsert = "INSERT INTO detalle2 (id_boleta, id_mercaderia, cantCajon, cantBolsa, cantKg, cantUnidad, cantBandeja, cantRistra) VALUES (?,?,?,?,?,?,?,?)";
            float cantidad = Float.parseFloat(detalles.getValueAt(i, 2).toString());
            d2.setId_boleta(id_boleta);
            d2.setId_mercaderia(detalles.getValueAt(i, 0).toString());
            switch (obtenerIdTipo(detalles.getValueAt(i, 3).toString())) {
                case 1:
                    d2.setCantCajon(cantidad);
                    d2.setCantBolsa(0);
                    d2.setCantKg(0);
                    d2.setCantUnidad(0);
                    d2.setCantBandeja(0);
                    d2.setCantRistra(0);
                    break;
                case 2:
                    d2.setCantCajon(0);
                    d2.setCantBolsa(cantidad);
                    d2.setCantKg(0);
                    d2.setCantUnidad(0);
                    d2.setCantBandeja(0);
                    d2.setCantRistra(0);
                    break;
                case 3:
                    d2.setCantCajon(0);
                    d2.setCantBolsa(0);
                    d2.setCantKg(cantidad);
                    d2.setCantUnidad(0);
                    d2.setCantBandeja(0);
                    d2.setCantRistra(0);
                    break;
                case 4:
                    d2.setCantCajon(0);
                    d2.setCantBolsa(0);
                    d2.setCantKg(0);
                    d2.setCantUnidad(cantidad);
                    d2.setCantBandeja(0);
                    d2.setCantRistra(0);
                    break;
                case 5:
                    d2.setCantCajon(0);
                    d2.setCantBolsa(0);
                    d2.setCantKg(0);
                    d2.setCantUnidad(0);
                    d2.setCantBandeja(cantidad);
                    d2.setCantRistra(0);
                    break;
                case 6:
                    d2.setCantCajon(0);
                    d2.setCantBolsa(0);
                    d2.setCantKg(0);
                    d2.setCantUnidad(0);
                    d2.setCantBandeja(0);
                    d2.setCantRistra(cantidad);
                    break;

            }

            try {
                PreparedStatement ps = con.prepareStatement(sqlInsert);
                ps.setInt(1, id_boleta);
                ps.setString(2, d2.getId_mercaderia());
                ps.setFloat(3, d2.getCantCajon());
                ps.setFloat(4, d2.getCantBolsa());
                ps.setFloat(5, d2.getCantKg());
                ps.setFloat(6, d2.getCantUnidad());
                ps.setFloat(7, d2.getCantBandeja());
                ps.setFloat(8, d2.getCantRistra());

                ps.executeUpdate();

            } catch (SQLException ex) {
                Logger.getLogger(manejadorBoleta.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        manejadorFechas mf = new manejadorFechas(con);
        String rutaPDF = mf.obtenerDireccionCarpetasBoletas(fecha);
        File dir = new File(rutaPDF);
        //System.out.println(dir.getAbsolutePath());
        //File rutaPDF = new File(dir.getAbsolutePath() + "\\BOLETAS");
        dir.mkdirs();
        JasperReport jr = null;
        //String titulo = "BOLETA DE CLIENTE ID "+b.setId_cliente(filaS);
        String rutaRelativa = "";
        String ruta = "src\\Reportes\\boleta_1.jasper";
        //String ruta = "src\\Reportes\\boletaDefinitiva.jasper";

        try {
            Map parametro = new HashMap();
            parametro.put("boleta", id_boleta);
            jr = (JasperReport) JRLoader.loadObjectFromFile(ruta);
            JasperPrint jp = JasperFillManager.fillReport(jr, parametro, con);
            JasperViewer jv = new JasperViewer(jp, false);
            JasperExportManager.exportReportToPdfFile(jp, rutaPDF + "\\boleta_" + id_boleta + "_" + cliente + "_" + fecha + ".pdf");

            //jv.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
            //jv.setVisible(true);
            //jv.setTitle("BOLETA DE CLIENTE");
        } catch (JRException ex) {
            Logger.getLogger(InicioNuevo.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void eliminarBoleta(int id_boleta, String fecha, String cliente) {

        String sqlEliminarBoleta = "DELETE from boleta where id_boleta = " + id_boleta;
        String sqlEliminarDetalle = "DELETE from detalle2 where id_boleta = " + id_boleta;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            st1.executeUpdate(sqlEliminarDetalle);
            st1.executeUpdate(sqlEliminarBoleta);

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorBoleta.class.getName()).log(Level.SEVERE, null, ex);
        }

        manejadorFechas mf = new manejadorFechas(con);

        String rutaPDF = mf.obtenerDireccionCarpetasBoletas(fecha);
        File dir = new File(rutaPDF + "\\boleta_" + id_boleta + "_" + cliente + "_" + fecha + ".pdf");

        if (dir.delete()) {
            System.out.println("ARCHIVO ELIMINADO CON EXITO");
        } else {
            System.err.println("NO SE PUDO ELIMINAR EL ARCHIVO");
        }

    }

    public void busquedaBoletas(JTable boletas, String fechaDesde, String fechaHasta) {
        modeloBoleta.addColumn("Nro");
        modeloBoleta.addColumn("CLIENTE");
        modeloBoleta.addColumn("CANT ITEMS");
        modeloBoleta.addColumn("FECHA");
        modeloBoleta.addColumn("Id_cliente");
        boletas.setModel(modeloBoleta);

        //CODIGO PARA OCULTAR LA COLUMNA DE ID CLIENTE
        boletas.getColumnModel().getColumn(4).setMaxWidth(0);
        boletas.getColumnModel().getColumn(4).setMinWidth(0);
        boletas.getColumnModel().getColumn(4).setPreferredWidth(0);

        //CODIGO PARA OCULTAR LA COLUMNA NRO BOLETA
        boletas.getColumnModel().getColumn(0).setMaxWidth(0);
        boletas.getColumnModel().getColumn(0).setMinWidth(0);
        boletas.getColumnModel().getColumn(0).setPreferredWidth(0);

        Object[] boleta = new Object[5];
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "SELECT b.id_boleta, c.nombre, b.cantidad_items, DATE_FORMAT(b.fecha, '%d-%m-%Y'), b.id_cliente "
                + "from boleta as b "
                + "join cliente as c "
                + "on b.id_cliente = c.id_cliente "
                + "where b.fecha > '" + fechaDesde + "' and b.fecha < '" + fechaHasta + "' "
                + "order by fecha asc";

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                boleta[0] = rs1.getInt(1);
                boleta[1] = rs1.getString(2);
                boleta[2] = rs1.getInt(3);
                boleta[3] = rs1.getString(4);
                boleta[4] = rs1.getInt(5);

                modeloBoleta.addRow(boleta);
            }

            //con.close();
        } catch (Exception e) {
            System.err.println("ERROR AL TRAER BOLETAS: " + e);
        }

    }

    public void busquedaPedidos(JTable pedidos, String fechaDesde, String fechaHasta) {
        modeloReporte.addColumn("PEDIDO NRO");
        modeloReporte.addColumn("CANT BOLETAS");
        modeloReporte.addColumn("FECHA");
        pedidos.setModel(modeloReporte);

        Object[] reporte = new Object[3];
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "SELECT id_pedido, DATE_FORMAT(fecha_pedido, '%d-%m-%Y') as fecha, cant_boletas from pedido "
                + "where fecha_pedido > '" + fechaDesde + "' and fecha_pedido < '" + fechaHasta + "' "
                + "order by fecha asc";

        //System.out.println("SQl: " + sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                reporte[0] = rs1.getInt(1);
                reporte[1] = rs1.getInt(3);
                reporte[2] = rs1.getString(2);

                modeloReporte.addRow(reporte);
            }

            //con.close();
        } catch (Exception e) {
            System.err.println("ERROR AL TRAER BOLETAS: " + e);
        }
    }

    public void filtroBoletaPorCliente(JTable boletas, String busqueda) {
        //modeloBoleta.addColumn("Nro");
        //modeloBoleta.addColumn("CLIENTE");
        //modeloBoleta.addColumn("CANT ITEMS");
        //modeloBoleta.addColumn("FECHA");
        //modeloBoleta.addColumn("Id_cliente");
        boletas.setModel(modeloBoleta);

        int totalFilas = boletas.getRowCount();
        //System.out.println("cantidad de filas: " + totalFilas);

        for (int i = 0; totalFilas > i; i++) {
            //System.out.println("cantidad de filas: "+modelo.getRowCount());
            modeloBoleta.removeRow(0);
        }

        Object[] boleta = new Object[5];
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "SELECT b.id_boleta, c.nombre, b.cantidad_items, DATE_FORMAT(b.fecha, '%d-%m-%Y'), b.id_cliente "
                + "from boleta as b "
                + "join cliente as c "
                + "on b.id_cliente = c.id_cliente "
                + "where c.nombre like '%" + busqueda + "%' "
                + "order by fecha asc";

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                boleta[0] = rs1.getInt(1);
                boleta[1] = rs1.getString(2);
                boleta[2] = rs1.getInt(3);
                boleta[3] = rs1.getString(4);
                boleta[4] = rs1.getInt(5);

                modeloBoleta.addRow(boleta);
            }

            //con.close();
        } catch (Exception e) {
            System.err.println("Error busqueda: " + e);
        }

    }

    public void filtroBoletaUltPedido(InicioNuevo.MiModelo modeloTablasUltPedido, String busqueda, int zona, String id_usuario) {
        //modeloBoleta.addColumn("Nro");
        //modeloBoleta.addColumn("CLIENTE");
        //modeloBoleta.addColumn("CANT ITEMS");
        //modeloBoleta.addColumn("FECHA");
        //modeloBoleta.addColumn("Id_cliente");
        //tablaBoletasUltPedido.setModel(this.modeloBoletasUltPedido);

        Object[] boleta = new Object[5];
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "";

        if (zona == 0) {
            sql = "SELECT b.id_boleta, c.nombre, b.cantidad_items, DATE_FORMAT(b.fecha, '%d-%m-%Y'), b.id_cliente "
                    + "from boleta as b "
                    + "join cliente as c "
                    + "on b.id_cliente = c.id_cliente "
                    + "where c.nombre like '%" + busqueda + "%'and id_pedido = 0 and b.id_usuario = '" + id_usuario + "' "
                    + "order by fecha asc";
        } else {
            sql = "SELECT b.id_boleta, c.nombre, b.cantidad_items, DATE_FORMAT(b.fecha, '%d-%m-%Y'), b.id_cliente "
                    + "from boleta as b "
                    + "join cliente as c "
                    + "on b.id_cliente = c.id_cliente "
                    + "where c.nombre like '%" + busqueda + "%' and id_pedido = 0 and b.zona = " + zona + " and b.id_usuario = '" + id_usuario + "' "
                    + "order by fecha asc";
        }

        //System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                boleta[0] = rs1.getInt(1);
                boleta[1] = rs1.getString(2);
                boleta[2] = rs1.getInt(3);
                boleta[3] = rs1.getString(4);
                boleta[4] = rs1.getInt(5);

                modeloTablasUltPedido.addRow(boleta);
            }

            //con.close();
        } catch (Exception e) {
            System.err.println("Error busqueda: " + e);
        }

    }

    public void filtroBoletaZona(InicioNuevo.MiModelo modeloTablasUltPedido, int zona) {
        //modeloBoleta.addColumn("Nro");
        //modeloBoleta.addColumn("CLIENTE");
        //modeloBoleta.addColumn("CANT ITEMS");
        //modeloBoleta.addColumn("FECHA");
        //modeloBoleta.addColumn("Id_cliente");
        //tablaBoletasUltPedido.setModel(this.modeloBoletasUltPedido);

        Object[] boleta = new Object[5];
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sql = "";
        if (zona != 0) {
            sql = "SELECT b.id_boleta, c.nombre, b.cantidad_items, DATE_FORMAT(b.fecha, '%d-%m-%Y'), b.id_cliente "
                    + "from boleta as b "
                    + "join cliente as c "
                    + "on b.id_cliente = c.id_cliente "
                    + "where b.zona = " + zona + " and id_pedido = 0 "
                    + "order by fecha asc";
        } else {
            sql = "SELECT b.id_boleta, c.nombre, b.cantidad_items, DATE_FORMAT(b.fecha, '%d-%m-%Y'), b.id_cliente "
                    + "from boleta as b "
                    + "join cliente as c "
                    + "on b.id_cliente = c.id_cliente "
                    + "where id_pedido = 0 "
                    + "order by fecha asc";
        }

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                boleta[0] = rs1.getInt(1);
                boleta[1] = rs1.getString(2);
                boleta[2] = rs1.getInt(3);
                boleta[3] = rs1.getString(4);
                boleta[4] = rs1.getInt(5);

                modeloTablasUltPedido.addRow(boleta);
            }

            //con.close();
        } catch (Exception e) {
            System.err.println("Error busqueda: " + e);
        }

    }

    public void imprimirBoleta(Boleta b) {
        Cliente cli = new Cliente();

        manejadorCliente mc = new manejadorCliente(con);
        manejadorBoleta mb = new manejadorBoleta(con);

        cli = mc.obtenerCliente(b.getId_cliente());

        String cliente = cli.getNombre();

        float total;

        if (this.verificarPreciosCargados(b.getId_boleta())) {
            total = 0;
        } else {
            total = mb.traerTotalBoleta(b.getId_boleta());
        }

        String ruta = "src\\Reportes\\boletaDefinitiva.jrxml";
        //File f = new File("src\\Reportes\\boleta.jasper");
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        JasperReport jr = null;

        manejadorFechas mf = new manejadorFechas(con);
        String rutaPDF = mf.obtenerDireccionCarpetasBoletas(mf.obtenerFechaBoleta(b.getId_boleta()));
        File f = new File(rutaPDF);
        f.mkdirs();

        try {
            Map parametro = new HashMap();
            parametro.put("id_boleta", b.getId_boleta());
            parametro.put("total", total);
            JasperReport reporte = JasperCompileManager.compileReport(ruta);
            JasperPrint mostrarReporte = JasperFillManager.fillReport(reporte, parametro, con);
            //JasperPrint mostrarReporte = JasperFillManager.fillReport(reporte, null, con3);
            JasperExportManager.exportReportToPdfFile(mostrarReporte, rutaPDF + "\\boleta_" + b.getId_boleta() + "_" + cliente + "_" + mf.obtenerFechaBoleta(b.getId_boleta()) + ".pdf");
            //JasperViewer.viewReport(mostrarReporte, false);
            try {
                File path = new File(rutaPDF + "\\boleta_" + b.getId_boleta() + "_" + cliente + "_" + mf.obtenerFechaBoleta(b.getId_boleta()) + ".pdf");
                Desktop.getDesktop().open(path);
            } catch (IOException ex) {
                ex.printStackTrace();
            }

        } catch (JRException ex) {
            Logger.getLogger(Clientes.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void listaMercaderiasActualizar(int id_pedido, String id_mercaderia, String tipo_cant, float precio) {

        String sql = "SELECT d.id_detalle, d.id_mercaderia, m.nombre, tc.tipo_cantidad, d.cantidad, d.precio, tc.id_tipocant "
                + "FROM detalle as d "
                + "JOIN boleta as b ON d.id_boleta = b.id_boleta "
                + "JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia "
                + "JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant "
                + "JOIN pedido as p on b.id_pedido = p.id_pedido "
                + "WHERE b.id_pedido = " + id_pedido + " and d.id_mercaderia = '" + id_mercaderia + "' and tc.tipo_cantidad = '" + tipo_cant + "' "
                + "order by m.id_tipo asc, m.orden asc, m.nombre asc, d.id_tipocant asc";

        //System.out.println("MERCADERIAS A ACTUALIZAR \n "
        //      + sql);
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                actualizarPrecios(rs1.getInt(1), precio);

            }

            //con.close();
        } catch (Exception e) {
            System.err.println("Error busqueda: " + e);
        }

    }

    public void actualizarPrecios(int id_detalle, float precio) {
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        /*
        String sql = "UPDATE detalle "
                + "SET precio = " + precio + "* cantidad "
                + "WHERE id_detalle = " + id_detalle;*/
        String sql = "UPDATE detalle "
                + "SET precio = " + precio + " "
                + "WHERE id_detalle = " + id_detalle;

        System.out.println(sql);

        try {
            Statement ps = con.createStatement();
            ps.executeUpdate(sql);
            //con.close();

        } catch (Exception e) {
            System.out.println("Error al actualizar: " + e);
        }

    }

    public void traerResumenBoleta(DefaultTableModel modelo, int id_boleta) {

        String sqlDetalle = "SELECT d.id_detalle, d.id_mercaderia, m.nombre, d.cantCajon, d.cantBolsa, d.cantKg, d.cantUnidad, d.cantBandeja "
                + "FROM detalle2 as d "
                + "JOIN mercaderia as m on d.id_mercaderia = m.id_mercaderia "
                + "WHERE id_boleta = " + id_boleta;

        String sqlDetalle2 = "SELECT d.id_detalle, d.id_mercaderia, m.nombre, d.cantidad, t.tipo_cantidad "
                + "FROM detalle as d "
                + "JOIN mercaderia as m on d.id_mercaderia = m.id_mercaderia "
                + "JOIN tipocantidad as t on t.id_tipocant = d.id_tipocant "
                + "WHERE d.id_boleta = " + id_boleta;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String tipo;
        float cantidad;
        Object[] detalle = new Object[4];

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sqlDetalle2);

            while (rs1.next()) {
                //detalle[0] = rs1.getInt(1);
                detalle[0] = rs1.getString(2);
                detalle[1] = rs1.getString(3);

                detalle[2] = rs1.getFloat(4);
                detalle[3] = rs1.getString(5);

                modelo.addRow(detalle);
            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorBoleta.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void traerResumenBoletaPrecios(DefaultTableModel modelo, int id_boleta, int id_pedido) {

        /*
        String sqlDetalle2 = "SELECT d.id_detalle, d.id_mercaderia, m.nombre, d.cantidad, t.tipo_cantidad, gc.costo_unitario, d.precio "
                + "FROM detalle as d "
                + "JOIN mercaderia as m on d.id_mercaderia = m.id_mercaderia "
                + "JOIN tipocantidad as t on t.id_tipocant = d.id_tipocant "
                + "JOIN gestion_costo2 as gc on d.id_mercaderia = gc.detalle "
                + "WHERE d.id_boleta = " + id_boleta + " and gc.id_pedido = " + id_pedido;*/
        String sqlDetalle2 = "SELECT d.id_detalle, d.id_mercaderia, m.nombre, d.cantidad, t.tipo_cantidad, gc.costo_unitario, d.precio "
                + "FROM detalle as d "
                + "JOIN mercaderia as m on d.id_mercaderia = m.id_mercaderia "
                + "JOIN tipocantidad as t on t.id_tipocant = d.id_tipocant "
                + "JOIN gestion_costo2 as gc on d.id_mercaderia = gc.detalle "
                + "WHERE d.id_boleta = " + id_boleta + " and gc.id_pedido = 0 "
                + "UNION (SELECT d.id_detalle, d.id_mercaderia, m.nombre, d.cantidad, t.tipo_cantidad, 0 as precio_costo, d.precio "
                + "FROM detalle as d JOIN mercaderia as m on d.id_mercaderia = m.id_mercaderia "
                + "JOIN tipocantidad as t on t.id_tipocant = d.id_tipocant "
                + "WHERE d.id_boleta = " + id_boleta + " and d.id_detalle "
                + "NOT IN "
                + "(SELECT d.id_detalle "
                + "FROM detalle as d "
                + "JOIN mercaderia as m on d.id_mercaderia = m.id_mercaderia "
                + "JOIN tipocantidad as t on t.id_tipocant = d.id_tipocant "
                + "JOIN gestion_costo2 as gc on d.id_mercaderia = gc.detalle "
                + "WHERE d.id_boleta = " + id_boleta + " and gc.id_pedido = 0))";

        String sql = "SELECT d.id_detalle, d.id_mercaderia, m.nombre, d.cantidad, tc.tipo_cantidad, d.costo_unitario, d.precio, (d.costo_unitario*d.cantidad) "
                + "FROM detalle AS d "
                + "JOIN boleta as b ON d.id_boleta = b.id_boleta "
                + "JOIN mercaderia AS m ON m.id_mercaderia = d.id_mercaderia "
                + "JOIN tipocantidad AS tc ON tc.id_tipocant = d.id_tipocant "
                + "WHERE b.id_boleta = " + id_boleta;

        /*
        String sqlDetalle2 = "SELECT d.id_detalle, d.id_mercaderia, m.nombre, d.cantidad, t.tipo_cantidad "
                + "FROM detalle as d "
                + "JOIN mercaderia as m on d.id_mercaderia = m.id_mercaderia "
                + "JOIN tipocantidad as t on t.id_tipocant = d.id_tipocant "
                + "WHERE d.id_boleta = " + id_boleta;*/
        System.out.println(sqlDetalle2);
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String tipo;
        float cantidad;
        Object[] detalle = new Object[7];

        try {
            Statement st1 = con.createStatement();
            //ResultSet rs1 = st1.executeQuery(sqlDetalle2);
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                detalle[0] = rs1.getInt(1);
                detalle[1] = rs1.getString(2);
                detalle[2] = rs1.getString(3);
                detalle[3] = rs1.getFloat(4);
                detalle[4] = rs1.getString(5);
                detalle[5] = rs1.getFloat(8);

                if (rs1.getFloat(7) == 0) {
                    detalle[6] = "";
                } else {
                    detalle[6] = rs1.getFloat(7);
                }

                modelo.addRow(detalle);
            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorBoleta.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void actualizarBoletaEditada(Boleta b, int cont) {
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();

        String sqlActualizarBoleta = "UPDATE boleta set cantidad_items = " + cont + ", zona = " + b.getZona() + " where id_boleta = " + b.getId_boleta();

        try {
            Statement st0 = con.createStatement();
            st0.executeUpdate(sqlActualizarBoleta);

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorBoleta.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean tienePrecios(int id) {
        boolean b = false;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sql = "SELECT d.precio "
                + "From Detalle as d "
                + "JOIN boleta as b ON b.id_boleta = d.id_boleta "
                + "WHERE b.id_boleta = " + id + " and d.precio <> 0";

        //System.out.println(sql);
        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                b = true;
            }

            //con.close();
        } catch (Exception e) {
            System.out.println("ERROR AL EJECUTAR LA CONSULTA SI LA BOLETA TIENE PRECIOS PUESTOS");
        }

        return b;
    }

    /*
    public void actualizarPrecioBoleta(float precioU, int id_detalle){
        conexionDB c = new conexionDB();
        Connection con = c.conectar();
        
        String sql = "UPDATE DETALLE "
                + "SET precio = "+precioU+" "
                + "WHERE id_detalle = "+id_detalle;
        
        System.out.println(sql);
        
        try {
            Statement st = con.createStatement();
            st.executeUpdate(sql);

            con.clearWarnings();
        } catch (Exception e) {
            System.out.println("ERROR AL ACTUALIZAR ");
        }
    }*/
    public boolean verificarPreciosCargados(int id_boleta) {
        boolean b = false;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sql = "SELECT precio FROM detalle "
                + "WHERE id_boleta = " + id_boleta;

        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                if (rs1.wasNull()) {
                    b = true;
                }

            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCosto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return b;
    }

    public void actualizarTotalBoleta(int id_boleta) {
        float total = this.traerTotalBoleta(id_boleta);

        String sql = "UPDATE boleta set total = " + total + ", saldo = " + total + " WHERE id_boleta = " + id_boleta;
        System.out.println(sql);

        try {
            Statement st = con.createStatement();
            st.executeUpdate(sql);

            con.clearWarnings();
        } catch (Exception e) {
            System.out.println("ERROR AL ACTUALIZAR TOTAL DE LA BOLETA ");
        }

    }

    public boolean isBoletaCliente(int id_boleta, int id_cliente) {
        boolean b = false;

        String sql = "SELECT * from boleta where id_boleta = " + id_boleta + " and id_cliente = " + id_cliente;

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {

                b = true;

            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCosto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return b;
    }

    public boolean isBoletaCtaCte(int id_boleta, int id_cliente) {
        boolean b = false;

        String sql = "SELECT * from boleta where id_boleta = " + id_boleta + " and id_cliente = " + id_cliente + " and saldo = 0";

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                b = true;
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorCosto.class.getName()).log(Level.SEVERE, null, ex);
        }

        return b;
    }

    public void actualizarTotalBoletaManual(int id_boleta, float total) {

        String sql = "UPDATE boleta set total = " + total + ", saldo = " + total + " WHERE id_boleta = " + id_boleta;
        System.out.println(sql);

        try {
            Statement st = con.createStatement();
            st.executeUpdate(sql);

            con.clearWarnings();
        } catch (Exception e) {
            System.out.println("ERROR AL ACTUALIZAR TOTAL DE LA BOLETA ");
        }

    }

    public void actualizarZonaBoleta(int id_boleta, int zona) {
        String sql = "UPDATE boleta "
                + "SET zona = " + zona + " "
                + "WHERE id_boleta = " + id_boleta;

        System.out.println(sql);

        Statement st;
        try {
            st = con.createStatement();
            st.executeUpdate(sql);
        } catch (SQLException ex) {
            Logger.getLogger(manejadorBoleta.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void traerTipoCantidadComboBox(JComboBox comboBoxTipoCantidad) {
        String sql = "SELECT tipo_cantidad from tipocantidad";

        try {

            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                comboBoxTipoCantidad.addItem(rs1.getString(1));
                comboBoxTipoCantidad.setSelectedIndex(0);
            }

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }
    }

}
