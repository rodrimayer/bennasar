/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.conexionDB;
//import Vista.CargarPedido;
import Vista.CargarPedidoDidactico;
import Vista.GestionDeCosto;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;
import javax.swing.SwingConstants;

/**
 *
 * @author rodrimayer
 */
public class metodoBusquedaGestion
{

    public GestionDeCosto c ;
    public JFrame cpd;
    Connection con;
    
    public metodoBusquedaGestion(GestionDeCosto c, Connection con) {
        this.c = c;
        this.con = con;
    }    
    
    
    public boolean compararMercaderia(String cadena) {
        Object[] lista = this.c.getcomboBoxMercaderia().getSelectedObjects();//       traer todos los items mejor.
        boolean encontrado = false;

        int xx = this.c.getcomboBoxMercaderia().getItemCount();

        for (int i = 0; i < xx; i++) {
            //  System.out.println("elemento seleccionado - "+lista[i]);
            if (cadena.equals(this.c.getcomboBoxMercaderia().getItemAt(i))) {
                //  nn=(String)boxNombre.getItemAt(i).toString(); 
                encontrado = true;
                break;
            }

        }
    return encontrado;

    }
    
     public DefaultComboBoxModel getListaMercaderia(String cadenaEscrita, String campo) {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        try {
            //conexionDB con = new conexionDB();
            //Connection conex = con.conectar();
            Statement st1 = con.createStatement();            
            ResultSet res = st1.executeQuery("select nombre from mercaderia WHERE " + campo + " LIKE '" + cadenaEscrita + "%'");
            //System.out.println("TRAER MERCADERIA: select nombre from mercaderia WHERE " + campo + " LIKE '" + cadenaEscrita + "%'");
            
            while (res.next()) {
                modelo.addElement(res.getString("nombre"));
            }
            res.close();
            //con.close();
        } catch (SQLException ex) {

        }
        return modelo;
    }
     
     public boolean compararTipo(String cadena) {
        Object[] lista = this.c.getcomboBoxTipoCantidad().getSelectedObjects();//       traer todos los items mejor.
        boolean encontrado = false;

        int xx = this.c.getcomboBoxTipoCantidad().getItemCount();

        for (int i = 0; i < xx; i++) {
            //  System.out.println("elemento seleccionado - "+lista[i]);
            if (cadena.equals(this.c.getcomboBoxTipoCantidad().getItemAt(i))) {
                //  nn=(String)boxNombre.getItemAt(i).toString(); 
                encontrado = true;
                break;
            }

        }
    return encontrado;

    }
    
     public DefaultComboBoxModel getListaTipo(String cadenaEscrita, String campo) {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        try {
            conexionDB con = new conexionDB();
            Connection conex = con.conectar();
            Statement st1 = conex.createStatement();            
            ResultSet res = st1.executeQuery("select tipo_cantidad from tipocantidad WHERE " + campo + " LIKE '" + cadenaEscrita + "%'");
            System.out.println("TRAER TIPO MERCADERIA: select tipo from tipomercaderia WHERE " + campo + " LIKE '" + cadenaEscrita + "%'");
            
            while (res.next()) {
                modelo.addElement(res.getString("tipo_cantidad"));
            }
            res.close();
            conex.close();
        } catch (SQLException ex) {

        }
        return modelo;
    }
    



    
}
