/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.conexionDB;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author rmmayer
 *
 */
public class manejadorCtaCte {

    Connection con;

    public manejadorCtaCte(Connection con) {
        this.con = con;
    }

    public float traerCtaCteBoletas(int id_cliente) {
        float totalCta_cte = 0;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sql = "SELECT sum(b.saldo) "
                + "FROM boleta as b "
                + "JOIN cliente as c "
                + "on b.id_cliente = c.id_cliente "
                + "WHERE c.id_cliente = " + id_cliente;

        //System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                totalCta_cte = rs1.getFloat(1);
            }

        } catch (SQLException err) {
            System.out.println("Error: " + err);
        }

        return totalCta_cte;
    }

    public float traerCtaCteDeudaManual(int id_cliente) {
        float totalCta_cte = 0;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sql = "SELECT sum(saldo) from deuda_cta_cte where id_cliente = " + id_cliente;
        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                totalCta_cte = rs1.getFloat(1);
            }

        } catch (SQLException err) {
            System.out.println("Error: " + err);
        }

        return totalCta_cte;
    }

    public float traerCtaCteTotal(int id_cliente) {
        float totalCta_cte = 0;

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sql = "SELECT sum(b.saldo) "
                + "FROM boleta as b "
                + "JOIN cliente as c "
                + "on b.id_cliente = c.id_cliente "
                + "WHERE c.id_cliente = " + id_cliente;

        //System.out.println(sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                totalCta_cte = rs1.getFloat(1);
            }

        } catch (SQLException err) {
            System.out.println("Error: " + err);
        }

        sql = "SELECT sum(saldo) from deuda_cta_cte where id_cliente = " + id_cliente;
        System.out.println(sql);

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                totalCta_cte = totalCta_cte + rs1.getFloat(1);
            }

        } catch (SQLException err) {
            System.out.println("Error: " + err);
        }

        return totalCta_cte;
    }

    public void agregarDeudaManual(int id_cliente, float deuda) {
        Calendar calendario = GregorianCalendar.getInstance();
        Date fecha = calendario.getTime();
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("yyyy/MM/dd HH:mm");
        
        boolean b = false;
        String sql = "SELECT saldo from deuda_cta_cte where id_cliente = " + id_cliente;
        System.out.println(sql);
        String sql2 = "";

        try {
            System.out.println("se debe ingresar el registro");
            sql2 = "INSERT INTO deuda_cta_cte (id_cliente, fecha_deuda, total, saldo) VALUES (" + id_cliente + ",'"+formatoDeFecha.format(fecha)+"'," + deuda + "," + deuda + ")";
            System.out.println(sql2);
            Statement st1 = con.createStatement();
            st1.executeUpdate(sql2);

        } catch (SQLException err) {
            System.out.println("Error: " + err);
        }

    }
    public void filtroResumenCCPorNombre(DefaultTableModel modelo, JTable tablaResumenCC, String busqueda) {

        Object[] o = new Object[3];

        String sql = "SELECT c.id_cliente, c.nombre, sum(b.saldo) "
                + "FROM boleta as b "
                + "JOIN cliente as c ON c.id_cliente = b.id_cliente "
                + "WHERE c.nombre like '%" + busqueda + "%' "
                + "GROUP BY c.id_cliente, c.nombre "
                + "ORDER BY sum(b.saldo) desc";

        System.out.println(sql);

        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                if (rs.getFloat(3) != 0) {
                    o[0] = rs.getInt(1);
                    o[1] = rs.getString(2);
                    o[2] = rs.getFloat(3);

                    modelo.addRow(o);
                }

            }

            String sql2 = "SELECT c.id_cliente, c.nombre, sum(cc.saldo) "
                    + "FROM deuda_cta_cte as cc "
                    + "JOIN cliente as c ON c.id_cliente = cc.id_cliente "
                    + "WHERE c.nombre like '%" + busqueda + "%' "
                    + "GROUP BY c.id_cliente, c.nombre "
                    + "ORDER BY sum(cc.saldo) desc";

            System.out.println(sql2);

            Statement st2 = con.createStatement();
            ResultSet rs2 = st.executeQuery(sql2);
            boolean b = true;
            while (rs2.next()) {
                b = true;
                for (int i = 0; i < tablaResumenCC.getRowCount(); i++) {
                    int id_cliente = Integer.parseInt(tablaResumenCC.getValueAt(i, 0).toString());
                    float nuevaDeuda = Float.parseFloat(tablaResumenCC.getValueAt(i, 2).toString()) + rs2.getFloat(3);
                    if (rs2.getInt(1) == id_cliente) {
                        tablaResumenCC.setValueAt(nuevaDeuda, i, 2);
                        b = false;
                    }
                }

                if (b) {
                    o[0] = rs2.getInt(1);
                    o[1] = rs2.getString(2);
                    o[2] = rs2.getFloat(3);

                    modelo.addRow(o);
                }

            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorTablas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
