/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import Conexion.conexionDB;
import Vista.AgregarPago;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import Vista.CuentaCorriente;
import Vista.PreciosBoletas;
import java.text.SimpleDateFormat;

/**
 *
 * @author Rodri Mayer
 */
public class manejadorTablas {

    Connection con;

    public manejadorTablas(Connection con) {
        this.con = con;
    }

    public class MiModelo extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }

    public void modeloTablaBoletasPedido(JTable tabla, DefaultTableModel modelo) {
        tabla.setModel(modelo);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        modelo.addColumn("Nro");
        modelo.addColumn("Cliente");
        modelo.addColumn("Cant Items");
        modelo.addColumn("Fecha");
        modelo.addColumn("Id_cliente");

        //CODIGO PARA OCULTAR LA COLUMNA DE ID CLIENTE
        tabla.getColumnModel().getColumn(4).setMaxWidth(0);
        tabla.getColumnModel().getColumn(4).setMinWidth(0);
        tabla.getColumnModel().getColumn(4).setPreferredWidth(0);

        tabla.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
    }

    public void cargarTablaBoletasPedido(DefaultTableModel modelo, int id_pedido) {
        int cant = modelo.getRowCount();

        for (int i = 0; i < cant; i++) {
            modelo.removeRow(0);
        }

        Object[] boleta = new Object[5];

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        String sql = "SELECT b.id_boleta, c.nombre, b.cantidad_items, DATE_FORMAT(b.fecha, '%d-%m-%Y'), b.id_cliente "
                + "from boleta as b "
                + "join cliente as c "
                + "on b.id_cliente = c.id_cliente "
                + "where b.id_pedido = " + id_pedido
                + " order by b.id_boleta asc";

        //System.out.println("SQL DE CARGA DE LAS BOLETAS: " + sql);
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                boleta[0] = rs1.getInt(1);
                boleta[1] = rs1.getString(2);
                boleta[2] = rs1.getInt(3);
                boleta[3] = rs1.getString(4);
                boleta[4] = rs1.getInt(5);

                modelo.addRow(boleta);
            }

            //con.close();
        } catch (Exception e) {
            System.err.println("ERROR AL TRAER BOLETAS: " + e);
        }
    }

    public void modeloTablaCargarOtrosCostos(DefaultTableModel modelo, JTable tabla, int id_pedido) {
        modelo.addColumn("ID");
        modelo.addColumn("CONCEPTO");
        modelo.addColumn("COSTO");

        //CODIGO PARA OCULTAR LA COLUMNA ID COSTO
        tabla.getColumnModel().getColumn(0).setMaxWidth(0);
        tabla.getColumnModel().getColumn(0).setMinWidth(0);
        tabla.getColumnModel().getColumn(0).setPreferredWidth(0);

        Object[] costos = new Object[3];

        String sql = "SELECT c.id_gastos, c.concepto, gc.costo_total "
                + "FROM gestion_costo2 as gc "
                + "JOIN concepto_gastos as c "
                + "ON gc.id_gastos = c.id_gastos "
                + "WHERE gc.id_pedido = " + id_pedido + " and gc.id_gastos <> 1";

        //System.out.println(sql);
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                costos[0] = rs1.getInt(1);
                costos[1] = rs1.getString(2);
                costos[2] = rs1.getFloat(3);

                modelo.addRow(costos);

            }

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(manejadorTablas.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    public void modeloTablaVerduraOrden(JTable tabla, DefaultTableModel modelo) {
        tabla.setModel(modelo);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        modelo.addColumn("ORDEN");
        modelo.addColumn("Id");
        modelo.addColumn("VERDURA");

        //CODIGO PARA OCULTAR LA COLUMNA DE ID MERCADERIA        
        tabla.getColumnModel().getColumn(1).setMaxWidth(0);
        tabla.getColumnModel().getColumn(1).setMinWidth(0);
        tabla.getColumnModel().getColumn(1).setPreferredWidth(0);

        tabla.getColumnModel().getColumn(0).setMaxWidth(65);
        tabla.getColumnModel().getColumn(0).setMinWidth(65);
        tabla.getColumnModel().getColumn(0).setPreferredWidth(65);

        tabla.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);

        String sql = "SELECT id_mercaderia, nombre, orden "
                + "FROM mercaderia "
                + "WHERE id_tipo = 1 and inactivo = 0 "
                + "ORDER BY orden asc";

        //System.out.println(sql);
        Object[] verdura = new Object[3];

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                verdura[0] = rs1.getInt(3);
                verdura[1] = rs1.getString(1);
                verdura[2] = rs1.getString(2);

                modelo.addRow(verdura);
            }

            //con.close();
        } catch (Exception e) {
            System.err.println("ERROR AL TRAER LAS VERDURAS: " + e);
        }

    }

    public void modeloTablaFrutaOrden(JTable tabla, DefaultTableModel modelo) {
        tabla.setModel(modelo);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        modelo.addColumn("ORDEN");
        modelo.addColumn("Id");
        modelo.addColumn("FRUTA");

        //CODIGO PARA OCULTAR LA COLUMNA DE ID MERCADERIA        
        tabla.getColumnModel().getColumn(1).setMaxWidth(0);
        tabla.getColumnModel().getColumn(1).setMinWidth(0);
        tabla.getColumnModel().getColumn(1).setPreferredWidth(0);

        tabla.getColumnModel().getColumn(0).setMaxWidth(65);
        tabla.getColumnModel().getColumn(0).setMinWidth(65);
        tabla.getColumnModel().getColumn(0).setPreferredWidth(65);

        tabla.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);

        String sql = "SELECT id_mercaderia, nombre, orden "
                + "FROM mercaderia "
                + "WHERE id_tipo = 2 and inactivo = 0 "
                + "ORDER BY orden asc";

        //System.out.println(sql);
        Object[] verdura = new Object[3];

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                verdura[0] = rs1.getInt(3);
                verdura[1] = rs1.getString(1);
                verdura[2] = rs1.getString(2);

                modelo.addRow(verdura);
            }

            //con.close();
        } catch (Exception e) {
            System.err.println("ERROR AL TRAER LAS FRUTAS: " + e);
        }

    }

    public void modeloTablaBoletasCanceladas(JTable tablaBoletas, CuentaCorriente.ModeloTablaBoletasCanceladas mcc) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaBoletas.setModel(mcc);

        //mcc.addColumn("ID_DET");
        mcc.addColumn("BOLETA");
        mcc.addColumn("TOTAL");

        /*
        tablaBoletas.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaBoletas.getColumnModel().getColumn(0).setMinWidth(0);
        tablaBoletas.getColumnModel().getColumn(0).setPreferredWidth(0);*/
        tablaBoletas.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaBoletas.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

    }

    public void modeloTablaPagosRealizados(JTable tablaBoletas, CuentaCorriente.ModeloTablaPagosRealizados mcc) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaBoletas.setModel(mcc);

        mcc.addColumn("FECHA");
        mcc.addColumn("TOTAL");
        mcc.addColumn("CONDICION");

        tablaBoletas.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaBoletas.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaBoletas.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);

    }

    public void modeloTablaBoletasAdeudadas(JTable tablaBoletas, CuentaCorriente.ModeloTablaBoletasAdeudadas mcc) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);
        DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
        rightRenderer.setHorizontalAlignment(JLabel.RIGHT);

        tablaBoletas.setModel(mcc);

        //mcc.addColumn("ID_DETALLE");
        mcc.addColumn("BOLETA N°");
        mcc.addColumn("TOTAL BOLETA");
        mcc.addColumn("DEBE");
        mcc.addColumn("FECHA");

        /*tablaBoletas.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaBoletas.getColumnModel().getColumn(0).setMinWidth(0);
        tablaBoletas.getColumnModel().getColumn(0).setPreferredWidth(0);*/
        tablaBoletas.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tablaBoletas.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaBoletas.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaBoletas.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        //tablaBoletas.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);

    }

    public void modeloTablaDeudasManuales(JTable tablaPagos, CuentaCorriente.ModeloTablaDeudasManuales modelo) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        tablaPagos.setModel(modelo);
        modelo.addColumn("ID");
        modelo.addColumn("FECHA");
        modelo.addColumn("TOTAL");
        modelo.addColumn("SALDO");

        tablaPagos.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaPagos.getColumnModel().getColumn(0).setMinWidth(0);
        tablaPagos.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablaPagos.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaPagos.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaPagos.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);

    }

    public void modeloTablaBoletasPrecios(JTable tablaBoletasPrecios, PreciosBoletas.MiModelo modelo) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        tablaBoletasPrecios.setModel(modelo);
        modelo.addColumn("ID_DETALLE");
        modelo.addColumn("ID_MERCA");
        modelo.addColumn("MERCADERIA");
        modelo.addColumn("CANTIDAD");
        modelo.addColumn("TIPO CANTIDAD");
        modelo.addColumn("PRECIO COSTO");
        modelo.addColumn("PRECIO CLIENTE");

        tablaBoletasPrecios.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaBoletasPrecios.getColumnModel().getColumn(0).setMinWidth(0);
        tablaBoletasPrecios.getColumnModel().getColumn(0).setPreferredWidth(0);
        tablaBoletasPrecios.getColumnModel().getColumn(1).setMaxWidth(0);
        tablaBoletasPrecios.getColumnModel().getColumn(1).setMinWidth(0);
        tablaBoletasPrecios.getColumnModel().getColumn(1).setPreferredWidth(0);

        tablaBoletasPrecios.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaBoletasPrecios.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        tablaBoletasPrecios.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
        tablaBoletasPrecios.getColumnModel().getColumn(5).setCellRenderer(centerRenderer);
        tablaBoletasPrecios.getColumnModel().getColumn(6).setCellRenderer(centerRenderer);
    }

    public void modeloTablaPagos(JTable tablaPagos, AgregarPago.ModeloTablaPagos modelo) {
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        tablaPagos.setModel(modelo);
        modelo.addColumn("ID_CONDVTA");
        modelo.addColumn("CONDICION");
        modelo.addColumn("MONTO");

        tablaPagos.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaPagos.getColumnModel().getColumn(0).setMinWidth(0);
        tablaPagos.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablaPagos.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaPagos.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);

    }

    public void traerBoletasAdeudadas(DefaultTableModel modelo, int id_cliente) {
        Object[] a = new Object[5];
        manejadorFechas mfe = new manejadorFechas(con);

        /*String sql = "SELECT d.id_detalle, d.id_cta_cte, d.id_boleta, d.saldo, b.fecha_boleta, b.total "
                + "FROM DETALLE_CTA_CTE as d "
                + "JOIN BOLETA as b "
                + "ON d.id_boleta = b.id_boleta "
                + "WHERE d.id_cta_cte = " + id_cta_cte + " and d.saldo > 0";*/
        String sql = "SELECT b.id_boleta, b.id_cliente, c.nombre, b.fecha, b.total, b.saldo "
                + "FROM boleta as b "
                + "JOIN cliente as c on b.id_cliente = c.id_cliente "
                + "WHERE b.id_cliente = " + id_cliente + " and saldo > 0";

        System.out.println(sql);
        int cant = modelo.getRowCount();

        for (int i = 0; i < cant; i++) {
            modelo.removeRow(0);
        }

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                a[0] = rs1.getInt(1);
                a[1] = rs1.getFloat(5);
                a[2] = rs1.getFloat(6);
                a[3] = mfe.convertirFecha2(rs1.getString(4));;

                modelo.addRow(a);
            }

            //con.close();
        } catch (Exception e) {
            System.err.println("ERROR TRAYENDO ARTICULOS: " + e);
        }
    }

    public void traerBoletasCtaCtePagadas(DefaultTableModel modelo, int id_cliente) {
        try {
            Object[] o = new Object[2];
            String sql = "SELECT id_boleta, total from boleta where id_cliente = " + id_cliente + " and total <> 0 and saldo = 0";
            System.out.println(sql);

            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                o[0] = rs.getInt(1);
                o[1] = rs.getFloat(2);

                modelo.addRow(o);
            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorTablas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void traerPagosCtaCte(DefaultTableModel modelo, int id_cliente) {
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");
        Object[] o = new Object[3];

        String sql = "SELECT f.fecha_factCtaCte, f.monto, cv.desc_condVta "
                + "FROM facturacion_ctacte as f "
                + "JOIN condicion_vta as cv "
                + "ON f.id_condVta = cv.id_condVta "
                + "WHERE id_cliente = " + id_cliente;

        System.out.println(sql);

        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                o[0] = formatoDeFecha.format(rs.getDate(1));
                o[1] = rs.getFloat(2);
                o[2] = rs.getString(3);

                modelo.addRow(o);
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorTablas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void traerDeudasManuales(DefaultTableModel modelo, int id_cliente) {
        SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");
        Object[] o = new Object[4];

        String sql = "SELECT id_deudaCtaCte, fecha_deuda, total, saldo "
                + "FROM deuda_cta_cte "
                + "WHERE id_cliente = " + id_cliente;

        System.out.println(sql);

        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                o[0] = rs.getInt(1);
                o[1] = formatoDeFecha.format(rs.getDate(2));
                o[2] = rs.getFloat(3);
                o[3] = rs.getFloat(4);

                modelo.addRow(o);
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorTablas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void modeloTablaResumenCC(JTable tabla, DefaultTableModel modelo) {
        tabla.setModel(modelo);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        modelo.addColumn("ID");
        modelo.addColumn("Cliente");
        //modelo.addColumn("Deuda Manual");
        //modelo.addColumn("Deuda Boletas");
        modelo.addColumn("Total");

        //CODIGO PARA OCULTAR LA COLUMNA DE ID CLIENTE
        tabla.getColumnModel().getColumn(0).setMaxWidth(0);
        tabla.getColumnModel().getColumn(0).setMinWidth(0);
        tabla.getColumnModel().getColumn(0).setPreferredWidth(0);

        tabla.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        //tabla.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        //tabla.getColumnModel().getColumn(4).setCellRenderer(centerRenderer);
    }

    public void traerResumenCC(DefaultTableModel modelo, JTable tablaResumenCC) {

        Object[] o = new Object[3];

        String sql = "SELECT c.id_cliente, c.nombre, sum(b.saldo) "
                + "FROM boleta as b "
                + "JOIN cliente as c ON c.id_cliente = b.id_cliente "
                + "GROUP BY c.id_cliente, c.nombre "
                + "ORDER BY sum(b.saldo) desc";

        System.out.println(sql);

        try {
            Statement st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {
                if (rs.getFloat(3) != 0) {
                    o[0] = rs.getInt(1);
                    o[1] = rs.getString(2);
                    o[2] = rs.getFloat(3);

                    modelo.addRow(o);
                }

            }

            String sql2 = "SELECT c.id_cliente, c.nombre, sum(cc.saldo) "
                    + "FROM deuda_cta_cte as cc "
                    + "JOIN cliente as c ON c.id_cliente = cc.id_cliente "
                    + "GROUP BY c.id_cliente, c.nombre "
                    + "ORDER BY sum(cc.saldo) desc";

            System.out.println(sql2);

            Statement st2 = con.createStatement();
            ResultSet rs2 = st.executeQuery(sql2);
            boolean b = true;
            while (rs2.next()) {
                b = true;
                for (int i = 0; i < tablaResumenCC.getRowCount(); i++) {
                    int id_cliente = Integer.parseInt(tablaResumenCC.getValueAt(i, 0).toString());
                    float nuevaDeuda = Float.parseFloat(tablaResumenCC.getValueAt(i, 2).toString()) + rs2.getFloat(3);
                    if (rs2.getInt(1) == id_cliente) {
                        tablaResumenCC.setValueAt(nuevaDeuda, i, 2);
                        b = false;
                    }
                }

                if (b) {
                    o[0] = rs2.getInt(1);
                    o[1] = rs2.getString(2);
                    o[2] = rs2.getFloat(3);

                    modelo.addRow(o);
                }

            }

        } catch (SQLException ex) {
            Logger.getLogger(manejadorTablas.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void modeloTablaBoletasZonas(JTable tabla, DefaultTableModel modelo, int zona, String id_usuario) {
        tabla.setModel(modelo);
        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        modelo.addColumn("Nro");
        modelo.addColumn("Cliente");
        modelo.addColumn("Id_cliente");

        //CODIGO PARA OCULTAR LA COLUMNA DE ID CLIENTE
        tabla.getColumnModel().getColumn(2).setMaxWidth(0);
        tabla.getColumnModel().getColumn(2).setMinWidth(0);
        tabla.getColumnModel().getColumn(2).setPreferredWidth(0);

        tabla.getColumnModel().getColumn(0).setCellRenderer(centerRenderer);
        tabla.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);

        String sql = "SELECT b.id_boleta, c.nombre, c.id_cliente "
                + "FROM boleta as b "
                + "JOIN cliente as c ON b.id_cliente = c.id_cliente "
                + "WHERE zona = "+zona+" and b.id_pedido = 0 and b.id_usuario = '"+id_usuario+"'";
        
        Statement st;
        try {
            st = con.createStatement();
            ResultSet rs = st.executeQuery(sql);
            Object[] o = new Object[3];
            
            while(rs.next()){
                o[0] = rs.getInt(1);
                o[1] = rs.getString(2);
                o[2] = rs.getInt(3);
                
                modelo.addRow(o);
                
            }
        } catch (SQLException ex) {
            Logger.getLogger(manejadorTablas.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

}
