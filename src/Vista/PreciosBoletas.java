/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Conexion.conexionDB;
import Controlador.*;
import Modelo.Boleta;
import Modelo.Cliente;
import Modelo.Localidad;
import Modelo.Mercaderia;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Administrador
 */
public class PreciosBoletas extends javax.swing.JFrame {

    /**
     * Creates new form CargarPedidoDidactico
     */
    //metodoBusquedaCliente mb = new metodoBusquedaCliente(this);
    Connection con;

    manejadorNumeros mn = new manejadorNumeros();

    String codigo;
    Cliente cli = new Cliente();
    Mercaderia m = new Mercaderia();
    Localidad l = new Localidad();
    Boleta bo = new Boleta();
    JTable tablaBoletasSinCerrar;
    JTable tablaBoletasCtaCte;
    boolean primerNumero = true;
    boolean start = true;
    boolean editado = false;

    public class MiModelo extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            if (column == 6) {
                return true;
            } else {
                return false;
            }

        }
    }
    MiModelo modelo = new MiModelo();

    DefaultTableModel modeloBoletasPedido = new DefaultTableModel();
    DefaultTableModel modeloInicio = new DefaultTableModel();
    int id_pedido;
    int tipo;
    int tam;
    int filaS;

    public PreciosBoletas(JTable tablaBoletasSinCerrar, JTable tablaBoletasCtaCte, int tipo, Connection con) {
        //TIPO 1: CARGAR PEDIDO NUEVO 
        //TIPO 2: EDITAR PEDIDO

        initComponents();
        this.con = con;
        this.modeloBoletasPedido = modeloBoletasPedido;
        this.modeloInicio = modeloInicio;
        manejadorCliente mc = new manejadorCliente(con);
        manejadorLocalidad ml = new manejadorLocalidad(con);
        manejadorBoleta mbol = new manejadorBoleta(con);
        manejadorTablas mt = new manejadorTablas(con);
        manejadorCtaCte mcc = new manejadorCtaCte(con);
        //bo = ;

        this.setResizable(false);
        this.id_pedido = id_pedido;
        this.tipo = tipo;

        this.tablaBoletasSinCerrar = tablaBoletasSinCerrar;
        this.tablaBoletasCtaCte = tablaBoletasCtaCte;

        mt.modeloTablaBoletasPrecios(tablaResumenPedido, modelo);

        if (tipo == 1) {
            filaS = tablaBoletasSinCerrar.getSelectedRow();

            if (filaS == 0) {
                this.btnBack.setEnabled(false);
                this.btnNext.setEnabled(true);
            }

            mbol.traerBoleta(Integer.parseInt(tablaBoletasSinCerrar.getValueAt(filaS, 0).toString()), bo);

            tam = tablaBoletasSinCerrar.getRowCount();
            System.out.println("FILA SELECCIONADA -> " + filaS);
            System.out.println("TAMAÑO -> " + tam);
            if (filaS == tam - 1) {
                //this.botonGuardarPedido.setEnabled(false);
                this.botonGuardarPedido.setText("FINALIZAR REVISION");
                this.btnBack.setEnabled(true);
                this.btnNext.setEnabled(false);

            } else {
                this.botonGuardarPedido.setEnabled(true);
            }
        } else {
            //this.botonGuardarPedido.setText("CARGAR PRECIO MANUAL");
            this.botonGuardarPedido.setVisible(false);
            this.btnBack.setVisible(false);
            this.btnNext.setVisible(false);
            filaS = tablaBoletasCtaCte.getSelectedRow();
            this.btnVerCtaCte.setVisible(false);

            if (filaS == 0) {
                this.btnBack.setEnabled(false);
                this.btnNext.setEnabled(true);
            }

            mbol.traerBoleta(Integer.parseInt(tablaBoletasCtaCte.getValueAt(filaS, 0).toString()), bo);

            tam = tablaBoletasCtaCte.getRowCount();
            System.out.println("FILA SELECCIONADA -> " + filaS);
            System.out.println("TAMAÑO -> " + tam);
            if (filaS == tam - 1) {
                //this.botonGuardarPedido.setEnabled(false);
                //this.botonGuardarPedido.setText("FINALIZAR REVISION");
                this.btnBack.setEnabled(true);
                this.btnNext.setEnabled(false);

            } else {
                //this.botonGuardarPedido.setEnabled(true);
            }
        }

        this.setTitle("CARGA DE PRECIOS BOLETA N° " + bo.getId_boleta());
        cli = mc.obtenerCliente(bo.getId_cliente());
        labelCliente.setText(cli.getNombre());
        labelDireccion.setText(cli.getDireccion());
        labelLocalidad.setText(ml.obtenerLocalidad(cli.getIdLocalidad()).getLocalidad());

        float deuda = mcc.traerCtaCteTotal(cli.getIdCliente());
        if (deuda < 0) {
            this.labelTotalCtaCte.setBackground(Color.RED);
        }
        this.labelTotalCtaCte.setText("$ " + deuda);

        mbol.traerResumenBoletaPrecios(modelo, bo.getId_boleta(), bo.getIdPedido());

        //this.botonGuardarPedido.setText("GUARDAR EDICION");
        //this.botonGuardarPedido.setEnabled(true);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        labelDireccion = new javax.swing.JLabel();
        labelLocalidad = new javax.swing.JLabel();
        labelCliente = new javax.swing.JLabel();
        panelResumenPedido = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaResumenPedido = new javax.swing.JTable();
        btnBack = new javax.swing.JButton();
        btnNext = new javax.swing.JButton();
        botonGuardarPedido = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        btnVerCtaCte = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        labelTotalCtaCte = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos del Cliente"));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("CLIENTE SELECCIONADO:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setText("DIRECCION:");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setText("LOCALIDAD:");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        labelDireccion.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelDireccion.setText("jLabel10");

        labelLocalidad.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelLocalidad.setText("jLabel10");

        labelCliente.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelCliente.setText("jLabel10");
        labelCliente.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                labelClientePropertyChange(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel9)
                    .addComponent(jLabel8))
                .addGap(60, 60, 60)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelLocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel10)
                    .addComponent(labelCliente))
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(labelDireccion))
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(labelLocalidad))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelResumenPedido.setBorder(javax.swing.BorderFactory.createTitledBorder("Resumen Pedido"));

        tablaResumenPedido.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "Mercaderia", "Cantidad", "Tipo"
            }
        ));
        tablaResumenPedido.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaResumenPedidoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaResumenPedido);

        btnBack.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/backward.png"))); // NOI18N
        btnBack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnBackActionPerformed(evt);
            }
        });

        btnNext.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/forward.png"))); // NOI18N
        btnNext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNextActionPerformed(evt);
            }
        });

        botonGuardarPedido.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        botonGuardarPedido.setText("GUARDAR E IMPRIMIR");
        botonGuardarPedido.setPreferredSize(new java.awt.Dimension(133, 25));
        botonGuardarPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarPedidoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelResumenPedidoLayout = new javax.swing.GroupLayout(panelResumenPedido);
        panelResumenPedido.setLayout(panelResumenPedidoLayout);
        panelResumenPedidoLayout.setHorizontalGroup(
            panelResumenPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelResumenPedidoLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelResumenPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelResumenPedidoLayout.createSequentialGroup()
                        .addComponent(btnBack)
                        .addGap(20, 20, 20)
                        .addComponent(botonGuardarPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 521, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(20, 20, 20)
                        .addComponent(btnNext))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );

        panelResumenPedidoLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btnBack, btnNext});

        panelResumenPedidoLayout.setVerticalGroup(
            panelResumenPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelResumenPedidoLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 394, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 18, Short.MAX_VALUE)
                .addGroup(panelResumenPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelResumenPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(btnBack, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnNext, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(botonGuardarPedido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        panelResumenPedidoLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btnBack, btnNext});

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Cuenta Corriente"));

        btnVerCtaCte.setText("VER DETALLE");
        btnVerCtaCte.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerCtaCteActionPerformed(evt);
            }
        });

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("TOTAL CUENTA CORRIENTE");

        labelTotalCtaCte.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTotalCtaCte.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnVerCtaCte, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelTotalCtaCte, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel11)
                .addGap(27, 27, 27)
                .addComponent(labelTotalCtaCte, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(btnVerCtaCte, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel11, labelTotalCtaCte});

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(panelResumenPedido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(5, 5, 5))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(panelResumenPedido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void labelClientePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_labelClientePropertyChange
        // TODO add your handling code here
        /*
         * if (this.comboBoxNombreCliente.getSelectedIndex() != -1 &&
         * this.comboBoxNombreCliente.getSelectedItem().toString().equals(this.labelCliente.getText()))
         * { panelMercaderia.setVisible(true); panelCliente.setEnabled(false);
         * this.comboBoxNombreCliente.setEditable(false);
         * comboBoxNombreCliente.setEnabled(false);
         * this.comboBoxTipoMercaderia.setVisible(true);
         * this.comboBoxMercaderia.setVisible(true);
         * comboBoxTipoMercaderia.setEnabled(true);
         * comboBoxTipoMercaderia.setEditable(true);
         * //comboBoxMercaderia.setEnabled(true);
         * //comboBoxMercaderia.setEditable(true); }
         */
    }//GEN-LAST:event_labelClientePropertyChange

    private void botonGuardarPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarPedidoActionPerformed
        // TODO add your handling code here:
        manejadorBoleta mbol = new manejadorBoleta(con);

        if (tablaResumenPedido.isEditing()) {
            tablaResumenPedido.getCellEditor().stopCellEditing();
        }

        if (this.botonGuardarPedido.getText().equals("GUARDAR E IMPRIMIR")) {

            boolean correcto = true;

            for (int i = 0; i < this.tablaResumenPedido.getRowCount(); i++) {
                for (int j = 5; j < this.tablaResumenPedido.getColumnCount(); j++) {

                    if (tablaResumenPedido.getValueAt(i, j).toString().equals("") || !(mn.isNumeric(tablaResumenPedido.getValueAt(i, j).toString()))) {
                        correcto = false;
                    }
                }
            }

            if (correcto) {
                for (int i = 0; i < this.tablaResumenPedido.getRowCount(); i++) {
                    float precioU = Float.parseFloat(this.tablaResumenPedido.getValueAt(i, 6).toString());
                    int id_det = Integer.parseInt(this.tablaResumenPedido.getValueAt(i, 0).toString());
                    mbol.actualizarPrecios(id_det, precioU);

                }
                mbol.actualizarTotalBoleta(bo.getId_boleta());

                this.tablaBoletasSinCerrar.setRowSelectionInterval(filaS + 1, filaS + 1);
                filaS++;

                if (filaS < tam) {

                    Boleta boleta = new Boleta();
                    int id_boleta = Integer.parseInt(tablaBoletasSinCerrar.getValueAt(filaS, 0).toString());
                    mbol.traerBoleta(id_boleta, boleta);

                    JOptionPane.showMessageDialog(null, "BOLETA ACTUALIZADA CON EXITO");

                    PreciosBoletas cp = new PreciosBoletas(tablaBoletasSinCerrar, null, 1, con);
                    cp.setLocationRelativeTo(null);
                    cp.setVisible(true);
                    this.dispose();

                }

                //mbol.imprimirBoleta(bo);
            } else {
                JOptionPane.showMessageDialog(null, "EXISTEN PRECIOS VACIOS O CON VALORES INCORRECTOS");
            }

        } else {
            boolean correcto = true;

            for (int i = 0; i < this.tablaResumenPedido.getRowCount(); i++) {
                for (int j = 5; j < this.tablaResumenPedido.getColumnCount(); j++) {

                    if (tablaResumenPedido.getValueAt(i, j).toString().equals("") || !(mn.isNumeric(tablaResumenPedido.getValueAt(i, j).toString()))) {
                        correcto = false;
                    }
                }
            }

            if (correcto) {
                for (int i = 0; i < this.tablaResumenPedido.getRowCount(); i++) {
                    float precioU = Float.parseFloat(this.tablaResumenPedido.getValueAt(i, 6).toString());
                    int id_det = Integer.parseInt(this.tablaResumenPedido.getValueAt(i, 0).toString());
                    mbol.actualizarPrecios(id_det, precioU);

                }
                mbol.actualizarTotalBoleta(bo.getId_boleta());               
                JOptionPane.showMessageDialog(null, "BOLETA ACTUALIZADA CON EXITO");
                this.dispose();

                //mbol.imprimirBoleta(bo);
            } else {
                JOptionPane.showMessageDialog(null, "EXISTEN PRECIOS VACIOS O CON VALORES INCORRECTOS");
            }
        }


    }//GEN-LAST:event_botonGuardarPedidoActionPerformed

    private void tablaResumenPedidoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaResumenPedidoMouseClicked
        // TODO add your handling code here:

    }//GEN-LAST:event_tablaResumenPedidoMouseClicked

    private void btnNextActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNextActionPerformed
        // TODO add your handling code here:
        manejadorBoleta mbol = new manejadorBoleta(con);
        if (tipo == 1) {
            this.tablaBoletasSinCerrar.setRowSelectionInterval(filaS + 1, filaS + 1);
            filaS++;

            if (filaS < tam) {

                Boleta boleta = new Boleta();
                int id_boleta = Integer.parseInt(tablaBoletasSinCerrar.getValueAt(filaS, 0).toString());
                mbol.traerBoleta(id_boleta, boleta);

                //JOptionPane.showMessageDialog(null, "BOLETA ACTUALIZADA CON EXITO");
                PreciosBoletas cp = new PreciosBoletas(tablaBoletasSinCerrar, null, 1, con);
                cp.setLocationRelativeTo(null);
                cp.setVisible(true);
                this.dispose();

            }

            if (tablaResumenPedido.isEditing()) {
                tablaResumenPedido.getCellEditor().stopCellEditing();
            }
        } else {
            this.tablaBoletasCtaCte.setRowSelectionInterval(filaS + 1, filaS + 1);
            filaS++;

            if (filaS < tam) {

                Boleta boleta = new Boleta();
                int id_boleta = Integer.parseInt(tablaBoletasCtaCte.getValueAt(filaS, 0).toString());
                mbol.traerBoleta(id_boleta, boleta);

                //JOptionPane.showMessageDialog(null, "BOLETA ACTUALIZADA CON EXITO");
                PreciosBoletas cp = new PreciosBoletas(tablaBoletasCtaCte, null, 1, con);
                cp.setLocationRelativeTo(null);
                cp.setVisible(true);
                this.dispose();

            }

            if (tablaResumenPedido.isEditing()) {
                tablaResumenPedido.getCellEditor().stopCellEditing();
            }
        }


        /*
        if (this.botonGuardarPedido.getText().equals("GUARDAR E IMPRIMIR")) {

            boolean correcto = true;

            for (int i = 0; i < this.tablaResumenPedido.getRowCount(); i++) {
                for (int j = 5; j < this.tablaResumenPedido.getColumnCount(); j++) {

                    if (tablaResumenPedido.getValueAt(i, j).toString().equals("") || !(mn.isNumeric(tablaResumenPedido.getValueAt(i, j).toString()))) {
                        correcto = false;
                    }
                }
            }

            if (correcto) {
                for (int i = 0; i < this.tablaResumenPedido.getRowCount(); i++) {
                    float precioU = Float.parseFloat(this.tablaResumenPedido.getValueAt(i, 6).toString());
                    int id_det = Integer.parseInt(this.tablaResumenPedido.getValueAt(i, 0).toString());
                    mbol.actualizarPrecios(id_det, precioU);

                }

                this.tablaBoletasSinCerrar.setRowSelectionInterval(filaS + 1, filaS + 1);
                filaS++;

                if (filaS < tam) {

                    Boleta boleta = new Boleta();
                    int id_boleta = Integer.parseInt(tablaBoletasSinCerrar.getValueAt(filaS, 0).toString());
                    mbol.traerBoleta(id_boleta, boleta);

                    JOptionPane.showMessageDialog(null, "BOLETA ACTUALIZADA CON EXITO");

                    PreciosBoletas cp = new PreciosBoletas(tablaBoletasSinCerrar);
                    cp.setLocationRelativeTo(null);
                    cp.setVisible(true);
                    this.dispose();

                }
                
                //mbol.imprimirBoleta(bo);
            }else{
                JOptionPane.showMessageDialog(null, "EXISTEN PRECIOS VACIOS O CON VALORES INCORRECTOS");
            }

        }*/
    }//GEN-LAST:event_btnNextActionPerformed

    private void btnBackActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnBackActionPerformed
        // TODO add your handling code here:
        manejadorBoleta mbol = new manejadorBoleta(con);
        if (tipo == 1) {
            this.tablaBoletasSinCerrar.setRowSelectionInterval(filaS - 1, filaS - 1);
            filaS--;

            if (filaS < tam) {

                Boleta boleta = new Boleta();
                int id_boleta = Integer.parseInt(tablaBoletasSinCerrar.getValueAt(filaS, 0).toString());
                mbol.traerBoleta(id_boleta, boleta);

                //JOptionPane.showMessageDialog(null, "BOLETA ACTUALIZADA CON EXITO");
                PreciosBoletas cp = new PreciosBoletas(tablaBoletasSinCerrar, null, 1, con);
                cp.setLocationRelativeTo(null);
                cp.setVisible(true);
                this.dispose();

            }
        } else {
            this.tablaBoletasCtaCte.setRowSelectionInterval(filaS - 1, filaS - 1);
            filaS--;

            if (filaS < tam) {

                Boleta boleta = new Boleta();
                int id_boleta = Integer.parseInt(tablaBoletasCtaCte.getValueAt(filaS, 0).toString());
                mbol.traerBoleta(id_boleta, boleta);

                //JOptionPane.showMessageDialog(null, "BOLETA ACTUALIZADA CON EXITO");
                PreciosBoletas cp = new PreciosBoletas(tablaBoletasCtaCte, null, 1, con);
                cp.setLocationRelativeTo(null);
                cp.setVisible(true);
                this.dispose();

            }
        }


    }//GEN-LAST:event_btnBackActionPerformed

    private void btnVerCtaCteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerCtaCteActionPerformed
        // TODO add your handling code here:

        CuentaCorriente cta = new CuentaCorriente(cli, con);
        cta.setLocationRelativeTo(null);
        cta.setVisible(true);
    }//GEN-LAST:event_btnVerCtaCteActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(PreciosBoletas.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(PreciosBoletas.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(PreciosBoletas.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(PreciosBoletas.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new PreciosBoletas(null, null, 1, null).setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonGuardarPedido;
    private javax.swing.JButton btnBack;
    private javax.swing.JButton btnNext;
    private javax.swing.JButton btnVerCtaCte;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelCliente;
    private javax.swing.JLabel labelDireccion;
    private javax.swing.JLabel labelLocalidad;
    private javax.swing.JLabel labelTotalCtaCte;
    private javax.swing.JPanel panelResumenPedido;
    private javax.swing.JTable tablaResumenPedido;
    // End of variables declaration//GEN-END:variables
}
