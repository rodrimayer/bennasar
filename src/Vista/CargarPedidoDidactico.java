/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Conexion.conexionDB;
import Controlador.*;
import Modelo.Boleta;
import Modelo.Cliente;
import Modelo.Localidad;
import Modelo.Mercaderia;
import Modelo.Usuario;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Administrador
 */
public class CargarPedidoDidactico extends javax.swing.JFrame {

    /**
     * Creates new form CargarPedidoDidactico
     */
    //metodoBusquedaCliente mb = new metodoBusquedaCliente(this);
    Connection con;

    String codigo;
    Cliente cli = new Cliente();
    Mercaderia m = new Mercaderia();
    Localidad l = new Localidad();
    Boleta bo = new Boleta();
    JTable tablaBoletasSinCerrar;
    boolean primerNumero = true;
    boolean start = true;
    boolean editado = false;

    public class MiModelo extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            return false;
        }
    }
    MiModelo modelo = new MiModelo();

    DefaultTableModel modeloBoletasPedido = new DefaultTableModel();
    DefaultTableModel modeloInicio = new DefaultTableModel();
    Usuario user = new Usuario();
    int id_pedido;
    int tipo;
    ArrayList<Cliente> arrayClientes;
    ArrayList<Mercaderia> arrayMercaderias;

    public CargarPedidoDidactico(JTable tablaBoletasSinCerrar, int id_pedido, Boleta boleta, int tipo, InicioNuevo.MiModelo modeloInicio, BoletasPedidos.MiModelo modeloBoletasPedido, Connection con, Usuario user) {
        //TIPO 1: CARGAR PEDIDO NUEVO 
        //TIPO 2: EDITAR PEDIDO
        //TIPO 3: VISTA PEDIDO

        initComponents();
        this.modeloBoletasPedido = modeloBoletasPedido;
        this.modeloInicio = modeloInicio;
        this.con = con;
        this.user = user;
        bo = boleta;

        this.setResizable(false);
        this.id_pedido = id_pedido;
        txtTotalCant.setEditable(true);
        this.tipo = tipo;

        this.tablaBoletasSinCerrar = tablaBoletasSinCerrar;
        final metodoBusquedaCliente mb = new metodoBusquedaCliente(this, 1, con);
        final metodoBusquedaMercaderia mm = new metodoBusquedaMercaderia(this, con);
        manejadorCliente mc = new manejadorCliente(con);
        manejadorLocalidad ml = new manejadorLocalidad(con);
        manejadorMercaderia manM = new manejadorMercaderia(con);
        manejadorBoleta mbol = new manejadorBoleta(con);
        manejadorCtaCte mcc = new manejadorCtaCte(con);

        DefaultTableCellRenderer centerRenderer = new DefaultTableCellRenderer();
        centerRenderer.setHorizontalAlignment(JLabel.CENTER);

        this.tablaResumenPedido.setVisible(true);
        botonEditarCliente.setEnabled(false);
        //botonEditarMercaderia.setVisible(true);
        //botonEditarMercaderia.setEnabled(false);
        botonEliminarMercaderia.setEnabled(false);
        botonAceptarMercaderia.setEnabled(false);
        txtTotalCant.setText("");
        botonComa.setEnabled(false);
        boton0.setEnabled(false);
        Dimension d = new Dimension(506, 121);
        tablaResumenPedido.setModel(modelo);
        modelo.addColumn("ID");
        modelo.addColumn("MERCADERIA");
        modelo.addColumn("CANTIDAD");
        modelo.addColumn("TIPO CANTIDAD");

        tablaResumenPedido.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaResumenPedido.getColumnModel().getColumn(0).setMinWidth(0);
        tablaResumenPedido.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablaResumenPedido.getColumnModel().getColumn(1).setCellRenderer(centerRenderer);
        tablaResumenPedido.getColumnModel().getColumn(2).setCellRenderer(centerRenderer);
        tablaResumenPedido.getColumnModel().getColumn(3).setCellRenderer(centerRenderer);
        //tablaResumenPedido.setMaximumSize(d);
        //tablaResumenPedido.setMinimumSize(d);
        //tablaResumenPedido.setPreferredSize(d);
        panelResumenPedido.setMaximumSize(d);
        panelResumenPedido.setMinimumSize(d);
        panelResumenPedido.setSize(d);
        panelResumenPedido.setBounds(panelResumenPedido.getX(), panelResumenPedido.getY(), 506, 121);
        panelResumenPedido.setPreferredSize(d);

        comboBoxMercaderia.setEnabled(false);
        comboBoxMercaderia.setEditable(false);

        boton1.setEnabled(false);
        boton2.setEnabled(false);
        boton3.setEnabled(false);
        boton4.setEnabled(false);
        boton5.setEnabled(false);
        boton6.setEnabled(false);
        boton7.setEnabled(false);
        boton8.setEnabled(false);
        boton9.setEnabled(false);
        boton0.setEnabled(false);
        botonComa.setEnabled(false);
        botonBorrar.setEnabled(false);
        botonGuardarPedido.setEnabled(false);

        arrayClientes = new ArrayList<>();
        arrayMercaderias = new ArrayList<>();

        mc.traerClientesArray(arrayClientes);
        manM.traerMercaderiasArray(arrayMercaderias);

        /*
        for (int i = 0; i < arrayClientes.size(); i++) {
            System.out.println(i + " cliente: " + arrayClientes.get(i).getNombre());
        }*/
        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        mbol.traerTipoCantidadComboBox(comboBoxTipoCantidad);

        /*String sql = "SELECT tipo_cantidad from tipocantidad";

        try {

            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                comboBoxTipoCantidad.addItem(rs1.getString(1));
                comboBoxTipoCantidad.setSelectedIndex(0);
            }

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }*/
        if (tipo == 1) {

            this.setTitle("PEDIDO NUEVO");
            labelCliente.setText("");
            labelDireccion.setText("");
            labelLocalidad.setText("");

        } else {
            this.setTitle("EDITAR PEDIDO N° " + boleta.getId_boleta());
            cli = mc.obtenerCliente(boleta.getId_cliente());
            labelCliente.setText(cli.getNombre());
            labelDireccion.setText(cli.getDireccion());
            labelLocalidad.setText(ml.obtenerLocalidad(cli.getIdLocalidad()).getLocalidad());
            this.comboBoxNombreCliente.getEditor().setItem(cli.getNombre());
            float deuda = mcc.traerCtaCteTotal(cli.getIdCliente());
            if (deuda < 0) {
                this.labelTotalCtaCte.setBackground(Color.RED);
            }
            this.labelTotalCtaCte.setText("$ " + deuda);
            mbol.traerResumenBoleta(modelo, boleta.getId_boleta());
            this.comboBoxNombreCliente.setEnabled(false);
            this.comboBoxNombreCliente.setEditable(false);
            this.botonEditarCliente.setEnabled(false);
            this.botonGuardarPedido.setText("GUARDAR EDICION");

            this.comboBoxZona.setSelectedIndex(boleta.getZona() - 1);
            this.comboBoxMercaderia.requestFocus();
            this.botonGuardarPedido.setEnabled(true);
        }

        String sqlClientes = "SELECT nombre from cliente";

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sqlClientes);

            while (rs1.next()) {
                comboBoxNombreCliente.addItem(rs1.getString(1));
            }

            comboBoxNombreCliente.setSelectedIndex(-1);
            start = false;

            //con.close();
        } catch (SQLException ex) {
            Logger.getLogger(CargarPedidoDidactico.class.getName()).log(Level.SEVERE, null, ex);
        }

        this.comboBoxNombreCliente.getEditor()
                .getEditorComponent().addKeyListener(new KeyAdapter() {

                    @Override
                    public void keyReleased(KeyEvent evt
                    ) {

                        String campo;
                        String cadenaEscrita = comboBoxNombreCliente.getEditor().getItem().toString();

                        //System.out.println(cadenaEscrita);
                        campo = "nombre";

                        if (evt.getKeyChar() == KeyEvent.VK_ENTER || evt.getKeyChar() == KeyEvent.VK_TAB) {
                            //JOptionPane.showMessageDialog(null, "APRETO tab");
                            if (mb.comparar(cadenaEscrita, 1)) {// compara si el texto escrito se encuentra en la lista
                                // busca el texto escrito en la base de datos                      
                                System.out.println("encontro el cliente exacto");
                                //obtenerCliente(cadenaEscrita);
                                comboBoxZona.setEnabled(true);
                                comboBoxZona.setEditable(true);
                                comboBoxZona.requestFocus();
                                cadenaEscrita = comboBoxNombreCliente.getEditor().getItem().toString();
                                //cli = mc.obtenerClienteArray(cadenaEscrita, arrayClientes);
                                Object ob = comboBoxNombreCliente.getSelectedItem();
                                System.out.println(ob.toString());

                                //cli = (Cliente) comboBoxNombreCliente.getS;
                                //cli.setNombre((Cliente) comboBoxNombreCliente.getItemAt(1).); = ;
                                if (ob instanceof Cliente) {
                                    cli = (Cliente) ob;
                                }
                                System.out.println("cliente: " + cli.getNombre());
                            } else {// en caso contrario toma como default el elemento 0 o sea el primero de la lista y lo busca.
                                if (comboBoxNombreCliente.getItemAt(0) != null) {
                                    //comboBoxNombreCliente.getEditor().getItem().toString();
                                    System.out.println("no lo encontro");
                                    //obtenerCliente(cadenaEscrita);
                                    //comboBoxNombreCliente.setSelectedIndex(0);
                                    comboBoxZona.setEnabled(true);
                                    comboBoxZona.setEditable(true);
                                    comboBoxZona.requestFocus();
                                    System.out.println("cadena escrita: " + comboBoxNombreCliente.getEditor().getItem().toString());

                                    //cadenaEscrita = comboBoxNombreCliente.getEditor().getItem().toString();
                                    if (comboBoxNombreCliente.getSelectedIndex() == -1) {
                                        comboBoxNombreCliente.setSelectedIndex(0);
                                    }
                                    //

                                    //System.out.println("INDEX: " + comboBoxNombreCliente.getSelectedItem().toString());
                                    System.out.println("INDEX: " + comboBoxNombreCliente.getSelectedIndex());

                                    Object ob = comboBoxNombreCliente.getSelectedItem();
                                    System.out.println(ob.toString());

                                    //cli = (Cliente) comboBoxNombreCliente.getS;
                                    //cli.setNombre((Cliente) comboBoxNombreCliente.getItemAt(1).); = ;
                                    if (ob instanceof Cliente) {
                                        cli = (Cliente) ob;
                                    }
                                    System.out.println("cliente: " + cli.getNombre());

                                }

                            }
                            System.out.println("ID_LOCALIDAD: " + cli.getIdLocalidad());

                            l = ml.obtenerLocalidad(cli.getIdLocalidad());

                            System.out.println("LOCALIDAD: " + l.getLocalidad());

                            labelCliente.setText(cli.getNombre());
                            labelDireccion.setText(cli.getDireccion());
                            labelLocalidad.setText(l.getLocalidad());
                            comboBoxZona.setSelectedIndex(cli.getZona_def() - 1);
                            float deuda = mcc.traerCtaCteTotal(cli.getIdCliente());
                            if (deuda < 0) {
                                labelTotalCtaCte.setBackground(Color.RED);
                            }
                            labelTotalCtaCte.setText("$ " + String.valueOf(deuda));

                        }
                        if (evt.getKeyCode() >= 65 && evt.getKeyCode() <= 90 || evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105 || evt.getKeyCode() == 8) {

                            //comboBoxNombreCliente.setModel(mb.getLista(cadenaEscrita, campo));
                            //comboBoxNombreCliente.setModel(mb.getListaClientesArray(cadenaEscrita, campo, arrayClientes));
                            mb.filtrarClientes(comboBoxNombreCliente, cadenaEscrita, arrayClientes);

                            if (comboBoxNombreCliente.getItemCount() > 0) {
                                comboBoxNombreCliente.getEditor().setItem(cadenaEscrita);
                                comboBoxNombreCliente.showPopup();

                            } else {
                                comboBoxNombreCliente.addItem(cadenaEscrita);
                            }
                        }

                    }
                }
                );

        //this.pack();
        this.comboBoxMercaderia.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent evt) {

                String campo;
                String cadenaEscrita = comboBoxMercaderia.getEditor().getItem().toString();

                //System.out.println(cadenaEscrita);
                campo = "nombre";

                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (mm.comparar(cadenaEscrita)) {// compara si el texto escrito se encuentra en la lista
                        // busca el texto escrito en la base de datos                      

                        //obtenerMercaderia(cadenaEscrita);
                        Object obMer = comboBoxMercaderia.getSelectedItem();                        
                        System.out.println("index:"+comboBoxMercaderia.getSelectedIndex());
                        System.out.println(obMer.toString());

                        //cli = (Cliente) comboBoxNombreCliente.getS;
                        //cli.setNombre((Cliente) comboBoxNombreCliente.getItemAt(1).); = ;
                        if (obMer instanceof Mercaderia) {
                            m = (Mercaderia) obMer;
                        }
                        panelCantidad.setVisible(true);
                        //txtTotalCant.requestFocus();
                        manM.traerTipoCantidad(comboBoxTipoCantidad);
                        comboBoxTipoCantidad.requestFocus();
                        System.out.println("ENTRO 1");
                        //System.out.println("ELEMENTO SELECCIONADO: " + comboBoxMercaderia.getEditor().getItem().toString());
                        System.out.println("ELEMENTO SELECCIONADO: " + m.getNombre());
                        String comparar = comboBoxMercaderia.getEditor().getItem().toString();
                        manM.seleccionarVerdurasEspeciales(comparar, comboBoxTipoCantidad);

                        //l = ml.obtenerLocalidad(cli.getIdLocalidad());
                        //buscar(nn);
                    } else {// en caso contrario toma como default el elemento 0 o sea el primero de la lista y lo busca.
                        if (comboBoxMercaderia.getItemAt(0) != null) {

                            if (comboBoxMercaderia.getSelectedIndex() == -1) {
                                comboBoxMercaderia.setSelectedIndex(0);
                            }
                            //obtenerMercaderia(cadenaEscrita);
                            Object obMer = comboBoxMercaderia.getSelectedItem();
                            System.out.println(obMer.toString());

                            //cli = (Cliente) comboBoxNombreCliente.getS;
                            //cli.setNombre((Cliente) comboBoxNombreCliente.getItemAt(1).); = ;
                            if (obMer instanceof Mercaderia) {
                                m = (Mercaderia) obMer;
                            }
                            System.out.println("ENTRO 2");
                            System.out.println("ELEMENTO SELECCIONADO: " + comboBoxMercaderia.getEditor().getItem().toString());
                            String comparar = comboBoxMercaderia.getItemAt(0).toString();
                            manM.traerTipoCantidad(comboBoxTipoCantidad);
                            comboBoxTipoCantidad.requestFocus();
                            //l = ml.obtenerLocalidad(cli.getIdLocalidad());
                            manM.seleccionarVerdurasEspeciales(comparar, comboBoxTipoCantidad);

                        }
                        botonAceptarMercaderia.setEnabled(true);

                        boton1.setEnabled(true);
                        boton2.setEnabled(true);
                        boton3.setEnabled(true);
                        boton4.setEnabled(true);
                        boton5.setEnabled(true);
                        boton6.setEnabled(true);
                        boton7.setEnabled(true);
                        boton8.setEnabled(true);
                        boton9.setEnabled(true);
                        boton0.setEnabled(true);
                        botonComa.setEnabled(true);
                        botonBorrar.setEnabled(true);

                    }

                }
                if (evt.getKeyCode() >= 65 && evt.getKeyCode() <= 90 || evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105 || evt.getKeyCode() == 8) {

                    //comboBoxMercaderia.setModel(mm.getLista(cadenaEscrita, campo));
                    mb.filtrarMercaderias(comboBoxMercaderia, cadenaEscrita, arrayMercaderias);

                    if (comboBoxMercaderia.getItemCount() > 0) {
                        comboBoxMercaderia.getEditor().setItem(cadenaEscrita);
                        comboBoxMercaderia.showPopup();

                    } else {
                        comboBoxMercaderia.addItem(cadenaEscrita);
                    }
                }

            }
        }
        );

        this.comboBoxZona.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent evt) {

                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    comboBoxMercaderia.requestFocus();
                    //comboBoxTipoMercaderia.setEditable(true);
                    //comboBoxTipoMercaderia.setEnabled(true);
                }
            }
        });

        comboBoxTipoCantidad.setEnabled(true);

        this.comboBoxTipoCantidad.addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent evt) {

                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    txtTotalCant.requestFocus();
                }
            }
        });

    }

    public void obtenerMercaderia(String bus) {

        String sql = "SELECT id_mercaderia, nombre from mercaderia "
                + "where nombre = '" + bus + "'";

        String codigo = "";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                m.setId_mercaderia(rs1.getString(1));
                m.setNombre(rs1.getString(2));
            }

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

    }

    public void obtenerCliente(String bus) {
        manejadorLocalidad ml = new manejadorLocalidad(con);
        manejadorCtaCte mcc = new manejadorCtaCte(con);

        String sql = "SELECT c.id_cliente, c.nombre, c.direccion, l.nombre, l.id_localidad, c.zona_def "
                + "from cliente as c "
                + "join localidad as l "
                + "on c.id_localidad = l.id_localidad "
                + "where c.nombre = '" + bus + "'";

        String codigo = "";

        System.out.println(sql);

        //conexionDB c = new conexionDB();
        //Connection con = c.conectar();
        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                cli.setIdCliente(rs1.getInt(1));
                cli.setNombre(rs1.getString(2));
                cli.setDireccion(rs1.getString(3));
                cli.setLocalidad(rs1.getInt(5));
                cli.setZona_def(rs1.getInt(6));
            }

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

        System.out.println("ID_LOCALIDAD: " + cli.getIdLocalidad());

        l = ml.obtenerLocalidad(cli.getIdLocalidad());

        System.out.println("LOCALIDAD: " + l.getLocalidad());

        this.labelCliente.setText(cli.getNombre());
        this.labelDireccion.setText(cli.getDireccion());
        this.labelLocalidad.setText(l.getLocalidad());
        this.comboBoxZona.setSelectedIndex(cli.getZona_def() - 1);
        float deuda = mcc.traerCtaCteTotal(cli.getIdCliente());
        if (deuda < 0) {
            this.labelTotalCtaCte.setBackground(Color.RED);
        }
        this.labelTotalCtaCte.setText("$ " + String.valueOf(deuda));
    }

    /*
    public Cliente obtenerClienteArray(String bus, ArrayList<Cliente> arrayClientes) {
        Cliente cli = new Cliente();
        manejadorLocalidad ml = new manejadorLocalidad(con);
        manejadorCtaCte mcc = new manejadorCtaCte(con);

        for (int i = 0; i < arrayClientes.size(); i++){
            if(bus.equalsIgnoreCase(arrayClientes.get(i).getNombre())){
                cli = arrayClientes.get(i);
            }
        }
         

        return cli;
    }*/
    public JComboBox getcomboBoxNombreCliente() {
        return comboBoxNombreCliente;
    }

    public void setcomboBoxNombreCliente(JComboBox comboBoxNombreCliente) {
        this.comboBoxNombreCliente = comboBoxNombreCliente;
    }

    public JComboBox getcomboBoxMercaderia() {
        return comboBoxMercaderia;
    }

    public void setcomboBoxMercaderia(JComboBox comboBoxMercaderia) {
        this.comboBoxMercaderia = comboBoxMercaderia;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelCliente = new javax.swing.JPanel();
        comboBoxZona = new javax.swing.JComboBox();
        comboBoxNombreCliente = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        botonEditarCliente = new javax.swing.JButton();
        panelMercaderia = new javax.swing.JPanel();
        comboBoxMercaderia = new javax.swing.JComboBox();
        jLabel2 = new javax.swing.JLabel();
        panelCantidad = new javax.swing.JPanel();
        boton1 = new javax.swing.JButton();
        boton2 = new javax.swing.JButton();
        boton3 = new javax.swing.JButton();
        boton4 = new javax.swing.JButton();
        boton5 = new javax.swing.JButton();
        boton6 = new javax.swing.JButton();
        boton7 = new javax.swing.JButton();
        boton8 = new javax.swing.JButton();
        boton9 = new javax.swing.JButton();
        botonComa = new javax.swing.JButton();
        boton0 = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        comboBoxTipoCantidad = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        txtTotalCant = new javax.swing.JTextField();
        botonBorrar = new javax.swing.JButton();
        botonAceptarMercaderia = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        labelDireccion = new javax.swing.JLabel();
        labelLocalidad = new javax.swing.JLabel();
        labelCliente = new javax.swing.JLabel();
        panelResumenPedido = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaResumenPedido = new javax.swing.JTable();
        botonGuardarPedido = new javax.swing.JButton();
        botonEliminarMercaderia = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jButton1 = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        labelTotalCtaCte = new javax.swing.JLabel();
        btnImprimirBoleta = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        panelCliente.setBorder(javax.swing.BorderFactory.createTitledBorder("Cliente"));

        comboBoxZona.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10" }));
        comboBoxZona.setSelectedIndex(-1);
        comboBoxZona.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                comboBoxZonaFocusLost(evt);
            }
        });
        comboBoxZona.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxZonaActionPerformed(evt);
            }
        });
        comboBoxZona.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                comboBoxZonaKeyTyped(evt);
            }
        });

        comboBoxNombreCliente.setEditable(true);
        comboBoxNombreCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxNombreClienteActionPerformed(evt);
            }
        });

        jLabel5.setText("Seleccione Cliente:");

        jLabel6.setText("Seleccione Zona:");

        botonEditarCliente.setText("EDITAR CLIENTE");
        botonEditarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEditarClienteActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelClienteLayout = new javax.swing.GroupLayout(panelCliente);
        panelCliente.setLayout(panelClienteLayout);
        panelClienteLayout.setHorizontalGroup(
            panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelClienteLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panelClienteLayout.createSequentialGroup()
                        .addGroup(panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(comboBoxZona, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(botonEditarCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 187, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panelClienteLayout.createSequentialGroup()
                        .addGroup(panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5)
                            .addComponent(comboBoxNombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        panelClienteLayout.setVerticalGroup(
            panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelClienteLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(comboBoxNombreCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelClienteLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(botonEditarCliente, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
                    .addComponent(comboBoxZona, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE))
                .addGap(13, 13, 13))
        );

        panelMercaderia.setBorder(javax.swing.BorderFactory.createTitledBorder("Mercaderia"));

        comboBoxMercaderia.setEditable(true);
        comboBoxMercaderia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxMercaderiaActionPerformed(evt);
            }
        });

        jLabel2.setText("Seleccione Mercaderia:");

        javax.swing.GroupLayout panelMercaderiaLayout = new javax.swing.GroupLayout(panelMercaderia);
        panelMercaderia.setLayout(panelMercaderiaLayout);
        panelMercaderiaLayout.setHorizontalGroup(
            panelMercaderiaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelMercaderiaLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMercaderiaLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(comboBoxMercaderia, javax.swing.GroupLayout.PREFERRED_SIZE, 311, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        panelMercaderiaLayout.setVerticalGroup(
            panelMercaderiaLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelMercaderiaLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(comboBoxMercaderia, javax.swing.GroupLayout.PREFERRED_SIZE, 36, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(43, Short.MAX_VALUE))
        );

        panelCantidad.setBorder(javax.swing.BorderFactory.createTitledBorder("Cantidad"));

        boton1.setText("1");
        boton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton1ActionPerformed(evt);
            }
        });

        boton2.setText("2");
        boton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton2ActionPerformed(evt);
            }
        });

        boton3.setText("3");
        boton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton3ActionPerformed(evt);
            }
        });

        boton4.setText("4");
        boton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton4ActionPerformed(evt);
            }
        });

        boton5.setText("5");
        boton5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton5ActionPerformed(evt);
            }
        });

        boton6.setText("6");
        boton6.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton6ActionPerformed(evt);
            }
        });

        boton7.setText("7");
        boton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton7ActionPerformed(evt);
            }
        });

        boton8.setText("8");
        boton8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton8ActionPerformed(evt);
            }
        });

        boton9.setText("9");
        boton9.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton9ActionPerformed(evt);
            }
        });

        botonComa.setText(",");
        botonComa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonComaActionPerformed(evt);
            }
        });

        boton0.setText("0");
        boton0.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                boton0ActionPerformed(evt);
            }
        });

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("TIPO CANTIDAD:");

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("TOTAL:");

        txtTotalCant.setEditable(false);
        txtTotalCant.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txtTotalCant.setHorizontalAlignment(javax.swing.JTextField.CENTER);
        txtTotalCant.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtTotalCantKeyReleased(evt);
            }
        });

        botonBorrar.setText("<-");
        botonBorrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonBorrarActionPerformed(evt);
            }
        });

        botonAceptarMercaderia.setText("ACEPTAR MERCADERIA");
        botonAceptarMercaderia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAceptarMercaderiaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panelCantidadLayout = new javax.swing.GroupLayout(panelCantidad);
        panelCantidad.setLayout(panelCantidadLayout);
        panelCantidadLayout.setHorizontalGroup(
            panelCantidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelCantidadLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCantidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botonAceptarMercaderia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panelCantidadLayout.createSequentialGroup()
                        .addGroup(panelCantidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(panelCantidadLayout.createSequentialGroup()
                                .addComponent(boton1, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(boton2, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(boton3, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelCantidadLayout.createSequentialGroup()
                                .addComponent(boton7, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(boton8, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(boton9, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelCantidadLayout.createSequentialGroup()
                                .addComponent(boton4, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(boton5, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(boton6, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panelCantidadLayout.createSequentialGroup()
                                .addComponent(botonComa, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(boton0, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(botonBorrar, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(panelCantidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(comboBoxTipoCantidad, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtTotalCant)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        panelCantidadLayout.setVerticalGroup(
            panelCantidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panelCantidadLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panelCantidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(boton1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(boton2, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(boton3, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelCantidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(boton4, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(boton5, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(boton6, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(comboBoxTipoCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelCantidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(boton7, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(boton8, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(boton9, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panelCantidadLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(boton0, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                    .addComponent(botonComa, javax.swing.GroupLayout.DEFAULT_SIZE, 37, Short.MAX_VALUE)
                    .addComponent(botonBorrar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txtTotalCant, javax.swing.GroupLayout.Alignment.TRAILING))
                .addGap(18, 18, 18)
                .addComponent(botonAceptarMercaderia, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("Datos del Cliente"));

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("CLIENTE SELECCIONADO:");

        jLabel8.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel8.setText("DIRECCION:");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel9.setText("LOCALIDAD:");

        jLabel10.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N

        labelDireccion.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelDireccion.setText("jLabel10");

        labelLocalidad.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelLocalidad.setText("jLabel10");

        labelCliente.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        labelCliente.setText("jLabel10");
        labelCliente.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                labelClientePropertyChange(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(19, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(jLabel9)
                    .addComponent(jLabel8))
                .addGap(60, 60, 60)
                .addComponent(jLabel10)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(labelDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelCliente, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(labelLocalidad, javax.swing.GroupLayout.PREFERRED_SIZE, 316, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(jLabel10)
                    .addComponent(labelCliente))
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8)
                    .addComponent(labelDireccion))
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(labelLocalidad))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panelResumenPedido.setBorder(javax.swing.BorderFactory.createTitledBorder("Resumen Pedido"));

        tablaResumenPedido.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "ID", "Mercaderia", "Cantidad", "Tipo"
            }
        ));
        tablaResumenPedido.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaResumenPedidoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablaResumenPedido);

        botonGuardarPedido.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        botonGuardarPedido.setText("GUARDAR PEDIDO");
        botonGuardarPedido.setPreferredSize(new java.awt.Dimension(133, 25));
        botonGuardarPedido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonGuardarPedidoActionPerformed(evt);
            }
        });

        botonEliminarMercaderia.setText("ELIMINAR MERCADERIA");
        botonEliminarMercaderia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarMercaderiaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 761, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(botonGuardarPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 298, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(botonEliminarMercaderia, javax.swing.GroupLayout.PREFERRED_SIZE, 361, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {botonEliminarMercaderia, botonGuardarPedido});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 394, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(botonGuardarPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(botonEliminarMercaderia, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {botonEliminarMercaderia, botonGuardarPedido});

        javax.swing.GroupLayout panelResumenPedidoLayout = new javax.swing.GroupLayout(panelResumenPedido);
        panelResumenPedido.setLayout(panelResumenPedidoLayout);
        panelResumenPedidoLayout.setHorizontalGroup(
            panelResumenPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelResumenPedidoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        panelResumenPedidoLayout.setVerticalGroup(
            panelResumenPedidoLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panelResumenPedidoLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Cuenta Corriente"));

        jButton1.setText("VER DETALLE");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel11.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel11.setText("TOTAL CUENTA CORRIENTE");

        labelTotalCtaCte.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelTotalCtaCte.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(labelTotalCtaCte, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel11)
                .addGap(27, 27, 27)
                .addComponent(labelTotalCtaCte, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(27, 27, 27)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel3Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel11, labelTotalCtaCte});

        btnImprimirBoleta.setText("IMPRIMIR BOLETA");
        btnImprimirBoleta.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnImprimirBoletaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelMercaderia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelCantidad, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(panelCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(panelResumenPedido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(btnImprimirBoleta, javax.swing.GroupLayout.PREFERRED_SIZE, 1112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(panelCliente, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(panelMercaderia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(panelCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(panelResumenPedido, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(btnImprimirBoleta)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonComaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonComaActionPerformed
        // TODO add your handling code here:

        String cantidad = txtTotalCant.getText();

        txtTotalCant.setText(cantidad + ".");
        botonComa.setEnabled(false);
        //boton0.setEnabled(false);
    }//GEN-LAST:event_botonComaActionPerformed

    private void boton0ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton0ActionPerformed
        // TODO add your handling code here:

        String cantidad = txtTotalCant.getText();

        txtTotalCant.setText(cantidad + "0");

        /*
        String borrar = txtTotalCant.getText().substring(txtTotalCant.getText().length() - 1, txtTotalCant.getText().length());

        System.out.println("elemento a borrar: " + borrar);

        if (borrar.equals("0")) {
            boton0.setEnabled(false);
        }


        if (primerNumero) {
            botonComa.setEnabled(true);
            boton0.setEnabled(false);
            primerNumero = false;
        }*/
    }//GEN-LAST:event_boton0ActionPerformed

    private void comboBoxNombreClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxNombreClienteActionPerformed
        // TODO add your handling code here:
        manejadorCliente mc = new manejadorCliente(con);
        manejadorLocalidad ml = new manejadorLocalidad(con);
        manejadorCtaCte mcc = new manejadorCtaCte(con);

        /*
        if (!start) {
            

            Object ob = comboBoxNombreCliente.getSelectedItem();
            System.out.println(ob.toString());

            if (ob instanceof Cliente) {
                cli = (Cliente) ob;
            }

            l = ml.obtenerLocalidad(cli.getIdLocalidad());

            System.out.println("LOCALIDAD: " + l.getLocalidad());

            labelCliente.setText(cli.getNombre());
            labelDireccion.setText(cli.getDireccion());
            labelLocalidad.setText(l.getLocalidad());
            comboBoxZona.setSelectedIndex(cli.getZona_def() - 1);
            float deuda = mcc.traerCtaCteTotal(cli.getIdCliente());
            if (deuda < 0) {
                labelTotalCtaCte.setBackground(Color.RED);
            }
            labelTotalCtaCte.setText("$ " + String.valueOf(deuda));
        }*/
    }//GEN-LAST:event_comboBoxNombreClienteActionPerformed

    private void labelClientePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_labelClientePropertyChange
        // TODO add your handling code here
        /*
         * if (this.comboBoxNombreCliente.getSelectedIndex() != -1 &&
         * this.comboBoxNombreCliente.getSelectedItem().toString().equals(this.labelCliente.getText()))
         * { panelMercaderia.setVisible(true); panelCliente.setEnabled(false);
         * this.comboBoxNombreCliente.setEditable(false);
         * comboBoxNombreCliente.setEnabled(false);
         * this.comboBoxTipoMercaderia.setVisible(true);
         * this.comboBoxMercaderia.setVisible(true);
         * comboBoxTipoMercaderia.setEnabled(true);
         * comboBoxTipoMercaderia.setEditable(true);
         * //comboBoxMercaderia.setEnabled(true);
         * //comboBoxMercaderia.setEditable(true); }
         */
    }//GEN-LAST:event_labelClientePropertyChange

    private void botonBorrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonBorrarActionPerformed
        // TODO add your handling code here:

        String borrar = txtTotalCant.getText().substring(txtTotalCant.getText().length() - 1, txtTotalCant.getText().length());

        System.out.println("elemento a borrar: " + borrar);
        System.out.println("cantidad de cifras: " + txtTotalCant.getText().toString().length());

        if (borrar.equals(".")) {
            botonComa.setEnabled(true);
            //boton0.setEnabled(true);
        }

        txtTotalCant.setText(txtTotalCant.getText().substring(0, txtTotalCant.getText().length() - 1));

        /*
        if (txtTotalCant.getText().toString().length() == 0) {
            botonComa.setEnabled(false);
            boton0.setEnabled(false);
            primerNumero = true;
        }*/
    }//GEN-LAST:event_botonBorrarActionPerformed

    private void boton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton1ActionPerformed
        // TODO add your handling code here:

        String cantidad = txtTotalCant.getText();

        txtTotalCant.setText(cantidad + "1");

        /*
        if (primerNumero) {
            botonComa.setEnabled(true);
            boton0.setEnabled(true);
            primerNumero = false;
        }*/

    }//GEN-LAST:event_boton1ActionPerformed

    private void boton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton2ActionPerformed
        // TODO add your handling code here:
        String cantidad = txtTotalCant.getText();

        txtTotalCant.setText(cantidad + "2");

        /*
        if (primerNumero) {
            botonComa.setEnabled(true);
            boton0.setEnabled(true);
            primerNumero = false;
        }*/

    }//GEN-LAST:event_boton2ActionPerformed

    private void boton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton3ActionPerformed
        // TODO add your handling code here
        String cantidad = txtTotalCant.getText();

        txtTotalCant.setText(cantidad + "3");

        /*
        if (primerNumero) {
            botonComa.setEnabled(true);
            boton0.setEnabled(true);
            primerNumero = false;
        }*/
    }//GEN-LAST:event_boton3ActionPerformed

    private void boton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton4ActionPerformed
        // TODO add your handling code here:

        String cantidad = txtTotalCant.getText();

        txtTotalCant.setText(cantidad + "4");

        /*
        if (primerNumero) {
            botonComa.setEnabled(true);
            boton0.setEnabled(true);
            primerNumero = false;
        }*/
    }//GEN-LAST:event_boton4ActionPerformed

    private void boton5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton5ActionPerformed
        // TODO add your handling code here:

        String cantidad = txtTotalCant.getText();

        txtTotalCant.setText(cantidad + "5");

        /*
        if (primerNumero) {
            botonComa.setEnabled(true);
            boton0.setEnabled(true);
            primerNumero = false;
        }*/
    }//GEN-LAST:event_boton5ActionPerformed

    private void boton6ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton6ActionPerformed
        // TODO add your handling code here:

        String cantidad = txtTotalCant.getText();

        txtTotalCant.setText(cantidad + "6");

        /*
        if (primerNumero) {
            botonComa.setEnabled(true);
            boton0.setEnabled(true);
            primerNumero = false;
        }*/
    }//GEN-LAST:event_boton6ActionPerformed

    private void boton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton7ActionPerformed
        // TODO add your handling code here:

        String cantidad = txtTotalCant.getText();

        txtTotalCant.setText(cantidad + "7");

        /*
        if (primerNumero) {
            botonComa.setEnabled(true);
            boton0.setEnabled(true);
            primerNumero = false;
        }*/
    }//GEN-LAST:event_boton7ActionPerformed

    private void boton8ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton8ActionPerformed
        // TODO add your handling code here:

        String cantidad = txtTotalCant.getText();

        txtTotalCant.setText(cantidad + "8");

        /*
        if (primerNumero) {
            botonComa.setEnabled(true);
            boton0.setEnabled(true);
            primerNumero = false;
        }*/
    }//GEN-LAST:event_boton8ActionPerformed

    private void boton9ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_boton9ActionPerformed
        // TODO add your handling code here:
        String cantidad = txtTotalCant.getText();

        txtTotalCant.setText(cantidad + "9");
        /*
        if (primerNumero) {
            botonComa.setEnabled(true);
            boton0.setEnabled(true);
            primerNumero = false;
        }*/
    }//GEN-LAST:event_boton9ActionPerformed

    private void comboBoxZonaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxZonaActionPerformed
        // TODO add your handling code here:
        if (tipo == 1) {
            if (this.comboBoxNombreCliente.getSelectedIndex() != -1 && this.comboBoxNombreCliente.getSelectedItem().toString().equals(this.labelCliente.getText()) && comboBoxZona.getSelectedIndex() != -1) {
                panelMercaderia.setVisible(true);
                panelCliente.setEnabled(false);
                this.comboBoxNombreCliente.setEditable(false);
                comboBoxNombreCliente.setEnabled(false);
                //this.comboBoxTipoMercaderia.setVisible(true);
                this.comboBoxMercaderia.setVisible(true);
                comboBoxMercaderia.setEnabled(true);
                comboBoxMercaderia.setEditable(true);
                botonEditarCliente.setEnabled(true);
                //comboBoxMercaderia.setEnabled(true);
                //comboBoxMercaderia.setEditable(true);
            }
        } else {
            this.comboBoxMercaderia.requestFocus();
            this.comboBoxMercaderia.setVisible(true);
            //this.comboBoxMercaderia.setVisible(true);
            comboBoxMercaderia.setEnabled(true);
            comboBoxMercaderia.setEditable(true);
        }

    }//GEN-LAST:event_comboBoxZonaActionPerformed

    private void comboBoxMercaderiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxMercaderiaActionPerformed
        // TODO add your handling code here:
        /*manejadorMercaderia manM = new manejadorMercaderia(con);
        botonAceptarMercaderia.setVisible(true);
        botonAceptarMercaderia.setEnabled(true);
        manM.traerTipoCantidad(comboBoxTipoCantidad);

        String comparar = comboBoxMercaderia.getEditor().getItem().toString();
        manM.seleccionarVerdurasEspeciales(comparar, comboBoxTipoCantidad);
        comboBoxTipoCantidad.setSelectedIndex(0);

        boton1.setEnabled(true);
        boton2.setEnabled(true);
        boton3.setEnabled(true);
        boton4.setEnabled(true);
        boton5.setEnabled(true);
        boton6.setEnabled(true);
        boton7.setEnabled(true);
        boton8.setEnabled(true);
        boton9.setEnabled(true);
        boton0.setEnabled(true);
        botonComa.setEnabled(true);
        botonBorrar.setEnabled(true);*/


    }//GEN-LAST:event_comboBoxMercaderiaActionPerformed

    private void botonGuardarPedidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonGuardarPedidoActionPerformed
        // TODO add your handling code here:
        manejadorFechas mfechas = new manejadorFechas(con);
        manejadorPedido mp = new manejadorPedido(con);
        manejadorBoleta mbol = new manejadorBoleta(con);
        manejadorTablas mt = new manejadorTablas(con);
        if (tipo == 1) {
            int cont = 0;
            //manejadorBoleta mb = new manejadorBoleta();
            Boleta b = new Boleta();
            //manejadorFechas mf = new manejadorFechas();

            cont = mbol.contadorItems(tablaResumenPedido);

            System.out.println("cantidad items: " + cont);
            System.out.println("codigo cliente: " + cli.getIdCliente());

            b.setId_cliente(cli.getIdCliente());
            b.setIdPedido(id_pedido);
            b.setCantidad_items(cont);
            b.setFecha(mfechas.obtenerFechaActual());
            b.setZona(Integer.parseInt(comboBoxZona.getSelectedItem().toString()));

            //mf.obtenerDireccionCarpetas(b.getFecha());
            mbol.guardarBoleta(b, this.tablaResumenPedido, cli.getNombre(), Login.user.getId_usuario());
            //JOptionPane.showMessageDialog(null, "REGISTRO GUARDADO CON EXITO");
            mbol.actualizarBoletas(tablaBoletasSinCerrar, b.getIdPedido(), (InicioNuevo.MiModelo) modeloInicio, user.getId_usuario());

            if (b.getIdPedido() != 0) {
                System.out.println("numeros de boleta del pedido: " + tablaBoletasSinCerrar.getRowCount());
                mp.actualizarCantBoletasPedido(id_pedido, tablaBoletasSinCerrar.getRowCount());
                //mp.guardarReportes(b.getIdPedido());
            }

        } else if (tipo != 1) {

            //comboBoxZona.requestFocus();
            int cont = 0;
            //manejadorBoleta mb = new manejadorBoleta();
            Boleta b = new Boleta();
            //manejadorFechas mf = new manejadorFechas();

            cont = mbol.contadorItems(tablaResumenPedido);

            System.out.println("cantidad items: " + cont);
            System.out.println("codigo cliente: " + cli.getIdCliente());

            String fechaAño = bo.getFecha().toString().substring(6, 10);
            String fechaMes = bo.getFecha().toString().substring(3, 5);
            String fechaDia = bo.getFecha().toString().substring(0, 2);
            String fechaBoleta = fechaAño + fechaMes + fechaDia;

            System.out.println("FECHA EDITADA: " + fechaBoleta);
            this.comboBoxZona.setEnabled(true);
            this.comboBoxZona.setEditable(true);
            //this.comboBoxZona.requestFocus();            
            //bo.setZona(comboBoxZona.getSelectedIndex()+1);
            bo.setZona(Integer.parseInt(comboBoxZona.getEditor().getItem().toString()));
            //System.out.println("FECHA ACTUAL: "+mf.obtenerFechaActual());

            if (editado) {
                System.out.println("SE AGREGARON O ELIMINARON ELEMENTOS A LA TABLA");
                if (this.tablaResumenPedido.getRowCount() == 0) {
                    JOptionPane.showMessageDialog(null, "INGRESE POR LO MENOS UN ITEM EN LA BOLETA EDITADA");
                } else {
                    mbol.guardarCambios(tablaResumenPedido, bo.getId_boleta(), cont, bo.getZona(), fechaBoleta, cli.getNombre());
                    JOptionPane.showMessageDialog(null, "BOLETA ACTUALIZADA CON EXITO");
                    if (tipo == 2) {
                        mbol.actualizarBoletas(tablaBoletasSinCerrar, id_pedido, (InicioNuevo.MiModelo) modeloInicio, user.getId_usuario());
                    } else {
                        System.out.println("ENTRO POR AQUIIIIIIIIIIIIIIIIIIIII");
                        mt.cargarTablaBoletasPedido(modeloBoletasPedido, id_pedido);
                        //mb.boletasPedido(tablaBoletasSinCerrar, id_pedido);
                    }

                }
            } else {
                mbol.actualizarBoletaEditada(bo, cont);
                JOptionPane.showMessageDialog(null, "BOLETA ACTUALIZADA CON EXITO");
                System.out.println("NO SE AGREGO O ELIMINO NADA DE LA TABLA");
            }

            System.out.println("fecha boleta: " + bo.getFecha());

        }
        this.dispose();

    }//GEN-LAST:event_botonGuardarPedidoActionPerformed

    private void tablaResumenPedidoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaResumenPedidoMouseClicked
        // TODO add your handling code here:

        if (tablaResumenPedido.getSelectedRowCount() == 1) {
            //botonEditarMercaderia.setEnabled(true);
            botonEliminarMercaderia.setEnabled(true);

        } else {
            //botonEditarMercaderia.setEnabled(false);
            botonEliminarMercaderia.setEnabled(false);
        }
    }//GEN-LAST:event_tablaResumenPedidoMouseClicked

    private void botonEditarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEditarClienteActionPerformed
        // TODO add your handling code here:
        comboBoxNombreCliente.setEnabled(true);
        comboBoxNombreCliente.setEditable(true);
        comboBoxMercaderia.setEnabled(false);
        comboBoxMercaderia.setEditable(false);
        botonEditarCliente.setEnabled(false);
    }//GEN-LAST:event_botonEditarClienteActionPerformed

    private void botonEliminarMercaderiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarMercaderiaActionPerformed
        // TODO add your handling code here:

        modelo.removeRow(tablaResumenPedido.getSelectedRow());

        if (tablaResumenPedido.getRowCount() == 0) {
            botonGuardarPedido.setEnabled(false);
        }

        editado = true;

    }//GEN-LAST:event_botonEliminarMercaderiaActionPerformed

    private void txtTotalCantKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTotalCantKeyReleased
        // TODO add your handling code here:
        if (evt.getKeyChar() == KeyEvent.VK_ENTER || evt.getKeyChar() == KeyEvent.VK_TAB) {
            if (comboBoxMercaderia.getSelectedIndex() == -1) {
                JOptionPane.showMessageDialog(null, "POR FAVOR SELECCIONE MERCADERIA");
            } else {

                Object[] detalle = new Object[4];
                manejadorNumeros mn = new manejadorNumeros();

                /* try {
                    // TODO add your handling code here:

                    
                    String sqlID = "SELECT id_mercaderia from mercaderia "
                            + "WHERE nombre = '" + this.comboBoxMercaderia.getSelectedItem().toString() + "' and inactivo = 0";

                    //conexionDB c = new conexionDB();
                    //Connection con = c.conectar();
                    Statement st1 = con.createStatement();
                    ResultSet rs1 = st1.executeQuery(sqlID);

                    while (rs1.next()) {
                        detalle[0] = rs1.getString(1);
                        System.out.println("ID: " + detalle[0].toString());
                    }

                } catch (SQLException ex) {
                    Logger.getLogger(CargarPedidoDidactico.class
                            .getName()).log(Level.SEVERE, null, ex);
                }*/
                detalle[0] = m.getId_mercaderia();

                if (detalle[0] == null) {
                    JOptionPane.showMessageDialog(null, "SELECCIONE UN ARTICULO VALIDO");
                } else {
                    //detalle[1] = this.comboBoxMercaderia.getSelectedItem().toString();
                    detalle[1] = m.getNombre();

                    if (mn.isNumeric(txtTotalCant.getText()) || (txtTotalCant.getText().equals(""))) {
                        if (txtTotalCant.getText().equals("")) {
                            detalle[2] = "1";

                        } else {
                            detalle[2] = txtTotalCant.getText();
                        }

                        if (comboBoxTipoCantidad.getSelectedIndex() == -1) {
                            detalle[3] = "Cajon";
                        } else {
                            detalle[3] = this.comboBoxTipoCantidad.getSelectedItem().toString();
                        }

                        modelo.addRow(detalle);

                        comboBoxZona.setEnabled(false);
                        //comboBoxMercaderia.setEnabled(true);
                        //comboBoxMercaderia.setEditable(true);
                        //comboBoxTipoMercaderia.setSelectedIndex(-1);
                        comboBoxTipoCantidad.setSelectedIndex(0);
                        comboBoxMercaderia.setEnabled(false);
                        comboBoxMercaderia.setEditable(false);
                        txtTotalCant.setText("");
                        boton1.setEnabled(false);
                        boton2.setEnabled(false);
                        boton3.setEnabled(false);
                        boton4.setEnabled(false);
                        boton5.setEnabled(false);
                        boton6.setEnabled(false);
                        boton7.setEnabled(false);
                        boton8.setEnabled(false);
                        boton9.setEnabled(false);
                        boton0.setEnabled(false);
                        botonComa.setEnabled(false);
                        botonBorrar.setEnabled(false);
                        //comboBoxMercaderia.setEnabled(true);
                        //comboBoxMercaderia.setEditable(true);        
                        //botonEditarMercaderia.setVisible(false);
                        botonAceptarMercaderia.setEnabled(false);
                        comboBoxMercaderia.setEnabled(true);
                        comboBoxMercaderia.setEditable(true);
                        comboBoxMercaderia.setSelectedIndex(-1);
                        botonGuardarPedido.setEnabled(true);

                        /*
                        this.comboBoxTipoMercaderia.requestFocus();
                        comboBoxTipoMercaderia.removeAllItems();
                        comboBoxTipoMercaderia.addItem("VERDURA");
                        comboBoxTipoMercaderia.addItem("FRUTA");
                        comboBoxTipoMercaderia.setSelectedIndex(-1);*/
                        this.comboBoxMercaderia.requestFocus();
                        editado = true;

                    } else {
                        JOptionPane.showMessageDialog(null, "SELECCIONE UNA CANTIDAD VALIDA");
                    }

                }

            }
        }

        comboBoxZona.setEnabled(true);
        comboBoxZona.setEditable(false);


    }//GEN-LAST:event_txtTotalCantKeyReleased

    private void comboBoxZonaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_comboBoxZonaFocusLost
        // TODO add your handling code here:
        //this.comboBoxTipoMercaderia.requestFocus();
    }//GEN-LAST:event_comboBoxZonaFocusLost

    private void comboBoxZonaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_comboBoxZonaKeyTyped
        // TODO add your handling code here:

        if (evt.getKeyChar() == KeyEvent.VK_TAB) {
            comboBoxMercaderia.requestFocus();
        }
    }//GEN-LAST:event_comboBoxZonaKeyTyped

    private void botonAceptarMercaderiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAceptarMercaderiaActionPerformed
        // TODO add your handling code here:
        if (comboBoxMercaderia.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(null, "POR FAVOR SELECCIONE MERCADERIA");
        } else {

            Object[] detalle = new Object[4];
            manejadorNumeros mn = new manejadorNumeros();

            try {
                // TODO add your handling code here:

                String sqlID = "SELECT id_mercaderia from Mercaderia "
                        + "WHERE nombre = '" + this.comboBoxMercaderia.getSelectedItem().toString() + "' and inactivo = 0";

                //conexionDB c = new conexionDB();
                //Connection con = c.conectar();
                Statement st1 = con.createStatement();
                ResultSet rs1 = st1.executeQuery(sqlID);

                while (rs1.next()) {
                    detalle[0] = rs1.getString(1);
                    System.out.println("ID: " + detalle[0].toString());
                }

            } catch (SQLException ex) {
                Logger.getLogger(CargarPedidoDidactico.class
                        .getName()).log(Level.SEVERE, null, ex);
            }

            if (detalle[0] == null) {
                JOptionPane.showMessageDialog(null, "SELECCIONE UN ARTICULO VALIDO");
            } else {
                detalle[1] = this.comboBoxMercaderia.getSelectedItem().toString();

                if (mn.isNumeric(txtTotalCant.getText()) || (txtTotalCant.getText().equals(""))) {
                    if (txtTotalCant.getText().equals("")) {
                        detalle[2] = "1";

                    } else {
                        detalle[2] = txtTotalCant.getText();
                    }

                    if (comboBoxTipoCantidad.getSelectedIndex() == -1) {
                        detalle[3] = "Cajon";
                    } else {
                        detalle[3] = this.comboBoxTipoCantidad.getSelectedItem().toString();
                    }

                    modelo.addRow(detalle);

                    comboBoxZona.setEnabled(false);
                    //comboBoxTipoMercaderia.setEnabled(true);
                    //comboBoxTipoMercaderia.setEditable(true);
                    //comboBoxTipoMercaderia.setSelectedIndex(-1);
                    comboBoxTipoCantidad.setSelectedIndex(0);
                    comboBoxMercaderia.setEnabled(false);
                    comboBoxMercaderia.setEditable(false);
                    txtTotalCant.setText("");
                    boton1.setEnabled(false);
                    boton2.setEnabled(false);
                    boton3.setEnabled(false);
                    boton4.setEnabled(false);
                    boton5.setEnabled(false);
                    boton6.setEnabled(false);
                    boton7.setEnabled(false);
                    boton8.setEnabled(false);
                    boton9.setEnabled(false);
                    boton0.setEnabled(false);
                    botonComa.setEnabled(false);
                    botonBorrar.setEnabled(false);
                    //comboBoxMercaderia.setEnabled(true);
                    //comboBoxMercaderia.setEditable(true);        
                    //botonEditarMercaderia.setVisible(false);
                    botonAceptarMercaderia.setEnabled(false);
                    //comboBoxTipoMercaderia.setEnabled(true);
                    //comboBoxTipoMercaderia.setEditable(true);
                    //comboBoxTipoMercaderia.setSelectedIndex(-1);
                    comboBoxMercaderia.setEnabled(true);
                    comboBoxMercaderia.setEditable(true);
                    comboBoxMercaderia.setSelectedIndex(-1);
                    botonGuardarPedido.setEnabled(true);

                    /*
                    this.comboBoxTipoMercaderia.requestFocus();
                    comboBoxTipoMercaderia.removeAllItems();
                    comboBoxTipoMercaderia.addItem("VERDURA");
                    comboBoxTipoMercaderia.addItem("FRUTA");
                    comboBoxTipoMercaderia.setSelectedIndex(-1);*/
                    this.comboBoxMercaderia.requestFocus();
                    editado = true;

                } else {
                    JOptionPane.showMessageDialog(null, "SELECCIONE UNA CANTIDAD VALIDA");
                }

            }

        }
    }//GEN-LAST:event_botonAceptarMercaderiaActionPerformed

    private void btnImprimirBoletaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnImprimirBoletaActionPerformed
        // TODO add your handling code here:        
        manejadorBoleta mbol = new manejadorBoleta(con);
        mbol.imprimirBoleta(bo);
    }//GEN-LAST:event_btnImprimirBoletaActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        CuentaCorriente cta = new CuentaCorriente(cli, con);
        cta.setLocationRelativeTo(null);
        cta.setVisible(true);

    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(CargarPedidoDidactico.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(CargarPedidoDidactico.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(CargarPedidoDidactico.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(CargarPedidoDidactico.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new CargarPedidoDidactico(null, 0, null, 0, null, null, null, null).setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton boton0;
    private javax.swing.JButton boton1;
    private javax.swing.JButton boton2;
    private javax.swing.JButton boton3;
    private javax.swing.JButton boton4;
    private javax.swing.JButton boton5;
    private javax.swing.JButton boton6;
    private javax.swing.JButton boton7;
    private javax.swing.JButton boton8;
    private javax.swing.JButton boton9;
    private javax.swing.JButton botonAceptarMercaderia;
    private javax.swing.JButton botonBorrar;
    private javax.swing.JButton botonComa;
    private javax.swing.JButton botonEditarCliente;
    private javax.swing.JButton botonEliminarMercaderia;
    private javax.swing.JButton botonGuardarPedido;
    private javax.swing.JButton btnImprimirBoleta;
    private javax.swing.JComboBox comboBoxMercaderia;
    private javax.swing.JComboBox comboBoxNombreCliente;
    private javax.swing.JComboBox comboBoxTipoCantidad;
    private javax.swing.JComboBox comboBoxZona;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel labelCliente;
    private javax.swing.JLabel labelDireccion;
    private javax.swing.JLabel labelLocalidad;
    private javax.swing.JLabel labelTotalCtaCte;
    private javax.swing.JPanel panelCantidad;
    private javax.swing.JPanel panelCliente;
    private javax.swing.JPanel panelMercaderia;
    private javax.swing.JPanel panelResumenPedido;
    private javax.swing.JTable tablaResumenPedido;
    private javax.swing.JTextField txtTotalCant;
    // End of variables declaration//GEN-END:variables
}
