/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Conexion.conexionDB;
import Controlador.*;
import Modelo.Mercaderia;
import Modelo.Pedido;
import Modelo.Tipo_Cantidad;
import Modelo.Usuario;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Administrador
 */
public class GestionDeCostoNueva extends javax.swing.JFrame {

    /**
     * Creates new form ResumenListaCompras
     */
    Connection con;

    manejadorNumeros mn = new manejadorNumeros();

    Mercaderia m = new Mercaderia();
    Tipo_Cantidad tc = new Tipo_Cantidad();
    boolean bandera = true;
    int contador = 0;
    int tipo;

    public class MiModeloGastos extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            if (column == 4) { //columnIndex: the column you want to make it editable
                return true;
            } else {
                return false;
            }
        }
    }

    public class MiModeloOtrosCostos extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            if (column == 2) {
                return true;
            } else {
                return false;
            }

        }
    }

    MiModeloOtrosCostos modeloGastos = new MiModeloOtrosCostos();
    MiModeloGastos modeloMercaderia = new MiModeloGastos();
    Pedido p = new Pedido();
    JFrame inicio;
    JTable tablaBoletasSinCerrar;
    Usuario user;

    public GestionDeCostoNueva(Pedido p, JTable tablaBoletasSinCerrar, Connection con, JFrame inicio, Usuario user) {
        initComponents();
        this.setTitle("GESTION DE COSTO");
        this.setResizable(false);
        //this.pedidosSinCosto = pedidosSinCosto;
        this.p = p;
        this.con = con;
        this.user = user;
        this.tablaBoletasSinCerrar = tablaBoletasSinCerrar;
        this.inicio = inicio;

        manejadorPedido mp = new manejadorPedido(con);
        manejadorCosto mc = new manejadorCosto(con);
        manejadorTablas mt = new manejadorTablas(con);
        botonEliminar.setEnabled(false);
        //botonEliminarMercaderia.setEnabled(false);

        labelFecha.setText(p.getFecha_pedido());
        labelCantBoletas.setText(Integer.toString(p.getCant_boletas()));

        tablaOtrosGastos.setModel(modeloGastos);
        mt.modeloTablaCargarOtrosCostos(modeloGastos, tablaOtrosGastos, p.getId_pedido());
        mc.cargarCostos(comboBoxGastos);

        mp.modeloCostoMercaderiaUltPedido(tablaCostosMercaderia, p.getId_pedido());
        this.labelTotalCostos.setText("$ " + String.valueOf(mc.actualizarTotales(tablaCostosMercaderia, tablaOtrosGastos)));

        if (p.getId_pedido() != 0) {
            //mp.costoMercaderia(tablaCostosMercaderia, p.getId_pedido());
            labelPedido.setText(Integer.toString(p.getId_pedido()));
            System.out.println("NO ES EL ULTIMO PEDIDO");
            tipo = 1; // NO ES ULTIMO PEDIDO

        } else if (mc.verificarGestionCosto()) {
            //mp.modeloCostoMercaderiaUltPedido(tablaCostosMercaderia, p.getId_pedido());
            //mp.cargarCostosPedido(tablaCostosMercaderia, p.getId_pedido());
            labelPedido.setText("NUEVO");
            System.out.println("ES EL ULTIMO PEDIDO ");
            System.out.println("EL ULTIMO PEDIDO TIENE COSTOS CARGADOS");
            botonFinalizar.setText("ACTUALIZAR COSTOS");
            tipo = 2; //ULTIMO PEDIDO
            //this.labelTotalCostos.setText("$ " + String.valueOf(mc.actualizarTotales(tablaCostosMercaderia, tablaOtrosGastos)));

        }

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaCostosMercaderia = new javax.swing.JTable();
        jLabel1 = new javax.swing.JLabel();
        labelCantBoletas = new javax.swing.JLabel();
        labelPedido = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        labelFecha = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaOtrosGastos = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        comboBoxGastos = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        txtCostoValor = new javax.swing.JTextField();
        botonAgregar = new javax.swing.JButton();
        botonEliminar = new javax.swing.JButton();
        botonFinalizar = new javax.swing.JButton();
        jLabel6 = new javax.swing.JLabel();
        labelTotalCostos = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosed(java.awt.event.WindowEvent evt) {
                formWindowClosed(evt);
            }
        });

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("MERCADERIAS"));

        tablaCostosMercaderia.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaCostosMercaderia.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaCostosMercaderiaMouseClicked(evt);
            }
        });
        tablaCostosMercaderia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaCostosMercaderiaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaCostosMercaderiaKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tablaCostosMercaderia);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 644, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("CANT BOLETAS:");

        labelCantBoletas.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelCantBoletas.setText("jLabel2");

        labelPedido.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelPedido.setText("jLabel2");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("PEDIDO NRO: ");

        labelFecha.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelFecha.setText("jLabel2");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("FECHA PEDIDO:");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("OTROS GASTOS"));

        tablaOtrosGastos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaOtrosGastos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tablaOtrosGastosMouseReleased(evt);
            }
        });
        jScrollPane3.setViewportView(tablaOtrosGastos);

        jLabel4.setText("Insertar Gastos:");

        comboBoxGastos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxGastosActionPerformed(evt);
            }
        });

        jLabel5.setText("Costo:");

        txtCostoValor.setEditable(false);
        txtCostoValor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCostoValorKeyReleased(evt);
            }
        });

        botonAgregar.setText("AGREGAR");
        botonAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarActionPerformed(evt);
            }
        });

        botonEliminar.setText("ELIMINAR");
        botonEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addComponent(botonAgregar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(comboBoxGastos, 0, 147, Short.MAX_VALUE))
                        .addGap(37, 37, 37)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(botonEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel5)
                            .addComponent(txtCostoValor, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel2Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {comboBoxGastos, txtCostoValor});

        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 229, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxGastos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCostoValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonAgregar)
                    .addComponent(botonEliminar))
                .addContainerGap())
        );

        botonFinalizar.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        botonFinalizar.setText("FINALIZAR GESTION DE COSTO");
        botonFinalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonFinalizarActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("TOTAL:");

        labelTotalCostos.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelTotalCostos.setText("jLabel2");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(jLabel2)
                                .addGap(18, 18, 18)
                                .addComponent(labelPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(32, 32, 32)
                                .addComponent(jLabel3)
                                .addGap(18, 18, 18)
                                .addComponent(labelFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(labelCantBoletas, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(labelTotalCostos, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(botonFinalizar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel6)
                        .addComponent(labelTotalCostos))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel2)
                        .addComponent(labelPedido)
                        .addComponent(jLabel3)
                        .addComponent(labelFecha)
                        .addComponent(jLabel1)
                        .addComponent(labelCantBoletas)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(botonFinalizar, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarActionPerformed
        // TODO add your handling code here:
        manejadorCosto mc = new manejadorCosto(con);
        if (comboBoxGastos.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(null, "SELECCIONE UN CONCEPTO DE GASTOS");
        } else {
            Object[] gasto = new Object[3];

            gasto[0] = mc.obtenerGastoID(comboBoxGastos.getSelectedItem().toString());
            gasto[1] = comboBoxGastos.getSelectedItem().toString();
            if (mn.isNumeric(txtCostoValor.getText())) {
                gasto[2] = txtCostoValor.getText();
                this.modeloGastos.addRow(gasto);
                txtCostoValor.setText("");
                comboBoxGastos.setSelectedIndex(-1);
                txtCostoValor.setEditable(false);
                //comboBoxGastos.setEditable(false);
                comboBoxGastos.requestFocus();
            } else {
                JOptionPane.showMessageDialog(null, "INGRESE UN COSTO VALIDO");
            }
        }


    }//GEN-LAST:event_botonAgregarActionPerformed

    private void botonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarActionPerformed
        // TODO add your handling code here:
        modeloGastos.removeRow(tablaOtrosGastos.getSelectedRow());
    }//GEN-LAST:event_botonEliminarActionPerformed

    private void tablaOtrosGastosMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaOtrosGastosMouseReleased
        // TODO add your handling code here:
        if (this.tablaOtrosGastos.getSelectedRowCount() == 1) {
            botonEliminar.setEnabled(true);
        } else {
            botonEliminar.setEnabled(false);
        }

    }//GEN-LAST:event_tablaOtrosGastosMouseReleased

    private void botonFinalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonFinalizarActionPerformed
        // TODO add your handling code here
        manejadorPedido mp = new manejadorPedido(con);
        manejadorCosto mc = new manejadorCosto(con);
        manejadorFechas mf = new manejadorFechas(con);
        float costoTotal = 0;

        if (tablaCostosMercaderia.isEditing()) {
            tablaCostosMercaderia.getCellEditor().stopCellEditing();
        }

        if (tablaOtrosGastos.isEditing()) {
            tablaOtrosGastos.getCellEditor().stopCellEditing();
        }

        this.labelTotalCostos.setText("$ " + String.valueOf(mc.actualizarTotales(tablaCostosMercaderia, tablaOtrosGastos))); //actualiza los totales en la tabla, no en la DB

        boolean correcto = true;

        for (int i = 0; i < this.tablaCostosMercaderia.getRowCount(); i++) {
            for (int j = 5; j < this.tablaCostosMercaderia.getColumnCount(); j++) {

                if (tablaCostosMercaderia.getValueAt(i, j).toString().equals("") || !(mn.isNumeric(tablaCostosMercaderia.getValueAt(i, j).toString()))) {
                    correcto = false;
                }
            }
        }

        if (correcto) {
            if (tipo == 1) {
                int n = JOptionPane.showConfirmDialog(null, "REALMENTE DESEA FINALIZAR LA GESTION DE COSTO?", "Confirmar accion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (n == 0) {

                    for (int i = 0; i < tablaCostosMercaderia.getRowCount(); i++) {
                        //mc.guardarCostoMercaderias(tablaCostosMercaderia, i, p.getId_pedido());
                        //mc.guardarCostoMercaderias2(tablaCostosMercaderia, i, p.getId_pedido());
                        float costo_unitario = Float.parseFloat(tablaCostosMercaderia.getValueAt(i, 6).toString());
                        int id_detalle = Integer.parseInt(tablaCostosMercaderia.getValueAt(i, 0).toString());
                        mc.actualizarCostosMercaderias(tablaOtrosGastos, costo_unitario, id_detalle);
                    }

                    for (int i = 0; i < tablaOtrosGastos.getRowCount(); i++) {
                        mc.guardarOtrosGastos(tablaOtrosGastos, i, p.getId_pedido());
                    }

                    costoTotal = mc.obtenerCostoTotal(p.getId_pedido());
                    mp.actualizarPedidoCosto(p.getId_pedido());

                    JOptionPane.showMessageDialog(null, "GESTION DE COSTO FINALIZADA CON EXITO");
                    JOptionPane.showMessageDialog(null, "EL COSTO TOTAL DEL PEDIDO FUE DE: " + costoTotal);

                    conexionDB c3 = new conexionDB();
                    Connection con3 = c3.conectar();
                    //manejadorBoleta mb = new manejadorBoleta();

                    //////////////////////////////////////////IMPRIMIR DOCUMENTO
                    String ruta3 = "src\\Reportes\\reporteGestionDeCostos.jrxml";
                    float totalCosto = 0;
                    String rutaPDF = mf.obtenerDireccionCarpetasPedidos(mf.obtenerFechaPedido(p.getId_pedido()));
                    File dir = new File(rutaPDF);
                    System.out.println(dir.getAbsolutePath());
                    //File reporte = new File(".");
                    dir.mkdirs();

                    DecimalFormat df = new DecimalFormat("#.00");
                    String costoTxt = "";

                    totalCosto = mc.obtenerCostoTotal(p.getId_pedido());
                    //totalCosto = (double) totalCosto;
                    //totalCosto = df.format(totalCosto);

                    costoTxt = df.format(totalCosto).toString();
                    System.out.println("costo total: " + df.format(totalCosto));

                    try {
                        Map parametro = new HashMap();
                        parametro.put("id_pedido", p.getId_pedido());
                        //parametro.put("costoTotal", totalCosto);
                        parametro.put("costoTotal", costoTxt);
                        JasperReport reporte = JasperCompileManager.compileReport(ruta3);
                        JasperPrint mostrarReporte = JasperFillManager.fillReport(reporte, parametro, con3);
                        JasperExportManager.exportReportToPdfFile(mostrarReporte, rutaPDF + "\\gestionDeCosto_pedidoNro_" + p.getId_pedido() + ".pdf");
                        JasperViewer.viewReport(mostrarReporte, false);

                    } catch (JRException ex) {
                        Logger.getLogger(Clientes.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    //this.pedidosSinCosto.dispose();
                    PedidosSinGestionDeCostos ps = new PedidosSinGestionDeCostos(con, inicio,user);
                    ps.setLocationRelativeTo(null);
                    ps.setVisible(true);

                    ////////////////////////////////////////////////////////////////////////////////////////////////////// 
                }
            } else {
                System.out.println("SE DEBEN ACTUALIZAR LOS PRECIOS");
                int n = JOptionPane.showConfirmDialog(null, "REALMENTE DESEA ACTUALIZAR LOS COSTOS?", "Confirmar accion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (n == 0) {
                    mc.eliminarCostos(0);
                    for (int i = 0; i < tablaCostosMercaderia.getRowCount(); i++) {
                        //mc.guardarCostoMercaderias(tablaCostosMercaderia, i, p.getId_pedido());
                        //mc.guardarCostoMercaderias2(tablaCostosMercaderia, i, p.getId_pedido());
                        float costo_unitario = Float.parseFloat(tablaCostosMercaderia.getValueAt(i, 6).toString());
                        int id_detalle = Integer.parseInt(tablaCostosMercaderia.getValueAt(i, 0).toString());
                        mc.actualizarCostosMercaderias(tablaOtrosGastos, costo_unitario, id_detalle);
                    }

                    for (int i = 0; i < tablaOtrosGastos.getRowCount(); i++) {
                        mc.guardarOtrosGastos(tablaOtrosGastos, i, p.getId_pedido());
                    }

                    costoTotal = mc.obtenerCostoTotal(p.getId_pedido());
                    mp.actualizarPedidoCosto(p.getId_pedido());

                    JOptionPane.showMessageDialog(null, "GESTION DE COSTOS ACTUALIZADA CON ÉXITO");
                    this.dispose();
                }

            }

        } else {

            JOptionPane.showMessageDialog(null, "EXISTEN PRECIOS SIN COMPLETAR O PRECIOS INVALIDOS, POR FAVOR REVISE NUEVAMENTE");
        }


    }//GEN-LAST:event_botonFinalizarActionPerformed

    private void comboBoxGastosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxGastosActionPerformed
        // TODO add your handling code here:
        txtCostoValor.setEditable(true);
    }//GEN-LAST:event_comboBoxGastosActionPerformed

    private void tablaCostosMercaderiaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaCostosMercaderiaKeyReleased
        // TODO add your handling code here:
        /*if (evt.getKeyChar() == evt.VK_ENTER) {

            if (tablaCostosMercaderia.isEditing()) {
                tablaCostosMercaderia.getCellEditor().stopCellEditing();
            }

            if (this.tablaCostosMercaderia.getSelectedColumn() == 5) {
                int filaS = tablaCostosMercaderia.getSelectedRow();
                float cantidad = Float.parseFloat(tablaCostosMercaderia.getValueAt(filaS, 3).toString());
                float precioU = Float.parseFloat(tablaCostosMercaderia.getValueAt(filaS, 5).toString());
                float total = cantidad * precioU;
                tablaCostosMercaderia.setValueAt(total, filaS, 6);
            }
        }*/
    }//GEN-LAST:event_tablaCostosMercaderiaKeyReleased

    private void tablaCostosMercaderiaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaCostosMercaderiaKeyPressed
        // TODO add your handling code here:

        if (evt.getKeyChar() == evt.VK_ENTER) {

            if (tablaCostosMercaderia.isEditing()) {
                tablaCostosMercaderia.getCellEditor().stopCellEditing();
            }

            if (this.tablaCostosMercaderia.getSelectedColumn() == 6) {
                int filaS = tablaCostosMercaderia.getSelectedRow();
                float cantidad = Float.parseFloat(tablaCostosMercaderia.getValueAt(filaS, 5).toString());
                float precioU = Float.parseFloat(tablaCostosMercaderia.getValueAt(filaS, 6).toString());
                float total = cantidad * precioU;
                tablaCostosMercaderia.setValueAt(total, filaS, 7);
            }
        }
    }//GEN-LAST:event_tablaCostosMercaderiaKeyPressed

    private void txtCostoValorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCostoValorKeyReleased
        // TODO add your handling code here:
        manejadorCosto mc = new manejadorCosto(con);
        if (evt.getKeyChar() == evt.VK_ENTER) {
            if (comboBoxGastos.getSelectedIndex() == -1) {
                JOptionPane.showMessageDialog(null, "SELECCIONE UN CONCEPTO DE GASTOS");
            } else {
                Object[] gasto = new Object[3];

                gasto[0] = mc.obtenerGastoID(comboBoxGastos.getSelectedItem().toString());
                gasto[1] = comboBoxGastos.getSelectedItem().toString();
                if (mn.isNumeric(txtCostoValor.getText())) {
                    gasto[2] = txtCostoValor.getText();
                    this.modeloGastos.addRow(gasto);
                    txtCostoValor.setText("");
                    comboBoxGastos.setSelectedIndex(-1);
                    txtCostoValor.setEditable(false);
                    //comboBoxGastos.setEditable(false);
                    comboBoxGastos.requestFocus();
                } else {
                    JOptionPane.showMessageDialog(null, "INGRESE UN COSTO VALIDO");
                }
            }
        }
    }//GEN-LAST:event_txtCostoValorKeyReleased

    private void tablaCostosMercaderiaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaCostosMercaderiaMouseClicked
        // TODO add your handling code here:

    }//GEN-LAST:event_tablaCostosMercaderiaMouseClicked

    private void formWindowClosed(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosed
        // TODO add your handling code here:
        if (tipo == 1) {
            PedidosSinGestionDeCostos ps = new PedidosSinGestionDeCostos(con, inicio, user);
            ps.setLocationRelativeTo(null);
            ps.setVisible(true);
        } else {
            CerrarPedido cp = new CerrarPedido(tablaBoletasSinCerrar, con, inicio,user);
            cp.setLocationRelativeTo(null);
            cp.setVisible(true);
        }

    }//GEN-LAST:event_formWindowClosed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GestionDeCostoNueva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GestionDeCostoNueva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GestionDeCostoNueva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GestionDeCostoNueva.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new GestionDeCostoNueva(null, null, null,null,null).setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAgregar;
    private javax.swing.JButton botonEliminar;
    private javax.swing.JButton botonFinalizar;
    private javax.swing.JComboBox comboBoxGastos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel labelCantBoletas;
    private javax.swing.JLabel labelFecha;
    private javax.swing.JLabel labelPedido;
    private javax.swing.JLabel labelTotalCostos;
    private javax.swing.JTable tablaCostosMercaderia;
    private javax.swing.JTable tablaOtrosGastos;
    private javax.swing.JTextField txtCostoValor;
    // End of variables declaration//GEN-END:variables
}
