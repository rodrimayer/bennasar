/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Conexion.conexionDB;
import Controlador.*;
import Modelo.Mercaderia;
import Modelo.Pedido;
import Modelo.Tipo_Cantidad;
import Modelo.Usuario;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Administrador
 */
public class GestionDeCosto extends javax.swing.JFrame {

    /**
     * Creates new form ResumenListaCompras
     */
    Connection con;

    manejadorNumeros mn = new manejadorNumeros();

    Mercaderia m = new Mercaderia();
    Tipo_Cantidad tc = new Tipo_Cantidad();
    boolean bandera = true;
    int contador = 0;

    public class MiModeloGastos extends DefaultTableModel {

        public boolean isCellEditable(int row, int column) {
            if (column == 4) { //columnIndex: the column you want to make it editable
                return true;
            } else {
                return false;
            }
        }
    }
    MiModeloGastos modeloGastos = new MiModeloGastos();
    MiModeloGastos modeloMercaderia = new MiModeloGastos();
    Pedido p = new Pedido();
    JFrame pedidosSinCosto;
    JFrame inicio;
    Usuario user;

    public GestionDeCosto(Pedido p, JFrame pedidosSinCosto, Connection con, JFrame inicio, Usuario user) {
        initComponents();
        this.setTitle("GESTION DE COSTO");
        this.setResizable(false);
        this.pedidosSinCosto = pedidosSinCosto;
        this.p = p;
        this.user = user;
        this.con = con;
        this.inicio = inicio;
        botonEditar.setEnabled(false);
        botonEliminar.setEnabled(false);
        botonEliminarMercaderia.setEnabled(false);

        manejadorPedido mp = new manejadorPedido(con);
        manejadorCosto mc = new manejadorCosto(con);
        manejadorMercaderia mm = new manejadorMercaderia(con);
        final metodoBusquedaGestion mbg = new metodoBusquedaGestion(this, con);

        labelFecha.setText(p.getFecha_pedido());
        labelCantBoletas.setText(Integer.toString(p.getCant_boletas()));

        /*this.tablaCostosMercaderia.setModel(modeloMercaderia);
        modeloMercaderia.addColumn("ID_MERCADERIA");
        modeloMercaderia.addColumn("MERCADERIA");
        modeloMercaderia.addColumn("ID_TIPO");
        modeloMercaderia.addColumn("CANTIDAD");
        modeloMercaderia.addColumn("TIPO_CANTIDAD");
        modeloMercaderia.addColumn("PRECIO UNITARIO");
        modeloMercaderia.addColumn("TOTAL");*/
        if (p.getId_pedido() != 0) {
            mp.costoMercaderia(tablaCostosMercaderia, p.getId_pedido());
            labelPedido.setText(Integer.toString(p.getId_pedido()));
            System.out.println("NO ES EL ULTIMO PEDIDO");

        } else {

            if (mc.verificarGestionCosto()) {
                mp.modeloCostoMercaderiaUltPedido(tablaCostosMercaderia, p.getId_pedido());
                labelPedido.setText("NUEVO");
                System.out.println("ES EL ULTIMO PEDIDO ");
                System.out.println("EL ULTIMO PEDIDO TIENE COSTOS CARGADOS");
                botonFinalizar.setText("ACTUALIZAR COSTOS");
            } else {
                mp.costoMercaderiaUltPedidoSinCostos(tablaCostosMercaderia, p.getId_pedido());
                labelPedido.setText("NUEVO");
                System.out.println("EL ULTIMO PEDIDO NO TIENE COSTOS CARGADOS");
            }

        }

        Object[] otrosGastos = new Object[3];

        tablaOtrosGastos.setModel(modeloGastos);
        modeloGastos.addColumn("ID");
        modeloGastos.addColumn("CONCEPTO");
        modeloGastos.addColumn("COSTO");

        //CODIGO PARA OCULTAR LA COLUMNA ID COSTO
        tablaOtrosGastos.getColumnModel().getColumn(0).setMaxWidth(0);
        tablaOtrosGastos.getColumnModel().getColumn(0).setMinWidth(0);
        tablaOtrosGastos.getColumnModel().getColumn(0).setPreferredWidth(0);

        mc.cargarCostos(comboBoxGastos);
        this.txtCostoValor.setEditable(false);
        mm.traerTodaMercaderia(comboBoxMercaderia);
        mm.traerTipoCantidad(comboBoxTipoCantidad);

        this.comboBoxMercaderia.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent evt) {

                String campo;
                String cadenaEscrita = comboBoxMercaderia.getEditor().getItem().toString();

                //System.out.println(cadenaEscrita);
                campo = "nombre";

                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (mbg.compararMercaderia(cadenaEscrita)) {// compara si el texto escrito se encuentra en la lista
                        // busca el texto escrito en la base de datos                      

                        obtenerMercaderia(cadenaEscrita);

                        comboBoxTipoCantidad.requestFocus();
                        botonAceptarMercaderia.setVisible(true);

                    } else {// en caso contrario toma como default el elemento 0 o sea el primero de la lista y lo busca.
                        if (comboBoxMercaderia.getItemAt(0) != null) {

                            comboBoxMercaderia.setSelectedIndex(0);
                            obtenerMercaderia(comboBoxMercaderia.getEditor().getItem().toString());
                            comboBoxTipoCantidad.requestFocus();
                        }

                    }

                }
                if (evt.getKeyCode() >= 65 && evt.getKeyCode() <= 90 || evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105 || evt.getKeyCode() == 8) {

                    comboBoxMercaderia.setModel(mbg.getListaMercaderia(cadenaEscrita, campo));

                    if (comboBoxMercaderia.getItemCount() > 0) {
                        comboBoxMercaderia.getEditor().setItem(cadenaEscrita);
                        comboBoxMercaderia.showPopup();

                    } else {
                        comboBoxMercaderia.addItem(cadenaEscrita);
                    }
                }

            }
        });

        this.comboBoxTipoCantidad.getEditor().getEditorComponent().addKeyListener(new KeyAdapter() {

            public void keyReleased(KeyEvent evt) {

                String campo;
                String cadenaEscrita = comboBoxTipoCantidad.getEditor().getItem().toString();

                //System.out.println(cadenaEscrita);
                campo = "tipo_cantidad";

                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    if (mbg.compararTipo(cadenaEscrita)) {// compara si el texto escrito se encuentra en la lista
                        // busca el texto escrito en la base de datos                      

                        obtenerTipoCantidad(cadenaEscrita);

                        txtCantidad.requestFocus();
                        botonAceptarMercaderia.setVisible(true);

                    } else {// en caso contrario toma como default el elemento 0 o sea el primero de la lista y lo busca.
                        if (comboBoxTipoCantidad.getItemAt(0) != null) {

                            comboBoxTipoCantidad.setSelectedIndex(0);
                            obtenerTipoCantidad(comboBoxTipoCantidad.getEditor().getItem().toString());
                            txtCantidad.requestFocus();
                        }

                    }

                }
                if (evt.getKeyCode() >= 65 && evt.getKeyCode() <= 90 || evt.getKeyCode() >= 96 && evt.getKeyCode() <= 105 || evt.getKeyCode() == 8) {

                    comboBoxTipoCantidad.setModel(mbg.getListaTipo(cadenaEscrita, campo));

                    if (comboBoxTipoCantidad.getItemCount() > 0) {
                        comboBoxTipoCantidad.getEditor().setItem(cadenaEscrita);
                        comboBoxTipoCantidad.showPopup();

                    } else {
                        comboBoxTipoCantidad.addItem(cadenaEscrita);
                    }
                }

            }
        });

    }

    public void obtenerMercaderia(String bus) {

        String sql = "SELECT id_mercaderia, nombre from mercaderia "
                + "where nombre = '" + bus + "'";

        String codigo = "";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                m.setId_mercaderia(rs1.getString(1));
                m.setNombre(rs1.getString(2));
            }

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

    }

    public void obtenerTipoCantidad(String bus) {

        String sql = "SELECT id_tipocant, tipo_cantidad from tipocantidad "
                + "where tipo_cantidad = '" + bus + "'";

        String codigo = "";

        System.out.println(sql);

        conexionDB c = new conexionDB();
        Connection con = c.conectar();

        try {
            Statement st1 = con.createStatement();
            ResultSet rs1 = st1.executeQuery(sql);

            while (rs1.next()) {
                tc.setId_tipocant(rs1.getInt(1));
                tc.setTipo_cantidad(rs1.getString(2));
            }

        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

    }

    public JComboBox getcomboBoxMercaderia() {
        return comboBoxMercaderia;
    }

    public void setcomboBoxMercaderia(JComboBox comboBoxMercaderia) {
        this.comboBoxMercaderia = comboBoxMercaderia;
    }

    public JComboBox getcomboBoxTipoCantidad() {
        return comboBoxTipoCantidad;
    }

    public void setcomboBoxTipoMercaderia(JComboBox comboBoxTipoCantidad) {
        this.comboBoxTipoCantidad = comboBoxTipoCantidad;
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablaCostosMercaderia = new javax.swing.JTable();
        botonEliminarMercaderia = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        labelCantBoletas = new javax.swing.JLabel();
        labelPedido = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        labelFecha = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tablaOtrosGastos = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        comboBoxGastos = new javax.swing.JComboBox();
        jLabel5 = new javax.swing.JLabel();
        txtCostoValor = new javax.swing.JTextField();
        botonAgregar = new javax.swing.JButton();
        botonEditar = new javax.swing.JButton();
        botonEliminar = new javax.swing.JButton();
        botonFinalizar = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        comboBoxMercaderia = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        comboBoxTipoCantidad = new javax.swing.JComboBox();
        jLabel8 = new javax.swing.JLabel();
        txtCantidad = new javax.swing.JTextField();
        botonAceptarMercaderia = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder("MERCADERIAS"));

        tablaCostosMercaderia.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaCostosMercaderia.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablaCostosMercaderiaMouseClicked(evt);
            }
        });
        tablaCostosMercaderia.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                tablaCostosMercaderiaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                tablaCostosMercaderiaKeyReleased(evt);
            }
        });
        jScrollPane1.setViewportView(tablaCostosMercaderia);

        botonEliminarMercaderia.setText("ELIMINAR MERCADERIA SELECCIONADA");
        botonEliminarMercaderia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarMercaderiaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(botonEliminarMercaderia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 680, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 298, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(botonEliminarMercaderia)
                .addContainerGap())
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("CANT BOLETAS:");

        labelCantBoletas.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelCantBoletas.setText("jLabel2");

        labelPedido.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelPedido.setText("jLabel2");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("PEDIDO NRO: ");

        labelFecha.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        labelFecha.setText("jLabel2");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("FECHA PEDIDO:");

        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder("OTROS GASTOS"));

        tablaOtrosGastos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablaOtrosGastos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseReleased(java.awt.event.MouseEvent evt) {
                tablaOtrosGastosMouseReleased(evt);
            }
        });
        jScrollPane3.setViewportView(tablaOtrosGastos);

        jLabel4.setText("Insertar Gastos:");

        comboBoxGastos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboBoxGastosActionPerformed(evt);
            }
        });

        jLabel5.setText("Costo:");

        txtCostoValor.setEditable(false);
        txtCostoValor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCostoValorKeyReleased(evt);
            }
        });

        botonAgregar.setText("AGREGAR");
        botonAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAgregarActionPerformed(evt);
            }
        });

        botonEditar.setText("EDITAR");
        botonEditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEditarActionPerformed(evt);
            }
        });

        botonEliminar.setText("ELIMINAR");
        botonEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonEliminarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addComponent(comboBoxGastos, javax.swing.GroupLayout.PREFERRED_SIZE, 147, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(37, 37, 37)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addComponent(txtCostoValor)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addComponent(botonAgregar)
                        .addGap(51, 51, 51)
                        .addComponent(botonEditar, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(51, 51, 51)
                        .addComponent(botonEliminar)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(comboBoxGastos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCostoValor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(botonAgregar)
                    .addComponent(botonEditar)
                    .addComponent(botonEliminar))
                .addContainerGap())
        );

        botonFinalizar.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        botonFinalizar.setText("FINALIZAR GESTION DE COSTO");
        botonFinalizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonFinalizarActionPerformed(evt);
            }
        });

        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder("Carga Manual de Mercaderia"));

        jLabel6.setText("MERCADERIA: ");

        comboBoxMercaderia.setEditable(true);

        jLabel7.setText("TIPO CANTIDAD:");

        comboBoxTipoCantidad.setEditable(true);

        jLabel8.setText("CANTIDAD:");

        txtCantidad.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCantidadKeyReleased(evt);
            }
        });

        botonAceptarMercaderia.setText("ACEPTAR");
        botonAceptarMercaderia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                botonAceptarMercaderiaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6)
                    .addComponent(jLabel7))
                .addGap(24, 24, 24)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(comboBoxTipoCantidad, 0, 170, Short.MAX_VALUE)
                    .addComponent(comboBoxMercaderia, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 64, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel8)
                        .addGap(62, 62, 62)
                        .addComponent(txtCantidad, javax.swing.GroupLayout.DEFAULT_SIZE, 153, Short.MAX_VALUE))
                    .addComponent(botonAceptarMercaderia, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(57, 57, 57))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(jLabel6))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jLabel8)
                                .addComponent(txtCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(comboBoxMercaderia, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(30, 30, 30)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel7))
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(comboBoxTipoCantidad, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(botonAceptarMercaderia)))
                .addContainerGap(27, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(labelPedido, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(32, 32, 32)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addComponent(labelFecha, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 69, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(labelCantBoletas, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(botonFinalizar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(labelPedido)
                    .addComponent(jLabel3)
                    .addComponent(labelFecha)
                    .addComponent(jLabel1)
                    .addComponent(labelCantBoletas))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(botonFinalizar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void botonAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAgregarActionPerformed
        // TODO add your handling code here:
        manejadorCosto mc = new manejadorCosto(con);
        if (comboBoxGastos.getSelectedIndex() == -1) {
            JOptionPane.showMessageDialog(null, "SELECCIONE UN CONCEPTO DE GASTOS");
        } else {
            Object[] gasto = new Object[3];

            gasto[0] = mc.obtenerGastoID(comboBoxGastos.getSelectedItem().toString());
            gasto[1] = comboBoxGastos.getSelectedItem().toString();
            if (mn.isNumeric(txtCostoValor.getText())) {
                gasto[2] = txtCostoValor.getText();
                this.modeloGastos.addRow(gasto);
                txtCostoValor.setText("");
                comboBoxGastos.setSelectedIndex(-1);
                txtCostoValor.setEditable(false);
                //comboBoxGastos.setEditable(false);
                comboBoxGastos.requestFocus();
            } else {
                JOptionPane.showMessageDialog(null, "INGRESE UN COSTO VALIDO");
            }
        }


    }//GEN-LAST:event_botonAgregarActionPerformed

    private void botonEditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEditarActionPerformed
        // TODO add your handling code here:
        int filaS = tablaOtrosGastos.getSelectedRow();

        //comboBoxGastos.setEditable(true);             
        //comboBoxGastos.getEditor().setItem(tablaOtrosGastos.getValueAt(filaS, 1));
        comboBoxGastos.setSelectedItem(tablaOtrosGastos.getValueAt(filaS, 1));
        //comboBoxGastos.setEditable(false); 
        txtCostoValor.setText((tablaOtrosGastos.getValueAt(filaS, 2).toString()));

        modeloGastos.removeRow(filaS);
    }//GEN-LAST:event_botonEditarActionPerformed

    private void botonEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarActionPerformed
        // TODO add your handling code here:
        modeloGastos.removeRow(tablaOtrosGastos.getSelectedRow());
    }//GEN-LAST:event_botonEliminarActionPerformed

    private void tablaOtrosGastosMouseReleased(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaOtrosGastosMouseReleased
        // TODO add your handling code here:

        if (this.tablaOtrosGastos.getSelectedRowCount() == 1) {
            botonEditar.setEnabled(true);
            botonEliminar.setEnabled(true);
        } else {
            botonEditar.setEnabled(false);
            botonEliminar.setEnabled(false);
        }
    }//GEN-LAST:event_tablaOtrosGastosMouseReleased

    private void botonFinalizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonFinalizarActionPerformed
        // TODO add your handling code here
        manejadorPedido mp = new manejadorPedido(con);
        manejadorCosto mc = new manejadorCosto(con);
        manejadorFechas mf = new manejadorFechas(con);
        float costoTotal = 0;

        if (tablaCostosMercaderia.isEditing()) {
            tablaCostosMercaderia.getCellEditor().stopCellEditing();
        }

        mc.actualizarTotales(tablaCostosMercaderia, tablaOtrosGastos); //actualiza los totales en la tabla, no en la DB

        boolean correcto = true;

        for (int i = 0; i < this.tablaCostosMercaderia.getRowCount(); i++) {
            for (int j = 5; j < this.tablaCostosMercaderia.getColumnCount(); j++) {

                if (tablaCostosMercaderia.getValueAt(i, j).toString().equals("") || !(mn.isNumeric(tablaCostosMercaderia.getValueAt(i, j).toString()))) {
                    correcto = false;
                }
            }
        }

        if (correcto) {
            if (p.getId_pedido() != 0) {
                int n = JOptionPane.showConfirmDialog(null, "REALMENTE DESEA FINALIZAR LA GESTION DE COSTO?", "Confirmar accion", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

                if (n == 0) {

                    for (int i = 0; i < tablaCostosMercaderia.getRowCount(); i++) {
                        //mc.guardarCostoMercaderias(tablaCostosMercaderia, i, p.getId_pedido());
                        mc.guardarCostoMercaderias2(tablaCostosMercaderia, i, p.getId_pedido());
                    }

                    for (int i = 0; i < tablaOtrosGastos.getRowCount(); i++) {
                        mc.guardarOtrosGastos(tablaOtrosGastos, i, p.getId_pedido());
                    }

                    costoTotal = mc.obtenerCostoTotal(p.getId_pedido());
                    mp.actualizarPedidoCosto(p.getId_pedido());

                    JOptionPane.showMessageDialog(null, "GESTION DE COSTO FINALIZADA CON EXITO");
                    JOptionPane.showMessageDialog(null, "EL COSTO TOTAL DEL PEDIDO FUE DE: " + costoTotal);

                    //conexionDB c3 = new conexionDB();
                    //Connection con3 = c3.conectar();
                    manejadorBoleta mb = new manejadorBoleta(con);

//////////////////////////////////////////IMPRIMIR DOCUMENTO
                    String ruta3 = "src\\Reportes\\reporteGestionDeCostos.jrxml";
                    float totalCosto = 0;
                    String rutaPDF = mf.obtenerDireccionCarpetasPedidos(mf.obtenerFechaPedido(p.getId_pedido()));
                    File dir = new File(rutaPDF);
                    System.out.println(dir.getAbsolutePath());
                    //File reporte = new File(".");
                    dir.mkdirs();

                    DecimalFormat df = new DecimalFormat("#.00");
                    String costoTxt = "";

                    totalCosto = mc.obtenerCostoTotal(p.getId_pedido());
                    //totalCosto = (double) totalCosto;
                    //totalCosto = df.format(totalCosto);

                    costoTxt = df.format(totalCosto).toString();
                    System.out.println("costo total: " + df.format(totalCosto));

                    try {
                        Map parametro = new HashMap();
                        parametro.put("id_pedido", p.getId_pedido());
                        //parametro.put("costoTotal", totalCosto);
                        parametro.put("costoTotal", costoTxt);
                        JasperReport reporte = JasperCompileManager.compileReport(ruta3);
                        JasperPrint mostrarReporte = JasperFillManager.fillReport(reporte, parametro, con);
                        JasperExportManager.exportReportToPdfFile(mostrarReporte, rutaPDF + "\\gestionDeCosto_pedidoNro_" + p.getId_pedido() + ".pdf");
                        JasperViewer.viewReport(mostrarReporte, false);

                    } catch (JRException ex) {
                        Logger.getLogger(Clientes.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    this.pedidosSinCosto.dispose();
                    PedidosSinGestionDeCostos ps = new PedidosSinGestionDeCostos(con, inicio,user);
                    ps.setLocationRelativeTo(null);
                    ps.setVisible(true);

////////////////////////////////////////////////////////////////////////////////////////////////////// 
                    this.dispose();
                }
            } else {
                System.out.println("SE DEBEN ACTUALIZAR LOS PRECIOS");
            }

        } else {

            JOptionPane.showMessageDialog(null, "EXISTEN PRECIOS SIN COMPLETAR O PRECIOS INVALIDOS, POR FAVOR REVISE NUEVAMENTE");
        }


    }//GEN-LAST:event_botonFinalizarActionPerformed

    private void comboBoxGastosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboBoxGastosActionPerformed
        // TODO add your handling code here:
        txtCostoValor.setEditable(true);
    }//GEN-LAST:event_comboBoxGastosActionPerformed

    private void botonAceptarMercaderiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonAceptarMercaderiaActionPerformed
        // TODO add your handling code here:
        manejadorPedido mp = new manejadorPedido(con);
        contador = 0;

        if (!(mn.isNumeric(txtCantidad.getText().toString()))) {
            JOptionPane.showMessageDialog(null, "INGRESE UNA CANTIDAD VALIDAD");
        } else if (comboBoxMercaderia.getSelectedIndex() == -1 || comboBoxMercaderia.getSelectedItem().toString().equals("")) {
            JOptionPane.showMessageDialog(null, "INGRESE UNA MERCADERIA");

        } else if (comboBoxTipoCantidad.getSelectedIndex() == -1 || comboBoxTipoCantidad.getSelectedItem().toString().equals("")) {
            JOptionPane.showMessageDialog(null, "INGRESE UN TIPO DE CANTIDAD");
        } else {

            float cantidad = Float.parseFloat(txtCantidad.getText());

            /*
            System.out.println("MERCADERIA A AGREGAR: "+comboBoxMercaderia.getEditor().getItem().toString());
            m.setId_mercaderia(null);
            m.setNombre(comboBoxMercaderia.getEditor().getItem().toString());
            tc.setId_tipocant(0);
            System.out.println("TIPO: "+comboBoxTipoCantidad.getEditor().getItem().toString());
            tc.setTipo_cantidad(null);*/
            obtenerMercaderia(comboBoxMercaderia.getEditor().getItem().toString());
            obtenerTipoCantidad(comboBoxTipoCantidad.getEditor().getItem().toString());
            mp.cargarNuevoCosto(m, tc, cantidad);

            comboBoxMercaderia.setSelectedIndex(-1);
            comboBoxTipoCantidad.setSelectedIndex(-1);
            txtCantidad.setText("");

            comboBoxMercaderia.requestFocus();
        }

    }//GEN-LAST:event_botonAceptarMercaderiaActionPerformed

    private void txtCantidadKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCantidadKeyReleased
        // TODO add your handling code here:
        manejadorPedido mp = new manejadorPedido(con);

        if (evt.getKeyChar() == evt.VK_ENTER) {

            if ((contador % 2) == 0) {
                if (!(mn.isNumeric(txtCantidad.getText().toString()))) {
                    JOptionPane.showMessageDialog(null, "INGRESE UNA CANTIDAD VALIDAD");
                    //bandera = false;
                    contador++;
                } else if (comboBoxMercaderia.getSelectedIndex() == -1 || comboBoxMercaderia.getSelectedItem().toString().equals("")) {
                    JOptionPane.showMessageDialog(null, "INGRESE UNA MERCADERIA");
                    //bandera = false;
                    contador++;
                } else if (comboBoxTipoCantidad.getSelectedIndex() == -1 || comboBoxTipoCantidad.getSelectedItem().toString().equals("")) {
                    JOptionPane.showMessageDialog(null, "INGRESE UN TIPO DE CANTIDAD");
                    //bandera = false;
                    contador++;
                } else {

                    float cantidad = Float.parseFloat(txtCantidad.getText());

                    mp.cargarNuevoCosto(m, tc, cantidad);

                    comboBoxMercaderia.setSelectedIndex(-1);
                    comboBoxTipoCantidad.setSelectedIndex(-1);
                    txtCantidad.setText("");

                    comboBoxMercaderia.requestFocus();
                    //bandera = true;
                }
            } else {
                contador++;
            }

            //contador++;
            System.out.println("contador: " + contador);

        }
    }//GEN-LAST:event_txtCantidadKeyReleased

    private void tablaCostosMercaderiaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaCostosMercaderiaKeyReleased
        // TODO add your handling code here:
        /*if (evt.getKeyChar() == evt.VK_ENTER) {

            if (tablaCostosMercaderia.isEditing()) {
                tablaCostosMercaderia.getCellEditor().stopCellEditing();
            }

            if (this.tablaCostosMercaderia.getSelectedColumn() == 5) {
                int filaS = tablaCostosMercaderia.getSelectedRow();
                float cantidad = Float.parseFloat(tablaCostosMercaderia.getValueAt(filaS, 3).toString());
                float precioU = Float.parseFloat(tablaCostosMercaderia.getValueAt(filaS, 5).toString());
                float total = cantidad * precioU;
                tablaCostosMercaderia.setValueAt(total, filaS, 6);
            }
        }*/
    }//GEN-LAST:event_tablaCostosMercaderiaKeyReleased

    private void tablaCostosMercaderiaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_tablaCostosMercaderiaKeyPressed
        // TODO add your handling code here:

        if (evt.getKeyChar() == evt.VK_ENTER) {

            if (tablaCostosMercaderia.isEditing()) {
                tablaCostosMercaderia.getCellEditor().stopCellEditing();
            }

            if (this.tablaCostosMercaderia.getSelectedColumn() == 5) {
                int filaS = tablaCostosMercaderia.getSelectedRow();
                float cantidad = Float.parseFloat(tablaCostosMercaderia.getValueAt(filaS, 3).toString());
                float precioU = Float.parseFloat(tablaCostosMercaderia.getValueAt(filaS, 5).toString());
                float total = cantidad * precioU;
                tablaCostosMercaderia.setValueAt(total, filaS, 6);
            }
        }
    }//GEN-LAST:event_tablaCostosMercaderiaKeyPressed

    private void txtCostoValorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCostoValorKeyReleased
        // TODO add your handling code here:
        manejadorCosto mc = new manejadorCosto(con);
        if (evt.getKeyChar() == evt.VK_ENTER) {
            if (comboBoxGastos.getSelectedIndex() == -1) {
                JOptionPane.showMessageDialog(null, "SELECCIONE UN CONCEPTO DE GASTOS");
            } else {
                Object[] gasto = new Object[3];

                gasto[0] = mc.obtenerGastoID(comboBoxGastos.getSelectedItem().toString());
                gasto[1] = comboBoxGastos.getSelectedItem().toString();
                if (mn.isNumeric(txtCostoValor.getText())) {
                    gasto[2] = txtCostoValor.getText();
                    this.modeloGastos.addRow(gasto);
                    txtCostoValor.setText("");
                    comboBoxGastos.setSelectedIndex(-1);
                    txtCostoValor.setEditable(false);
                    //comboBoxGastos.setEditable(false);
                    comboBoxGastos.requestFocus();
                } else {
                    JOptionPane.showMessageDialog(null, "INGRESE UN COSTO VALIDO");
                }
            }
        }
    }//GEN-LAST:event_txtCostoValorKeyReleased

    private void botonEliminarMercaderiaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_botonEliminarMercaderiaActionPerformed
        // TODO add your handling code here:

        int filaS = this.tablaCostosMercaderia.getSelectedRow();

        DefaultTableModel modelo = (DefaultTableModel) tablaCostosMercaderia.getModel();

        modelo.removeRow(filaS);
        botonEliminarMercaderia.setEnabled(false);
    }//GEN-LAST:event_botonEliminarMercaderiaActionPerformed

    private void tablaCostosMercaderiaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablaCostosMercaderiaMouseClicked
        // TODO add your handling code here:

        int sel = this.tablaCostosMercaderia.getSelectedRowCount();

        if (sel == 1) {
            botonEliminarMercaderia.setEnabled(true);
        } else {
            botonEliminarMercaderia.setEnabled(false);
        }
    }//GEN-LAST:event_tablaCostosMercaderiaMouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /*
         * Set the Nimbus look and feel
         */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /*
         * If Nimbus (introduced in Java SE 6) is not available, stay with the
         * default look and feel. For details see
         * http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(GestionDeCosto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(GestionDeCosto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(GestionDeCosto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(GestionDeCosto.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /*
         * Create and display the form
         */
        java.awt.EventQueue.invokeLater(new Runnable() {

            public void run() {
                new GestionDeCosto(null, null, null, null, null).setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton botonAceptarMercaderia;
    private javax.swing.JButton botonAgregar;
    private javax.swing.JButton botonEditar;
    private javax.swing.JButton botonEliminar;
    private javax.swing.JButton botonEliminarMercaderia;
    private javax.swing.JButton botonFinalizar;
    private javax.swing.JComboBox comboBoxGastos;
    private javax.swing.JComboBox comboBoxMercaderia;
    private javax.swing.JComboBox comboBoxTipoCantidad;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JLabel labelCantBoletas;
    private javax.swing.JLabel labelFecha;
    private javax.swing.JLabel labelPedido;
    private javax.swing.JTable tablaCostosMercaderia;
    private javax.swing.JTable tablaOtrosGastos;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtCostoValor;
    // End of variables declaration//GEN-END:variables
}
