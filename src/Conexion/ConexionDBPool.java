/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Conexion;

import java.sql.Connection;
import java.sql.SQLException;
import org.apache.commons.dbcp2.BasicDataSource;

/**
 *
 * @author rmmayer
 */

int tipo;
public class ConexionDBPool{

    //con = DriverManager.getConnection("jdbc:mysql://sql550.main-hosting.eu/u984580419_bd_delivery?autoReconnect=true&amp","u984580419_rmmayer","1103RodriM");
    
    int tipo;
    private final String DB = "db_delivery";
    private final String URL = "jdbc:mysql://localhost/"+DB;
    private final String USER = "root";
    private final String PASS = "";
    
    private final String DB_prod = "u984580419_bd_delivery";
    private final String DB_tst = "u984580419_delivery_tst";
    private final String DB_qa = "u984580419_delivery_QA";
    
    private final String URL_prod = "jdbc:mysql://sql550.main-hosting.eu/"+DB_prod+"?autoReconnect=true&amp;autoReconnectForPools=true";
    private final String URL_tst = "jdbc:mysql://sql550.main-hosting.eu/"+DB_tst+"?autoReconnect=true&amp;autoReconnectForPools=true";
    private final String URL_qa = "jdbc:mysql://sql550.main-hosting.eu/"+DB_qa+"?autoReconnect=true&amp;autoReconnectForPools=true";
    //private final String URL2 = "jdbc:mysql://sql550.main-hosting.eu/"+DB2;
    
    private final String USER_prod = "u984580419_rmmayer";
    private final String USER_tst = "u984580419_delivery_tst";
    private final String USER_qa = "u984580419_delivery_QA";
    
    private final String PASS_prod = "1103RodriM";
    private final String PASS_qa = "deliveryQA23";

    private static ConexionDBPool dataSource;
    private BasicDataSource basicDataSource = null;

    private ConexionDBPool(int tipo) {
        basicDataSource = new BasicDataSource();
        //basicDataSource.setDriverClassName("com.mysql.cj.jdbc.Driver");
        basicDataSource.setDriverClassName("com.mysql.jdbc.Driver");
        basicDataSource.setUsername(USER_qa);
        basicDataSource.setPassword(PASS_qa);
        basicDataSource.setUrl(URL_qa);

        basicDataSource.setMinIdle(5);
        basicDataSource.setMaxIdle(20);
        basicDataSource.setMaxTotal(50);
        //basicDataSource.addConnectionProperty("useSSL", "true");
        //basicDataSource.setMaxWaitMillis(1);
        //basicDataSource.set
        //basicDataSource.set

    }

    public static ConexionDBPool getInstance() {
        if (dataSource == null) {
            dataSource = new ConexionDBPool(tipo);
            return dataSource;
        } else {
            return dataSource;
        }
    }
    
    public Connection getConection(){
        Connection con = null;
        
        try{
            con = this.basicDataSource.getConnection();
            
        }catch(SQLException ex){
            System.out.println(ex.getMessage());
        }
        
        return con;
    }
    
    public void closeConnection(Connection con) throws SQLException{
        con.close();
    }

}
