-- phpMyAdmin SQL Dump
-- version 4.0.4
-- http://www.phpmyadmin.net
--
-- Servidor: localhost
-- Tiempo de generación: 20-06-2017 a las 01:05:40
-- Versión del servidor: 5.6.12-log
-- Versión de PHP: 5.4.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `db_delivery`
--
CREATE DATABASE IF NOT EXISTS `db_delivery` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `db_delivery`;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `boleta`
--

CREATE TABLE IF NOT EXISTS `boleta` (
  `id_boleta` int(5) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(5) NOT NULL,
  `fecha` date NOT NULL,
  `zona` int(4) NOT NULL,
  `id_pedido` int(1) NOT NULL,
  `cantidad_items` int(5) NOT NULL,
  PRIMARY KEY (`id_boleta`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

--
-- Volcado de datos para la tabla `boleta`
--

INSERT INTO `boleta` (`id_boleta`, `id_cliente`, `fecha`, `zona`, `id_pedido`, `cantidad_items`) VALUES
(1, 15, '2017-01-15', 1, 1, 6),
(2, 17, '2017-01-15', 1, 1, 4),
(3, 14, '2017-01-16', 2, 1, 7),
(4, 18, '2017-01-16', 2, 1, 2),
(5, 14, '2017-01-17', 2, 1, 2),
(7, 16, '2017-01-17', 5, 1, 1),
(8, 12, '2017-01-17', 3, 1, 8),
(9, 15, '2017-01-19', 2, 2, 2),
(10, 13, '2017-01-19', 1, 1, 4),
(11, 19, '2017-01-19', 1, 2, 3),
(12, 24, '2017-01-19', 1, 1, 1),
(13, 22, '2017-01-19', 1, 1, 1),
(14, 14, '2017-01-19', 2, 1, 2),
(15, 17, '2017-01-19', 1, 2, 3),
(16, 22, '2017-01-22', 2, 2, 23),
(18, 16, '2017-01-22', 3, 2, 1),
(19, 13, '2017-01-22', 3, 2, 1),
(20, 17, '2017-01-23', 3, 2, 4),
(21, 17, '2017-02-09', 2, 3, 4),
(22, 18, '2017-02-09', 2, 3, 2),
(23, 29, '2017-03-02', 2, 4, 2),
(24, 29, '2017-03-02', 1, 3, 3),
(25, 24, '2017-03-04', 2, 4, 6),
(26, 17, '2017-03-06', 1, 5, 4),
(27, 19, '2017-03-06', 1, 5, 5),
(28, 29, '2017-03-06', 2, 5, 2),
(29, 29, '2017-03-09', 2, 6, 3),
(30, 17, '2017-03-09', 2, 6, 5),
(31, 15, '2017-03-09', 1, 6, 3),
(32, 17, '2017-03-09', 1, 0, 5),
(33, 12, '2017-03-11', 4, 0, 7);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE IF NOT EXISTS `categoria` (
  `id_categoria` int(5) NOT NULL AUTO_INCREMENT,
  `descuento` float NOT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cliente`
--

CREATE TABLE IF NOT EXISTS `cliente` (
  `id_cliente` int(5) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(30) NOT NULL,
  `direccion` varchar(100) NOT NULL,
  `id_localidad` int(5) NOT NULL,
  `inactivo` int(11) NOT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Volcado de datos para la tabla `cliente`
--

INSERT INTO `cliente` (`id_cliente`, `nombre`, `direccion`, `id_localidad`, `inactivo`) VALUES
(12, 'Cafe 25', 'San Martin', 1, 0),
(13, 'Cafe Tacuba', '25 de Mayo 300', 2, 0),
(14, 'Il Postino', 'Mendoza y 25 de Mayo', 1, 0),
(15, 'Ni Fu Ni Fa', 'Corrientes', 1, 0),
(16, 'William', 'Aconquija 1305', 2, 0),
(17, 'Mc Donald', '25 de mayo y San Juan', 1, 0),
(18, 'ABCD', '9 de julio', 1, 0),
(19, 'Juanito Perez', 'sdasd', 1, 0),
(20, 'Pedrito', 'asdasdad', 1, 0),
(21, 'Juana VIP', 'Peatonal', 6, 0),
(22, 'Don Pepe', 'Pasaje 2 de Abril', 1, 0),
(23, 'Juan Ignacio Pepegrillo', 'maipu', 1, 0),
(24, 'Rey', 'Chacabuco frente plaza san martin', 1, 0),
(26, 'conrado', 'farmacia itati', 1, 1),
(27, 'Rodrigo', 'Maipu 76', 1, 1),
(28, 'OLIVAS', 'AV. 24 DE SEPTIEMBRE ESQUINA JUNIN', 1, 0),
(29, 'Rock and Charly', 'Shopping el Solar', 2, 0),
(30, 'Agostina Rubio', 'Barrio Santa Rosa', 6, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `concepto_gastos`
--

CREATE TABLE IF NOT EXISTS `concepto_gastos` (
  `id_gastos` int(11) NOT NULL AUTO_INCREMENT,
  `concepto` varchar(20) NOT NULL,
  PRIMARY KEY (`id_gastos`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `concepto_gastos`
--

INSERT INTO `concepto_gastos` (`id_gastos`, `concepto`) VALUES
(1, 'Mercaderia'),
(2, 'Entrada'),
(3, 'Changadas'),
(4, 'Combustible'),
(5, 'Varios');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `contacto`
--

CREATE TABLE IF NOT EXISTS `contacto` (
  `id_contacto` int(5) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(5) NOT NULL,
  `dueño` varchar(20) NOT NULL,
  `encargado1` text NOT NULL,
  `encargado2` text NOT NULL,
  `encargado3` text NOT NULL,
  `encargado4` text NOT NULL,
  `telefono` varchar(30) NOT NULL,
  `celular` varchar(30) NOT NULL,
  `mail` varchar(30) NOT NULL,
  PRIMARY KEY (`id_contacto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=21 ;

--
-- Volcado de datos para la tabla `contacto`
--

INSERT INTO `contacto` (`id_contacto`, `id_cliente`, `dueño`, `encargado1`, `encargado2`, `encargado3`, `encargado4`, `telefono`, `celular`, `mail`) VALUES
(1, 12, 'Rodrigo', 'Leandro\ntarde', 'Pepe\ntarde-noche', 'Jose Di Marco\n381525523', 'Facu Barros\n\n3886450042', '3886', '3886', 'rodrimmayer@gmail.com'),
(2, 13, 'Facu Barros', 'Juan perez\nMañana\n8-12', 'Pepe Argento\nTarde\n15-21', 'Rodrigo\nNoche\n21-00', '', '388541235', '551188885', 'facula12xeneixe@boca.com'),
(3, 14, 'Carla Tapia', 'Juan Cruz\nLlamar al 427285', 'Leandro Mayer', '', 'Santi', '3118189', '151818', 'carla@ilpostino.com'),
(4, 15, 'Milton Casco', 'Juan', 'pedro', 'Juanito', 'Maria', '284188', '8848', 'sdasd@gmail.com'),
(5, 16, 'Nicolas Paz', 'Garavente\nasdasd\nwoqi\ns', '', '', '', '25952925', '181848', 'Jpaz@william.com.ar'),
(6, 17, 'Agustin Mendez', 'Lucho\nmañana y tarde', 'Nacho\nnoche\n12-5am', 'sadsadasd', '', '258186', '51929629', 'gral@gmail.com'),
(7, 18, 'pepe', 'Juan', '', '', '', '818', '888', 'asdsad@'),
(8, 19, 'sadsada', 'sdasdas', 'sadsadsa', 'asaa', '', '185181', '184891', 'sadasdad'),
(9, 20, 'asdasd', 'asdasd	sadasd', 'asdsad', '', '', '48198', '8881', 'asdasdasd'),
(10, 21, 'Pipi', 'Flaco Schiavi\ntodo el dia', 'Pipi\nSe fue a Sta Fe', '', '', '219292', '292929', 'juanavip@gmail.com'),
(11, 22, 'Pepe Argento', 'Leo\nTarde', 'facu bardo\nmediodia', '', '', '2155151', '5185151', 'donpepe@yahoo.com.ar'),
(12, 23, '', '', '', '', '', '388644774848', '388644778', ''),
(13, 24, '', '', '', '', '', '3886447784', '518181818185', ''),
(15, 26, '', '', '', '', '', '', '', ''),
(16, 27, 'rodrigo mayer', '', '', '', '', '429132', '', ''),
(17, 0, '', '', '', '', '', '', '', ''),
(18, 28, '', '', '', '', '', '', '', ''),
(19, 29, 'Pepe', 'Jesus', '', '', '', '', '', ''),
(20, 30, 'Agostina', 'Iara', '', '', '', '', '', 'agosrubio@gmail.com');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle`
--

CREATE TABLE IF NOT EXISTS `detalle` (
  `id_detalle` int(5) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(5) NOT NULL,
  `id_mercaderia` varchar(20) NOT NULL,
  `precio` float DEFAULT NULL,
  `cantidad` float NOT NULL,
  `id_tipocant` int(11) NOT NULL,
  PRIMARY KEY (`id_detalle`),
  KEY `id_tipocant` (`id_tipocant`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=193 ;

--
-- Volcado de datos para la tabla `detalle`
--

INSERT INTO `detalle` (`id_detalle`, `id_boleta`, `id_mercaderia`, `precio`, `cantidad`, `id_tipocant`) VALUES
(1, 1, 'LR', NULL, 2.5, 1),
(2, 1, 'PER', NULL, 3, 1),
(3, 1, 'FR', NULL, 6, 3),
(4, 1, 'HB', NULL, 6, 5),
(5, 1, 'TC', NULL, 6, 1),
(6, 1, 'TP', NULL, 5.5, 1),
(7, 2, 'TC', NULL, 15, 3),
(8, 2, 'TP', NULL, 30, 4),
(9, 2, 'BN', NULL, 5, 1),
(10, 2, 'FR', NULL, 15, 1),
(11, 3, 'TC', NULL, 3.5, 1),
(12, 3, 'TP', NULL, 6, 3),
(13, 3, 'U', NULL, 4, 3),
(14, 3, 'AJ', NULL, 10, 4),
(15, 3, 'PT', NULL, 15, 4),
(16, 3, 'PA', NULL, 10.5, 2),
(17, 3, 'NO', NULL, 5, 2),
(18, 4, 'HB', NULL, 50, 5),
(19, 4, 'HC', NULL, 25.5, 1),
(20, 5, 'TC', NULL, 6, 1),
(21, 5, 'PER', NULL, 6, 1),
(22, 6, 'BG', NULL, 6, 3),
(23, 7, 'PA', NULL, 5, 2),
(24, 8, 'BN', NULL, 5.5, 1),
(25, 8, 'MV', NULL, 6, 1),
(26, 8, 'PA', NULL, 6, 2),
(27, 8, 'NC', NULL, 6.5, 2),
(28, 8, 'TC', NULL, 5, 1),
(29, 8, 'PV', NULL, 6.5, 3),
(30, 8, 'PAM', NULL, 10, 4),
(31, 8, 'PT', NULL, 9, 4),
(34, 10, 'PA', NULL, 6.5, 2),
(35, 10, 'U', NULL, 6, 1),
(36, 10, 'MEL', NULL, 4, 4),
(37, 10, 'SA', NULL, 3, 4),
(42, 11, 'PAM', NULL, 6, 4),
(43, 11, 'PR', NULL, 5, 3),
(44, 11, 'PV', NULL, 15, 1),
(45, 12, 'LI', NULL, 5, 1),
(47, 13, 'PU', NULL, 20, 4),
(49, 9, 'MR', NULL, 5, 3),
(50, 9, 'MEL', NULL, 10, 3),
(51, 14, 'ME', NULL, 5, 4),
(52, 14, 'FR', NULL, 5, 1),
(53, 15, 'CZ', NULL, 5, 1),
(54, 15, 'AJP', NULL, 5, 4),
(55, 15, 'AJV', NULL, 2, 2),
(56, 16, 'TP', NULL, 1, 1),
(57, 16, 'TR', NULL, 2.5, 1),
(58, 16, 'BN', NULL, 3, 1),
(59, 16, 'BG', NULL, 5.5, 3),
(60, 16, 'PA', NULL, 6, 2),
(61, 16, 'U', NULL, 5, 3),
(62, 16, 'AJP', NULL, 6, 4),
(63, 16, 'HC', NULL, 12, 5),
(64, 16, 'ZPC', NULL, 15, 3),
(65, 16, 'NC', NULL, 9, 2),
(66, 16, 'ARA', NULL, 5.5, 3),
(67, 16, 'MEL', NULL, 4, 4),
(68, 16, 'SA', NULL, 5, 4),
(69, 16, 'PH', NULL, 3, 4),
(70, 16, 'MV', NULL, 7.5, 3),
(71, 16, 'PER', NULL, 3, 1),
(72, 16, 'LC', NULL, 5, 1),
(73, 16, 'CE', NULL, 6, 3),
(74, 16, 'FR', NULL, 5, 1),
(75, 16, 'CH', NULL, 2, 3),
(76, 16, 'AC', NULL, 5, 1),
(77, 16, 'DU', NULL, 5, 3),
(78, 16, 'COR', NULL, 6, 1),
(79, 16, 'AJ', NULL, 10, 4),
(80, 17, 'LR', NULL, 56, 1),
(81, 18, 'PER', NULL, 5, 1),
(82, 19, 'LR', NULL, 5, 1),
(95, 20, 'PA', NULL, 5, 1),
(96, 20, 'FR', NULL, 5, 1),
(97, 20, 'ES', NULL, 15, 3),
(98, 20, 'BS', NULL, 10, 1),
(102, 22, 'TP', NULL, 12, 3),
(103, 22, 'TC', NULL, 12, 4),
(107, 23, 'BA', NULL, 25, 1),
(108, 23, 'BN', NULL, 15.5, 3),
(119, 25, 'HB', NULL, 12, 5),
(120, 25, 'HC', NULL, 12, 5),
(121, 25, 'BN', NULL, 15, 1),
(122, 25, 'BG', NULL, 15, 5),
(123, 25, 'BR', NULL, 15, 4),
(124, 25, 'PA', NULL, 15.5, 2),
(125, 24, 'PA', NULL, 15, 2),
(126, 24, 'AJP', NULL, 120, 4),
(127, 24, 'HC', NULL, 15, 5),
(128, 21, 'TP', NULL, 7.5, 3),
(129, 21, 'TR', NULL, 15, 4),
(130, 21, 'PA', NULL, 5.5, 1),
(131, 21, 'ES', NULL, 25, 4),
(132, 26, 'TC', NULL, 1, 1),
(133, 26, 'TP', NULL, 1, 1),
(134, 26, 'TR', NULL, 1, 1),
(135, 26, 'BN', NULL, 1, 1),
(136, 27, 'AP', NULL, 15, 4),
(137, 27, 'BA', NULL, 15, 5),
(138, 27, 'ES', NULL, 120, 4),
(139, 27, 'MR', NULL, 15, 1),
(140, 27, 'U', NULL, 1, 1),
(142, 28, 'CE', NULL, 1, 1),
(143, 28, 'JE', NULL, 1, 1),
(144, 29, 'PA', NULL, 15, 2),
(145, 29, 'BN', NULL, 10.5, 1),
(146, 29, 'ACH', NULL, 9.5, 4),
(147, 30, 'AC', NULL, 25, 4),
(148, 30, 'PR', NULL, 7, 2),
(149, 30, 'PA', NULL, 15, 2),
(150, 30, 'NC', NULL, 10.5, 2),
(151, 30, 'NT', NULL, 14, 2),
(152, 31, 'AC', NULL, 15, 4),
(153, 31, 'PAM', NULL, 10, 1),
(154, 31, 'PV', NULL, 20.5, 2),
(175, 33, 'PER', NULL, 5, 1),
(176, 33, 'NC', NULL, 6, 2),
(177, 33, 'LC', NULL, 3, 1),
(178, 33, 'U', NULL, 3, 1),
(179, 33, 'PA', NULL, 17, 2),
(180, 33, 'AJP', NULL, 5, 1),
(181, 33, 'MEL', NULL, 12, 1),
(188, 32, 'CH', NULL, 5, 2),
(189, 32, 'DU', NULL, 7.5, 1),
(190, 32, 'U', NULL, 6, 1),
(191, 32, 'CV', NULL, 25, 4),
(192, 32, 'AR', NULL, 6, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalle2`
--

CREATE TABLE IF NOT EXISTS `detalle2` (
  `id_detalle` int(11) NOT NULL AUTO_INCREMENT,
  `id_boleta` int(11) NOT NULL,
  `id_mercaderia` varchar(20) NOT NULL,
  `cantCajon` float NOT NULL,
  `cantBolsa` float NOT NULL,
  `cantKg` float NOT NULL,
  `cantUnidad` float NOT NULL,
  `cantBandeja` float NOT NULL,
  PRIMARY KEY (`id_detalle`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=191 ;

--
-- Volcado de datos para la tabla `detalle2`
--

INSERT INTO `detalle2` (`id_detalle`, `id_boleta`, `id_mercaderia`, `cantCajon`, `cantBolsa`, `cantKg`, `cantUnidad`, `cantBandeja`) VALUES
(1, 1, 'LR', 2.5, 0, 0, 0, 0),
(2, 1, 'PER', 3, 0, 0, 0, 0),
(3, 1, 'FR', 0, 0, 6, 0, 0),
(4, 1, 'HB', 0, 0, 0, 0, 6),
(5, 1, 'TC', 6, 0, 0, 0, 0),
(6, 1, 'TP', 5.5, 0, 0, 0, 0),
(7, 2, 'TC', 0, 0, 15, 0, 0),
(8, 2, 'TP', 0, 0, 0, 30, 0),
(9, 2, 'BN', 5, 0, 0, 0, 0),
(10, 2, 'FR', 15, 0, 0, 0, 0),
(11, 3, 'TC', 3.5, 0, 0, 0, 0),
(12, 3, 'TP', 0, 0, 6, 0, 0),
(13, 3, 'U', 0, 0, 4, 0, 0),
(14, 3, 'AJ', 0, 0, 0, 10, 0),
(15, 3, 'PT', 0, 0, 0, 15, 0),
(16, 3, 'PA', 0, 10.5, 0, 0, 0),
(17, 3, 'NO', 0, 5, 0, 0, 0),
(18, 4, 'HB', 0, 0, 0, 0, 50),
(19, 4, 'HC', 25.5, 0, 0, 0, 0),
(20, 5, 'TC', 6, 0, 0, 0, 0),
(21, 5, 'PER', 6, 0, 0, 0, 0),
(23, 7, 'PA', 0, 5, 0, 0, 0),
(24, 8, 'BN', 5.5, 0, 0, 0, 0),
(25, 8, 'MV', 6, 0, 0, 0, 0),
(26, 8, 'PA', 0, 6, 0, 0, 0),
(27, 8, 'NC', 0, 6.5, 0, 0, 0),
(28, 8, 'TC', 5, 0, 0, 0, 0),
(29, 8, 'PV', 0, 0, 6.5, 0, 0),
(30, 8, 'PAM', 0, 0, 0, 10, 0),
(31, 8, 'PT', 0, 0, 0, 9, 0),
(34, 10, 'PA', 0, 6.5, 0, 0, 0),
(35, 10, 'U', 6, 0, 0, 0, 0),
(36, 10, 'MEL', 0, 0, 0, 4, 0),
(37, 10, 'SA', 0, 0, 0, 3, 0),
(42, 11, 'PAM', 0, 0, 0, 6, 0),
(43, 11, 'PR', 0, 0, 5, 0, 0),
(44, 11, 'PV', 15, 0, 0, 0, 0),
(45, 12, 'LI', 5, 0, 0, 0, 0),
(47, 13, 'PU', 0, 0, 0, 20, 0),
(49, 9, 'MR', 0, 0, 5, 0, 0),
(50, 9, 'MEL', 0, 0, 10, 0, 0),
(51, 14, 'ME', 0, 0, 0, 5, 0),
(52, 14, 'FR', 5, 0, 0, 0, 0),
(53, 15, 'CZ', 5, 0, 0, 0, 0),
(54, 15, 'AJP', 0, 0, 0, 5, 0),
(55, 15, 'AJV', 0, 2, 0, 0, 0),
(56, 16, 'TR', 2.5, 0, 0, 0, 0),
(57, 16, 'BN', 3, 0, 0, 0, 0),
(58, 16, 'BG', 0, 0, 5.5, 0, 0),
(59, 16, 'PA', 0, 6, 0, 0, 0),
(60, 16, 'U', 0, 0, 5, 0, 0),
(61, 16, 'AJP', 0, 0, 0, 6, 0),
(62, 16, 'HC', 0, 0, 0, 0, 12),
(63, 16, 'ZPC', 0, 0, 15, 0, 0),
(64, 16, 'NC', 0, 9, 0, 0, 0),
(65, 16, 'ARA', 0, 0, 5.5, 0, 0),
(66, 16, 'MEL', 0, 0, 0, 4, 0),
(67, 16, 'SA', 0, 0, 0, 5, 0),
(68, 16, 'PH', 0, 0, 0, 3, 0),
(69, 16, 'MV', 0, 0, 7.5, 0, 0),
(70, 16, 'PER', 3, 0, 0, 0, 0),
(71, 16, 'LC', 5, 0, 0, 0, 0),
(72, 16, 'CE', 0, 0, 6, 0, 0),
(73, 16, 'FR', 5, 0, 0, 0, 0),
(74, 16, 'CH', 0, 0, 2, 0, 0),
(75, 16, 'AC', 5, 0, 0, 0, 0),
(76, 16, 'DU', 0, 0, 5, 0, 0),
(77, 16, 'COR', 6, 0, 0, 0, 0),
(78, 16, 'AJ', 0, 0, 0, 10, 0),
(80, 18, 'PER', 5, 0, 0, 0, 0),
(81, 19, 'LR', 5, 0, 0, 0, 0),
(94, 20, 'PA', 5, 0, 0, 0, 0),
(95, 20, 'FR', 5, 0, 0, 0, 0),
(96, 20, 'ES', 0, 0, 15, 0, 0),
(97, 20, 'BS', 10, 0, 0, 0, 0),
(101, 22, 'TP', 0, 0, 12, 0, 0),
(102, 22, 'TC', 0, 0, 0, 12, 0),
(106, 23, 'BA', 25, 0, 0, 0, 0),
(107, 23, 'BN', 0, 0, 15.5, 0, 0),
(118, 25, 'HB', 0, 0, 0, 0, 12),
(119, 25, 'HC', 0, 0, 0, 0, 12),
(120, 25, 'BN', 15, 0, 0, 0, 0),
(121, 25, 'BG', 0, 0, 0, 0, 15),
(122, 25, 'BR', 0, 0, 0, 15, 0),
(123, 25, 'PA', 0, 15.5, 0, 0, 0),
(124, 24, 'PA', 0, 15, 0, 0, 0),
(125, 24, 'AJP', 0, 0, 0, 120, 0),
(126, 24, 'HC', 0, 0, 0, 0, 15),
(127, 21, 'TP', 0, 0, 7.5, 0, 0),
(128, 21, 'TR', 0, 0, 0, 15, 0),
(129, 21, 'PA', 5.5, 0, 0, 0, 0),
(130, 21, 'ES', 0, 0, 0, 25, 0),
(131, 26, 'TC', 1, 0, 0, 0, 0),
(132, 26, 'TP', 1, 0, 0, 0, 0),
(133, 26, 'TR', 1, 0, 0, 0, 0),
(134, 26, 'BN', 1, 0, 0, 0, 0),
(135, 27, 'AP', 0, 0, 0, 15, 0),
(136, 27, 'BA', 0, 0, 0, 0, 15),
(137, 27, 'ES', 0, 0, 0, 120, 0),
(138, 27, 'MR', 15, 0, 0, 0, 0),
(139, 27, 'U', 1, 0, 0, 0, 0),
(141, 28, 'CE', 1, 0, 0, 0, 0),
(142, 28, 'JE', 1, 0, 0, 0, 0),
(143, 29, 'PA', 0, 15, 0, 0, 0),
(144, 29, 'BN', 10.5, 0, 0, 0, 0),
(145, 29, 'ACH', 0, 0, 0, 9.5, 0),
(146, 30, 'AC', 0, 0, 0, 25, 0),
(147, 30, 'PR', 0, 7, 0, 0, 0),
(148, 30, 'PA', 0, 15, 0, 0, 0),
(149, 30, 'NC', 0, 10.5, 0, 0, 0),
(150, 30, 'NT', 0, 14, 0, 0, 0),
(151, 31, 'AC', 0, 0, 0, 15, 0),
(152, 31, 'PAM', 10, 0, 0, 0, 0),
(153, 31, 'PV', 0, 20.5, 0, 0, 0),
(173, 33, 'PER', 5, 0, 0, 0, 0),
(174, 33, 'NC', 0, 6, 0, 0, 0),
(175, 33, 'LC', 3, 0, 0, 0, 0),
(176, 33, 'U', 3, 0, 0, 0, 0),
(177, 33, 'PA', 0, 17, 0, 0, 0),
(178, 33, 'AJP', 5, 0, 0, 0, 0),
(179, 33, 'MEL', 12, 0, 0, 0, 0),
(186, 32, 'CH', 0, 5, 0, 0, 0),
(187, 32, 'DU', 7.5, 0, 0, 0, 0),
(188, 32, 'U', 6, 0, 0, 0, 0),
(189, 32, 'CV', 0, 0, 0, 25, 0),
(190, 32, 'AR', 6, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detallepedido`
--

CREATE TABLE IF NOT EXISTS `detallepedido` (
  `id_pedido` int(11) NOT NULL,
  `id_mercaderia` varchar(20) NOT NULL,
  `totalCajon` int(11) NOT NULL,
  `totalBolsa` int(11) NOT NULL,
  `totalKg` int(11) NOT NULL,
  `totalUnidad` int(11) NOT NULL,
  `totalBandeja` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gestion_costo`
--

CREATE TABLE IF NOT EXISTS `gestion_costo` (
  `id_gestionCosto` int(11) NOT NULL AUTO_INCREMENT,
  `id_pedido` int(11) NOT NULL,
  `id_gastos` int(11) NOT NULL,
  `detalle` varchar(30) DEFAULT NULL,
  `cantidad` varchar(40) DEFAULT NULL,
  `costo_unitario` float DEFAULT NULL,
  `costo_total` float NOT NULL,
  PRIMARY KEY (`id_gestionCosto`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `gestion_costo2`
--

CREATE TABLE IF NOT EXISTS `gestion_costo2` (
  `id_gestionCosto` int(11) NOT NULL AUTO_INCREMENT,
  `id_pedido` int(11) NOT NULL,
  `id_gastos` int(11) NOT NULL,
  `detalle` varchar(50) NOT NULL,
  `cantidad` float DEFAULT NULL,
  `id_tipocant` int(11) DEFAULT NULL,
  `costo_unitario` float DEFAULT NULL,
  `costo_total` float NOT NULL,
  PRIMARY KEY (`id_gestionCosto`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=48 ;

--
-- Volcado de datos para la tabla `gestion_costo2`
--

INSERT INTO `gestion_costo2` (`id_gestionCosto`, `id_pedido`, `id_gastos`, `detalle`, `cantidad`, `id_tipocant`, `costo_unitario`, `costo_total`) VALUES
(1, 1, 1, 'BN', 10.5, 1, 100, 1050),
(2, 1, 1, 'TP', 5.5, 1, 100, 550),
(3, 1, 1, 'TC', 20.5, 1, 100, 2050),
(4, 1, 1, 'LR', 2.5, 1, 100, 250),
(5, 1, 1, 'MV', 6, 1, 100, 600),
(6, 1, 1, 'PER', 9, 1, 100, 900),
(7, 1, 1, 'PA', 28, 2, 100, 2800),
(8, 1, 1, 'U', 6, 1, 100, 600),
(9, 1, 1, 'FR', 20, 1, 100, 2000),
(10, 1, 1, 'NC', 6.5, 2, 100, 650),
(11, 1, 1, 'NO', 5, 2, 100, 500),
(12, 1, 1, 'LI', 5, 1, 100, 500),
(13, 1, 1, 'HC', 25.5, 1, 100, 2550),
(14, 1, 1, 'HB', 56, 5, 100, 5600),
(15, 1, 2, 'Entrada', 1, NULL, NULL, 100),
(16, 1, 4, 'Combustible', 1, NULL, NULL, 250.5),
(17, 1, 5, 'Varios', 1, NULL, NULL, 120.8),
(18, 3, 1, 'PA', 5.5, 1, 120, 660),
(19, 3, 1, 'PA', 15, 2, 55, 825),
(20, 3, 1, 'HC', 15, 5, 15, 225),
(21, 3, 1, 'PR', 25, 1, 180, 4500),
(22, 3, 2, 'Entrada', 1, NULL, NULL, 150),
(23, 3, 3, 'Changadas', 1, NULL, NULL, 100),
(24, 3, 4, 'Combustible', 1, NULL, NULL, 250),
(25, 3, 5, 'Varios', 1, NULL, NULL, 500),
(26, 6, 1, 'BN', 10.5, 1, 10, 105),
(27, 6, 1, 'NC', 10.5, 2, 120, 1260),
(28, 6, 1, 'PA', 30, 2, 95, 2850),
(29, 6, 1, 'NT', 14, 2, 85.5, 1197),
(30, 6, 1, 'PR', 7, 2, 63.5, 444.5),
(31, 6, 1, 'PV', 20.5, 2, 120, 2460),
(32, 6, 1, 'PAM', 10, 1, 175, 1750),
(33, 6, 1, 'AJP', 2, 1, 86, 172),
(34, 6, 2, 'Entrada', 1, NULL, NULL, 120),
(35, 6, 4, 'Combustible', 1, NULL, NULL, 500),
(36, 5, 1, 'BN', 1, 1, 10, 10),
(37, 5, 1, 'TR', 1, 1, 10, 10),
(38, 5, 1, 'MR', 15, 1, 10, 150),
(39, 5, 1, 'TP', 1, 1, 10, 10),
(40, 5, 1, 'TC', 1, 1, 10, 10),
(41, 5, 1, 'U', 1, 1, 10, 10),
(42, 5, 1, 'CE', 1, 1, 10, 10),
(43, 5, 1, 'JE', 1, 1, 10, 10),
(44, 5, 1, 'BA', 15, 5, 10, 150),
(45, 5, 1, 'AJP', 120, 2, 10, 1200),
(46, 5, 1, 'CE', 14, 2, 10, 140),
(47, 5, 5, 'Varios', 1, NULL, NULL, 450);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `localidad`
--

CREATE TABLE IF NOT EXISTS `localidad` (
  `id_localidad` int(5) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(50) NOT NULL,
  PRIMARY KEY (`id_localidad`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `localidad`
--

INSERT INTO `localidad` (`id_localidad`, `nombre`) VALUES
(1, 'San Miguel de Tucuman'),
(2, 'Yerba Buena'),
(3, 'Aguilares'),
(4, 'Concepcion'),
(5, 'Monteros'),
(6, 'Ledesma');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `mercaderia`
--

CREATE TABLE IF NOT EXISTS `mercaderia` (
  `id_mercaderia` varchar(10) NOT NULL,
  `nombre` varchar(30) NOT NULL,
  `id_tipo` int(3) NOT NULL,
  `precio` float DEFAULT NULL,
  `id_tipocant` int(11) NOT NULL,
  `orden` int(11) NOT NULL,
  `inactivo` int(11) NOT NULL,
  PRIMARY KEY (`id_mercaderia`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `mercaderia`
--

INSERT INTO `mercaderia` (`id_mercaderia`, `nombre`, `id_tipo`, `precio`, `id_tipocant`, `orden`, `inactivo`) VALUES
('AC', 'Acelga', 1, NULL, 1, 37, 0),
('ACH', 'Achicoria', 1, NULL, 1, 39, 0),
('AJ', 'Ajo', 1, NULL, 4, 25, 0),
('AJP', 'Aji Picante', 1, NULL, 1, 26, 0),
('AJV', 'Aji Vaina', 1, NULL, 1, 27, 0),
('AL', 'Albahaca', 1, NULL, 1, 49, 0),
('AN', 'Anana', 2, NULL, 1, 16, 0),
('AP', 'Apio', 1, NULL, 1, 33, 0),
('AR', 'Arvejas', 1, NULL, 1, 30, 0),
('ARA', 'Arandano', 2, NULL, 1, 19, 0),
('BA', 'Brote Alfalfa', 1, NULL, 1, 51, 0),
('BB', 'Batata Blanca', 1, NULL, 1, 22, 0),
('BC', 'Batata Colorada', 1, NULL, 1, 21, 0),
('BG', 'Brote Girasol', 1, NULL, 1, 52, 0),
('BJ', 'Berenjena', 1, NULL, 1, 53, 0),
('BN', 'Banana', 2, NULL, 1, 1, 0),
('BR', 'Brocoli', 1, NULL, 1, 29, 0),
('BS', 'Brote Soja', 1, NULL, 1, 50, 0),
('CB', 'Cebollon', 1, NULL, 1, 9, 0),
('CE', 'Cebolla', 1, NULL, 1, 8, 0),
('CH', 'Choclo', 1, NULL, 1, 24, 0),
('CHA', 'Chaucha', 1, NULL, 1, 31, 0),
('CI', 'Cebollin', 1, NULL, 1, 10, 0),
('CIL', 'Cilantro', 1, NULL, 1, 46, 0),
('CIR', 'Ciruela', 2, NULL, 1, 14, 0),
('COL', 'Coliflor', 1, NULL, 1, 28, 0),
('COR', 'Coreano', 1, NULL, 1, 20, 0),
('CV', 'Cebolla Verdeo', 1, NULL, 1, 35, 0),
('CY', 'Cayote', 1, NULL, 1, 43, 0),
('CZ', 'Cereza', 2, NULL, 1, 20, 0),
('DU', 'Durazno', 2, NULL, 1, 15, 0),
('ES', 'Espinaca', 1, NULL, 1, 36, 0),
('ESP', 'Esparrago', 1, NULL, 1, 48, 0),
('FR', 'Frutilla', 2, NULL, 1, 6, 0),
('HB', 'Huevo Blanco', 1, NULL, 1, 55, 0),
('HC', 'Huevo Color', 1, NULL, 1, 54, 0),
('JE', 'Jengibre', 1, NULL, 1, 47, 0),
('Ki', 'Kiwi', 2, NULL, 1, 10, 0),
('LC', 'Lechuga Crespa', 1, NULL, 1, 5, 0),
('LI', 'Limon', 2, NULL, 1, 13, 0),
('LM', 'Lechuga Mantecosa', 1, NULL, 1, 6, 0),
('LR', 'Lechuga Repollada', 1, NULL, 1, 4, 0),
('MA', 'Mandarina', 2, NULL, 1, 11, 0),
('ME', 'Menta', 1, NULL, 1, 45, 0),
('MEL', 'Melon', 2, NULL, 1, 18, 0),
('MR', 'Manzana Roja', 2, NULL, 1, 2, 0),
('MV', 'Manzana Verde', 2, NULL, 1, 3, 0),
('NC', 'Naranja Criolla', 2, NULL, 1, 7, 0),
('NO', 'Naranja Omblig', 2, NULL, 1, 9, 0),
('NT', 'Naranja Tanjarina', 2, NULL, 1, 8, 0),
('PA', 'Papa', 1, NULL, 1, 7, 0),
('PAM', 'Pimiento Amarillo', 1, NULL, 1, 13, 0),
('PE', 'Pepino', 1, NULL, 1, 23, 0),
('PER', 'Pera', 2, NULL, 1, 4, 0),
('PH', 'Palta Hass', 2, NULL, 1, 21, 0),
('PJ', 'Perejil', 1, NULL, 1, 38, 0),
('PM', 'Pomelo', 2, NULL, 1, 12, 0),
('PR', 'Pimiento Rojo', 1, NULL, 1, 11, 0),
('PT', 'Palta Torre', 2, NULL, 1, 22, 0),
('PU', 'Puerro', 1, NULL, 1, 34, 0),
('PV', 'Pimiento Verde', 1, NULL, 1, 12, 0),
('RA', 'Rabanito', 1, NULL, 1, 40, 0),
('RB', 'Repollo Blanco', 1, NULL, 1, 41, 0),
('RE', 'Remolacha', 1, NULL, 1, 32, 0),
('RM', 'Repollo Morado', 1, NULL, 1, 42, 0),
('RU', 'Rucula', 1, NULL, 1, 44, 0),
('SA', 'Sandia', 2, NULL, 1, 17, 0),
('TC', 'Tomate Cherry', 1, NULL, 1, 3, 0),
('TP', 'Tomate Perita', 1, NULL, 1, 2, 0),
('TR', 'Tomate Redondo', 1, NULL, 1, 1, 0),
('U', 'Uva', 2, NULL, 1, 5, 0),
('ZB', 'Zapallo Brasileño', 1, NULL, 1, 19, 0),
('ZC', 'Zanahoria Chica', 1, NULL, 1, 15, 0),
('ZG', 'Zanahoria Grande', 1, NULL, 1, 14, 0),
('ZPC', 'Zapallo Criollo', 1, NULL, 1, 18, 0),
('ZPZ', 'Zapallito Cuza', 1, NULL, 1, 17, 0),
('ZV', 'Zapallito Verde', 1, NULL, 1, 16, 0);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `orden`
--

CREATE TABLE IF NOT EXISTS `orden` (
  `id_orden` int(5) NOT NULL AUTO_INCREMENT,
  `id_mercaderia` varchar(10) NOT NULL,
  `antes` varchar(5) NOT NULL,
  `despues` varchar(5) NOT NULL,
  PRIMARY KEY (`id_orden`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

--
-- Volcado de datos para la tabla `orden`
--

INSERT INTO `orden` (`id_orden`, `id_mercaderia`, `antes`, `despues`) VALUES
(1, 'TR', '0', 'TP'),
(2, 'TP', 'TR', 'TC'),
(5, 'TC', 'TP', 'LR'),
(6, 'LR', 'TC', 'LC'),
(7, 'LC', 'LR', 'LM'),
(8, 'LM', 'LC', 'PA'),
(9, 'PA', 'LM', 'CE'),
(10, 'CE', 'PA', 'CB'),
(11, 'CB', 'CE', 'CI'),
(12, 'CI', 'CB', 'PR'),
(13, 'PR', 'CI', 'PV'),
(14, 'PV', 'PR', 'PAM'),
(15, 'PAM', 'PV', 'ZG'),
(16, 'ZG', 'PAM', 'ZC'),
(17, 'ZC', 'ZG', 'ZV'),
(18, 'ZV', 'ZC', 'ZPZ'),
(19, 'ZPZ', 'ZV', 'ZPC'),
(20, 'ZPC', 'ZPZ', 'ZB'),
(21, 'ZB', 'ZPC', 'COR'),
(22, 'COR', 'ZB', 'BC'),
(23, 'BC', 'COR', 'BB'),
(24, 'BB', 'BC', 'PE'),
(25, 'PE', 'BB', 'CH'),
(26, 'CH', 'PE', 'AJ'),
(27, 'AJ', 'CH', 'COL'),
(28, 'COL', 'AJ', 'BR'),
(29, 'BR', 'COL', 'AR'),
(30, 'AR', 'BR', 'CHA'),
(31, 'CHA', 'AR', 'RE'),
(32, 'RE', 'CHA', 'AP'),
(33, 'AP', 'RE', 'PU'),
(34, 'PU', 'AP', 'CV'),
(35, 'CV', 'PU', 'ES'),
(36, 'ES', 'CV', 'AC'),
(37, 'AC', 'ES', 'PJ'),
(38, 'PJ', 'AC', 'ACH'),
(39, 'ACH', 'PJ', 'RA'),
(40, 'RA', 'ACH', 'RB'),
(41, 'RB', 'RA', 'RM'),
(42, 'RM', 'RB', 'CY'),
(43, 'CY', 'RM', 'RU'),
(44, 'RU', 'CY', 'ME'),
(45, 'ME', 'RU', 'AL'),
(46, 'AL', 'ME', 'BS'),
(47, 'BS', 'AL', 'BA'),
(48, 'BA', 'BS', 'BG'),
(49, 'BG', 'BA', 'BJ'),
(50, 'BJ', 'BG', 'HC'),
(51, 'HC', 'BJ', 'HB'),
(52, 'HB', 'HC', '1'),
(53, 'BN', '0', 'MR'),
(54, 'MR', 'BN', 'MV'),
(55, 'MV', 'MR', 'PER'),
(56, 'PER', 'MV', 'U'),
(57, 'U', 'PER', 'FR'),
(58, 'FR', 'U', 'NC'),
(59, 'NC', 'FR', 'NT'),
(60, 'NT', 'NC', 'NO'),
(61, 'NO', 'NT', 'KI'),
(62, 'KI', 'NO', 'MA'),
(63, 'MA', 'KI', 'PM'),
(64, 'PM', 'MA', 'LI'),
(65, 'LI', 'PM', 'CIR'),
(66, 'CIR', 'LI', 'DU'),
(67, 'DU', 'CIR', 'AN'),
(68, 'AN', 'DU', 'SA'),
(69, 'SA', 'AN', 'MEL'),
(70, 'MEL', 'SA', 'PH'),
(71, 'PH', 'MEL', 'PT'),
(72, 'PT', 'PH', '1');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pedido`
--

CREATE TABLE IF NOT EXISTS `pedido` (
  `id_pedido` int(5) NOT NULL AUTO_INCREMENT,
  `fecha_pedido` date NOT NULL,
  `cant_boletas` int(5) NOT NULL,
  `gestion_costo` int(11) NOT NULL,
  PRIMARY KEY (`id_pedido`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Volcado de datos para la tabla `pedido`
--

INSERT INTO `pedido` (`id_pedido`, `fecha_pedido`, `cant_boletas`, `gestion_costo`) VALUES
(1, '2017-01-18', 11, 1),
(2, '2017-01-25', 7, 0),
(3, '2017-02-09', 3, 1),
(4, '2017-03-06', 2, 0),
(5, '2017-03-06', 3, 1),
(6, '2017-03-09', 3, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipocantidad`
--

CREATE TABLE IF NOT EXISTS `tipocantidad` (
  `id_tipocant` int(5) NOT NULL AUTO_INCREMENT,
  `tipo_cantidad` varchar(20) NOT NULL,
  PRIMARY KEY (`id_tipocant`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Volcado de datos para la tabla `tipocantidad`
--

INSERT INTO `tipocantidad` (`id_tipocant`, `tipo_cantidad`) VALUES
(1, 'Cajon'),
(2, 'Bolsa'),
(3, 'Kg'),
(4, 'Unidad'),
(5, 'Bandeja');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipomercaderia`
--

CREATE TABLE IF NOT EXISTS `tipomercaderia` (
  `id_tipo` int(5) NOT NULL AUTO_INCREMENT,
  `tipo` varchar(20) NOT NULL,
  PRIMARY KEY (`id_tipo`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `tipomercaderia`
--

INSERT INTO `tipomercaderia` (`id_tipo`, `tipo`) VALUES
(1, 'VERDURA'),
(2, 'FRUTA');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
